<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config= array(
    'default' => array(
        'host' => '127.0.0.1',
        'port'        => 11211,
        'weight'    => 100
    ),
    'server1' => array(
        'host' => '127.0.0.1',
        'port'        => 11211,
        'weight'    => 100
    )
);

/* End of file memcached.php */
/* Location: ./application/config/memcached.php */