<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
//saikat vai routes tart

$route['jobs'] = "jobs/webjobsview/index";
$route['jobs/details/(:num)'] = "jobs/webjobsview/details/$1";
$route['jobs/(:num)'] = "jobs/webjobsview/index/$1";
$route['job/category/([a-zA-Z]+)']="jobs/webjobsview/categorywise/$1";
$route['job/category/([a-zA-Z]+)/(:num)'] = "jobs/webjobsview/categorywise/$1/$2";

//saikat vai routes end
$route['nasifmama/(:any)']="nasifmama/index/$1";
$route['/'] = "pages/home/index";
$route['login'] = "pages/login";
$route['login/(.*)'] = "pages/login/index/$1";
$route['AddPost'] = "admin/addProduct/index";
$route['AddPost/(:num)'] = "admin/addProduct/edit/$1";
$route['AddPost/Step2'] = "admin/addProduct/insertlocation";
$route['AddPost/Step3'] = "admin/addProduct/insertimages";
$route['register'] = "admin/adduser/index";
$route['postrequirement'] = "pages/postreq/index";
$route['details/(:num)/(.*)'] = "pages/details/index/$1/$2";
$route['details/getmorefrom/(:num)'] = "pages/details/getmorefrom/$1";
$route['details/getsimilar/(:num)'] = "pages/details/getsimilar/$1";

$route['category/([a-zA-Z]+)'] = "pages/category/cat/$1";
$route['category/([a-zA-Z]+)/subcategory/([a-zA-Z+-]+)'] = "pages/category/scat/$1/$2";
$route['category/([a-zA-Z]+)/size/([a-zA-Z0-9+-]+)'] = "pages/category/size/$1/$2";
$route['category/([a-zA-Z]+)/(:num)'] = "pages/category/cat/$1/$2";
$route['category/([a-zA-Z]+)/subcategory/([%0-9a-zA-Z-+]+)/(:num)'] = "pages/category/scat/$1/$2/$3";
$route['admin/addcategory'] = "Admin/addCategory/index";
$route['admin/addCategory/create'] = "admin/addCategory/create";
$route['admin/getFields/(.*)'] = "Admin/addField/getFields/$1";
$route['admin/productList/(.*)'] = "Admin/productList/index/$1";
$route['admin/userList/(:num)'] = "Admin/userList/index/$1";
//$route['admin/addProduct/(:num)'] = "Admin/addProduct/edit/$1";
$route['dbview'] = "test/dbview";
$route['upload/deleteImage/(.*)'] = "Admin/upload/deleteImage/$1";
//$route['schemacreate'] = "doctrine_tools/create_tables";
$route['dbview/(.*)'] = "test/dbview/$1";
$route['test/(.*)']= "test/view/$1";
$route['default_controller'] = 'pages/home/index';
//$route['(:any)'] = 'test/view/$1';
$route['404_override'] = '';

/* End of file routes.php */
/* Location: ./application/config/routes.php */