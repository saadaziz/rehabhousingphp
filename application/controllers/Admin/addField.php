<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class addField extends  CI_Controller {

    public $entityName='Entities\ProductField';
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        //$entityName='Entities\ProductField';
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
	public function index()
	{		
        $page = 'admin/addField';
		$data['title']='Add Fields to Category :: Admin Panel';
		$data['nameid']='parent';
		$data['tableid']='tblCategories';
		
        $data['cats']=$this->doctrine->FindByQuery('select p from Entities\ProductTypes p where p.parent is not null');
		$this->load->view('admin/topheader', $data);
        $this->load->view($page, $data);
		$this->load->view('admin/footer', $data);
		
	}
	public function createField()
	{
		try{
			$lst=array();
			$slugtitle = $this->input->post('title');
			$slugui = url_title($this->input->post('ui'),' ',FALSE);
			$slugsku = url_title($this->input->post('sku'),' ',FALSE);
			$slugreq = url_title($this->input->post('required'),' ',FALSE);
			$typeid = url_title($this->input->post('typeid'),' ',FALSE);
			$lstcount = url_title($this->input->post('lstcount'),' ',FALSE);
			//print $lstcount;
			if($lstcount!=1)
			{
				for($i=1;$i<=$lstcount;$i++)
					$lst[]=url_title($this->input->post('lsttxt'.$i),' ',FALSE);
			}
			//print $lst;
			$type = new Entities\ProductTypes;
			//print $typeid;
			$type=$this->doctrine->findByID('Entities\ProductTypes',$typeid);
			$pfield = new Entities\ProductField;
			$pfield->setFieldName($slugtitle);
			$pfield->setUitype($slugui);
			$pfield->setFieldSku($slugsku);
			$pfield->setRequired($slugreq);
			$pfield->setType($type);
			$pfield=$this->doctrine->save($pfield);
			foreach ($lst as $key) {
				$pfieldlst =new Entities\ProductFieldList;
				$pfieldlst->setValue($key);
				$pfieldlst->setPField($pfield);
				$this->doctrine->save($pfieldlst);
			}
			$text='';
			$text.='<tr><td class="span2">'.$pfield->getPFieldId().' </td><td class="span3">'. $pfield ->getFieldName().'</td>';
			$text.='<td class="span3">'. $type->getTypeName() .'</td><td class="span2">'. $pfield ->getFieldSku().'</td>';
			$text.='<td class="span2">'.$pfield ->getUitype().'-'.$lstcount.'</td><td class="span1">'.$pfield ->getRequired() .'</td></tr>';
			print $text;
		}
		catch(exception $ex)
		{
			print $ex->getMessage(); 
		}
	}
	public function getFields($typeid =FALSE)
	{
		if($typeid!=FALSE)
		{
			$text="<table id=\"tblfields\">
				<tr><td class=\"span2\">FieldID</td>
					<td class=\"span3\">Field Name</td>
					<td class=\"span3\">Type</td>
					<td class=\"span2\">SKU</td>
					<td class=\"span2\">UIType</td>
					<td class=\"span2\">Required</td></tr>";
			
			$arr=$this->doctrine->findCustom($this->entityName,array("type"=>$typeid));
			foreach ($arr as $field){
				$text.='<tr><td class="span2">'.$field->getPFieldId().' </td><td class="span3">'. $field ->getFieldName().'</td>';
				$text.='<td class="span3">'. $field ->getType()->getTypeName() .'</td><td class="span2">'. $field ->getFieldSku().'</td>';
				$text.='<td class="span2">'.$field ->getUitype().'</td><td class="span1">'.$field ->getRequired() .'</td></tr>';
			
			}
			$text.='</table>';
			print $text;
			
		}
		else {
			print 'false';
		}
	}
}