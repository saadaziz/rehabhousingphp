<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addJobs extends  CI_Controller {

    public $entityName='Entities\WebJobs';
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\WebJobs';
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }
    public function index()
    {
        $page = 'admin/addJobs';
		$data['title']='Add Jobs :: Admin Panel';
        $data['jobs']=$this->doctrine->findAll($this->entityName);
		$this->load->view('admin/topheader', $data);
		$data['tableid']='tblCategories';
		$data['error']='';
        $this->load->view($page, $data);
		$this->load->view('admin/footer', $data);
    }
    public function create()
    {
        	
        $this->form_validation->set_rules('job_catagory', 'job_catagory', 'required');
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required|max_length[100]');		
        $this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[100]');		
        $this->form_validation->set_rules('designation', 'Designation', 'trim|required|max_length[100]');		
		$this->form_validation->set_rules('last_date_of_submission', 'last_date_of_submission', 'required');
		$this->form_validation->set_rules('Education_requirements1', 'Education Requirements', 'required');	
			
		$this->form_validation->set_rules('no_of_vacancies', 'no_of_vacancies', 'required');
		$this->form_validation->set_rules('additional_requirement1', 'additional_requirement', 'required');		
		$this->form_validation->set_rules('apply_instruction', 'apply_instruction', 'required');	
		
		$this->form_validation->set_rules('Experience1', 'Experience', 'required');
		$this->form_validation->set_rules('Salary_range', 'Salary_range', 'required');
		$this->form_validation->set_rules('job_location', 'job_location', 'required');
		$this->form_validation->set_rules('job_source', 'job_source', 'required');
		$this->form_validation->set_rules('contact_info', 'Cell No', 'trim|required|max_length[30]');
		$this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');
        /*
        if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{
			print json_encode(array('error'=>'Please insert the required fields'.form_error('title').'-'.form_error('company_name').'-'
			.form_error('description').'-'.form_error('designation').'-'
			.form_error('last_date_of_submission').'-'.form_error('Education_requirements').'-'.form_error('Experience').'-'
			.form_error('Salary_range').'-'.form_error('job_location').'-'.form_error('job_source').'-'
			.form_error('contact_info'),'data'=>'no data',));
			//$this->load->view('admin/addUser',$data,TRUE);
			
		}	
       else
        {*/
        
        
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1000000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';

		$this->load->library('upload', $config);
		$newimage=new Entities\ImageLink;
		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('admin/addJobs', $error);
		}
		else
		{
			//$data = array('upload_data' => $this->upload->data());

			$filename='companylogo-'.date_format(new DateTime(), 'Y-m-d_H-i-s').'_user-companylogo_';
			
			$upload_data = $this->upload->data();
			$newimage->setImageName($filename.'_'.$upload_data['orig_name']);
			//echo $upload_data['orig_name'].'<br>';
			rename( $upload_data['full_path'],  $upload_data['file_path'].$filename.'_'.$upload_data['orig_name']);
						
			$newimage->setImageWatermark(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
			$newimage->setImageLink(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
			$newimage->setImageHeight($upload_data['image_height']);
			$newimage->setImageWidth($upload_data['image_width']);
			$newimage->setImageType('companylogoforjobs');
			$newpath= $upload_data['file_path'].$filename.'_'.$upload_data['orig_name'];
			$newimage=$this->doctrine->save($newimage);
			//$this->load->view('upload_success', $data);
		}
        
        
        
        
        	$newjobs = new Entities\WebJobs;
            $newjobs->setJobCatagory(url_title($this->input->post('job_catagory'),' ',FALSE));
            $newjobs->setCompanyName(url_title($this->input->post('company_name'),' ',FALSE));
			$newjobs->setTitle(url_title($this->input->post('title'),' ',FALSE));
			$newjobs->setCompanyLogo($newimage);
        	$newjobs->setNoOfVacancies(url_title($this->input->post('no_of_vacancies'),' ',FALSE));
			//concat additional requirement
			$addreq='';
			for($i=1;$i<=$this->input->post('additional_requirement_hn');$i++)
				$addreq.=$this->input->post('additional_requirement'.$i).'/end/';
			
            $newjobs->setAdditionalRequirement($addreq);
			//concat apply instruction
			//$addins='';
			//for($i=1;$i<=$this->input->post('apply_instruction_hn');$i++)
				//$addins.=$this->input->post('apply_instruction'.$i).'/end/';
			
            //$newjobs->setApplyInstruction($addins);
			$newjobs->setApplyInstruction($this->input->post('apply_instruction'));	
			
			
			
			$newjobs->setDescription($this->input->post('description'));
			$newjobs->setDesignation(url_title($this->input->post('designation'),' ',FALSE));
			$newjobs->setLastDateOfSubmission(new DateTime);
			//concat Education requirement
			$addedureq='';
			for($i=1;$i<=$this->input->post('Education_requirements_hn');$i++)
				$addedureq.=$this->input->post('Education_requirements'.$i).'/end/';
			
			
            $newjobs->setEducationRequirements($addedureq);
			
			//$newjobs->setEducationRequirements($this->input->post('Education_requirements'));
			//concat Experience
			$addExp='';
			for($i=1;$i<=$this->input->post('Experience_hn');$i++)
				$addExp.=$this->input->post('Experience'.$i).'/end/';
			
            $newjobs->setExperience($addExp);
			
			//$newjobs->setExperience($this->input->post('Experience'));
			
			$newjobs->setSalaryRange(url_title($this->input->post('Salary_range'),' ',FALSE));			
			$newjobs->setJobLocation(url_title($this->input->post('job_location'),' ',FALSE));
			$newjobs->setJobSource(url_title($this->input->post('job_source'),' ',FALSE));
			$newjobs->setContactInfo(url_title($this->input->post('contact_info'),' ',FALSE));
			$time = ($this->input->post('last_date_of_submission'));
			
			//echo $date = new DateTime('2000-01-01');
            $newjobs->setLastDateOfSubmission(new DateTime($time));
			$newjobs->setInsertTime(new DateTime());
			
			//echo $this->input->post('last_date_of_submission');
			
			$newjobs=$this->doctrine->save($newjobs);
				
			$tabledata="<tr><td>".$newjobs->getCompanyName()."</td><td>".$newjobs->getTitle()."</td>
			<td>".$newjobs->getDescription()."</td><td>".$newjobs->getDesignation()."</td>
			<td>".$newjobs->getLastDateOfSubmission()->format('Y-m-d')."</td><td>".$newjobs->getEducationRequirements()."</td>
			<td>".$newjobs->getExperience()."</td><td>".$newjobs->getSalaryRange()."</td>
			<td>".$newjobs->getJobLocation()."</td><td>".$newjobs->getJobSource()."</td>
			<td>".$newjobs->getContactInfo()."</td></tr>"; 
			
			//	print $tabledata;
		
	//	}
          

    }
}