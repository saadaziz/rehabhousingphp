<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Nazrul
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class addNews extends  CI_Controller {

    public $entityName='Entities\WebNews';
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\WebNews';
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index()
    {
        $page = 'admin/addnews';
		$data['title']='Add News :: Admin Panel';
		$data['mgs']='';
        $data['news']=$this->doctrine->findAll($this->entityName);
		$this->load->view('admin/topheader', $data);
        $this->load->view($page, $data);
		$this->load->view('admin/footer', $data);
    }
	
	public function edit($id){
		$page = 'admin/editnews';
		$data['title']='Edit News :: Admin Panel';
        $data['news']=$this->doctrine->findByID($this->entityName,$id);
		
		$this->load->view('admin/topheader', $data);
        $this->load->view($page, $data);
		$this->load->view('admin/footer', $data);
		
		//$theme = new Entities\WebThemes;
			//$theme=$this->doctrine->findByID($this->entityName,'1');
		//echo $id;
		
		
	}
	 public function create()
    {
        
        $this->form_validation->set_rules('news_title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');  $slugtitle = url_title($this->input->post('news_title'),' ',FALSE);
            $slugpar = $this->input->post('description');
			$ShortTextDetails = strip_tags($slugpar);
			$ShortText = str_split($ShortTextDetails, 150);
			//$ShortText[0];
            $new= new Entities\WebNews;
            //$parid= new Entities\WebNews;
            $new->setNewsTitle($slugtitle);
            $new->setDescription($slugpar);	
            $new->setShortText($ShortText[0]);			
            $new->setInsertTime(new DateTime);
			
			
			
			$this->doctrine->save($new);
			$tabledata="<tr><td>". $new->getNewsId() ."</td><td>".$new->getNewsTitle()."</td><td>".$new->getDescription()."</td><td>".$new->getInsertTime()->format('Y-m-d H:i:s')."</td></tr>";
			print json_encode(array('saad'=>'saikat',
			'table'=>$tabledata));
			//header(Location :)

	}
		 public function update()
    {
        
        $this->form_validation->set_rules('news_title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if ($this->form_validation->run() === FALSE)
        {
            print 'Fields not valid';
        }
        else
        {
            $slugtitle = url_title($this->input->post('news_title'),' ',FALSE);
            $slugpar = $this->input->post('description');
			$ShortTextDetails = strip_tags($slugpar);
			$ShortText = str_split($ShortTextDetails, 150);
            $new= new Entities\WebNews;
            //$parid= new Entities\WebNews;
            $id=$this->input->post('id');
            $new=$this->doctrine->findByID($this->entityName,$id);
            $new->setNewsTitle($slugtitle);
            $new->setShortText($ShortText[0]);	
            $new->setDescription($slugpar);			
            $new->setInsertTime(new DateTime);
			
			
			
			$this->doctrine->save($new);
			//$tabledata="<tr><td>". $new->getNewsId() ."</td><td>".$new->getNewsTitle()."</td><td>".$new->getDescription()."</td><td>".$new->getInsertTime()->format('Y-m-d H:i:s')."</td></tr>";
			//print $tabledata;
			//header(Location :)
			
			//send data to the view site
	        $page = 'admin/addnews';
			$data['title']='Add News :: Admin Panel';
	        $data['mgs']="Your data Update Successfully!";
			$this->load->view('admin/topheader', $data);
       		$data['news']=$this->doctrine->findAll($this->entityName);
	        $this->load->view($page, $data);
			$this->load->view('admin/footer', $data);

    } 
		

    }
	
}