<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class addProduct extends  CI_Controller {
	
	public $entityName='Entities\ProductDetails';
	public static $fields;
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\ProductDetails';
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
	public function edit($productid=FALSE)
	{
		if($this->session->userdata('user'))
		{
			if($productid!=FALSE)
			{
				$product= $this->doctrine->findByID('Entities\ProductDetails',$productid);
				$user=($this->doctrine->findByID('Entities\MemberDetails',$this->session->userdata['user']['userid']));
				if($product->getUser()->getUserId()==$this->session->userdata['user']['userid'] || $user->getPriviledgeLevel()=='Super Admin')
				{
					$page = 'admin/addProduct';
					$data['title']='Edit Product :: Admin Panel';
					
					$data['editmode']='1';
					$data['pid']=$productid;
					
					$data['pname']=$product->getTitle();
					
					$data['unitdetails']=$this->doctrine->findCustom('Entities\ProductUnitDetails',array('product'=>$product));
					$unitcount=count($data['unitdetails']);
					$data['fields']=$unitcount+1;
					$data['unitcount']=$unitcount;
					$tp=$product->getType();
					$data['cats']=$this->doctrine->findCustom('Entities\ProductTypes',array('parent'=>$tp->getParent()->getProductTypesId()));
					$data['catid']=$product->getType()->getProductTypesId();
					
					$data['headid']=$tp->getParent()->getProductTypesId();
					$data['heads']=$this->doctrine->FindByQuery('select p from Entities\ProductTypes p where p.parent is null');
					
					$data['pdesc']=$product->getDescription();
					$data['nego']=$product->getNegotiable();
					$data['extrafields']=$this->getFields($tp,$product);
					$this->load->view('pages/header', $data);
					$this->load->view('pages/top_fixed_bar', $data);
					$this->load->view('pages/top_menu', $data);
					$this->load->view('pages/top_search', $data);
					$this->load->view($page, $data);
					$this->load->view('pages/footer', $data);
				}
				else 
					redirect('/login/editpost'.$this->session->userdata['user']['userid'].$product->getUser()->getUserId(), 'refresh');
			}
		}
		else 
			redirect('/login/addpost', 'refresh');
		
	}
	public function index()
	{
		if(($this->session->userdata('user')))
		{
			$page = 'admin/addProduct';
			$data['title']='Add Products :: Rehabhousing';
			$data['catid']='';
			$data['editmode']='0';
			$data['heads']=$this->doctrine->FindByQuery('select p from Entities\ProductTypes p where p.parent is null');
			$this->load->view('pages/header', $data);
			$this->load->view('pages/top_fixed_bar', $data);
			$this->load->view('pages/top_menu', $data);
			$this->load->view('pages/top_search', $data);
			$this->load->view($page, $data);
			$this->load->view('pages/footer', $data);
		}
		else {
			redirect('/login/addpost', 'refresh');
		}
		
	}
	public function getCats($headid=FALSE)
	{
		$text=' <option value="0" selected="selected">Select One</option>';
		if($headid!=FALSE)
		{
			$cats = $this->doctrine->findCustomWithOrder('Entities\ProductTypes',array('parent'=>$headid),array('viewOrder'=>'asc'));
			foreach ($cats as $key) {
				$text.='<option value="'.$key->getProductTypesId().'" >'.$key->getTypeName().'</option>';
			}
		}
		print $text;		
	}

	public function insertlocation()
	{
		$editing = ($this->input->post('editmode')=="1")?1:0;
		$product =($this->input->post('pid')==0) ?new Entities\ProductDetails:$this->doctrine->findByID($this->entityName,$this->input->post('pid'));
		try{
			$typeid=$this->input->post('catid');
			$user =is_null($product->getUser())? $this->doctrine->findByID('Entities\MemberDetails',$this->session->userdata['user']['userid']):$product->getuser();
			$product->setTitle(url_title($this->input->post('pname'),' ',FALSE));
			$product->setDescription($this->input->post('pdesc'));
			
			$product->setApproval('pending');
			$product->setNegotiable($this->input->post('pnegotiable'));
			$product->setInsertTime(new DateTime());
			$product->setActiveFrom(new DateTime());
			$product->setActiveTo(date_modify(new DateTime(),'+60 day'));
			$product->setApplyForTopProject('no');
			$product->setTopProject('no');
			$product->setStatus('Ongoing');
			$product->setType($this->doctrine->findByID('Entities\ProductTypes',$typeid));
			$product->setUser($user);
			$product=$this->doctrine->save($product);
			$arr=array();
			$this->fields = $this->doctrine->findCustom('Entities\ProductField',array('type'=>$typeid));
			foreach ($this->fields as $key ) {
				$newfname=str_replace(array(' ','(',')'),'_',$key->getFieldName());
				array_push($arr,array($key->getPFieldId()=>$this->input->post('p'.$newfname)));
				//echo $key->getPFieldId();
			}
			$flag=0;
			$bedroom='';
			$size='';
			$price='';
			$condition='Ready';
			foreach ($arr as $key) {
				foreach ($key as $key2 =>$value) {
					$pdetails = $this->doctrine->findByID('Entities\ProductField',$key2);
					$pdetailsmap =($editing!=1)? new Entities\ProductFieldDetailsMap:$this->doctrine->findOneCustom('Entities\ProductFieldDetailsMap',array('product'=>$product,'pField'=>$key2));
					//echo $pdetails->getFieldName().' '.$value.'<br>';
					$bedroom=($pdetails->getFieldName()=="Bed Room")?$value:$bedroom;
					$price=(strpos($pdetails->getFieldName(),"Price")!==FALSE)?$value:$price;
					$size=(strpos($pdetails->getFieldName(),"Size")!==FALSE)?$value:$size;
					$condition=(strpos($pdetails->getFieldName(),"Status")!==FALSE)?$value:$condition;
					if($value!='Not Selected')
					{
						$pdetailsmap->setProduct($product);
						$pdetailsmap->setValue($value);
						$pdetailsmap->setPField($pdetails);
						$this->doctrine->save($pdetailsmap);
					}
				}
			}
			$units=array();
			
			$lstcount = ($this->input->post("lstcount")!='')?$this->input->post("lstcount"):1;
			//if($editing==1)$lstcount++;
			$data['saad']=$lstcount;
			if($typeid==6)
			{
				$chr='A';
				for($i=1;$i<=$lstcount;$i++)
				{
					$flag=1;
					$unitdets =($editing!=1)? new Entities\ProductUnitDetails:$this->doctrine->findOneCustom('Entities\ProductUnitDetails',array('product'=>$product,'typeName'=>'Type '.$chr));
					if(is_null($unitdets)) $unitdets=new Entities\ProductUnitDetails;
					$unitdets->setProduct($product);
					$bedroom.=$this->input->post("punitbed".$i).'/';
					$size.=$this->input->post("punitSize".$i).'/';
					$price.=$this->input->post("punitPrice".$i).'/';
					
					$unitdets->setBedroom($this->input->post("punitbed".$i));
					$unitdets->setBathroom($this->input->post("punitbath".$i));
					$unitdets->setSize($this->input->post("punitSize".$i));
					$unitdets->setPrice($this->input->post("punitPrice".$i));
					$unitdets->setTypeName('Type '.$chr);
					$chr++;
					//echo $this->input->post("punitbed".$i);
					////array_push($units,$unitdets);
					
					if (!empty ($_FILES['pUnitimg'.$i]['name'])) {
						//$data['saad']='got in';
						$config['upload_path'] = './uploads/';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$config['max_size']	= '2048';
						$config['max_width']  = '1048';
						$config['max_height']  = '1048';
						$config['overwrite'] = TRUE;
						$config['remove_spaces'] = TRUE;
						$this->load->library('upload', $config);
						if ( ! $this->upload->do_upload('pUnitimg'.$i))
						{
							$data['saad']= var_dump($this->upload->display_errors()).'for: '.$_FILES["pUnitimg".$i];
						}
						else
						{
							$data['saad']='';
							$newuser = $product->getUser();
							$filename='floorplan-'.date_format(new DateTime(), 'Y-m-d_H-i-s').'_user-'.$newuser->getUserId().'_';
							$newimage=new Entities\ImageLink;
							$upload_data = $this->upload->data();
							$newimage->setImageName($filename.'_'.$upload_data['orig_name']);
							//echo $upload_data['orig_name'].'<br>';
							rename( $upload_data['full_path'],  $upload_data['file_path'].$filename.'_'.$upload_data['orig_name']);
							$newimage->setProduct($product);
							$newimage->setImageWatermark(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
							$newimage->setImageLink(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
							$newimage->setImageHeight($upload_data['image_height']);
							$newimage->setImageWidth($upload_data['image_width']);
							$newimage->setImageType('floorplan');
							$newimage=$this->doctrine->save($newimage);
							$unitdets->setImage($newimage);
						}
					}
					else{
						$data['saad']=$_FILES['pUnitimg1']["name"];
					}
					$unitdets= $this->doctrine->save($unitdets);
				}
			}
			$product->setBedroom(substr($bedroom,0,strlen($bedroom)-1));
			$product->setPrice(($flag==1)?substr($price,0,strlen($price)-1):substr($price,0,strlen($price)-1));
			$product->setSize(substr($size,0,strlen($size)-1));
			$product->setStatus($condition);
			$product= $this->doctrine->save($product);
			//$data['title']="Insert images for ".$product->getProductId()."::Admin Panel";
			$data['pid']=$product->getProductId();
			$data['pname']=$product->getTitle();
			$data['error']='';

			$div=$this->doctrine->findAll('Entities\LocationDivision');
			$arr = array("0"=>"Select a Division");
			foreach ($div as $key ) {
				array_push($arr,$key->getDivisionEn());
			}
			$data['division']=$arr;
			$data['editmode']=$this->input->post('editmode');
			$data['title']="Insert Location:: Add Post";
			if($editing==1)
			{
				
				$data['selectedDiv']=$product->getDivision()->getDivisionId();
				$data['lat']=$product->getLatitude();
				$data['long']=$product->getLongitude();
				$data['zoneid']=$product->getZone()->getLocId();
				$data['zones']=$this->doctrine->findCustom('Entities\LocationZones',array('division'=>$product->getDivision()));
				$data['js']="<script type='text/javascript'>initialize(new google.maps.LatLng(".$data['lat'].", ".$data['long']."));</script>";
			}
			else {
				$data['lat']='';
				$data['long']='';
				$data['zoneid']='';
				$data['zones']='';
				$data['js']="";
			}
			
			$data['address']=$product->getAddress();
            $this->load->view('pages/header', $data);
            $this->load->view('pages/top_fixed_bar', $data);
            $this->load->view('pages/top_menu', $data);
            $this->load->view('pages/top_search', $data);
            $this->load->view('admin/insertLocation', $data);
            $this->load->view('pages/footer', $data);
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
			echo '<pre>'.$ex->getTraceAsString().'</pre>';
			
			echo $this->input->post('pzone');
			echo $product->getZone()->getNameEn();
		}
		//echo $bedroom.' '.$condition;
	}
	public function setPriceinMAP($plmap,$prc,$product)
{
	if(is_null( $plmap))
	{
		$plmap=new Entities\PriceLocationMap;
		$plmap->setLowPrice($prc);
		$plmap->setHighPrice($prc);
		$plmap->setType($product->getType());
		$plmap->setLocation($product->getZone());
	}
	else{
		$plmap->setLowPrice(($prc<$plmap->getLowPrice())?$prc:$plmap->getLowPrice());
		$plmap->setHighPrice(($prc>$plmap->getHighPrice())?$prc:$plmap->getHighPrice());
		$plmap->setType($product->getType());
		$plmap->setLocation($product->getZone());
	}
	$this->doctrine->save($plmap);
	
}
	public function insertimages()
	{
		$product = $this->doctrine->findByID($this->entityName,$this->input->post('pid'));
		$product->setZone($this->doctrine->findByID('Entities\LocationZones',$this->input->post('pzone')));
		$product->setDivision($this->doctrine->findByID('Entities\LocationDivision',$this->input->post('division')));//city to division
		$product->setAddress($this->input->post('address'));
		$product->setLatitude($this->input->post('lat'));
		$product->setLongitude($this->input->post('long'));
		$product = $this->doctrine->save($product);
		if($product->getPrice()!='' ||$product->getPrice()!='0'){
			
			if($product->getType()->getProductTypesId()!=6)
			{
				
				$plmap=$this->doctrine->findOneCustom('Entities\PriceLocationMap',array('location'=>$product->getZone(),'type'=>$product->getType()));
				$this->setPriceinMAP($plmap,$product->getPrice(),$product);
			}
			else{
				$prices=$this->doctrine->findCustom('Entities\ProductUnitDetails',array('product'=>$product));
				foreach ($prices as $key ) {
					$plmap=$this->doctrine->findOneCustom('Entities\PriceLocationMap',array('location'=>$product->getZone(),'type'=>$product->getType()));
					$this->setPriceinMAP($plmap,$key->getPrice(),$product);
				}
			}
		}
		$data['edit']=$this->input->post('editmode');
		$data['title']="Insert Images:: Add Post";
		$data['imgs']=$this->doctrine->findCustom('Entities\ImageLink',array('product'=>$product));
		$data['pid']=$product->getProductId();
		$data['pname']=$product->getTitle();
		$data['error']='';
		$data['saad']='';

        $this->load->view('pages/header', $data);
        $this->load->view('pages/top_fixed_bar', $data);
        $this->load->view('pages/top_menu', $data);
        $this->load->view('pages/top_search', $data);
        $this->load->view('admin/upload', $data);
        $this->load->view('pages/footer', $data);
	}
	public function do_upload() {
        $upload_path_url = base_url() . 'uploads/';
        $config['upload_path'] = FCPATH . 'uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
		$config['max_size']	= '2048';
		$config['max_width']  = '1048';
		$config['max_height']  = '1048';
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            //$error = array('error' => $this->upload->display_errors());
            //$this->load->view('upload', $error);

            //Load the list of existing files in the upload directory
            $existingFiles = get_dir_file_info($config['upload_path']);
            $foundFiles = array();
            $f=0;
            foreach ($existingFiles as $fileName => $info) {
              if($fileName!='thumbs'){//Skip over thumbs directory
                //set the data for the json array   
                $foundFiles[$f]['name'] = $fileName;
                $foundFiles[$f]['size'] = $info['size'];
                $foundFiles[$f]['url'] = $upload_path_url . $fileName;
                $foundFiles[$f]['thumbnailUrl'] = $upload_path_url . 'thumbs/' . $fileName;
                $foundFiles[$f]['deleteUrl'] = base_url() . 'upload/deleteImage/' . $fileName;
                $foundFiles[$f]['deleteType'] = 'DELETE';
                $foundFiles[$f]['error'] = null;
                $f++;
              }
            }
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('files' => $foundFiles)));
        } else {
            $data = $this->upload->data();

            $product=$this->doctrine->findByID('Entities\ProductDetails',$this->input->post('productid'));
            $newuser = $product->getUser();
			$filename='productimg-'.date_format(new DateTime(), 'Y-m-d_H-i-s').'_user-'.$newuser->getUserId().'_';
			$newimage=new Entities\ImageLink;
			$upload_data = $data;
			$newimage->setImageName($filename.'_'.$upload_data['orig_name']);
			//echo $upload_data['orig_name'].'<br>';
			rename( $upload_data['full_path'],  $upload_data['file_path'].$filename.'_'.$upload_data['orig_name']);
			$newimage->setProduct($product);
			$newimage->setImageWatermark(base_url().'uploads/watermark/'.$filename.'_'.$upload_data['orig_name']);
			$newimage->setImageLink(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
			$newimage->setImageHeight($upload_data['image_height']);
			$newimage->setImageWidth($upload_data['image_width']);
			$newimage->setImageType('productimage');
			$newpath= $data['file_path'].$filename.'_'.$upload_data['orig_name'];
			$newimage=$this->doctrine->save($newimage);
            // creating watermarks
            $image_cfg = array();
        	$image_cfg['image_library'] = 'GD2';
			$image_cfg['source_image'] = $newpath;
            $image_cfg['new_image'] = $data['file_path'] . 'watermark/';
			$image_cfg['wm_text'] = 'Rehabhousing.com';
			$image_cfg['wm_type'] = 'text';
			$image_cfg['wm_font_size'] = '25';
			$image_cfg['wm_font_color'] = 'cccccc';
			$image_cfg['wm_vrt_alignment'] = 'middle';
			$image_cfg['wm_hor_alignment'] = 'center';
            $this->load->library('image_lib', $image_cfg);
			$this->image_lib->watermark();
			//Creating thumbnails
            $config = array();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $newpath;
            $config['create_thumb'] = TRUE;
            $config['new_image'] = $data['file_path'] . 'thumbs/';
            $config['maintain_ratio'] = TRUE;
            $config['thumb_marker'] = '';
            $config['width'] = 305;
            $config['height'] = 340;
			$this->image_lib->initialize($config);
            $this->image_lib->resize();
			
            //set the data for the json array	
            $info->name = $newimage->getImageName();
            $info->size = $data['file_size'];
            $info->type = $data['file_type'];
            $info->url = $newimage->getImageWatermark();
            // I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$data['file_name']
            $info->thumbnailUrl = base_url().'uploads/thumbs/'. $newimage->getImageName();
            $info->deleteUrl = base_url() . 'upload/deleteImage/' .$newimage->getImageId();
            $info->deleteType = 'DELETE';
            $info->error = $this->image_lib->display_errors();

            $files[] = $info;
            //this is why we put this in the constants to pass only json data
            if (IS_AJAX) {
                print json_encode(array("files" => $files));
                //this has to be the only data returned or you will get an error.
                //if you don't give this a json array it will give you a Empty file upload result error
                //it you set this without the if(IS_AJAX)...else... you get ERROR:TRUE (my experience anyway)
                // so that this will still work if javascript is not enabled
            } else {
            	 //echo json_encode(array("files" => $files));
                $file_data['upload_data'] = $this->upload->data();
                $this->load->view('pages/', $file_data);
            }
        }
    }

	public function imginsert()
	{
		$product = new Entities\ProductDetails;
		$product=$this->doctrine->findByID('Entities\ProductDetails',$this->input->post('productid'));
		
		$arr = array();
		//echo $this->input->post('imgcount');
		//echo $product->getTitle();
		if($this->input->post('imgcount')=='2')
			array_push($arr,'productimg1');
		else{
			for($i=1;$i<=$this->input->post('imgcount')-1;$i++)
				array_push($arr,'productimg'.$i);
		}	
		//print_r($arr);
		$this->maininsertion($arr,$product);
	
		header('Content-type: text/xml');
		//echo json_encode(array());
		
	}
	public function maininsertion($arr,$product)
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '2048';
		$config['max_width']  = '1048';
		$config['max_height']  = '1048';
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->load->library('upload', $config);
	
		foreach ($arr as $key) {
			if (!empty($_FILES[$key]['name'])) {
				try{
					//echo $key;
					if ( ! $this->upload->do_upload($key))
					{
						echo $this->upload->display_errors().'for: '.$_FILES[$key]['name'];
					}
					else
					{
						$newuser = $product->getUser();
						$filename='productimg'.date_format(new DateTime(), 'Y-m-d_H-i-s').'_user-'.$newuser->getUserId().'_';
						$newimage=new Entities\ImageLink;
						$upload_data = $this->upload->data();
						$newimage->setImageName($filename.$upload_data['orig_name']);
						//echo $upload_data['orig_name'].'<br>';
						rename( $upload_data['full_path'],  $upload_data['file_path'].$filename.'_'.$upload_data['orig_name']);
						
						$newimage->setProduct($product);
						$newimage->setImageWatermark(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
						$newimage->setImageLink(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
						$newimage->setImageHeight($upload_data['image_height']);
						$newimage->setImageWidth($upload_data['image_width']);
						$newimage->setImageType('productimage');
						$newimage=$this->doctrine->save($newimage);
					}
				}
				catch(Exception $ex)
				{
					echo '<pre>'.$ex->getMessage().'</pre>';
					log_message('error', $ex->getTrace());
					
				}
			}
		}
		
	}
	
	public function getFields($typeid=FALSE,$product=FALSE)
	{
		$tabledata='';
		$html='';
		if($typeid!=FALSE)
		{
			$this->fields = $this->doctrine->findCustom('Entities\ProductField',array('type'=>$typeid));
			foreach ($this->fields as $key) {
				$tabledata.=$key->getFieldName();
				$html.=$this->makehtml($key,$product);
			}
			if($product==FALSE)
				print json_encode(array("tabledata"=>$tabledata,
								'htmls'=>$html,
								));
			else 
				return $html;
			
		}
		/*else
			print json_encode(array("tabledata"=>'later',
								'htmls'=>'$html',
								));*/
	}
	public function setlatlang($pzoneid=FALSE)
	{
		if($pzoneid!=FALSE)
		{
			$zn = $this->doctrine->findByID('Entities\LocationZones',$pzoneid);
			print json_encode(array('lat'=>$zn->getLat(),
								'long'=>$zn->getLong()
							));
		}
		
	}
	public function makehtml($pfield,$product=FALSE)
	{
		if($product==FALSE)
		{
			$newfname=str_replace(array(' ','(',')'),'_',$pfield->getFieldName());
			$ret='<p><label class="control-label" for="p'.$pfield->getFieldName().'">'.$pfield->getFieldName();
			$ret.=(($pfield->getRequired()=='yes'))?' <span class="required">*</span>':'';
			if($pfield->getUitype()=='text'){
				$ret.='</label><input class="'.(($pfield->getFieldSku()=='date')?'dtpicker':' ').' form-control input-xsmall" id="p'.$newfname.'" type="text" name="p'.$newfname.'" placeholder="Type your '.$pfield->getFieldName().' here" /></p>';
			}
			else if($pfield->getUitype()=='chkbox')	
				$ret.='<input type="checkbox" id="p'.$newfname.'" name="p'.$newfname.'" class="form-control" style="margin:-4px 0 0 0;" /></label>';
			else if($pfield->getUitype()=='list')
			{
				$ret.='</label><select class="form-control" id="p'.$newfname.'"  name="p'.$newfname.'" >';
				$listvals = $this->doctrine->findCustom('Entities\ProductFieldList',array('pField'=>$pfield->getPFieldId()));
				$ret.=($pfield->getRequired()=='yes')?'':'<option value="Not Selected">Please Select</option>';
				foreach ($listvals as $key )
					$ret.='<option value="'.$key->getValue().'">'.$key->getValue().'</option>';
				
				$ret.='</select>';
			}
			return $ret;
		}
		else {
			$val= $this->doctrine->findOneCustom('Entities\ProductFieldDetailsMap',array('pField'=>$pfield,'product'=>$product));
			if(is_null($val)) $val = new Entities\ProductFieldDetailsMap;
			$newfname=str_replace(array(' ','(',')'),'_',$pfield->getFieldName());
			$ret='<p><label class="control-label" for="p'.$pfield->getFieldName().'">'.$pfield->getFieldName();
			$ret.=(($pfield->getRequired()=='yes'))?' <span class="required">*</span>':'';
			if($pfield->getUitype()=='text'){
				$ret.='</label><input class="'.(($pfield->getFieldSku()=='date')?'dtpicker':' ').'" id="p'.$newfname.'" value="'.$val->getValue().'" type="text" name="p'.$newfname.'" placeholder="Type your '.$pfield->getFieldName().' here" /></p>';
			}
			else if($pfield->getUitype()=='chkbox')	
				$ret.='<input type="checkbox" id="p'.$newfname.'" name="p'.$newfname.'" style="margin:-4px 0 0 0;" /></label>';
			else if($pfield->getUitype()=='list')
			{
				$ret.='</label><select id="p'.$newfname.'"  name="p'.$newfname.'" >';
				$listvals = $this->doctrine->findCustom('Entities\ProductFieldList',array('pField'=>$pfield->getPFieldId()));
				$ret.=($pfield->getRequired()=='yes')?'':'<option value="Not Selected">Please Select</option>';
				foreach ($listvals as $key )
					$ret.='<option value="'.$key->getValue().'" '.((is_null($val))?($key->getValue()==$val->getValue())?'selected="selected"':'':'').'>'.$key->getValue().'</option>';
				
				$ret.='</select>';
			}
			
			return $ret;
		}
	}
	public function getZones($divid=FALSE,$type='district')
	{
		if($divid!=FALSE)
		{
			$text='<option value="0" selected="selected">Select One</option>';
			$zones = $this->doctrine->findCustom('Entities\LocationZones',array('division'=>$divid));
			foreach ($zones as $key) 
			{
				$text.='<option value="'.$key->getLocId().'">'.$key->getNameEn().'</option>';
			}
			print $text;
		}
	}
}