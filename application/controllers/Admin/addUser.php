<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class addUser extends  CI_Controller {
	
	public $entityName='Entities\MemberDetails';
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\MemberDetails';
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
	public function index()
	{
		$page = 'admin/addUser';
		$data['title']='Rehabhousing.com :: Registration';
		$this->load->view('pages/header', $data);
		$this->load->view('pages/top_fixed_bar', $data);
		$this->load->view('pages/top_menu', $data);
		$this->load->view('pages/top_search', $data);
		$this->load->view($page, $data);
		$this->load->view('pages/footer', $data);
		
	}
	public function email_check($str)
	{
		if (!is_null($this->doctrine->findOneByFieldName($this->entityName,'email',$str)))
		{
			
			$this->form_validation->set_message('email_check', 'This email is already is in use by another member.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	public function cell_check($str)
	{
		if (!is_null($this->doctrine->findOneByFieldName($this->entityName,'cellNo',$str)))
		{
			$this->form_validation->set_message('cell_check', 'This cell phone is already is in use by another member.');
			return FALSE;
		}
		else
			return TRUE;
			
	}
	public function website_check($str)
	{
		$data=$this->doctrine->findOneByFieldName('Entities\CompanyDetails','website',$str);
		if (!is_null($data))
		{
			//$user=$this->doctrine->findOneByFieldName($this->entityName,'company',$data->getCompanyId());
			$this->form_validation->set_message('website_check', 'This company is already registered by '.$data->getContactPerson().' from '.$data->getDesignation());
			return FALSE;
		}
		else
			return TRUE;
			
	}
	public function createUser()
	{
		$data['title']='Add Users :: Admin Panel';
		//$ret_text=array();
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[100]');			
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[50]|matches[retype_password]');			
		$this->form_validation->set_rules('retype_password', 'Retype password', 'trim|required|max_length[50]|min_length[8]');			
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[50]|callback_email_check');			
		$this->form_validation->set_rules('cell_no', 'Cell No', 'trim|required|max_length[30]|callback_cell_check');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');
		$company_status=url_title($this->input->post('iscompany'),' ',FALSE);
		
		if($company_status=='on')
		{
			//$this->form_validation->set_rules('ufile','File', 'required');
			$this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[250]');
			$this->form_validation->set_rules('phone_no', 'Phone No', 'trim|required|max_length[30]');
			$this->form_validation->set_rules('website', 'Web site', 'trim|lowercase|max_length[50]|callback_website_check|required');
			$this->form_validation->set_rules('contact_person', 'Contact Persons', 'trim|max_length[100]|required');
			$this->form_validation->set_rules('designation', 'Designation', 'trim|max_length[50]|required');
		}
		if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{
			print json_encode(array('error'=>'Please insert the required fields'.form_error('name').'-'.form_error('password').'-'.form_error('retype_password').'-'
			.form_error('email').'-'.form_error('cell_no').'-'.form_error('phone_no').'-'.form_error('address').'-'.form_error('website'),
			'data'=>'no data',
			'stat'=>'0',
			//'tabledata'=>$company_status,
					));
			//$this->load->view('admin/addUser',$data,TRUE);
			
		}
		else
		{
			$newuser = new Entities\MemberDetails;
			$newuser->setActive('active');
			$newuser->setName(url_title($this->input->post('name'),' ',FALSE));
			$newuser->setEmail((strtolower ($this->input->post('email'))));
			$newuser->setPassword(md5((url_title($this->input->post('password'),' ',FALSE))));
			$newuser->setPriviledgeLevel('user');
			$newuser->setInsertTime(new DateTime());
			$newuser->setAmount(0);
			$newuser->setPremium('no');
			$newuser->setCellNo(url_title($this->input->post('cell_no'),' ',FALSE));
			if($company_status!='on')
			{
				$newuser=$this->doctrine->save($newuser);
				print json_encode(array('error'=>'no error',
				'data'=>'User registered. Please check your email.',
				'stat'=>'1',
				'tabledata'=>'<tr><td>'.$newuser->getUserId().'</td><td>'.$newuser->getName().
				'</td><td>no</td><td>'.$newuser->getCellNo().'</td><td>'.$newuser->getActive().'</td></tr>',
				));
			}
			else
			{
				/*$config['upload_path'] = './uploads/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '500';
				$config['max_width']  = '1024';
				$config['max_height']  = '1024';
				$config['overwrite'] = TRUE;
		    	$config['remove_spaces'] = TRUE;
				
				$this->load->library('upload', $config);*/
				//company & user insert
				$newcompany= new Entities\CompanyDetails;
				$newcompany->setAddress($this->input->post('address'));
				$newcompany->setPhoneNo(url_title($this->input->post('phone_no'),' ',FALSE));
				$newcompany->setRehab((url_title($this->input->post('chkrehab'),' ',FALSE)=='on')?"yes":"no");
				$newcompany->setBlda((url_title($this->input->post('chkblda'),' ',FALSE)=='on')?"yes":"no");
				$newcompany->setWebsite($this->input->post('website'));
				$newcompany->setContactPerson(url_title($this->input->post('contact_person'),' ',FALSE));
				$newcompany->setDesignation($this->input->post('designation'));
				$newcompany=$this->doctrine->save($newcompany);
				$newuser->setCompany($newcompany);
				$newuser=$this->doctrine->save($newuser);
				//$this->upload->do_upload('ufile');
				//if ( ! $this->upload->do_upload("ufile"))
				//{
				print json_encode(array('error'=>'no error',
					'data'=>'Company registered. Please check your Email for more information.',
					'stat'=>'1'
					/*,
					'tabledata'=>'<tr><td>'.$newuser->getUserId().'</td><td>'.$newuser->getName().
						'</td><td>'.$newcompany->getPhoneNo().'</td><td>'.$newuser->getCellNo().'</td><td>'.$newuser->getActive().'</td></tr>',*/
					));
				/*}
				else
				{
					$filename=date_format(new DateTime(), 'Y-m-d_H-i-s').'_user-'.$newuser->getUserId().'_';
					$newimage=new Entities\ImageLink;
					$upload_data = $this->upload->data();
					$newimage->setImageName($filename.$upload_data['orig_name']);
					rename( $upload_data['full_path'],  $upload_data['file_path'].$filename.'_'.$upload_data['orig_name']);
					
					$newimage->setCompany($newcompany);
					$newimage->setImageWatermark(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
					$newimage->setImageLink(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
					$newimage->setImageHeight($upload_data['image_height']);
					$newimage->setImageWidth($upload_data['image_width']);
					
					$newimage->setImageType('logo');
					$newimage=$this->doctrine->save($newimage);
					echo json_encode(array('error'=>'no error',
						'data'=>'Data inserted with company',
						'stat'=>'1',
						'tabledata'=>'<tr><td>'.$newuser->getUserId().'</td><td>'.$newuser->getName().
						'</td><td>'.$newcompany->getPhoneNo().'</td><td>'.$newuser->getCellNo().'</td><td>'.$newuser->getActive().'</td></tr>',
						));
					//var_dump();
				}*/
			}
		}
	}
}
