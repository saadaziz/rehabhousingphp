<?php

class insertAd extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$entityName='Entities\WebAdvertise';
		$this->load->helper(array('form', 'url'));
	}

	function index()
	{
		$this->load->view('admin/insertad', array('error' => ' ' ));
	}

	function insert()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('admin/insertad', $error);
		}
		else
		{
			$filename='CompanyAd-'.date_format(new DateTime(), 'Y-m-d_H-i-s').'_user-companyAd_';
			
			$upload_data = $this->upload->data();
			rename( $upload_data['full_path'],  $upload_data['file_path'].$filename.'_'.$upload_data['orig_name']);
			
			$imgName=$filename.'_'.$upload_data['orig_name'];
			$link=base_url().'uploads/'.$filename.'_'.$upload_data['orig_name'];
			
			
			// Data Save in ImageLink Table
			
			$newimage=new Entities\ImageLink;
			
			$newimage->setImageName($imgName);
			$newimage->setImageWatermark(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
			$newimage->setImageLink(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
			$newimage->setImageHeight($upload_data['image_height']);
			$newimage->setImageWidth($upload_data['image_width']);
			$newimage->setImageType('CompanyAd');
			$newpath= $upload_data['file_path'].$filename.'_'.$upload_data['orig_name'];
			$newimage=$this->doctrine->save($newimage);
			
			
			
			
			//Data input in webAdvertisement Table
			
			$newAds = new Entities\WebAdvertise;
			
			$newAds->setAdPlace($this->input->post('addPlace'));
			$newAds->setLink($this->input->post('link'));
			$newAds->setImageid($newimage);
			$newAds->setAltText($this->input->post('alt'));
			$newAds->setCompanyName($this->input->post('companyName'));
			$newAds->setInserttime(new DateTime());
		
			$newAds=$this->doctrine->save($newAds);
			
			
			$mgs="Your Ad Post Successfully!";
			
			//View data
			$url=base_url()."admin/insertAd/viewad";
			//$this->load->view("admin/viewAd",$mgs);
			header("Location: $url");
		}
	}
	public function viewAd()
	{
		$page="admin/viewad";
		$entityName='Entities\WebAdvertise';
		$data['ads']=$this->doctrine->findAll($entityName);
		$this->load->view($page,$data);
	}
	
	public function delete($id)
	{
		//echo $id=$this->input->post('eid');
		
		$newAds=$this->doctrine->findByID('Entities\WebAdvertise',$id);		
		$image = $newAds->getImageid();
		$this->doctrine->remove($newAds);
		$this->doctrine->remove($this->doctrine->findByID('Entities\ImageLink',$image));
		//echo $eid;
	}
}
?>