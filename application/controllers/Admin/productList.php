<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class productList extends  CI_Controller {
	
	public $entityName='Entities\ProductDetails';
	public static $fields;
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\ProductDetails';
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
	public function index($offset=0)
	{
		if($this->session->userdata('user'))
		{
			$user=($this->doctrine->findByID('Entities\MemberDetails',$this->session->userdata['user']['userid']));
			if($user->getPriviledgeLevel()=='Super Admin')
			{
				$page = 'admin/productList';
				$data['title']='Produt List :: Admin Panel';
				$data['offset']=$offset;
				$products=$this->doctrine->findCustomWithOrder($this->entityName,array(),array('insertTime'=>'desc'));
				$data['products']=array_slice($products,$offset,15);
				$this->load->library('pagination');
				$config['base_url'] = base_url().$page;
				$config['total_rows'] = count($products);
				$config['per_page'] = 15;
				$config['full_tag_open'] = '<div class="pegDiv"><ul class="pagination">';
				$config['full_tag_close'] = '</ul></div>';
				$config['cur_tag_open'] ='<li class="active"><a href="#">';
				$config['cur_tag_close']='</a></li>';
				$config['first_tag_open']='<li>';
				$config['first_tag_close']='</li>';
				$config['last_tag_close'] ='</li>';
				$config['last_tag_open'] ='<li>';
				$config['next_tag_open']='<li>';
				$config['next_tag_close']='</li>';
				$config['prev_tag_open']='<li>';
				$config['prev_tag_close']='</li>';
				$config['num_tag_open']='<li>';
				$config['num_tag_close']='</li>';
				$config['next_link']='&raquo;';
				$config['prev_link'] = '&laquo;';
				$this->pagination->initialize($config);
				$this->load->view('admin/topheader', $data);
		        $this->load->view($page, $data);
				$this->load->view('admin/footer', $data);
			}
			else {
				echo 'Not Super Admin!'.$user->getUserId();
			}
		}
		else {
			echo 'Not Logged in';
		}
	}
	public function insertTopProject()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '2048';
		$config['max_width']  = '1048';
		$config['max_height']  = '1048';
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('imgtop'))
		{
			$data['error']= $this->upload->display_errors().'for: '.$_FILES['imgtop'];
		}
		else
		{
			$product = new Entities\ProductDetails;
			$product= $this->doctrine->findByID($this->entityName,$this->input->post('pid'));
			$data['saad']='';
			$newuser = $product->getUser();
			$filename='topProjectimg-'.date_format(new DateTime(), 'Y-m-d_H-i-s').'_user-'.$newuser->getUserId().'_';
			$newimage=new Entities\ImageLink;
			$upload_data = $this->upload->data();
			$newimage->setImageName($filename.$upload_data['orig_name']);
			rename( $upload_data['full_path'],  $upload_data['file_path'].$filename.'_'.$upload_data['orig_name']);
			$newimage->setProduct($product);
			$newimage->setImageWatermark(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
			$newimage->setImageLink(base_url().'uploads/'.$filename.'_'.$upload_data['orig_name']);
			$newimage->setImageHeight($upload_data['image_height']);
			$newimage->setImageWidth($upload_data['image_width']);
			$newimage->setImageType('topproject');
			$newimage=$this->doctrine->save($newimage);
            $product->setApplyForTopProject('yes');
			$product->setTopProject('yes');
			$product->setImageForTopProject($newimage->getImageId());
			$product->setTopProjectTitle($this->input->post('txttitle'));
			$product->setTopProjectLocation($this->input->post('txtloc'));
			$product->setTopProjectPrice($this->input->post('txtprice'));
			$product->setTopProjectBedbath($this->input->post('txtbedbath'));
			$this->doctrine->save($product);
			echo 'Top project selected:'.$product->getTitle();
			//$unitdets->setImage($newimage);
		}
		
	}
	public function setapproved($value=FALSE,$type=FALSE,$offset)
	{
		if($value!=FALSE)
		{
			$arr = array();
			$viewtop='0';
			$keys='no1';
			$ids=explode('-', $value);
			foreach ($ids as $key) {
				if($key!='')
				{
					$product= $this->doctrine->findByID($this->entityName,$key);
					if($type!='topproject')	{
						$product->setApproval($type);
					}
					else{
						if($product->getTopProject()=='no')
						{
							$product->setApplyForTopProject('yes');
							$viewtop='1';
							$keys=$key;
						}
						else{
							$product->setTopProject('no');
							$product->setApplyForTopProject('no');
						}
					}
					$this->doctrine->save($product);
				}
			}
			$products=array_slice( $this->doctrine->findCustomWithOrder($this->entityName,array(),array('insertTime'=>'desc')), $offset,15);
			$html='<tr><th><input type="checkbox" id="checkall" /></th><th>ID</th><th>Title</th><th>Category</th><th>Top?</th><th>Approved?</th><th>User</th><th>Inserted</th></tr>';
			foreach ($products as $key ) {
				$var ='';
				if($key->getApproval() == 'rejected')
					$var ="<button type='button' class='btn btn-danger btn-sm'>Rejected</button>";
				else if($key->getApproval() == 'pending')
					$var ="<button type='button' class='btn btn-warning btn-sm'>Pending</button>";
				else
					$var ="<button type='button' class='btn btn-success btn-sm'>Approved</button>";
				$html.="<tr id=".$key->getProductId() ."><td class='span1'><input type='checkbox' /></td>".
					"<td class='span1'>". $key->getProductId() ."</td>".
					"<td class='span2'>".$key -> getTitle() ."</td>".
					"<td class='span2'><div class='tl' data-placement='bottom' data-toggle='tooltip' data-original-title=". $key ->getType()->getParent()->getTypeName().">".$key-> getType()->getTypeName() ."</div></td>".			
					"<td class='span2'>".(($key -> getTopProject()=='yes')?'<button type="button" class="btn btn-success btn-sm">'. $product -> getTopProject().'</button>'
                    :'<button type="button" class="btn btn-danger btn-sm">'. $product -> getTopProject().'</button>' )."</td>".
					"<td class='span2'>".$var."</td>".
					"<td class='span2'>".$key -> getUser()->getName() ."</td>".
					"<td class='span2'>".$key -> getPrice()."</td>".
					"<td class='span2'>".$key -> getInsertTime()->format('Y-m-d') ."</td>".
					"<td><a' class='btn btn-info btn-mini'>Edit</a></td></tr>";
				
			}
			//array_push($arr,array('htmls'=>$html));
			print json_encode(array('htmls'=>$html,
			'viewtop'=>$viewtop,
			'keys'=>$keys));
		}
	}
	
	
}