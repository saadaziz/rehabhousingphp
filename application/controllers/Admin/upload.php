<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upload extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->helper(array('form', 'url'));
    }

    public function index() {
        $this->load->view('admin/upload', array('error' => ''));
    }

    public function do_upload() {
        $upload_path_url = base_url() . 'uploads/';

        $config['upload_path'] = FCPATH . 'uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '20000';
		$config['max_size']	= '2048';
		$config['max_width']  = '1048';
		$config['max_height']  = '1048';
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            //$error = array('error' => $this->upload->display_errors());
            //$this->load->view('upload', $error);

            //Load the list of existing files in the upload directory
            $existingFiles = get_dir_file_info($config['upload_path']);
            $foundFiles = array();
            $f=0;
            foreach ($existingFiles as $fileName => $info) {
              if($fileName!='thumbs'){//Skip over thumbs directory
                //set the data for the json array   
                $foundFiles[$f]['name'] = $fileName;
                $foundFiles[$f]['size'] = $info['size'];
                $foundFiles[$f]['url'] = $upload_path_url . $fileName;
                $foundFiles[$f]['thumbnailUrl'] = $upload_path_url . 'thumbs/' . $fileName;
                $foundFiles[$f]['deleteUrl'] = base_url() . 'upload/deleteImage/' . $fileName;
                $foundFiles[$f]['deleteType'] = 'DELETE';
                $foundFiles[$f]['error'] = null;
                
                $f++;
              }
            }
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('files' => $foundFiles)));
        } else {
            $data = $this->upload->data();
            /*
             * Array
              (
              [file_name] => png1.jpg
              [file_type] => image/jpeg
              [file_path] => /home/ipresupu/public_html/uploads/
              [full_path] => /home/ipresupu/public_html/uploads/png1.jpg
              [raw_name] => png1
              [orig_name] => png.jpg
              [client_name] => png.jpg
              [file_ext] => .jpg
              [file_size] => 456.93
              [is_image] => 1
              [image_width] => 1198
              [image_height] => 1166
              [image_type] => jpeg
              [image_size_str] => width="1198" height="1166"
              )
             */
            // to re-size for thumbnail images un-comment and set path here and in json array
            $config = array();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['full_path'];
            $config['create_thumb'] = TRUE;
            $config['new_image'] = $data['file_path'] . 'thumbs/';
            $config['maintain_ratio'] = TRUE;
            $config['thumb_marker'] = '';
            $config['width'] = 120;
            $config['height'] = 125;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            
           
            //set the data for the json array	
            $info->name = $newimage->getImageName();
            $info->size = $data['file_size'];
            $info->type = $data['file_type'];
            $info->url = $newimage->getImageWatermark();
            // I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$data['file_name']
            $info->thumbnailUrl = base_url().'uploads/thumbs/'. $newimage->getImageName();
            $info->deleteUrl = base_url() . 'upload/deleteImage/' .$newimage->getImageId();
            $info->deleteType = 'DELETE';
            $info->error = $this->image_lib->display_errors();

            $files[] = $info;
            //this is why we put this in the constants to pass only json data
            if (IS_AJAX) {
                echo json_encode(array("files" => $files));
                //this has to be the only data returned or you will get an error.
                //if you don't give this a json array it will give you a Empty file upload result error
                //it you set this without the if(IS_AJAX)...else... you get ERROR:TRUE (my experience anyway)
                // so that this will still work if javascript is not enabled
            } else {
                $file_data['upload_data'] = $this->upload->data();
                $this->load->view('upload/upload_success', $file_data);
            }
        }
    }

    public function deleteImage($file=FALSE) {//gets the job done but you might want to add error checking and security
        if($file!=FALSE)
		{
			$file2=$this->doctrine->findByID('Entities\ImageLink',$file);
			$filename=$file2->getImageName();
			$success = unlink(FCPATH . 'uploads/' . $filename);
	        $success = unlink(FCPATH . 'uploads/thumbs/' . $filename);
	        $success = unlink(FCPATH . 'uploads/watermark/' . $filename);
			$this->doctrine->remove($file2);
	        //info to see if it is doing what it is supposed to	
	        $info->sucess = $success;
	        $info->path = base_url() . 'uploads/' . $filename;
	        $info->file = is_file(FCPATH . 'uploads/' . $filename);
	
	        if (IS_AJAX) {
	            //I don't think it matters if this is set but good for error checking in the console/firebug
	            echo json_encode(array($info));
	        } else {
	            //here you will need to decide what you want to show for a successful delete		
	            $file_data['delete_data'] = $file;
	            $this->load->view('admin/delete_success', $file_data);
	        }
		}

    }

}