<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class userList extends  CI_Controller {
	
	public $entityName='Entities\MemberDetails';
	public static $fields;
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\MemberDetails';
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
	public function index($offset=0)
	{
		if($this->session->userdata('user'))
		{
			$user=($this->doctrine->findByID('Entities\MemberDetails',$this->session->userdata['user']['userid']));
			if($user->getPriviledgeLevel()=='Super Admin')
			{
				$page = 'admin/userList';
				$data['title']='User List :: Admin Panel';
				$data['offset']=$offset;
		        $users=$this->doctrine->findCustomWithOrder($this->entityName,array(),array('insertTime'=>'desc'));
				$data['users']=array_slice($users,$offset,15);
				$this->load->library('pagination');
				
				$config['base_url'] = base_url().$page;
				$config['total_rows'] = count($users);
				$config['per_page'] = 15;
				$config['full_tag_open'] = '<div class="pegDiv"><ul class="pagination">';
				$config['full_tag_close'] = '</ul></div>';
				$config['cur_tag_open'] ='<li class="active"><a href="#">';
				$config['cur_tag_close']='</a></li>';
				$config['first_tag_open']='<li>';
				$config['first_tag_close']='</li>';
				$config['last_tag_close'] ='</li>';
				$config['last_tag_open'] ='<li>';
				$config['next_tag_open']='<li>';
				$config['next_tag_close']='</li>';
				$config['prev_tag_open']='<li>';
				$config['prev_tag_close']='</li>';
				$config['num_tag_open']='<li>';
				$config['num_tag_close']='</li>';
				$config['next_link']='&raquo;';
				$config['prev_link'] = '&laquo;';
				$this->pagination->initialize($config);
				$this->load->view('admin/topheader', $data);
		        $this->load->view($page, $data);
				$this->load->view('admin/footer', $data);
			}
			else 
				echo 'Not Super Admin!'.$user->getUserId();
		}
		else 
			echo 'Not Logged in';
	}
	public function login($id,$value,$offset=FALSE)
	{
		if($id!=FALSE)
		{
			$user= $this->doctrine->findByID('Entities\MemberDetails',$id);
			$arr=array('userid'=>$user->getUserId(),
					'username'=>$user->getName(),
					'companyid'=>(!is_null( $user->getCompany())?$user->getCompany()->getCompanyId():0),
					'login_time'=>date_format(new DateTime(), 'Y-m-d_H-i-s'));
				$this->session->set_userdata('user',$arr);
				$cookie = array(
					    'name'   => 'user',
					    'value'  => json_encode($arr),
					    'expire' => '86500'	);
				$this->input->set_cookie($cookie);
				print TRUE;
		}
	}
	public function changePass($id,$value,$offset=FALSE)
	{
		if($id!=FALSE)
		{
			$user= $this->doctrine->findByID('Entities\MemberDetails',str_replace('txt','',$id));
			$user->setPassword(md5($value));
			$this->doctrine->save($user);
		}		
	}
	public function setActive($id,$type,$offset=FALSE)
	{
		if($id!=FALSE)
		{
			$html='';
			$user= $this->doctrine->findByID('Entities\MemberDetails',$id);
			if($type=='active')
			{
				$html.=($user->getActive()=='active')?'<button type="button" class="btn btn-danger btn-sm active">Inactive</button>':
				'<button type="button" class="btn btn-success btn-sm active">active</button>';
				$user->setActive(($user->getActive()=='active')?'inactive':'active');
			}	
			else if($type=='premium'){
				$html.=($user->getPremium()!='yes')?'<button type="button" class="btn btn-success btn-sm premium">Premium</button>':
				'<button type="button" class="btn btn-danger btn-sm premium">Regular</button>';
				$user->setPremium(($user->getPremium()=='yes')?'no':'yes');
			}
			$this->doctrine->save($user);
			
			
			print json_encode(array('htmls'=>$html,
			'flg'=>'2'));
				
		}
		
	}
}