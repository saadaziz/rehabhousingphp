<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class adminTemplete extends  CI_Controller {
	
    public function index()
    {
		$data['title']='Rehabhousing.com :: Admin Templete';
		$data['SaleCats'] = $this->doctrine->findCustom('Entities\ProductTypes',array('parent'=>'1'));
		$data['RentCats'] = $this->doctrine->findCustom('Entities\ProductTypes',array('parent'=>'2'));
		$products=$this->doctrine->findCustom('Entities\ProductDetails',array('approval'=>'approved'),array('insertTime'=>'desc'));
		$data['activeads'] = array_slice($products,0,5);
		$data['ad1'] = array_slice($products,5,5);
		//$data['ad2'] = array_slice($products,10,5);
		$top_projects= $this->doctrine->findCustom('Entities\ProductDetails',array('topProject'=>'yes'),array('insertTime'=>'desc'));
		$data['top_projects']=$top_projects;
		$data['row_count']=(intval(count($top_projects)/3))+((count($top_projects)%3==0)?0:1);
		$data['realEstateJobs'] =array_slice( $this->doctrine->findCustomWithOrder('Entities\WebJobs',array(),array('insertTime'=>'desc')),0,7);
		$this->load->view('pages/header', $data);
		$this->load->view('pages/top_fixed_bar', $data);
		$this->load->view('pages/top_menu', $data);
		//$this->load->view('pages/top_search', $data);
		$this->load->view('adminpanel/topbar');
		$this->load->view('adminpanel/sidebar');
		$this->load->view('adminpanel/main_container');
		//$this->load->view('pages/home_container', $data);
		$this->load->view('pages/footer', $data);
    }
}