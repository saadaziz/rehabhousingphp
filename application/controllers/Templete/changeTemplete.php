<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class changeTemplete extends  CI_Controller {

    public $entityName='Entities\WebThemes';
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\WebThemes';
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index()
    {
        $page = 'templete/changeTemplete';
		$data['title']='Change Templete Color';
        $data['themes']=$this->doctrine->findByID($this->entityName,1);
		$this->load->view('templete/header', $data);
        $this->load->view('templete/userTemplete');
		$this->load->view('templete/footer');
		//$data['tableid']='tblCategories';
        $this->load->view($page, $data);
    }
    public function add()
    {
          
        $this->form_validation->set_rules('body_bg_color', 'Body Background Color', 'required');
		$this->form_validation->set_rules('slider_bg_color', 'Slider Background Color', 'required');		
        $this->form_validation->set_rules('text_color', 'Text Color', 'required');	
        $this->form_validation->set_rules('text_header_color', 'Text Header Color', 'required');	
		$this->form_validation->set_rules('top_menu_color', 'Top Menu Color', 'required');
		$this->form_validation->set_rules('top_menu_hover_color', 'Top Menu hover Color', 'required');		
		$this->form_validation->set_rules('footer_bg_color', 'Footer Background Color', 'required');
		$this->form_validation->set_rules('footer_text_color', 'Footer Text Color', 'required');
		$this->form_validation->set_rules('footer_menu_color', 'Footer Menu Color', 'required');
		$this->form_validation->set_rules('footer_menu_hover_color', 'Footer Menu Hover Color', 'required');
		$this->form_validation->set_rules('project_name_color', 'Project Name Color', 'required');
		$this->form_validation->set_rules('project_address_color', 'Project Address Color', 'required');
		$this->form_validation->set_rules('project_price_color', 'project Price Color', 'required');
		$this->form_validation->set_rules('project_feature_color', 'Project Feature Color', 'required');
        
        if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{
			print json_encode(array('error'=>'Please insert the required fields'.form_error('body_bg_color').'-'.form_error('slider_bg_color').'-'
			.form_error('text_color').'-'.form_error('text_header_color').'-'
			.form_error('top_menu_color').'-'.form_error('top_menu_hover_color').'-'.form_error('footer_bg_color').'-'
			.form_error('footer_text_color').'-'.form_error('footer_menu_color').'-'.form_error('footer_menu_hover_color').'-'
			.form_error('project_name_color').'-'.form_error('project_address_color').'-'.form_error('project_price_color').'-'
			.form_error('project_feature_color'),'data'=>'no data',));
			//$this->load->view('admin/addUser',$data,TRUE);
			
		}	
       else
        {
        	$theme = new Entities\WebThemes;
			$theme=$this->doctrine->findByID($this->entityName,'1');
            $theme->setBodyBgColor(url_title($this->input->post('body_bg_color'),' ',FALSE));
			$theme->setSliderBgColor(url_title($this->input->post('slider_bg_color'),' ',FALSE));
			$theme->setTextColor(url_title($this->input->post('text_color'),' ',FALSE));
			$theme->setTextHeaderColor(url_title($this->input->post('text_header_color'),' ',FALSE));
			$theme->setTopMenuColor(url_title($this->input->post('top_menu_color'),' ',FALSE));
			$theme->setTopMenuHoverColor(url_title($this->input->post('top_menu_hover_color'),' ',FALSE));
			$theme->setFooterBgColor(url_title($this->input->post('footer_bg_color'),' ',FALSE));			
			$theme->setFooterTextColor(url_title($this->input->post('footer_text_color'),' ',FALSE));
			$theme->setFooterMenuColor(url_title($this->input->post('footer_menu_color'),' ',FALSE));
			$theme->setFooterMenuHoverColor(url_title($this->input->post('footer_menu_hover_color'),' ',FALSE));
			
			$theme->setProjectNameColor(url_title($this->input->post('project_name_color'),' ',FALSE));
			$theme->setProjectAddressColor(url_title($this->input->post('project_address_color'),' ',FALSE));
			$theme->setProjectPriceColor(url_title($this->input->post('project_price_color'),' ',FALSE));
			$theme->setProjectFeatureColor(url_title($this->input->post('project_feature_color'),' ',FALSE));			
			$theme->setThemeName("hgrh");
			$theme=$this->doctrine->save($theme);
				
			$tabledata="<style type='text/css'> .bg-color{background: #".$theme->getBodyBgColor().";}
			.slider-bg {background: #".$theme->getSliderBgColor().";}
			.text-color{color: #".$theme->getTextColor().";}
			.text-header{color: #".$theme->getTextHeaderColor().";}
			.top-menu{color: #".$theme->getTopMenuColor().";}
			.menu a.cative-company,.menu a:hover {color: #".$theme->getTopMenuHoverColor().";}
			.main-footer{ background:#".$theme->getFooterBgColor().";}			
			.main-footer{ color:#".$theme->getFooterTextColor().";}
			.footer-menu:hover{color: #".$theme->getFooterMenuColor().";}
			.footer-menu{color:#".$theme->getFooterMenuHoverColor().";}
			
			.proName {color: #".$theme->getProjectNameColor().";}
			.proLoc {color: #".$theme->getProjectAddressColor().";}
			.proPrice {color: #".$theme->getProjectPriceColor().";}
			.proRoome {color: #".$theme->getProjectFeatureColor().";}
			</style>"; 
			
				 print $tabledata;
				
        //$this->load->view('templete/css');
        $data['title']='Change Templete Color';
        $data['themes']=$this->doctrine->findByID($this->entityName,1);
		$this->load->view('templete/header',$data);
        $this->load->view('templete/userTemplete');
		$this->load->view('templete/footer');
		
		}
          

    }
}