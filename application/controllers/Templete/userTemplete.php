<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserTemplete extends  CI_Controller {

    public $entityName='Entities\ProductTypes';
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\ProductTypes';
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }
    public function index()
    {
        $page = 'templete/userTemplete';
		//$data['title']='Add Category :: Admin Panel';
		//$data['nameid']='parent';
		//$data['tableid']='tblCategories';
       // $data['cats']=$this->doctrine->findAll($this->entityName);
		$this->load->view('templete/header');
        $this->load->view($page);
		$this->load->view('templete/footer');
    }
    public function create()
    {
        
        $this->form_validation->set_rules('title', 'title', 'required');
        if ($this->form_validation->run() === FALSE)
        {
            print 'Fields not valid';
        }
        else
        {
            $slugtitle = url_title($this->input->post('title'),' ',FALSE);
            $slugpar = url_title($this->input->post('parent'), 'dash', TRUE);
            $cat= new Entities\ProductTypes;
            $parid= new Entities\ProductTypes;
            $cat->setTypeName($slugtitle);

            if($slugpar!=0)
            {    $parid=$this->doctrine->findByID($this->entityName,$slugpar);

            	$cat->setParent($parid);
            }
            $cat->setCount(0);
            try{
                //echo '<pre>'.$cat->getTypeName().'</pre>';
                $this->doctrine->save($cat);
				//$cats=$this->doctrine->findAll($this->entityName);
				/*$serializer = $this->container->get('serializer');
				$reports = $serializer->serialize($doctrineobject, 'json');*/
				$optiontext= "<option value\"".$cat->getProductTypesId()."\">".$cat->getTypeName()."</option>";
				$var=(!is_null($parid))?$parid->getTypeName():"none";
				$tabledata="<tr><td>".$cat->getProductTypesId()."</td><td>".$cat->getTypeName()."</td><td>".$var."</td></tr>"; 
				//echo new Response($cats);
				//header('Content-type: application/json');
				print json_encode (array('table'=>$tabledata,
				'option'=>$optiontext,));
				
				/*foreach ($cats as $var) {
					echo $var->getTypeName();
				}*/
            }
            catch(Exception $ex)
            {
                /*$data['err']=$ex->getTraceAsString();
                $this->load->view('admin/error',$data );
                $this->load->view('admin/addCategory',$data);*/
                print $ex;
            }
            /*$cat->setTypeName('Want to Sell');
            $cat->setParent($parid);
            $cat->setCount(0);
            $this->doctrine->save($cat);*/

        }
		

    }
}