<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class companyWebsite extends  CI_Controller {
	
	public $entityName='Entities\CompanyWebsite';
	public static $fields;
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\CompanyWebsite';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }
	public function index()
	{
		$memberId=2;	
		$data['title']="Rehab Housing :: User's Company Website";
		$member=$this->doctrine->FindByID('Entities\MemberDetails',$memberId);
		$data['userid']=$this->doctrine->findOneCustom('Entities\CompanyWebsite',array('company'=>$member->getCompany()));
		$this->load->view('company_web/companyWebsite',$data);
		
	}
	
	public function selectTemplete(){
	/*	
			$this->form_validation->set_rules('theme_id', 'Theme Id', 'required');
        $this->form_validation->set_rules('aboutUs', 'About Us', 'required');
        $this->form_validation->set_rules('titleMeta', 'Title Meta', 'required');
        $this->form_validation->set_rules('descriptionMeta', 'Description Meta', 'required');		
        $this->form_validation->set_rules('keywordMeta', 'Keyword Meta', 'required');
		$this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');
  
        if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{
			print json_encode(array('error'=>'Please insert the required fields'.form_error('title').'-'.form_error('company_name').'-'
			.form_error('description').'-'.form_error('designation').'-'
			.form_error('last_date_of_submission').'-'.form_error('Education_requirements').'-'.form_error('Experience').'-'
			.form_error('Salary_range').'-'.form_error('job_location').'-'.form_error('job_source').'-'
			.form_error('contact_info'),'data'=>'no data',));
			//$this->load->view('admin/addUser',$data,TRUE);
			
		}	*/
		
		$CompanyWeb = new Entities\CompanyWebsite;
		$themeId=$this->input->post('theme_id');
		
		//Create a new web theme and get data from db..................
		$webtheme=$this->doctrine->findByID('Entities\WebThemes',$themeId);
		$ThemeName=$webtheme->getThemeName();
		$BodyBgColor=$webtheme->getBodyBgColor();
		$SliderBgColor=$webtheme->getSliderBgColor();
		$TextColor=$webtheme->getTextColor();
		$TextHeaderColor=$webtheme->getTextHeaderColor();
		$TopMenuColor=$webtheme->getTopMenuColor();
		$TopMenuHoverColor=$webtheme->getTopMenuHoverColor();
		$FooterBgColor=$webtheme->getFooterBgColor();
		$FooterTextColor=$webtheme->getFooterTextColor();
		$FooterMenuColor=$webtheme->getFooterMenuColor();
		$FooterMenuHoverColor=$webtheme->getFooterMenuHoverColor();
		$ProjectNameColor=$webtheme->getProjectNameColor();
		$ProjectAddressColor=$webtheme->getProjectAddressColor();
		$ProjectPriceColor=$webtheme->getProjectPriceColor();
		$ProjectFeatureColor=$webtheme->getProjectFeatureColor();
		
		//Set web theme record for user............
		$newtheme = new Entities\WebThemes;
		$newtheme->setThemeName($ThemeName);
		$newtheme->setBodyBgColor($BodyBgColor);
		$newtheme->setSliderBgColor($SliderBgColor);
		$newtheme->setTextColor($TextColor);
		$newtheme->setTextHeaderColor($TextHeaderColor);
		$newtheme->setTopMenuColor($TopMenuColor);
		$newtheme->setTopMenuHoverColor($TopMenuHoverColor);
		$newtheme->setFooterBgColor($FooterBgColor);
		$newtheme->setFooterTextColor($FooterTextColor);
		$newtheme->setFooterMenuColor($FooterMenuColor);
		$newtheme->setFooterMenuHoverColor($FooterMenuHoverColor);
		$newtheme->setProjectNameColor($ProjectNameColor);
		$newtheme->setProjectAddressColor($ProjectAddressColor);
		$newtheme->setProjectPriceColor($ProjectPriceColor);
		$newtheme->setProjectFeatureColor($ProjectFeatureColor);
		$newtheme=$this->doctrine->save($newtheme);
				
		
		//CompanyWeb->setTheme();
		$CompanyWeb->setAboutUs($this->input->post('aboutUs'));
		$CompanyWeb->setTitleMeta(url_title($this->input->post('titleMeta'),' ',FALSE));
		$CompanyWeb->setDescriptionMeta(url_title($this->input->post('descriptionMeta'),' ',FALSE));
		$CompanyWeb->setKeywordMeta(url_title($this->input->post('keywordMeta'),' ',FALSE));
		
		$CompanyWeb->setTheme($newtheme);
		$CompanyWeb->setCompany($this->doctrine->findByID('Entities\CompanyDetails',1));
		$CompanyWeb=$this->doctrine->save($CompanyWeb);
		
	}
	
//****************************************Create Function*******************************************	
	
	public function Change()
	{
		$fieldid = $this->input->post('fieldid');
		$oldTheme = $this->input->post('oldTheme');
		$theme_id = $this->input->post('theme_id');
		$aboutUs = $this->input->post('aboutUs');
		$titleMeta = $this->input->post('titleMeta');
		$descriptionMeta = $this->input->post('descriptionMeta');
		$keywordMeta = $this->input->post('keywordMeta');
		$companyId = $this->input->post('companyId');
		
		//Create a new web theme and get data from db..................
		$webtheme=$this->doctrine->findByID('Entities\WebThemes',$theme_id);
		$ThemeName=$webtheme->getThemeName();
		$BodyBgColor=$webtheme->getBodyBgColor();
		$SliderBgColor=$webtheme->getSliderBgColor();
		$TextColor=$webtheme->getTextColor();
		$TextHeaderColor=$webtheme->getTextHeaderColor();
		$TopMenuColor=$webtheme->getTopMenuColor();
		$TopMenuHoverColor=$webtheme->getTopMenuHoverColor();
		$FooterBgColor=$webtheme->getFooterBgColor();
		$FooterTextColor=$webtheme->getFooterTextColor();
		$FooterMenuColor=$webtheme->getFooterMenuColor();
		$FooterMenuHoverColor=$webtheme->getFooterMenuHoverColor();
		$ProjectNameColor=$webtheme->getProjectNameColor();
		$ProjectAddressColor=$webtheme->getProjectAddressColor();
		$ProjectPriceColor=$webtheme->getProjectPriceColor();
		$ProjectFeatureColor=$webtheme->getProjectFeatureColor();
		
		//Update web theme record for user............
		$newtheme = new Entities\WebThemes;
		
		$newtheme=$this->doctrine->findByID('Entities\WebThemes',$oldTheme);
		$newtheme->setThemeName($ThemeName);
		$newtheme->setBodyBgColor($BodyBgColor);
		$newtheme->setSliderBgColor($SliderBgColor);
		$newtheme->setTextColor($TextColor);
		$newtheme->setTextHeaderColor($TextHeaderColor);
		$newtheme->setTopMenuColor($TopMenuColor);
		$newtheme->setTopMenuHoverColor($TopMenuHoverColor);
		$newtheme->setFooterBgColor($FooterBgColor);
		$newtheme->setFooterTextColor($FooterTextColor);
		$newtheme->setFooterMenuColor($FooterMenuColor);
		$newtheme->setFooterMenuHoverColor($FooterMenuHoverColor);
		$newtheme->setProjectNameColor($ProjectNameColor);
		$newtheme->setProjectAddressColor($ProjectAddressColor);
		$newtheme->setProjectPriceColor($ProjectPriceColor);
		$newtheme->setProjectFeatureColor($ProjectFeatureColor);
		$newtheme=$this->doctrine->save($newtheme);
		
		//Update Record for Company website
		$CompanyWeb = new Entities\CompanyWebsite;
		
		$CompanyWeb=$this->doctrine->findByID('Entities\CompanyWebsite',$fieldid);
		$CompanyWeb->setAboutUs($aboutUs);
		$CompanyWeb->setTitleMeta($titleMeta);
		$CompanyWeb->setDescriptionMeta($descriptionMeta);
		$CompanyWeb->setKeywordMeta($keywordMeta);
		
		//$CompanyWeb->setCompany($this->doctrine->findByID('Entities\CompanyDetails',1));
		$CompanyWeb=$this->doctrine->save($CompanyWeb);
	}
}