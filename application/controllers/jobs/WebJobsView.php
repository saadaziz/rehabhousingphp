<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WebJobsView extends  CI_Controller {

    public $entityName='Entities\WebJobs';
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\WebJobs';
        $this->load->helper('form');
        $this->load->library(array('form_validation','pagination'));
    }
    public function index($offset=0)
    {
        $page = 'jobs/webjobsview';
		$data['title']='Rehab Housing :: View All Jobs';
		$d=date('Y-m-d');
		$data['offset']=$offset;
        $jobs=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' order by p.jobsId desc");
		$data['AccountingFinance']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='AccountingFinance'");
		$data['HRAdmin']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='HRAdmin'");
		$data['MarketingSales']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='MarketingSales'");
		$data['DesignCreative']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='DesignCreative'");
		$data['IT']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='IT'");
		$data['Legal']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='Legal'");
		$data['CustomerSupportFontDesk']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='CustomerSupportFontDesk'");
		
		//pagination
		$data['jobs']=array_slice($jobs,$offset,5);
		$config['base_url'] =base_url()."jobs";
		$config['total_rows'] = count($jobs);
		$config['per_page'] = 5;
        $config["num_links"] = 4;
        $config['uri_segment'] = 2;
		//$config['display_pages'] = FALSE;
		$config['full_tag_open'] = '<div class="pegDiv"><ul class="pagination">';
				$config['full_tag_close'] = '</ul></div>';
				$config['cur_tag_open'] ='<li class="active"><a href="#">';
				$config['cur_tag_close']='</a></li>';
				$config['first_tag_open']='<li>';
				$config['first_tag_close']='</li>';
				$config['last_tag_close'] ='</li>';
				$config['last_tag_open'] ='<li>';
				$config['next_tag_open']='<li>';
				$config['next_tag_close']='</li>';
				$config['prev_tag_open']='<li>';
				$config['prev_tag_close']='</li>';
				$config['num_tag_open']='<li>';
				$config['num_tag_close']='</li>';
				$config['next_link']='&raquo;';
				$config['prev_link'] = '&laquo;';
		$this->pagination->initialize($config);
		//echo $this->pagination->create_links();
		
		//start templete
		$this->load->view('pages/header', $data);
		$this->load->view('pages/top_fixed_bar', $data);
		$this->load->view('pages/top_menu', $data);
		$this->load->view('pages/top_search', $data);
		$this->load->view($page, $data);
		$this->load->view('pages/footer', $data);
    }
    public function details($id)
    {
    	$page = 'jobs/webjobsdetails';
		$data['title']='Rehab Housing :: View Jobs';
		$data['jobdetails']=$this->doctrine->findByID($this->entityName,$id);
		$d=date('Y-m-d');
		//$data['AccountingFinance']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='AccountingFinance'");
		//$data['HRAdmin']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='HRAdmin'");
		//$data['MarketingSales']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='MarketingSales'");
		//$data['DesignCreative']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='DesignCreative'");
		//$data['IT']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='IT'");
		//$data['Legal']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='Legal'");
		//$data['CustomerSupportFontDesk']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='CustomerSupportFontDesk'");
		
		
		//start Templete
		$this->load->view('pages/header', $data);
		$this->load->view('pages/top_fixed_bar', $data);
		$this->load->view('pages/top_menu', $data);
		$this->load->view('pages/top_search', $data);
		$this->load->view($page, $data);
		$this->load->view('pages/footer', $data);
    }
    public function categorywise($jobcat,$offset=0){
		$d=date('Y-m-d');
    	$jobcats=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='$jobcat'");
    	//var_dump($jobcats);
    	$page = 'jobs/jobsviewCatwise';
    	$data['jobCategory']=$jobcat;
    	
		$link="job/category";
		//pagination
		$data['catjobs']=array_slice($jobcats,$offset,5);
		$this->load->library('pagination');
		$config['base_url'] = base_url().$link."/".$jobcat;
		$config['total_rows'] = count($jobcat);
		$config['full_tag_open'] = '<div class="pegDiv"><ul class="pagination">';
				$config['full_tag_close'] = '</ul></div>';
				$config['cur_tag_open'] ='<li class="active"><a href="#">';
				$config['cur_tag_close']='</a></li>';
				$config['first_tag_open']='<li>';
				$config['first_tag_close']='</li>';
				$config['last_tag_close'] ='</li>';
				$config['last_tag_open'] ='<li>';
				$config['next_tag_open']='<li>';
				$config['next_tag_close']='</li>';
				$config['prev_tag_open']='<li>';
				$config['prev_tag_close']='</li>';
				$config['num_tag_open']='<li>';
				$config['num_tag_close']='</li>';
				$config['next_link']='&raquo;';
				$config['prev_link'] = '&laquo;';
		$config['per_page'] = 5;
		$this->pagination->initialize($config);
		
		//$data['AccountingFinance']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='AccountingFinance'");
		//$data['HRAdmin']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='HRAdmin'");
		//$data['MarketingSales']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='MarketingSales'");
		//$data['DesignCreative']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='DesignCreative'");
		//$data['IT']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='IT'");
		//$data['Legal']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='Legal'");
		//$data['CustomerSupportFontDesk']=$this->doctrine->FindByQuery("select p from Entities\WebJobs p where p.lastDateOfSubmission >= '".$d."' and p.jobCatagory='CustomerSupportFontDesk'");
		
		
		
		$data['title']='Rehab Housing :: View All Jobs';
		$this->load->view('pages/header', $data);
		$this->load->view('pages/top_fixed_bar', $data);
		$this->load->view('pages/top_menu', $data);
		$this->load->view('pages/top_search', $data);
		$this->load->view($page, $data);
		$this->load->view('pages/footer', $data);
    }
}