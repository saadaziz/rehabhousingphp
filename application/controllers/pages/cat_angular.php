<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class cat_angular extends  CI_Controller {
	  public $entityName='Entities\ProductDetails';
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\ProductDetails';
       /* $this->load->helper('form');
        $this->load->library('form_validation');*/
        //$this->load->helper('getFormattedTime');
        $this->load->helper(array('getMinPrice','getFormattedTime'));
    }
	
	public function load_views($data,$products,$comingFrom)
	{
		$offset=$data['offset'];
		$data['products']=array_slice($products,$offset,10);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'category/'.$comingFrom;
		$config['total_rows'] = count($products);
		$config['per_page'] = 10;
		$config['full_tag_open'] = '<div class="pegDiv"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] ='<li class="active"><a href="#">';
		$config['cur_tag_close']='</a></li>';
		$config['first_tag_open']='<li>';
		$config['first_tag_close']='</li>';
		$config['last_tag_close'] ='</li>';
		$config['last_tag_open'] ='<li>';
		$config['next_tag_open']='<li>';
		$config['next_tag_close']='</li>';
		$config['prev_tag_open']='<li>';
		$config['prev_tag_close']='</li>';
		$config['num_tag_open']='<li>';
		$config['num_tag_close']='</li>';
		$config['next_link']='&raquo;';
		$config['prev_link'] = '&laquo;';
		$this->pagination->initialize($config);
		$this->load->view('pages/header', $data);
		$this->load->view('pages/top_fixed_bar', $data);
		$this->load->view('pages/top_menu', $data);
		$this->load->view('pages/top_search', $data);
		$this->load->view('pages/cat_angular', $data);
		$this->load->view('pages/footer', $data);
	}
	public function returnproducts($arr=FALSE)
	{
		if($arr!=FALSE)
		{
			//$arr = json_decode($arr);
			//var_dump($arr);
			$products = $this->doctrine->findCustomWithOrder('Entities\ProductDetails',array('approval'=>'approved'),array('insertTime'=>'desc'));
			foreach ($products as $key ) {
				
			}
		}
	}
	public function cat($cat,$offset =FALSE)
    {
    	if($cat!=FALSE)
		{
			$data['offset']=($offset==FALSE)?0:$offset;
			$data['bd1']=urldecode($cat);
			$data['filters']=json_encode(array("key"=>"category" ,
												"value"=>$data['bd1']));
			$data['title']='Rehabhousing.com :: '.$cat.' Posts';
			//$data['cat']= urldecode($cat);
			$category = $this->doctrine->findOneCustom('Entities\ProductTypes',array('typeName'=>$data['bd1']));
			$scat=$this->doctrine->findCustom('Entities\ProductTypes',array('parent'=>$category));
			$products=array();
			foreach ($scat as $key) 
				$products=array_merge($products,$this->doctrine->findCustomWithOrder('Entities\ProductDetails',array('type'=>$key,'approval'=>'approved'),array('insertTime'=>'desc')));
			
			$data['cat']=($products);
			$this->load_views($data,$products,$cat);
		}
	}
    public function scat($cat,$scat,$offset =FALSE)
    {
    	if($cat!=FALSE)
		{
			$data['offset']=($offset==FALSE)?0:$offset;
			$data['bd1']=urldecode($cat);
			$data['bd2']=urldecode($scat);
			$arr =array();
			array_merge($arr,array("key"=>"category","value"=>$data['bd1']));
			array_merge($arr,array("key"=>"subcategory","value"=>$data['bd2']));
			$data['filters']=json_encode($arr);
			$data['title']='Rehabhousing.com :: '.$scat.' Posts';
			$category = $this->doctrine->findOneCustom('Entities\ProductTypes',array('typeName'=>urldecode($scat)));
			$products=$this->doctrine->findCustomWithOrder('Entities\ProductDetails',array('type'=>$category),array('insertTime'=>'desc'));
			$this->load_views($data,$products,$cat.'/subcategory/'.$scat);
		}
	}
}