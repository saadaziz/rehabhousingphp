<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */


class category extends  CI_Controller {
	  public $entityName='Entities\ProductDetails';
      public $glproducts;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\ProductDetails';
       /* $this->load->helper('form');
        $this->load->library('form_validation');*/
        $this->load->helper(array('getMinPrice','getFormattedTime','form'));
        //$this->load->driver('cache');
       // $this->glproducts=array();

    }
	public function getSingleImage ($pid=FALSE)
    {

        if($pid!=FALSE)
        {
            $product = $this->doctrine->findByID('Entities\ProductDetails',$pid);
            //echo $product->getTitle();
            $img = $this->doctrine->findOneCustom('Entities\ImageLink',array('product'=>$product));
            return ($img!=null)?$img->getImageWatermark():"#";
        }
    }
    public function loadcities($id =FALSE)
    {
        if($id!=FALSE)
        {
            $div = $this->doctrine->findByID('Entities\LocationDivision',$id);
            $cities = array_slice($this->doctrine->findCustomWithOrder('Entities\LocationZones',array('division'=>$div),array('amount'=>'desc')),0,10);
            $html='';
            foreach($cities as $key)
            {
                $html.=' <li><a class="districtAlter" key="zone" href="#zone:'.urlencode($key->getNameEn()).'">'.$key->getNameEn().'<span class="rightDgt">('.$key->getAmount().')</span></a></li>';
            }
            print $html;
        }
    }
    public function loadfiltereddata()
    {

        $data='';

        //echo $qb->getType();
        $filter = json_decode( $this->input->post('arr'),true);

        //foreach()
        //print count($products);
        $products = ($this->getConditions($filter));
        print $this->getHTMML(array_slice($products,0,10));
        //print $this->getConditions($filter);
    }
    public function getConditions($filter)
    {
        $query="select p from Entities\\ProductDetails p where ";
        $em = $this->doctrine->getEM();
        $qb=$em->createQueryBuilder();
        $initflg = 0;
        $orflag=array();
        $qb->select('p')->from('Entities\ProductDetails', 'p');

        for($i=0;$i<count($filter);$i++)
        {
            switch($filter[$i]['key']){
                case "subcategory":
                    $type=$this->doctrine->findOneCustom('Entities\ProductTypes',array('typeName' =>$filter[$i]['value']));
                    $query.=(($initflg==1)?' and ':(isset($orflag['subcategory']))?' or ': ' '). ' p.type='.$type->getProductTypesId();
                    if($initflg==1)
                        $qb->andWhere('p.type = :tp')->setParameter('tp',$type);
                    else{
                       /* if(isset($orflag['subcategory']))
                            $qb->orWhere('p.type = :tp')->setParameter('tp',$type);
                        else*/
                            $qb->Where('p.type = :tp')->setParameter('tp',$type);
                    }
                    $initflg=1;
                    $orflag['subcategory']=1;
                    break;
                case "district":
                    $div=$this->doctrine->findOneCustom('Entities\LocationDivision',array('divisionEn' =>$filter[$i]['value']));
                    //$query.=$filter[$i]['value'];
                    $query.=(isset($orflag['district'])?' or ':(($initflg==1)?' and ' : ' ')). ' p.division='.$div->getDivisionId();
                    /*if(isset($orflag['district']))
                        $qb->orWhere('p.division = :div')->setParameter('div',$div);
                    else{*/
                        if($initflg==1)
                            $qb->andWhere('p.division = :div')->setParameter('div',$div);
                        else
                            $qb->Where('p.division = :div')->setParameter('div',$div);
                   // }
                    $initflg=1;
                    $orflag['district']=1;
                    break;
                case "zone":
                    $div=$this->doctrine->findOneCustom('Entities\LocationZones',array('nameEn' =>$filter[$i]['value']));
                    //$query.=$filter[$i]['value'];
                    $query.=(isset($orflag['zone'])?' or ':(($initflg==1)?' and ' : ' ')). ' p.zone='.$div->getLocId();
                    /*if(isset($orflag['zone']))
                        $qb->orWhere('p.zone = :zone')->setParameter('zone',$div);
                    else{*/
                        if($initflg==1)
                            $qb->andWhere('p.zone = :zone')->setParameter('zone',$div);
                        else
                            $qb->Where('p.zone = :zone')->setParameter('zone',$div);
                    //}
                    $initflg=1;
                    $orflag['zone']=1;
                    break;
                case "size":
                    if($filter[$i]['value']=='More3000')
                    {
                        $min=3001;$max=20000;
                    }
                    else if($filter[$i]['value']!='Less500')
                    {
                        $tmp = explode('-',$filter[$i]['value']);
                        $min=$tmp[0];$max=$tmp[1];
                    }
                    $query.=(isset($orflag['size'])?' or ':(($initflg==1)?' and ' : ' ')). ' p.size='.$filter[$i]['value'].$min.$max;

                    $tmpunits = $this->doctrine->FindByQuery("select p from Entities\ProductUnitDetails p where p.size between $min and $max");
                    $intarr= array();
                    foreach($tmpunits as $key)
                    {
                        array_push($intarr,$key->getProduct()->getProductId());
                    }
                    // var_dump($intarr);

                    if($initflg==1)
                        $qb->andWhere($qb->expr()->in('p.productId','?1'))
                            ->setParameter(1,array_unique($intarr));
                    else
                        $qb->Where($qb->expr()->in('p.productId','?1'))
                            ->setParameter(1,array_unique($intarr));
                    $initflg=1;
                    $orflag['size']=1;
                    break;
                case "bed":
                    $bedroom=5;$query='';
                    $val=$filter[$i]['value'];
                    $tmpunits = $this->doctrine->FindByQuery(($filter[$i]['value']=='5+')?"select p from Entities\ProductUnitDetails p where p.bedroom >5":
                        "select p from Entities\ProductUnitDetails p where p.bedroom = $val");
                    $intarr= array();
                    foreach($tmpunits as $key)
                    {
                        array_push($intarr,$key->getProduct()->getProductId());
                    }
                    // var_dump($intarr);

                    if($initflg==1)
                        $qb->andWhere($qb->expr()->in('p.productId','?1'))
                            ->setParameter(1,array_unique($intarr));
                    else
                        $qb->Where($qb->expr()->in('p.productId','?1'))
                            ->setParameter(1, array_unique($intarr));
                    $initflg=1;
                    $orflag['size']=1;
                    break;
                default:
                    break;

            }
        }
        $qb->orderBy('p.insertTime', 'desc');
        $qry=$qb->getQuery();
        //$products=array();
        //$qry = $em->createQuery($query);
        //return $query;
        return $qry->getResult();
    }
	public function load_views($data,$products,$comingFrom)
	{
		$offset=$data['offset'];
		//$data['products']=array_slice($products,$offset,10);
        $data['cats']=array_slice($this->doctrine->findCustomWithOrder('Entities\ProductTypes',array('parent'=>null),array('viewOrder'=>'asc')),0,2);
        $cat = $this->doctrine->findOneCustom('Entities\ProductTypes',array('typeName'=>$data['bd1']));
        $data['subcats']=$this->doctrine->findCustomWithOrder('Entities\ProductTypes',array('parent'=>$cat),array('viewOrder'=>'asc'));
        $data['diva']=$this->getHTMML(array_slice($products,$offset,10));
        $data['districts']=$this->doctrine->findCustomWithOrder('Entities\LocationDivision',array(),array('viewOrder'=>'asc'));

         $data['areas']=array_slice( $this->doctrine->findCustomWithOrder('Entities\LocationZones',array(),array('amount'=>'desc')),0,12);
		$this->load->view('pages/header', $data);
		$this->load->view('pages/top_fixed_bar', $data);
		$this->load->view('pages/top_menu', $data);
		$this->load->view('pages/top_search', $data);
		$this->load->view('pages/category_container', $data);
		$this->load->view('pages/footer', $data);
	}
    public function size($cat,$size)
    {
        $data['offset']=0;
        $data['bd1']=urldecode($cat);
        $data['bd2']=urldecode($size);
        $data['filters']=json_encode(array(array("key"=>"category" ,
            "value"=>$data['bd1']),
            array("key"=>"size" ,
                "value"=>$data['bd2'])));
        $this->session->set_userdata('catfilter',array(array("key"=>"category" ,
            "value"=>$data['bd1']),
            array("key"=>"size" ,
                "value"=>$data['bd2'])));
        $data['filterlastindex']='1';
        $this->session->set_userdata('filterindex','1');
        $data['title']=$data['bd1'].' Posts in Rehabhousing.com';

        $min=0;$max=500;
        $em = $this->doctrine->getEM();

        $qb2=$em->createQueryBuilder();

        if($size=='More3000')
        {
            $min=3001;$max=20000;
        }
        else if($size!='Less500')
        {
            $tmp = explode('-',$size);
            $min=$tmp[0];$max=$tmp[1];
        }

        $tmpunits = $this->doctrine->FindByQuery("select p from Entities\ProductUnitDetails p where p.size between $min and $max");
        $intarr= array();
        foreach($tmpunits as $key)
        {
            array_push($intarr,$key->getProduct()->getProductId());
        }
       // var_dump($intarr);
        $qb->select('p')->from('Entities\ProductDetails', 'p')
            ->where($qb->expr()->in('p.productId','?1'))
            ->orderBy('p.insertTime', 'desc')
            ->setParameter(1,$intarr);
        $qry=$qb->getQuery();
        $products=$qry->getResult();
        $this->load_views($data,$products,$cat);
    }
	public function cat($cat,$offset =FALSE)
    {
    	if($cat!=FALSE)
		{
			$data['offset']=($offset==FALSE)?0:$offset;
			$data['bd1']=urldecode($cat);
			$data['filters']=json_encode(array(array("key"=>"category" ,
									"value"=>$data['bd1'])));
            $this->session->set_userdata('catfilter',array(array("key"=>"category" ,
                "value"=>$data['bd1'])));
			$data['filterlastindex']='0';
            $this->session->set_userdata('filterindex','0');
			$data['title']=$data['bd1'].' Posts in Rehabhousing.com';
			//$data['cat']= urldecode($cat);
			$category = $this->doctrine->findOneCustom('Entities\ProductTypes',array('typeName'=>$data['bd1']));
			$scat=$this->doctrine->findCustom('Entities\ProductTypes',array('parent'=>$category));
			$products=array();
			foreach ($scat as $key) 
				$products=array_merge($products,$this->doctrine->findCustomWithOrder('Entities\ProductDetails',array('type'=>$key,'approval'=>'approved'),array('insertTime'=>'desc')));
			$data['cat']=($products);
            $this->glproducts=$products;
            //$this->cache->memcached->save('products',$products,1200);
			$this->load_views($data,$products,$cat);
		}
	}
    public function scat($cat,$scat,$offset =FALSE)
    {
    	if($cat!=FALSE)
		{
			$data['offset']=($offset==FALSE)?0:$offset;
			$data['bd1']=urldecode($cat);
            $data['bd2']=(urldecode($scat)=='Apartment-Flat')?'Apartment/Flat':urldecode($scat);
			/*$arr =array();
			array_push($arr,array("key"=>"category","value"=>$data['bd1']));
			array_merge($arr,array("key"=>"subcategory","value"=>$data['bd2']));*/
			$data['filters']=json_encode(array(array("key"=>"category" ,
												"value"=>$data['bd1']),
												array("key"=>"subcategory" ,
												"value"=>$data['bd2'])));
            $this->session->set_userdata('catfilter',array(array("key"=>"category" ,
                "value"=>$data['bd1']),
                array("key"=>"subcategory" ,
                    "value"=>$data['bd2'])));
			$data['filterlastindex']='1';
            $this->session->set_userdata('filterindex','1');
			$data['title']='Rehabhousing.com :: '.$scat.' Posts';
			$category = $this->doctrine->findOneCustom('Entities\ProductTypes',array('typeName'=>$data['bd2']));
			$products=$this->doctrine->findCustomWithOrder('Entities\ProductDetails',array('type'=>$category,'approval'=>'approved'),array('insertTime'=>'desc'));

            $this->glproducts=$products;
            //$this->cache->memcached->save('products',json_encode($products_str),1200);
            $this->load_views($data,$products,$cat.'/subcategory/'.$scat);
		}
	}
    public function getmore($offset=FLASE)
    {//to get more data on mousescroll
        if($offset!=FAlSE)
        {
            /*$tmp=$this->glproducts;
            var_dump($tmp) ;*/
            $retpr =array();
            //echo '<pre>'.print_r ($retpr).'</pre>';
            /*for($i=0;$i<=$this->session->userdata['filterindex'];$i++)
            {*/
                //$qry=array_merge($qry,array($this->session->userdata['catfilter'][$i]['key']=>$this->session->userdata['catfilter'][$i]['value']));
            $i=$this->session->userdata['filterindex'];
                switch($this->session->userdata['catfilter'][$i]['key']){
                    case "category":
                        $category = $this->doctrine->findOneCustom('Entities\ProductTypes',array('typeName'=>$this->session->userdata['catfilter'][$i]['value']));
                        $scat=$this->doctrine->findCustom('Entities\ProductTypes',array('parent'=>$category));
                        $products=array();
                        foreach ($scat as $key)
                            $retpr=array_merge($products,$this->doctrine->findCustomWithOrder('Entities\ProductDetails',array('type'=>$key,'approval'=>'approved'),array('insertTime'=>'desc')));
                        $retpr = array_slice($retpr,$offset,8);
                        break;
                    case "subcategory":
                        $var=$this->session->userdata['catfilter'][$i]['value'];
                        $category = $this->doctrine->findOneCustom('Entities\ProductTypes',array('typeName'=>"$var"));
                        $retpr=array_slice($products=$this->doctrine->findCustomWithOrder('Entities\ProductDetails',array('type'=>$category,'approval'=>'approved'),array('insertTime'=>'desc'))
                            ,$offset,8);
                        //print_r($offset);
                        //echo $this->session->userdata['catfilter'][$i]['value'];
                        break;
                    default:
                        $category = $this->doctrine->findOneCustom('Entities\ProductTypes',array('typeName'=>'Sale'));
                        $scat=$this->doctrine->findCustom('Entities\ProductTypes',array('parent'=>$category));
                        $products=array();
                        foreach ($scat as $key)
                            $retpr=array_merge($products,$this->doctrine->findCustomWithOrder('Entities\ProductDetails',array('type'=>$key,'approval'=>'approved'),array('insertTime'=>'desc')));
                        $retpr = array_slice($retpr,$offset,8);
                        break;

                }

           // }
            $html=$this->getHTMML($retpr);
            print $html;
            //print_r(count($retpr));
            //if($this->session->userdata['catfilter'][0]['key'])
        }
    }
    public function getHTMML($retpr)
    {
        $html='';
        foreach($retpr as $key)
        {
            $html.='<div class="box effect4"><div class="por_whtBox"><img width="90" src="'.$this->getSingleImage($key->getProductId()).'"> </div><div class="por_ListText">'.
                '<ul><a href="'.base_url().'details/'.$key->getProductId().'/'.urlencode($key->getTitle()).'"><li class="por_ListHeading">'.$key->getTitle().'<span class="today">'.
                '<b class="bdt">BDT '.$this->getmin($key->getPrice()).'</b> (Per )<br><img width="100" src=""><br>'.
                $key->getInsertTime()->format('d,F Y').'</span></li><li>'.$key->getType()->getTypeName().' for '.$key->getType()->getParent()->getTypeName().
                ' at'.$key->getDivision()->getDivisionEn().'</li><li>'.$key->getSize().'</li><li>'.(($key->getBedroom()!='')?$key->getBedroom().' Bed': '').'</li>'.
                '<li class="porBy">Project By: '.$key->getUser()->getName().'  I  '.(($key->getUser()->getPremium()=='yes')?'Premium Member ':'Member ').
                '<span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved</li></ul></div></div>';
        }
        return $html;
    }
    public function getmin($string){
        $arr = explode('/', $string);
        return min($arr);
    }
}