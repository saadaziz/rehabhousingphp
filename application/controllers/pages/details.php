<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class details extends  CI_Controller {
	  public $entityName='Entities\ProductDetails';
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\ProductDetails';
       /* $this->load->helper('form');
        $this->load->library('form_validation');*/
    }
	public function getimg($ky)
	{
		$img=$this->doctrine->findOneCustom('Entities\ImageLink',array('product'=>$ky));
		if(!is_null($img))
	    	return $img->getImageWatermark();
	    else
			return base_url().'assets/img/default.png';
	}
	public function getsimilar($pid=FALSE)
	{
		if($pid!=FALSE)
		{
			$product= $this->doctrine->findByID('Entities\ProductDetails',$pid);
			$type=$product->getType();
			$var=$this->doctrine->findCustom('Entities\ProductDetails',array('type'=>$type),array('insertTime'=>'desc'));
			$data['var']=$var;
			$user_projects=array_rand($var, 8) ;
			$html='';
			foreach($user_projects as $key)
			{
				$html.='<li><div class="projectRowbootom">'
                                       .' <div class="projectColbottom">'
                                          . ' <div class="imgBox">'
                          .'<img src='.$this->getimg($var[$key]).' width="120" height="125"></div>'
                                            .'<div class="proName">'.$var[$key]->getTitle().'</div>'
                                            .'<div class="proLoc">'.$var[$key]->getZone()->getNameEn().', '.$var[$key]->getDivision()->getDivisionEn().'</div>'
                                            .'<div class="proPrice">BDT '.$var[$key]->getPrice().'/Sft</div>'
                                            .'<div class="proRoome">'.$var[$key]->getbedroom().' Bed</div>'
                                           .'<a class="details btn btn-default">Details</a>'
                                           .' </div></div></li>';
			}
			print json_encode(array('htmls'=>$html,
							'here'=>'1'));
		}
	}
	public function getmorefrom($userid=FALSE)
	{
		if($userid!=FALSE)
		{
			$var=$this->doctrine->findCustom('Entities\ProductDetails',array('topProject'=>'yes'),array('insertTime'=>'desc'));
			$data['var']=$var;
			$user_projects=array_rand($var, 4) ;
			$html='';
			foreach($user_projects as $key)
			{
				$html.='<div class="newsTopPro"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="topproName" colspan="2">'.$var[$key]->getTitle()
					.'</td></tr><tr><td class="catLeftTopImg"><img src='.$this->getimg($var[$key]).' width="80" height="70"></td><td class="topPorText">'
                    .$var[$key]->getZone()->getNameEn() .'<br />'
	                 .$var[$key]->getDivision()->getDivisionEn() .'<br />'
	                 .(($var[$key]->getBedroom()=='')?$var[$key]->getSize().' Katha':$var[$key]->getBedroom().' Bed').'<br /><b>'
	                  .$var[$key]->getTopProjectPrice().'</b></td></tr></table></div>';
			}
			print json_encode(array('htmls'=>$html,
							'here'=>'1'));
		}
	}
    public function index($id=FALSE,$title=FALSE)
    {
    	if($id!=FALSE)
		{
			$product = new Entities\ProductDetails;
			$product = $this->doctrine->findByID($this->entityName,$id);
			$data['title']='Rehabhousing.com :: '.$product->getTitle();
			$data['product']=$product;
			$data['from']=$product->getUser()->getUserId();
			$data['pdetailsmap']=$this->doctrine->findCustom('Entities\ProductFieldDetailsMap',array('product'=>$id));
			if($product->getType()->getProductTypesId()==6)
				$data['unitdetails']=$this->doctrine->findCustom('Entities\ProductUnitDetails',array('product'=>$id));
			$var=$this->doctrine->findCustom('Entities\ProductDetails',array('topProject'=>'yes'),array('insertTime'=>'desc'));
			$data['var']=$var;
			$top_projects=array_rand($var, 4) ;
			$data['top_projects']=$top_projects;
			$this->load->view('pages/header', $data);
			$this->load->view('pages/top_fixed_bar', $data);
			$this->load->view('pages/top_menu', $data);
			$this->load->view('pages/top_search', $data);
			$this->load->view('pages/details_container', $data);
			$this->load->view('pages/footer', $data);
		}
	}
}