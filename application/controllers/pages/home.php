<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class home extends  CI_Controller {
	  public $entityName='Entities\ProductTypes';
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $entityName='Entities\ProductTypes';
       /* $this->load->helper('form');
        $this->load->library('form_validation');*/
        
		$this->load->library('session');
    }
	public function locprices($typeid='6',$divid='8')
	{		//getting locations for price 
		$locdiv= $this->doctrine->findByID('Entities\LocationDivision',$divid);
		$aptsales= $this->doctrine->findByID('Entities\ProductTypes',$typeid);
		$loczones=array_slice($this->doctrine->findCustomWithOrder('Entities\LocationZones',array('division'=>$locdiv),array('amount'=>'desc')),0,20);
		$locprices=array();
		foreach ($loczones as $key) {
			$tmpo =$this->doctrine->findOneCustom('Entities\PriceLocationMap',array('location'=>$key,'type'=>$aptsales));
			if(!is_null($tmpo))array_push($locprices,$tmpo);
		}	
		$lochtmls=' <tbody><tr style="border-bottom: solid 2px #ccc;"><td><b>Location1</b></td><td style="text-align: right;"><b>Approx Price (Sft)</b></td></tr><tr><td colspan="2">&nbsp;</td></tr>';
		
		foreach (array_slice($locprices, 0,12) as $key2 ) {
			$lochtmls.=' <tr><td style="color: #454647" class="listHover"><a href="">'.$key2->getLocation()->getNameEn().'</a></td>'.
	                           ' <td style="text-align: right;" class="listHover"><a href="">'.$key2->getLowPrice().'-'.$key2->getHighPrice().' (Sft)</a></td></tr>';	     
		}
		print_r(json_encode(array('htmls'=> $lochtmls.'</tbody>','here'=>'1')));
	}
    public function index()
    {

		$data['title']='Rehabhousing.com';
		$data['SaleCats'] = $this->doctrine->findCustomWithOrder('Entities\ProductTypes',array('parent'=>'1'),array('viewOrder'=>'asc'));
		$data['RentCats'] = $this->doctrine->findCustomWithOrder('Entities\ProductTypes',array('parent'=>'2'),array('viewOrder'=>'asc'));
		$data['popdiv']=$this->doctrine->findCustomWithOrder('Entities\LocationDivision',array(),array('viewOrder'=>'asc'));

		$products=$this->doctrine->findCustom('Entities\ProductDetails',array('approval'=>'approved'),array('insertTime'=>'desc'));
		$data['activeads'] = array_slice($products,0,5);
		$data['ad1'] = array_slice($products,5,5);
		$data['ad2'] = array_slice($products,10,5);
		$top_projects= $this->doctrine->findCustom('Entities\ProductDetails',array('topProject'=>'yes'),array('insertTime'=>'desc'));
		$data['top_projects']=$top_projects;
		$data['row_count']=(intval(count($top_projects)/3))+((count($top_projects)%3==0)?0:1);
		$data['realEstateJobs'] =array_slice( $this->doctrine->findCustomWithOrder('Entities\WebJobs',array(),array('insertTime'=>'desc')),0,7);
		$this->load->view('pages/header', $data);
		$this->load->view('pages/top_fixed_bar', $data);
		$this->load->view('pages/top_menu', $data);
		$this->load->view('pages/top_search', $data);
		$this->load->view('pages/home_container', $data);
		$this->load->view('pages/footer', $data);
    }
    public function getNews()
    {
    	$i=0;
        $html='';
        $var=array_slice($this->doctrine->findCustomWithOrder('Entities\WebNews',array(),array('insertTime'=>'desc')), 0,3);
        foreach($var as $key)
        {
            $html.='<div class="news">'.
                            '<b><u>'.$key->getNewsTitle().'</u></b><br>'.
                             $key->getShortText().
                             '<a href="">Continue Reading..</a></div>';
			$i++;
			$html.=($i<3)?'<hr>':'';
        }
		print json_encode(array('htmls'=>$html,
							'here'=>'1'));

    }
}