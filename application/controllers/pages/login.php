<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class login extends  CI_Controller {
    static public  $redirectUrl='/';
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
	public function checklogin()
	{
		//echo $this->input->post('pass');
		$user = $this->doctrine->findOneCustom('Entities\MemberDetails',array('email'=>strtolower($this->input->post('email'))));
		if(is_null($user))
			print json_encode(array('stat'=>'0',
			'error'=>'User email Not Found.',
			'successmsg'=>''));
		else{
			if($user->getPassword()==($this->input->post('pass'))){
				$arr=array('userid'=>$user->getUserId(),
					'username'=>$user->getName(),
					'companyid'=>(!is_null( $user->getCompany())?$user->getCompany()->getCompanyId():0),
					'login_time'=>date_format(new DateTime(), 'Y-m-d_H-i-s'));
				$this->session->set_userdata('user',$arr);
				$cookie = array(
					    'name'   => 'user',
					    'value'  => json_encode($arr),
					    'expire' => '86500'	);
				$this->input->set_cookie($cookie);
				print json_encode(array('stat'=>'1',
						'error'=>'',
						'successmsg'=>'Successfully logged in'));
			}
			else
				print json_encode(array('stat'=>'0',
						'error'=>'Password doesn\'t match. Please try again.',
						'successmsg'=>''));
		}
	}
public function getmsg($val)
{
	if($val=='addpost')	
	{
		$this->redirectUrl=base_url().'/AddPost';
		return 'Please login/register before posting add.';
	}
	else if($val=='editpost')
	{
		$this->redirectUrl=base_url().'/AddPost';
		return 'Please login/register before editing.';
	}
}
    public function index($val=FALSE)
    {
    	$data['loginmsg']=($val==FALSE)?'':$this->getmsg($val);
    	$this->session->sess_destroy();
		delete_cookie("user");
		$data['redirecturl']=($this->redirectUrl!='')?$this->redirectUrl:'';
		$data['title']='Login here in rehabhousing.com';
		$this->load->view('pages/header', $data);
		$this->load->view('pages/top_fixed_bar', $data);
		$this->load->view('pages/top_menu', $data);
		$this->load->view('pages/top_search', $data);
		$this->load->view('pages/login_container', $data);
		$this->load->view('pages/footer', $data);
		
	}
}