<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Shad
 * Date: 12/1/13
 * Time: 5:07 PM
 */

class postreq extends  CI_Controller {
   // static public  $data=null;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        //$this->load->library('form_validation');
    }
	
    public function index()
    {
    	$inint = $this->doctrine->findByID('Entities\ProductTypes',3);
    	$data['cats']=$this->doctrine->findCustomWithOrder('Entities\ProductTypes',array('parent'=>$inint),array('viewOrder'=>'asc'));
		$data['city']=$this->doctrine->findCustomWithOrder('Entities\LocationDivision',array(),array('viewOrder'=>'asc'));
		$data['title']='Rehabhousing.com :: Post your requirement';
		$this->load->view('pages/header', $data);
		$this->load->view('pages/top_fixed_bar', $data);
		$this->load->view('pages/top_menu', $data);
		$this->load->view('pages/top_search', $data);
		$this->load->view('pages/post_req_container', $data);
		$this->load->view('pages/footer', $data);
		
	}
}