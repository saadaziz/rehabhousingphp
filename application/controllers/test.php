<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class test extends CI_Controller {
	var $name,$color;
	
	public function __construct()
	{
		parent::__construct();
		$this->name='shad';
		$this->color='red';
		$this->load->model('news_model');
	}
	function dbview($slg=FALSE)
	{
		//parent::__construct();
		if($slg===FALSE)
			$data['news'] = $this->news_model->get_news();
		$data['news'] = $this->news_model->get_news($slg);
		$this->load->view('db_view',$data);
	}
	/*function you(){
		
		$data['name']=$this->name;
		$data['color']=$this->color;
		$this->load->view('testing',$data);
	}*/
	function view($num='')
	{
		$page = 'testing';
		
		if ( ! file_exists('application/views/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		$data['name']=$this->name;
		$data['color']=$this->color;
		$data['num']=$num;
		//$this->load->view('templates/header', $data);
		$this->load->view($page, $data);
		//$this->load->view('templates/footer', $data);
	
	}
}