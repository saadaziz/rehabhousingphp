<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class test_angular extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $this->load->view('pages/test_angular');

    }
    public function getmin($string){
        $arr = explode('/', $string);
        return min($arr);
    }
    public function get_products()
    {
        $arr=array();
        $products=$this->doctrine->findCustomWithOrder('Entities\ProductDetails',array('approval'=>'approved'),array('insertTime'=>'desc'));
        foreach($products as $key)
        {
            array_push($arr,array("id"=>$key->getProductId(),
                                    "title"=>$key->getTitle(),
                                    "cat"=>$key->getType()->getParent()->getTypeName(),
                                    "subcat"=>$key->getType()->getTypeName(),
                                    "insertTime"=>$key->getInsertTime()->format('d, F Y'),
                                    "price"=>$this->getmin($key->getPrice()),
                                    "size"=>$this->getmin($key->getSize())));
        }
        print json_encode($arr);
    }
}