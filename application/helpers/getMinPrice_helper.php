<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('getmin'))
{
    function getmin($string){
		$arr = explode('/', $string);
		return min($arr);
	}
}