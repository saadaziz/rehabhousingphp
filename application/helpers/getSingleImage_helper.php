<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: shad-pc
 * Date: 1/6/14
 * Time: 2:04 PM
 */

if ( ! function_exists('getsingle'))
{
    function getsingle($pid){
        $CI =&get_instance();
        //$CI->load->library('doctrine');
        $product = $CI->doctrine->findByID('Entities\ProductDetails',$pid);
        $img = $CI->doctrine->findOneCustom('Entities\ImageLink',array('product'=>$product));
        return $img->getImageWatermark();
    }
}