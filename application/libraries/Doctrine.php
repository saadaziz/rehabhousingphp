<?php

class Doctrine
{
    // the Doctrine entity manager
    public $em = null;

    public function __construct()
    {
        // include our CodeIgniter application's database configuration
        require_once APPPATH.'config/database.php';

        // include Doctrine's fancy ClassLoader class
        require_once APPPATH.'libraries/Doctrine/Common/ClassLoader.php';

        // load the Doctrine classes
        $doctrineClassLoader = new \Doctrine\Common\ClassLoader('Doctrine', APPPATH.'libraries');
        $doctrineClassLoader->register();

        // load Symfony2 helpers
        // Don't be alarmed, this is necessary for YAML mapping files
        $symfonyClassLoader = new \Doctrine\Common\ClassLoader('Symfony', APPPATH.'libraries/Doctrine');
        $symfonyClassLoader->register();

        // load the entities
        $entityClassLoader = new \Doctrine\Common\ClassLoader('Entities', APPPATH.'models');
        $entityClassLoader->register();

        // load the proxy entities
        $proxyClassLoader = new \Doctrine\Common\ClassLoader('Proxies', APPPATH.'models');
        $proxyClassLoader->register();

        // set up the configuration 
        $config = new \Doctrine\ORM\Configuration;

        if(ENVIRONMENT == 'development')
            // set up simple array caching for development mode
            $cache = new \Doctrine\Common\Cache\ArrayCache;
        /*else
            // set up caching with APC for production mode
            $cache = new \Doctrine\Common\Cache\ApcCache;
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);*/

        // set up proxy configuration
        $config->setProxyDir(APPPATH.'models/Proxies');
        $config->setProxyNamespace('Proxies');

        // auto-generate proxy classes if we are in development mode
        $config->setAutoGenerateProxyClasses(ENVIRONMENT == 'development');

        // set up annotation driver
        $yamlDriver = new \Doctrine\ORM\Mapping\Driver\YamlDriver(APPPATH.'models/Mappings');
        $config->setMetadataDriverImpl($yamlDriver);

        // Database connection information
        $connectionOptions = array(
            'driver' => 'pdo_mysql',
            'user' => $db['default']['username'],
            'password' => $db['default']['password'],
            'host' => $db['default']['hostname'],
            'dbname' => $db['default']['database']
        );

        // create the EntityManager
        $em = \Doctrine\ORM\EntityManager::create($connectionOptions, $config);

        // store it as a member, for use in our CodeIgniter controllers.
        $this->em = $em;
    }
    public function getquerybuilder()
    {
        return $this->em->createQueryBuilder();
    }
    public function getEM()
    {
        return $this->em;
    }
    public function save($u)
    {
        $this->em->persist($u);
        $this->em->flush();
        return $u;
    }
    public function remove($u)
    {
        $this->em->remove($u);
        $this->em->flush();
        //return $u;
    }
    public function findByID($entityname,$id)
    {
        return $this->em->find($entityname,$id);
    }
    public function findAll($entityname)
    {
        return $this->em->getRepository($entityname)->findAll();
    }
    public function findOneByFieldName($entityname,$fieldname,$fieldValue)
    {
        return $this->em->getRepository($entityname)->findOneBy(array($fieldname => $fieldValue));

    }
	    public function findOneCustom($entityname,$array)
    {
        return $this->em->getRepository($entityname)->findOneBy($array);

    }
    public function findCustom($entityname,$array)
    {
        return $this->em->getRepository($entityname)->findBy($array);
        /*$entitySerializer=new \Doctrine\EntitySerializer($this->em);
        return $$entitySerializer->toArray($patientProfile);*/

    }
	public function findCustomWithOrder($entityname,$array,$array2)
    {
        return $this->em->getRepository($entityname)->findBy($array,$array2);
        /*$entitySerializer=new \Doctrine\EntitySerializer($this->em);
        return $$entitySerializer->toArray($patientProfile);*/

    }
	public function FindByQuery($query)
    {
        $q= $this->em->createQuery($query);
        return $q->getResult();
        /*$entitySerializer=new \Doctrine\EntitySerializer($this->em);
        return $$entitySerializer->toArray($patientProfile);*/

    }
	/**
	 * $entityname=entityname
	 * $id=which parameter to count
	 * $where= where query
	 */
	public function getCount($entityname,$id,$where)
	{
		$query = $this->em->createQueryBuilder()
		->select('Count(q.'.$id.')')
		->from($entityname, 'q')
		->where($where)
		->getQuery();
		return $query->getSingleScalarResult();
		//return em;
	}
}

