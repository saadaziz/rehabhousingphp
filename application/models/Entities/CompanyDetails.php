<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\CompanyDetails
 */
class CompanyDetails
{
    /**
     * @var integer $companyId
     */
    private $companyId;

    /**
     * @var string $address
     */
    private $address;

    /**
     * @var string $rehab
     */
    private $rehab;

    /**
     * @var string $website
     */
    private $website;

    /**
     * @var string $phoneNo
     */
    private $phoneNo;

    /**
     * @var string $blda
     */
    private $blda;

    /**
     * @var string $contactPerson
     */
    private $contactPerson;

    /**
     * @var string $designation
     */
    private $designation;


    /**
     * Get companyId
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return CompanyDetails
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set rehab
     *
     * @param string $rehab
     * @return CompanyDetails
     */
    public function setRehab($rehab)
    {
        $this->rehab = $rehab;
        return $this;
    }

    /**
     * Get rehab
     *
     * @return string 
     */
    public function getRehab()
    {
        return $this->rehab;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return CompanyDetails
     */
    public function setWebsite($website)
    {
        $this->website = $website;
        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set phoneNo
     *
     * @param string $phoneNo
     * @return CompanyDetails
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
        return $this;
    }

    /**
     * Get phoneNo
     *
     * @return string 
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * Set blda
     *
     * @param string $blda
     * @return CompanyDetails
     */
    public function setBlda($blda)
    {
        $this->blda = $blda;
        return $this;
    }

    /**
     * Get blda
     *
     * @return string 
     */
    public function getBlda()
    {
        return $this->blda;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     * @return CompanyDetails
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;
        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string 
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set designation
     *
     * @param string $designation
     * @return CompanyDetails
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
        return $this;
    }

    /**
     * Get designation
     *
     * @return string 
     */
    public function getDesignation()
    {
        return $this->designation;
    }
}