<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\CompanyWebsite
 */
class CompanyWebsite
{
    /**
     * @var integer $cWebsiteId
     */
    private $cWebsiteId;

    /**
     * @var text $aboutUs
     */
    private $aboutUs;

    /**
     * @var string $titleMeta
     */
    private $titleMeta;

    /**
     * @var string $descriptionMeta
     */
    private $descriptionMeta;

    /**
     * @var string $keywordMeta
     */
    private $keywordMeta;

    /**
     * @var Entities\CompanyDetails
     */
    private $company;

    /**
     * @var Entities\WebThemes
     */
    private $theme;


    /**
     * Get cWebsiteId
     *
     * @return integer 
     */
    public function getCWebsiteId()
    {
        return $this->cWebsiteId;
    }

    /**
     * Set aboutUs
     *
     * @param text $aboutUs
     * @return CompanyWebsite
     */
    public function setAboutUs($aboutUs)
    {
        $this->aboutUs = $aboutUs;
        return $this;
    }

    /**
     * Get aboutUs
     *
     * @return text 
     */
    public function getAboutUs()
    {
        return $this->aboutUs;
    }

    /**
     * Set titleMeta
     *
     * @param string $titleMeta
     * @return CompanyWebsite
     */
    public function setTitleMeta($titleMeta)
    {
        $this->titleMeta = $titleMeta;
        return $this;
    }

    /**
     * Get titleMeta
     *
     * @return string 
     */
    public function getTitleMeta()
    {
        return $this->titleMeta;
    }

    /**
     * Set descriptionMeta
     *
     * @param string $descriptionMeta
     * @return CompanyWebsite
     */
    public function setDescriptionMeta($descriptionMeta)
    {
        $this->descriptionMeta = $descriptionMeta;
        return $this;
    }

    /**
     * Get descriptionMeta
     *
     * @return string 
     */
    public function getDescriptionMeta()
    {
        return $this->descriptionMeta;
    }

    /**
     * Set keywordMeta
     *
     * @param string $keywordMeta
     * @return CompanyWebsite
     */
    public function setKeywordMeta($keywordMeta)
    {
        $this->keywordMeta = $keywordMeta;
        return $this;
    }

    /**
     * Get keywordMeta
     *
     * @return string 
     */
    public function getKeywordMeta()
    {
        return $this->keywordMeta;
    }

    /**
     * Set company
     *
     * @param Entities\CompanyDetails $company
     * @return CompanyWebsite
     */
    public function setCompany(\Entities\CompanyDetails $company = null)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Get company
     *
     * @return Entities\CompanyDetails 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set theme
     *
     * @param Entities\WebThemes $theme
     * @return CompanyWebsite
     */
    public function setTheme(\Entities\WebThemes $theme = null)
    {
        $this->theme = $theme;
        return $this;
    }

    /**
     * Get theme
     *
     * @return Entities\WebThemes 
     */
    public function getTheme()
    {
        return $this->theme;
    }
}