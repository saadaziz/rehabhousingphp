<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\ImageLink
 */
class ImageLink
{
    /**
     * @var integer $imageId
     */
    private $imageId;

    /**
     * @var string $imageLink
     */
    private $imageLink;

    /**
     * @var string $imageName
     */
    private $imageName;

    /**
     * @var string $imageWatermark
     */
    private $imageWatermark;

    /**
     * @var integer $imageHeight
     */
    private $imageHeight;

    /**
     * @var integer $imageWidth
     */
    private $imageWidth;

    /**
     * @var string $imageType
     */
    private $imageType;

    /**
     * @var Entities\ProductDetails
     */
    private $product;

    /**
     * @var Entities\CompanyDetails
     */
    private $company;


    /**
     * Get imageId
     *
     * @return integer 
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * Set imageLink
     *
     * @param string $imageLink
     * @return ImageLink
     */
    public function setImageLink($imageLink)
    {
        $this->imageLink = $imageLink;
        return $this;
    }

    /**
     * Get imageLink
     *
     * @return string 
     */
    public function getImageLink()
    {
        return $this->imageLink;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     * @return ImageLink
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
        return $this;
    }

    /**
     * Get imageName
     *
     * @return string 
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set imageWatermark
     *
     * @param string $imageWatermark
     * @return ImageLink
     */
    public function setImageWatermark($imageWatermark)
    {
        $this->imageWatermark = $imageWatermark;
        return $this;
    }

    /**
     * Get imageWatermark
     *
     * @return string 
     */
    public function getImageWatermark()
    {
        return $this->imageWatermark;
    }

    /**
     * Set imageHeight
     *
     * @param integer $imageHeight
     * @return ImageLink
     */
    public function setImageHeight($imageHeight)
    {
        $this->imageHeight = $imageHeight;
        return $this;
    }

    /**
     * Get imageHeight
     *
     * @return integer 
     */
    public function getImageHeight()
    {
        return $this->imageHeight;
    }

    /**
     * Set imageWidth
     *
     * @param integer $imageWidth
     * @return ImageLink
     */
    public function setImageWidth($imageWidth)
    {
        $this->imageWidth = $imageWidth;
        return $this;
    }

    /**
     * Get imageWidth
     *
     * @return integer 
     */
    public function getImageWidth()
    {
        return $this->imageWidth;
    }

    /**
     * Set imageType
     *
     * @param string $imageType
     * @return ImageLink
     */
    public function setImageType($imageType)
    {
        $this->imageType = $imageType;
        return $this;
    }

    /**
     * Get imageType
     *
     * @return string 
     */
    public function getImageType()
    {
        return $this->imageType;
    }

    /**
     * Set product
     *
     * @param Entities\ProductDetails $product
     * @return ImageLink
     */
    public function setProduct(\Entities\ProductDetails $product = null)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * Get product
     *
     * @return Entities\ProductDetails 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set company
     *
     * @param Entities\CompanyDetails $company
     * @return ImageLink
     */
    public function setCompany(\Entities\CompanyDetails $company = null)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Get company
     *
     * @return Entities\CompanyDetails 
     */
    public function getCompany()
    {
        return $this->company;
    }
}