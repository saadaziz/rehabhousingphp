<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\Ip2location
 */
class Ip2location
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var integer $ipFrom
     */
    private $ipFrom;

    /**
     * @var integer $ipTo
     */
    private $ipTo;

    /**
     * @var string $countryCode
     */
    private $countryCode;

    /**
     * @var string $countryName
     */
    private $countryName;

    /**
     * @var string $regionName
     */
    private $regionName;

    /**
     * @var string $cityName
     */
    private $cityName;

    /**
     * @var float $latitude
     */
    private $latitude;

    /**
     * @var float $longitude
     */
    private $longitude;

    /**
     * @var string $zipCode
     */
    private $zipCode;

    /**
     * @var string $timeZone
     */
    private $timeZone;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ipFrom
     *
     * @param integer $ipFrom
     * @return Ip2location
     */
    public function setIpFrom($ipFrom)
    {
        $this->ipFrom = $ipFrom;
        return $this;
    }

    /**
     * Get ipFrom
     *
     * @return integer 
     */
    public function getIpFrom()
    {
        return $this->ipFrom;
    }

    /**
     * Set ipTo
     *
     * @param integer $ipTo
     * @return Ip2location
     */
    public function setIpTo($ipTo)
    {
        $this->ipTo = $ipTo;
        return $this;
    }

    /**
     * Get ipTo
     *
     * @return integer 
     */
    public function getIpTo()
    {
        return $this->ipTo;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return Ip2location
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string 
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set countryName
     *
     * @param string $countryName
     * @return Ip2location
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;
        return $this;
    }

    /**
     * Get countryName
     *
     * @return string 
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * Set regionName
     *
     * @param string $regionName
     * @return Ip2location
     */
    public function setRegionName($regionName)
    {
        $this->regionName = $regionName;
        return $this;
    }

    /**
     * Get regionName
     *
     * @return string 
     */
    public function getRegionName()
    {
        return $this->regionName;
    }

    /**
     * Set cityName
     *
     * @param string $cityName
     * @return Ip2location
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
        return $this;
    }

    /**
     * Get cityName
     *
     * @return string 
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Ip2location
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Ip2location
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     * @return Ip2location
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string 
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set timeZone
     *
     * @param string $timeZone
     * @return Ip2location
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
        return $this;
    }

    /**
     * Get timeZone
     *
     * @return string 
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }
}