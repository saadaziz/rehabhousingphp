<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\LocationDivision
 */
class LocationDivision
{
    /**
     * @var integer $divisionId
     */
    private $divisionId;

    /**
     * @var string $divisionEn
     */
    private $divisionEn;

    /**
     * @var string $divisionBn
     */
    private $divisionBn;

    /**
     * @var float $lat
     */
    private $lat;

    /**
     * @var float $long
     */
    private $long;

    /**
     * @var integer $amount
     */
    private $amount;

    /**
     * @var integer $viewOrder
     */
    private $viewOrder;


    /**
     * Get divisionId
     *
     * @return integer 
     */
    public function getDivisionId()
    {
        return $this->divisionId;
    }

    /**
     * Set divisionEn
     *
     * @param string $divisionEn
     * @return LocationDivision
     */
    public function setDivisionEn($divisionEn)
    {
        $this->divisionEn = $divisionEn;
        return $this;
    }

    /**
     * Get divisionEn
     *
     * @return string 
     */
    public function getDivisionEn()
    {
        return $this->divisionEn;
    }

    /**
     * Set divisionBn
     *
     * @param string $divisionBn
     * @return LocationDivision
     */
    public function setDivisionBn($divisionBn)
    {
        $this->divisionBn = $divisionBn;
        return $this;
    }

    /**
     * Get divisionBn
     *
     * @return string 
     */
    public function getDivisionBn()
    {
        return $this->divisionBn;
    }

    /**
     * Set lat
     *
     * @param float $lat
     * @return LocationDivision
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * Get lat
     *
     * @return float 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set long
     *
     * @param float $long
     * @return LocationDivision
     */
    public function setLong($long)
    {
        $this->long = $long;
        return $this;
    }

    /**
     * Get long
     *
     * @return float 
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return LocationDivision
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set viewOrder
     *
     * @param integer $viewOrder
     * @return LocationDivision
     */
    public function setViewOrder($viewOrder)
    {
        $this->viewOrder = $viewOrder;
        return $this;
    }

    /**
     * Get viewOrder
     *
     * @return integer 
     */
    public function getViewOrder()
    {
        return $this->viewOrder;
    }
}