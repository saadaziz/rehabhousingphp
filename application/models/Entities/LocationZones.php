<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\LocationZones
 */
class LocationZones
{
    /**
     * @var integer $locId
     */
    private $locId;

    /**
     * @var string $nameEn
     */
    private $nameEn;

    /**
     * @var string $nameBn
     */
    private $nameBn;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var float $lat
     */
    private $lat;

    /**
     * @var float $long
     */
    private $long;

    /**
     * @var integer $amount
     */
    private $amount;

    /**
     * @var Entities\LocationDivision
     */
    private $division;

    /**
     * @var Entities\LocationZones
     */
    private $parent;


    /**
     * Get locId
     *
     * @return integer 
     */
    public function getLocId()
    {
        return $this->locId;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return LocationZones
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;
        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string 
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set nameBn
     *
     * @param string $nameBn
     * @return LocationZones
     */
    public function setNameBn($nameBn)
    {
        $this->nameBn = $nameBn;
        return $this;
    }

    /**
     * Get nameBn
     *
     * @return string 
     */
    public function getNameBn()
    {
        return $this->nameBn;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return LocationZones
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set lat
     *
     * @param float $lat
     * @return LocationZones
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * Get lat
     *
     * @return float 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set long
     *
     * @param float $long
     * @return LocationZones
     */
    public function setLong($long)
    {
        $this->long = $long;
        return $this;
    }

    /**
     * Get long
     *
     * @return float 
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return LocationZones
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set division
     *
     * @param Entities\LocationDivision $division
     * @return LocationZones
     */
    public function setDivision(\Entities\LocationDivision $division = null)
    {
        $this->division = $division;
        return $this;
    }

    /**
     * Get division
     *
     * @return Entities\LocationDivision 
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * Set parent
     *
     * @param Entities\LocationZones $parent
     * @return LocationZones
     */
    public function setParent(\Entities\LocationZones $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Get parent
     *
     * @return Entities\LocationZones 
     */
    public function getParent()
    {
        return $this->parent;
    }
}