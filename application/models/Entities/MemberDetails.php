<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\MemberDetails
 */
class MemberDetails
{
    /**
     * @var integer $userId
     */
    private $userId;

    /**
     * @var string $priviledgeLevel
     */
    private $priviledgeLevel;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $email
     */
    private $email;

    /**
     * @var string $password
     */
    private $password;

    /**
     * @var datetime $insertTime
     */
    private $insertTime;

    /**
     * @var string $active
     */
    private $active;

    /**
     * @var string $cellNo
     */
    private $cellNo;

    /**
     * @var integer $amount
     */
    private $amount;

    /**
     * @var string $premium
     */
    private $premium;

    /**
     * @var Entities\CompanyDetails
     */
    private $company;


    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set priviledgeLevel
     *
     * @param string $priviledgeLevel
     * @return MemberDetails
     */
    public function setPriviledgeLevel($priviledgeLevel)
    {
        $this->priviledgeLevel = $priviledgeLevel;
        return $this;
    }

    /**
     * Get priviledgeLevel
     *
     * @return string 
     */
    public function getPriviledgeLevel()
    {
        return $this->priviledgeLevel;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MemberDetails
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return MemberDetails
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return MemberDetails
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set insertTime
     *
     * @param datetime $insertTime
     * @return MemberDetails
     */
    public function setInsertTime($insertTime)
    {
        $this->insertTime = $insertTime;
        return $this;
    }

    /**
     * Get insertTime
     *
     * @return datetime 
     */
    public function getInsertTime()
    {
        return $this->insertTime;
    }

    /**
     * Set active
     *
     * @param string $active
     * @return MemberDetails
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * Get active
     *
     * @return string 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set cellNo
     *
     * @param string $cellNo
     * @return MemberDetails
     */
    public function setCellNo($cellNo)
    {
        $this->cellNo = $cellNo;
        return $this;
    }

    /**
     * Get cellNo
     *
     * @return string 
     */
    public function getCellNo()
    {
        return $this->cellNo;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return MemberDetails
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set premium
     *
     * @param string $premium
     * @return MemberDetails
     */
    public function setPremium($premium)
    {
        $this->premium = $premium;
        return $this;
    }

    /**
     * Get premium
     *
     * @return string 
     */
    public function getPremium()
    {
        return $this->premium;
    }

    /**
     * Set company
     *
     * @param Entities\CompanyDetails $company
     * @return MemberDetails
     */
    public function setCompany(\Entities\CompanyDetails $company = null)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Get company
     *
     * @return Entities\CompanyDetails 
     */
    public function getCompany()
    {
        return $this->company;
    }
}