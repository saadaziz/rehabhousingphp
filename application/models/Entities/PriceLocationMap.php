<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\PriceLocationMap
 */
class PriceLocationMap
{
    /**
     * @var integer $priceLocId
     */
    private $priceLocId;

    /**
     * @var integer $lowPrice
     */
    private $lowPrice;

    /**
     * @var integer $highPrice
     */
    private $highPrice;

    /**
     * @var Entities\LocationZones
     */
    private $location;

    /**
     * @var Entities\ProductTypes
     */
    private $type;


    /**
     * Get priceLocId
     *
     * @return integer 
     */
    public function getPriceLocId()
    {
        return $this->priceLocId;
    }

    /**
     * Set lowPrice
     *
     * @param integer $lowPrice
     * @return PriceLocationMap
     */
    public function setLowPrice($lowPrice)
    {
        $this->lowPrice = $lowPrice;
        return $this;
    }

    /**
     * Get lowPrice
     *
     * @return integer 
     */
    public function getLowPrice()
    {
        return $this->lowPrice;
    }

    /**
     * Set highPrice
     *
     * @param integer $highPrice
     * @return PriceLocationMap
     */
    public function setHighPrice($highPrice)
    {
        $this->highPrice = $highPrice;
        return $this;
    }

    /**
     * Get highPrice
     *
     * @return integer 
     */
    public function getHighPrice()
    {
        return $this->highPrice;
    }

    /**
     * Set location
     *
     * @param Entities\LocationZones $location
     * @return PriceLocationMap
     */
    public function setLocation(\Entities\LocationZones $location = null)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * Get location
     *
     * @return Entities\LocationZones 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set type
     *
     * @param Entities\ProductTypes $type
     * @return PriceLocationMap
     */
    public function setType(\Entities\ProductTypes $type = null)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return Entities\ProductTypes 
     */
    public function getType()
    {
        return $this->type;
    }
}