<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\ProductDetails
 */
class ProductDetails
{
    /**
     * @var integer $productId
     */
    private $productId;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var string $address
     */
    private $address;

    /**
     * @var string $price
     */
    private $price;

    /**
     * @var text $description
     */
    private $description;

    /**
     * @var float $longitude
     */
    private $longitude;

    /**
     * @var float $latitude
     */
    private $latitude;

    /**
     * @var string $topProject
     */
    private $topProject;

    /**
     * @var string $approval
     */
    private $approval;

    /**
     * @var string $applyForTopProject
     */
    private $applyForTopProject;

    /**
     * @var integer $imageForTopProject
     */
    private $imageForTopProject;

    /**
     * @var datetime $activeFrom
     */
    private $activeFrom;

    /**
     * @var datetime $activeTo
     */
    private $activeTo;

    /**
     * @var string $size
     */
    private $size;

    /**
     * @var string $bedroom
     */
    private $bedroom;

    /**
     * @var string $status
     */
    private $status;

    /**
     * @var datetime $insertTime
     */
    private $insertTime;

    /**
     * @var string $negotiable
     */
    private $negotiable;

    /**
     * @var string $topProjectTitle
     */
    private $topProjectTitle;

    /**
     * @var string $topProjectLocation
     */
    private $topProjectLocation;

    /**
     * @var string $topProjectPrice
     */
    private $topProjectPrice;

    /**
     * @var string $topProjectBedbath
     */
    private $topProjectBedbath;

    /**
     * @var Entities\MemberDetails
     */
    private $user;

    /**
     * @var Entities\ProductTypes
     */
    private $type;

    /**
     * @var Entities\LocationDivision
     */
    private $division;

    /**
     * @var Entities\LocationZones
     */
    private $zone;


    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ProductDetails
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return ProductDetails
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return ProductDetails
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param text $description
     * @return ProductDetails
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return ProductDetails
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return ProductDetails
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set topProject
     *
     * @param string $topProject
     * @return ProductDetails
     */
    public function setTopProject($topProject)
    {
        $this->topProject = $topProject;
        return $this;
    }

    /**
     * Get topProject
     *
     * @return string 
     */
    public function getTopProject()
    {
        return $this->topProject;
    }

    /**
     * Set approval
     *
     * @param string $approval
     * @return ProductDetails
     */
    public function setApproval($approval)
    {
        $this->approval = $approval;
        return $this;
    }

    /**
     * Get approval
     *
     * @return string 
     */
    public function getApproval()
    {
        return $this->approval;
    }

    /**
     * Set applyForTopProject
     *
     * @param string $applyForTopProject
     * @return ProductDetails
     */
    public function setApplyForTopProject($applyForTopProject)
    {
        $this->applyForTopProject = $applyForTopProject;
        return $this;
    }

    /**
     * Get applyForTopProject
     *
     * @return string 
     */
    public function getApplyForTopProject()
    {
        return $this->applyForTopProject;
    }

    /**
     * Set imageForTopProject
     *
     * @param integer $imageForTopProject
     * @return ProductDetails
     */
    public function setImageForTopProject($imageForTopProject)
    {
        $this->imageForTopProject = $imageForTopProject;
        return $this;
    }

    /**
     * Get imageForTopProject
     *
     * @return integer 
     */
    public function getImageForTopProject()
    {
        return $this->imageForTopProject;
    }

    /**
     * Set activeFrom
     *
     * @param datetime $activeFrom
     * @return ProductDetails
     */
    public function setActiveFrom($activeFrom)
    {
        $this->activeFrom = $activeFrom;
        return $this;
    }

    /**
     * Get activeFrom
     *
     * @return datetime 
     */
    public function getActiveFrom()
    {
        return $this->activeFrom;
    }

    /**
     * Set activeTo
     *
     * @param datetime $activeTo
     * @return ProductDetails
     */
    public function setActiveTo($activeTo)
    {
        $this->activeTo = $activeTo;
        return $this;
    }

    /**
     * Get activeTo
     *
     * @return datetime 
     */
    public function getActiveTo()
    {
        return $this->activeTo;
    }

    /**
     * Set size
     *
     * @param string $size
     * @return ProductDetails
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * Get size
     *
     * @return string 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set bedroom
     *
     * @param string $bedroom
     * @return ProductDetails
     */
    public function setBedroom($bedroom)
    {
        $this->bedroom = $bedroom;
        return $this;
    }

    /**
     * Get bedroom
     *
     * @return string 
     */
    public function getBedroom()
    {
        return $this->bedroom;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductDetails
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set insertTime
     *
     * @param datetime $insertTime
     * @return ProductDetails
     */
    public function setInsertTime($insertTime)
    {
        $this->insertTime = $insertTime;
        return $this;
    }

    /**
     * Get insertTime
     *
     * @return datetime 
     */
    public function getInsertTime()
    {
        return $this->insertTime;
    }

    /**
     * Set negotiable
     *
     * @param string $negotiable
     * @return ProductDetails
     */
    public function setNegotiable($negotiable)
    {
        $this->negotiable = $negotiable;
        return $this;
    }

    /**
     * Get negotiable
     *
     * @return string 
     */
    public function getNegotiable()
    {
        return $this->negotiable;
    }

    /**
     * Set topProjectTitle
     *
     * @param string $topProjectTitle
     * @return ProductDetails
     */
    public function setTopProjectTitle($topProjectTitle)
    {
        $this->topProjectTitle = $topProjectTitle;
        return $this;
    }

    /**
     * Get topProjectTitle
     *
     * @return string 
     */
    public function getTopProjectTitle()
    {
        return $this->topProjectTitle;
    }

    /**
     * Set topProjectLocation
     *
     * @param string $topProjectLocation
     * @return ProductDetails
     */
    public function setTopProjectLocation($topProjectLocation)
    {
        $this->topProjectLocation = $topProjectLocation;
        return $this;
    }

    /**
     * Get topProjectLocation
     *
     * @return string 
     */
    public function getTopProjectLocation()
    {
        return $this->topProjectLocation;
    }

    /**
     * Set topProjectPrice
     *
     * @param string $topProjectPrice
     * @return ProductDetails
     */
    public function setTopProjectPrice($topProjectPrice)
    {
        $this->topProjectPrice = $topProjectPrice;
        return $this;
    }

    /**
     * Get topProjectPrice
     *
     * @return string 
     */
    public function getTopProjectPrice()
    {
        return $this->topProjectPrice;
    }

    /**
     * Set topProjectBedbath
     *
     * @param string $topProjectBedbath
     * @return ProductDetails
     */
    public function setTopProjectBedbath($topProjectBedbath)
    {
        $this->topProjectBedbath = $topProjectBedbath;
        return $this;
    }

    /**
     * Get topProjectBedbath
     *
     * @return string 
     */
    public function getTopProjectBedbath()
    {
        return $this->topProjectBedbath;
    }

    /**
     * Set user
     *
     * @param Entities\MemberDetails $user
     * @return ProductDetails
     */
    public function setUser(\Entities\MemberDetails $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return Entities\MemberDetails 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type
     *
     * @param Entities\ProductTypes $type
     * @return ProductDetails
     */
    public function setType(\Entities\ProductTypes $type = null)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return Entities\ProductTypes 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set division
     *
     * @param Entities\LocationDivision $division
     * @return ProductDetails
     */
    public function setDivision(\Entities\LocationDivision $division = null)
    {
        $this->division = $division;
        return $this;
    }

    /**
     * Get division
     *
     * @return Entities\LocationDivision 
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * Set zone
     *
     * @param Entities\LocationZones $zone
     * @return ProductDetails
     */
    public function setZone(\Entities\LocationZones $zone = null)
    {
        $this->zone = $zone;
        return $this;
    }

    /**
     * Get zone
     *
     * @return Entities\LocationZones 
     */
    public function getZone()
    {
        return $this->zone;
    }
}