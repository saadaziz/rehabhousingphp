<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\ProductField
 */
class ProductField
{
    /**
     * @var integer $pFieldId
     */
    private $pFieldId;

    /**
     * @var string $fieldName
     */
    private $fieldName;

    /**
     * @var string $required
     */
    private $required;

    /**
     * @var string $uitype
     */
    private $uitype;

    /**
     * @var string $fieldSku
     */
    private $fieldSku;

    /**
     * @var Entities\ProductTypes
     */
    private $type;


    /**
     * Get pFieldId
     *
     * @return integer 
     */
    public function getPFieldId()
    {
        return $this->pFieldId;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     * @return ProductField
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    /**
     * Get fieldName
     *
     * @return string 
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * Set required
     *
     * @param string $required
     * @return ProductField
     */
    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }

    /**
     * Get required
     *
     * @return string 
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set uitype
     *
     * @param string $uitype
     * @return ProductField
     */
    public function setUitype($uitype)
    {
        $this->uitype = $uitype;
        return $this;
    }

    /**
     * Get uitype
     *
     * @return string 
     */
    public function getUitype()
    {
        return $this->uitype;
    }

    /**
     * Set fieldSku
     *
     * @param string $fieldSku
     * @return ProductField
     */
    public function setFieldSku($fieldSku)
    {
        $this->fieldSku = $fieldSku;
        return $this;
    }

    /**
     * Get fieldSku
     *
     * @return string 
     */
    public function getFieldSku()
    {
        return $this->fieldSku;
    }

    /**
     * Set type
     *
     * @param Entities\ProductTypes $type
     * @return ProductField
     */
    public function setType(\Entities\ProductTypes $type = null)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return Entities\ProductTypes 
     */
    public function getType()
    {
        return $this->type;
    }
}