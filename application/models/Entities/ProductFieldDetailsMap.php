<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\ProductFieldDetailsMap
 */
class ProductFieldDetailsMap
{
    /**
     * @var integer $mapid
     */
    private $mapid;

    /**
     * @var string $value
     */
    private $value;

    /**
     * @var Entities\ProductDetails
     */
    private $product;

    /**
     * @var Entities\ProductField
     */
    private $pField;

    /**
     * @var Entities\ImageLink
     */
    private $imageLink;


    /**
     * Get mapid
     *
     * @return integer 
     */
    public function getMapid()
    {
        return $this->mapid;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return ProductFieldDetailsMap
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set product
     *
     * @param Entities\ProductDetails $product
     * @return ProductFieldDetailsMap
     */
    public function setProduct(\Entities\ProductDetails $product = null)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * Get product
     *
     * @return Entities\ProductDetails 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set pField
     *
     * @param Entities\ProductField $pField
     * @return ProductFieldDetailsMap
     */
    public function setPField(\Entities\ProductField $pField = null)
    {
        $this->pField = $pField;
        return $this;
    }

    /**
     * Get pField
     *
     * @return Entities\ProductField 
     */
    public function getPField()
    {
        return $this->pField;
    }

    /**
     * Set imageLink
     *
     * @param Entities\ImageLink $imageLink
     * @return ProductFieldDetailsMap
     */
    public function setImageLink(\Entities\ImageLink $imageLink = null)
    {
        $this->imageLink = $imageLink;
        return $this;
    }

    /**
     * Get imageLink
     *
     * @return Entities\ImageLink 
     */
    public function getImageLink()
    {
        return $this->imageLink;
    }
}