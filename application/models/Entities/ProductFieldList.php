<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\ProductFieldList
 */
class ProductFieldList
{
    /**
     * @var integer $pFieldListId
     */
    private $pFieldListId;

    /**
     * @var string $value
     */
    private $value;

    /**
     * @var Entities\ProductField
     */
    private $pField;


    /**
     * Get pFieldListId
     *
     * @return integer 
     */
    public function getPFieldListId()
    {
        return $this->pFieldListId;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return ProductFieldList
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set pField
     *
     * @param Entities\ProductField $pField
     * @return ProductFieldList
     */
    public function setPField(\Entities\ProductField $pField = null)
    {
        $this->pField = $pField;
        return $this;
    }

    /**
     * Get pField
     *
     * @return Entities\ProductField 
     */
    public function getPField()
    {
        return $this->pField;
    }
}