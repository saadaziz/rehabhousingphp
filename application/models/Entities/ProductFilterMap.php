<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\ProductFilterMap
 */
class ProductFilterMap
{
    /**
     * @var integer $productFilterId
     */
    private $productFilterId;

    /**
     * @var string $filtername
     */
    private $filtername;

    /**
     * @var string $filtervalue
     */
    private $filtervalue;

    /**
     * @var Entities\ProductTypes
     */
    private $typeid;

    /**
     * @var Entities\ProductDetails
     */
    private $productid;


    /**
     * Get productFilterId
     *
     * @return integer 
     */
    public function getProductFilterId()
    {
        return $this->productFilterId;
    }

    /**
     * Set filtername
     *
     * @param string $filtername
     * @return ProductFilterMap
     */
    public function setFiltername($filtername)
    {
        $this->filtername = $filtername;
        return $this;
    }

    /**
     * Get filtername
     *
     * @return string 
     */
    public function getFiltername()
    {
        return $this->filtername;
    }

    /**
     * Set filtervalue
     *
     * @param string $filtervalue
     * @return ProductFilterMap
     */
    public function setFiltervalue($filtervalue)
    {
        $this->filtervalue = $filtervalue;
        return $this;
    }

    /**
     * Get filtervalue
     *
     * @return string 
     */
    public function getFiltervalue()
    {
        return $this->filtervalue;
    }

    /**
     * Set typeid
     *
     * @param Entities\ProductTypes $typeid
     * @return ProductFilterMap
     */
    public function setTypeid(\Entities\ProductTypes $typeid = null)
    {
        $this->typeid = $typeid;
        return $this;
    }

    /**
     * Get typeid
     *
     * @return Entities\ProductTypes 
     */
    public function getTypeid()
    {
        return $this->typeid;
    }

    /**
     * Set productid
     *
     * @param Entities\ProductDetails $productid
     * @return ProductFilterMap
     */
    public function setProductid(\Entities\ProductDetails $productid = null)
    {
        $this->productid = $productid;
        return $this;
    }

    /**
     * Get productid
     *
     * @return Entities\ProductDetails 
     */
    public function getProductid()
    {
        return $this->productid;
    }
}