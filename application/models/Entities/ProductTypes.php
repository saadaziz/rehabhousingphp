<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\ProductTypes
 */
class ProductTypes
{
    /**
     * @var integer $productTypesId
     */
    private $productTypesId;

    /**
     * @var string $typeName
     */
    private $typeName;

    /**
     * @var integer $amount
     */
    private $amount;

    /**
     * @var integer $viewOrder
     */
    private $viewOrder;

    /**
     * @var Entities\ProductTypes
     */
    private $parent;


    /**
     * Get productTypesId
     *
     * @return integer 
     */
    public function getProductTypesId()
    {
        return $this->productTypesId;
    }

    /**
     * Set typeName
     *
     * @param string $typeName
     * @return ProductTypes
     */
    public function setTypeName($typeName)
    {
        $this->typeName = $typeName;
        return $this;
    }

    /**
     * Get typeName
     *
     * @return string 
     */
    public function getTypeName()
    {
        return $this->typeName;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return ProductTypes
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set viewOrder
     *
     * @param integer $viewOrder
     * @return ProductTypes
     */
    public function setViewOrder($viewOrder)
    {
        $this->viewOrder = $viewOrder;
        return $this;
    }

    /**
     * Get viewOrder
     *
     * @return integer 
     */
    public function getViewOrder()
    {
        return $this->viewOrder;
    }

    /**
     * Set parent
     *
     * @param Entities\ProductTypes $parent
     * @return ProductTypes
     */
    public function setParent(\Entities\ProductTypes $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Get parent
     *
     * @return Entities\ProductTypes 
     */
    public function getParent()
    {
        return $this->parent;
    }
}