<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\ProductUnitDetails
 */
class ProductUnitDetails
{
    /**
     * @var integer $unitDetailsId
     */
    private $unitDetailsId;

    /**
     * @var integer $bedroom
     */
    private $bedroom;

    /**
     * @var integer $bathroom
     */
    private $bathroom;

    /**
     * @var integer $price
     */
    private $price;

    /**
     * @var integer $size
     */
    private $size;

    /**
     * @var string $typeName
     */
    private $typeName;

    /**
     * @var Entities\ImageLink
     */
    private $image;

    /**
     * @var Entities\ProductDetails
     */
    private $product;


    /**
     * Get unitDetailsId
     *
     * @return integer 
     */
    public function getUnitDetailsId()
    {
        return $this->unitDetailsId;
    }

    /**
     * Set bedroom
     *
     * @param integer $bedroom
     * @return ProductUnitDetails
     */
    public function setBedroom($bedroom)
    {
        $this->bedroom = $bedroom;
        return $this;
    }

    /**
     * Get bedroom
     *
     * @return integer 
     */
    public function getBedroom()
    {
        return $this->bedroom;
    }

    /**
     * Set bathroom
     *
     * @param integer $bathroom
     * @return ProductUnitDetails
     */
    public function setBathroom($bathroom)
    {
        $this->bathroom = $bathroom;
        return $this;
    }

    /**
     * Get bathroom
     *
     * @return integer 
     */
    public function getBathroom()
    {
        return $this->bathroom;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return ProductUnitDetails
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return ProductUnitDetails
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set typeName
     *
     * @param string $typeName
     * @return ProductUnitDetails
     */
    public function setTypeName($typeName)
    {
        $this->typeName = $typeName;
        return $this;
    }

    /**
     * Get typeName
     *
     * @return string 
     */
    public function getTypeName()
    {
        return $this->typeName;
    }

    /**
     * Set image
     *
     * @param Entities\ImageLink $image
     * @return ProductUnitDetails
     */
    public function setImage(\Entities\ImageLink $image = null)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     *
     * @return Entities\ImageLink 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set product
     *
     * @param Entities\ProductDetails $product
     * @return ProductUnitDetails
     */
    public function setProduct(\Entities\ProductDetails $product = null)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * Get product
     *
     * @return Entities\ProductDetails 
     */
    public function getProduct()
    {
        return $this->product;
    }
}