<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\Propertyrequirement
 */
class Propertyrequirement
{
    /**
     * @var integer $prid
     */
    private $prid;

    /**
     * @var string $wantto
     */
    private $wantto;

    /**
     * @var string $porpertytype
     */
    private $porpertytype;

    /**
     * @var string $conditions
     */
    private $conditions;

    /**
     * @var string $civisioncity
     */
    private $civisioncity;

    /**
     * @var string $area
     */
    private $area;

    /**
     * @var integer $sizefrom
     */
    private $sizefrom;

    /**
     * @var integer $sizeto
     */
    private $sizeto;

    /**
     * @var string $bed
     */
    private $bed;

    /**
     * @var string $bath
     */
    private $bath;

    /**
     * @var integer $pricefrom
     */
    private $pricefrom;

    /**
     * @var integer $priceto
     */
    private $priceto;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $email
     */
    private $email;

    /**
     * @var string $countrycode
     */
    private $countrycode;

    /**
     * @var string $mobilenumber
     */
    private $mobilenumber;

    /**
     * @var integer $attachbath
     */
    private $attachbath;

    /**
     * @var string $note
     */
    private $note;

    /**
     * @var string $loaninterest
     */
    private $loaninterest;

    /**
     * @var string $emailafterpost
     */
    private $emailafterpost;

    /**
     * @var string $buyassistantservice
     */
    private $buyassistantservice;

    /**
     * @var string $accepttermsconditions
     */
    private $accepttermsconditions;


    /**
     * Get prid
     *
     * @return integer 
     */
    public function getPrid()
    {
        return $this->prid;
    }

    /**
     * Set wantto
     *
     * @param string $wantto
     * @return Propertyrequirement
     */
    public function setWantto($wantto)
    {
        $this->wantto = $wantto;
        return $this;
    }

    /**
     * Get wantto
     *
     * @return string 
     */
    public function getWantto()
    {
        return $this->wantto;
    }

    /**
     * Set porpertytype
     *
     * @param string $porpertytype
     * @return Propertyrequirement
     */
    public function setPorpertytype($porpertytype)
    {
        $this->porpertytype = $porpertytype;
        return $this;
    }

    /**
     * Get porpertytype
     *
     * @return string 
     */
    public function getPorpertytype()
    {
        return $this->porpertytype;
    }

    /**
     * Set conditions
     *
     * @param string $conditions
     * @return Propertyrequirement
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
        return $this;
    }

    /**
     * Get conditions
     *
     * @return string 
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * Set civisioncity
     *
     * @param string $civisioncity
     * @return Propertyrequirement
     */
    public function setCivisioncity($civisioncity)
    {
        $this->civisioncity = $civisioncity;
        return $this;
    }

    /**
     * Get civisioncity
     *
     * @return string 
     */
    public function getCivisioncity()
    {
        return $this->civisioncity;
    }

    /**
     * Set area
     *
     * @param string $area
     * @return Propertyrequirement
     */
    public function setArea($area)
    {
        $this->area = $area;
        return $this;
    }

    /**
     * Get area
     *
     * @return string 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set sizefrom
     *
     * @param integer $sizefrom
     * @return Propertyrequirement
     */
    public function setSizefrom($sizefrom)
    {
        $this->sizefrom = $sizefrom;
        return $this;
    }

    /**
     * Get sizefrom
     *
     * @return integer 
     */
    public function getSizefrom()
    {
        return $this->sizefrom;
    }

    /**
     * Set sizeto
     *
     * @param integer $sizeto
     * @return Propertyrequirement
     */
    public function setSizeto($sizeto)
    {
        $this->sizeto = $sizeto;
        return $this;
    }

    /**
     * Get sizeto
     *
     * @return integer 
     */
    public function getSizeto()
    {
        return $this->sizeto;
    }

    /**
     * Set bed
     *
     * @param string $bed
     * @return Propertyrequirement
     */
    public function setBed($bed)
    {
        $this->bed = $bed;
        return $this;
    }

    /**
     * Get bed
     *
     * @return string 
     */
    public function getBed()
    {
        return $this->bed;
    }

    /**
     * Set bath
     *
     * @param string $bath
     * @return Propertyrequirement
     */
    public function setBath($bath)
    {
        $this->bath = $bath;
        return $this;
    }

    /**
     * Get bath
     *
     * @return string 
     */
    public function getBath()
    {
        return $this->bath;
    }

    /**
     * Set pricefrom
     *
     * @param integer $pricefrom
     * @return Propertyrequirement
     */
    public function setPricefrom($pricefrom)
    {
        $this->pricefrom = $pricefrom;
        return $this;
    }

    /**
     * Get pricefrom
     *
     * @return integer 
     */
    public function getPricefrom()
    {
        return $this->pricefrom;
    }

    /**
     * Set priceto
     *
     * @param integer $priceto
     * @return Propertyrequirement
     */
    public function setPriceto($priceto)
    {
        $this->priceto = $priceto;
        return $this;
    }

    /**
     * Get priceto
     *
     * @return integer 
     */
    public function getPriceto()
    {
        return $this->priceto;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Propertyrequirement
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Propertyrequirement
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set countrycode
     *
     * @param string $countrycode
     * @return Propertyrequirement
     */
    public function setCountrycode($countrycode)
    {
        $this->countrycode = $countrycode;
        return $this;
    }

    /**
     * Get countrycode
     *
     * @return string 
     */
    public function getCountrycode()
    {
        return $this->countrycode;
    }

    /**
     * Set mobilenumber
     *
     * @param string $mobilenumber
     * @return Propertyrequirement
     */
    public function setMobilenumber($mobilenumber)
    {
        $this->mobilenumber = $mobilenumber;
        return $this;
    }

    /**
     * Get mobilenumber
     *
     * @return string 
     */
    public function getMobilenumber()
    {
        return $this->mobilenumber;
    }

    /**
     * Set attachbath
     *
     * @param integer $attachbath
     * @return Propertyrequirement
     */
    public function setAttachbath($attachbath)
    {
        $this->attachbath = $attachbath;
        return $this;
    }

    /**
     * Get attachbath
     *
     * @return integer 
     */
    public function getAttachbath()
    {
        return $this->attachbath;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Propertyrequirement
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set loaninterest
     *
     * @param string $loaninterest
     * @return Propertyrequirement
     */
    public function setLoaninterest($loaninterest)
    {
        $this->loaninterest = $loaninterest;
        return $this;
    }

    /**
     * Get loaninterest
     *
     * @return string 
     */
    public function getLoaninterest()
    {
        return $this->loaninterest;
    }

    /**
     * Set emailafterpost
     *
     * @param string $emailafterpost
     * @return Propertyrequirement
     */
    public function setEmailafterpost($emailafterpost)
    {
        $this->emailafterpost = $emailafterpost;
        return $this;
    }

    /**
     * Get emailafterpost
     *
     * @return string 
     */
    public function getEmailafterpost()
    {
        return $this->emailafterpost;
    }

    /**
     * Set buyassistantservice
     *
     * @param string $buyassistantservice
     * @return Propertyrequirement
     */
    public function setBuyassistantservice($buyassistantservice)
    {
        $this->buyassistantservice = $buyassistantservice;
        return $this;
    }

    /**
     * Get buyassistantservice
     *
     * @return string 
     */
    public function getBuyassistantservice()
    {
        return $this->buyassistantservice;
    }

    /**
     * Set accepttermsconditions
     *
     * @param string $accepttermsconditions
     * @return Propertyrequirement
     */
    public function setAccepttermsconditions($accepttermsconditions)
    {
        $this->accepttermsconditions = $accepttermsconditions;
        return $this;
    }

    /**
     * Get accepttermsconditions
     *
     * @return string 
     */
    public function getAccepttermsconditions()
    {
        return $this->accepttermsconditions;
    }
}