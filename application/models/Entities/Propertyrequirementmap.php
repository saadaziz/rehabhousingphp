<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\Propertyrequirementmap
 */
class Propertyrequirementmap
{
    /**
     * @var integer $prmapid
     */
    private $prmapid;

    /**
     * @var Entities\Propertyrequirement
     */
    private $requirementid;

    /**
     * @var Entities\LocationZones
     */
    private $locid;


    /**
     * Get prmapid
     *
     * @return integer 
     */
    public function getPrmapid()
    {
        return $this->prmapid;
    }

    /**
     * Set requirementid
     *
     * @param Entities\Propertyrequirement $requirementid
     * @return Propertyrequirementmap
     */
    public function setRequirementid(\Entities\Propertyrequirement $requirementid = null)
    {
        $this->requirementid = $requirementid;
        return $this;
    }

    /**
     * Get requirementid
     *
     * @return Entities\Propertyrequirement 
     */
    public function getRequirementid()
    {
        return $this->requirementid;
    }

    /**
     * Set locid
     *
     * @param Entities\LocationZones $locid
     * @return Propertyrequirementmap
     */
    public function setLocid(\Entities\LocationZones $locid = null)
    {
        $this->locid = $locid;
        return $this;
    }

    /**
     * Get locid
     *
     * @return Entities\LocationZones 
     */
    public function getLocid()
    {
        return $this->locid;
    }
}