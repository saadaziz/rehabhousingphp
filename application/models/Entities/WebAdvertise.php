<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\WebAdvertise
 */
class WebAdvertise
{
    /**
     * @var integer $adId
     */
    private $adId;

    /**
     * @var string $adPlace
     */
    private $adPlace;

    /**
     * @var string $link
     */
    private $link;

    /**
     * @var datetime $inserttime
     */
    private $inserttime;

    /**
     * @var string $companyName
     */
    private $companyName;

    /**
     * @var string $altText
     */
    private $altText;

    /**
     * @var Entities\ImageLink
     */
    private $imageid;


    /**
     * Get adId
     *
     * @return integer 
     */
    public function getAdId()
    {
        return $this->adId;
    }

    /**
     * Set adPlace
     *
     * @param string $adPlace
     * @return WebAdvertise
     */
    public function setAdPlace($adPlace)
    {
        $this->adPlace = $adPlace;
        return $this;
    }

    /**
     * Get adPlace
     *
     * @return string 
     */
    public function getAdPlace()
    {
        return $this->adPlace;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return WebAdvertise
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set inserttime
     *
     * @param datetime $inserttime
     * @return WebAdvertise
     */
    public function setInserttime($inserttime)
    {
        $this->inserttime = $inserttime;
        return $this;
    }

    /**
     * Get inserttime
     *
     * @return datetime 
     */
    public function getInserttime()
    {
        return $this->inserttime;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return WebAdvertise
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set altText
     *
     * @param string $altText
     * @return WebAdvertise
     */
    public function setAltText($altText)
    {
        $this->altText = $altText;
        return $this;
    }

    /**
     * Get altText
     *
     * @return string 
     */
    public function getAltText()
    {
        return $this->altText;
    }

    /**
     * Set imageid
     *
     * @param Entities\ImageLink $imageid
     * @return WebAdvertise
     */
    public function setImageid(\Entities\ImageLink $imageid = null)
    {
        $this->imageid = $imageid;
        return $this;
    }

    /**
     * Get imageid
     *
     * @return Entities\ImageLink 
     */
    public function getImageid()
    {
        return $this->imageid;
    }
}