<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\WebJobs
 */
class WebJobs
{
    /**
     * @var integer $jobsId
     */
    private $jobsId;

    /**
     * @var string $companyName
     */
    private $companyName;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var text $description
     */
    private $description;

    /**
     * @var string $designation
     */
    private $designation;

    /**
     * @var datetime $insertTime
     */
    private $insertTime;

    /**
     * @var date $lastDateOfSubmission
     */
    private $lastDateOfSubmission;

    /**
     * @var string $educationRequirements
     */
    private $educationRequirements;

    /**
     * @var string $experience
     */
    private $experience;

    /**
     * @var string $salaryRange
     */
    private $salaryRange;

    /**
     * @var string $jobLocation
     */
    private $jobLocation;

    /**
     * @var string $jobSource
     */
    private $jobSource;

    /**
     * @var string $contactInfo
     */
    private $contactInfo;

    /**
     * @var string $jobCatagory
     */
    private $jobCatagory;

    /**
     * @var integer $noOfVacancies
     */
    private $noOfVacancies;

    /**
     * @var string $additionalRequirement
     */
    private $additionalRequirement;

    /**
     * @var string $applyInstruction
     */
    private $applyInstruction;

    /**
     * @var string $verified
     */
    private $verified;

    /**
     * @var Entities\MemberDetails
     */
    private $userid;


    /**
     * Get jobsId
     *
     * @return integer 
     */
    public function getJobsId()
    {
        return $this->jobsId;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return WebJobs
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return WebJobs
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param text $description
     * @return WebJobs
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set designation
     *
     * @param string $designation
     * @return WebJobs
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
        return $this;
    }

    /**
     * Get designation
     *
     * @return string 
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set insertTime
     *
     * @param datetime $insertTime
     * @return WebJobs
     */
    public function setInsertTime($insertTime)
    {
        $this->insertTime = $insertTime;
        return $this;
    }

    /**
     * Get insertTime
     *
     * @return datetime 
     */
    public function getInsertTime()
    {
        return $this->insertTime;
    }

    /**
     * Set lastDateOfSubmission
     *
     * @param date $lastDateOfSubmission
     * @return WebJobs
     */
    public function setLastDateOfSubmission($lastDateOfSubmission)
    {
        $this->lastDateOfSubmission = $lastDateOfSubmission;
        return $this;
    }

    /**
     * Get lastDateOfSubmission
     *
     * @return date 
     */
    public function getLastDateOfSubmission()
    {
        return $this->lastDateOfSubmission;
    }

    /**
     * Set educationRequirements
     *
     * @param string $educationRequirements
     * @return WebJobs
     */
    public function setEducationRequirements($educationRequirements)
    {
        $this->educationRequirements = $educationRequirements;
        return $this;
    }

    /**
     * Get educationRequirements
     *
     * @return string 
     */
    public function getEducationRequirements()
    {
        return $this->educationRequirements;
    }

    /**
     * Set experience
     *
     * @param string $experience
     * @return WebJobs
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
        return $this;
    }

    /**
     * Get experience
     *
     * @return string 
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set salaryRange
     *
     * @param string $salaryRange
     * @return WebJobs
     */
    public function setSalaryRange($salaryRange)
    {
        $this->salaryRange = $salaryRange;
        return $this;
    }

    /**
     * Get salaryRange
     *
     * @return string 
     */
    public function getSalaryRange()
    {
        return $this->salaryRange;
    }

    /**
     * Set jobLocation
     *
     * @param string $jobLocation
     * @return WebJobs
     */
    public function setJobLocation($jobLocation)
    {
        $this->jobLocation = $jobLocation;
        return $this;
    }

    /**
     * Get jobLocation
     *
     * @return string 
     */
    public function getJobLocation()
    {
        return $this->jobLocation;
    }

    /**
     * Set jobSource
     *
     * @param string $jobSource
     * @return WebJobs
     */
    public function setJobSource($jobSource)
    {
        $this->jobSource = $jobSource;
        return $this;
    }

    /**
     * Get jobSource
     *
     * @return string 
     */
    public function getJobSource()
    {
        return $this->jobSource;
    }

    /**
     * Set contactInfo
     *
     * @param string $contactInfo
     * @return WebJobs
     */
    public function setContactInfo($contactInfo)
    {
        $this->contactInfo = $contactInfo;
        return $this;
    }

    /**
     * Get contactInfo
     *
     * @return string 
     */
    public function getContactInfo()
    {
        return $this->contactInfo;
    }

    /**
     * Set jobCatagory
     *
     * @param string $jobCatagory
     * @return WebJobs
     */
    public function setJobCatagory($jobCatagory)
    {
        $this->jobCatagory = $jobCatagory;
        return $this;
    }

    /**
     * Get jobCatagory
     *
     * @return string 
     */
    public function getJobCatagory()
    {
        return $this->jobCatagory;
    }

    /**
     * Set noOfVacancies
     *
     * @param integer $noOfVacancies
     * @return WebJobs
     */
    public function setNoOfVacancies($noOfVacancies)
    {
        $this->noOfVacancies = $noOfVacancies;
        return $this;
    }

    /**
     * Get noOfVacancies
     *
     * @return integer 
     */
    public function getNoOfVacancies()
    {
        return $this->noOfVacancies;
    }

    /**
     * Set additionalRequirement
     *
     * @param string $additionalRequirement
     * @return WebJobs
     */
    public function setAdditionalRequirement($additionalRequirement)
    {
        $this->additionalRequirement = $additionalRequirement;
        return $this;
    }

    /**
     * Get additionalRequirement
     *
     * @return string 
     */
    public function getAdditionalRequirement()
    {
        return $this->additionalRequirement;
    }

    /**
     * Set applyInstruction
     *
     * @param string $applyInstruction
     * @return WebJobs
     */
    public function setApplyInstruction($applyInstruction)
    {
        $this->applyInstruction = $applyInstruction;
        return $this;
    }

    /**
     * Get applyInstruction
     *
     * @return string 
     */
    public function getApplyInstruction()
    {
        return $this->applyInstruction;
    }

    /**
     * Set verified
     *
     * @param string $verified
     * @return WebJobs
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;
        return $this;
    }

    /**
     * Get verified
     *
     * @return string 
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * Set userid
     *
     * @param Entities\MemberDetails $userid
     * @return WebJobs
     */
    public function setUserid(\Entities\MemberDetails $userid = null)
    {
        $this->userid = $userid;
        return $this;
    }

    /**
     * Get userid
     *
     * @return Entities\MemberDetails 
     */
    public function getUserid()
    {
        return $this->userid;
    }
}