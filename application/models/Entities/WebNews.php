<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\WebNews
 */
class WebNews
{
    /**
     * @var integer $newsId
     */
    private $newsId;

    /**
     * @var string $newsTitle
     */
    private $newsTitle;

    /**
     * @var text $description
     */
    private $description;

    /**
     * @var datetime $insertTime
     */
    private $insertTime;

    /**
     * @var string $shortText
     */
    private $shortText;


    /**
     * Get newsId
     *
     * @return integer 
     */
    public function getNewsId()
    {
        return $this->newsId;
    }

    /**
     * Set newsTitle
     *
     * @param string $newsTitle
     * @return WebNews
     */
    public function setNewsTitle($newsTitle)
    {
        $this->newsTitle = $newsTitle;
        return $this;
    }

    /**
     * Get newsTitle
     *
     * @return string 
     */
    public function getNewsTitle()
    {
        return $this->newsTitle;
    }

    /**
     * Set description
     *
     * @param text $description
     * @return WebNews
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set insertTime
     *
     * @param datetime $insertTime
     * @return WebNews
     */
    public function setInsertTime($insertTime)
    {
        $this->insertTime = $insertTime;
        return $this;
    }

    /**
     * Get insertTime
     *
     * @return datetime 
     */
    public function getInsertTime()
    {
        return $this->insertTime;
    }

    /**
     * Set shortText
     *
     * @param string $shortText
     * @return WebNews
     */
    public function setShortText($shortText)
    {
        $this->shortText = $shortText;
        return $this;
    }

    /**
     * Get shortText
     *
     * @return string 
     */
    public function getShortText()
    {
        return $this->shortText;
    }
}