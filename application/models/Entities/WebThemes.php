<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities\WebThemes
 */
class WebThemes
{
    /**
     * @var integer $themeId
     */
    private $themeId;

    /**
     * @var string $themeName
     */
    private $themeName;

    /**
     * @var string $bodyBgColor
     */
    private $bodyBgColor;

    /**
     * @var string $sliderBgColor
     */
    private $sliderBgColor;

    /**
     * @var string $textColor
     */
    private $textColor;

    /**
     * @var string $textHeaderColor
     */
    private $textHeaderColor;

    /**
     * @var string $topMenuColor
     */
    private $topMenuColor;

    /**
     * @var string $topMenuHoverColor
     */
    private $topMenuHoverColor;

    /**
     * @var string $footerBgColor
     */
    private $footerBgColor;

    /**
     * @var string $footerTextColor
     */
    private $footerTextColor;

    /**
     * @var string $footerMenuColor
     */
    private $footerMenuColor;

    /**
     * @var string $footerMenuHoverColor
     */
    private $footerMenuHoverColor;

    /**
     * @var string $projectNameColor
     */
    private $projectNameColor;

    /**
     * @var string $projectAddressColor
     */
    private $projectAddressColor;

    /**
     * @var string $projectPriceColor
     */
    private $projectPriceColor;

    /**
     * @var string $projectFeatureColor
     */
    private $projectFeatureColor;


    /**
     * Get themeId
     *
     * @return integer 
     */
    public function getThemeId()
    {
        return $this->themeId;
    }

    /**
     * Set themeName
     *
     * @param string $themeName
     * @return WebThemes
     */
    public function setThemeName($themeName)
    {
        $this->themeName = $themeName;
        return $this;
    }

    /**
     * Get themeName
     *
     * @return string 
     */
    public function getThemeName()
    {
        return $this->themeName;
    }

    /**
     * Set bodyBgColor
     *
     * @param string $bodyBgColor
     * @return WebThemes
     */
    public function setBodyBgColor($bodyBgColor)
    {
        $this->bodyBgColor = $bodyBgColor;
        return $this;
    }

    /**
     * Get bodyBgColor
     *
     * @return string 
     */
    public function getBodyBgColor()
    {
        return $this->bodyBgColor;
    }

    /**
     * Set sliderBgColor
     *
     * @param string $sliderBgColor
     * @return WebThemes
     */
    public function setSliderBgColor($sliderBgColor)
    {
        $this->sliderBgColor = $sliderBgColor;
        return $this;
    }

    /**
     * Get sliderBgColor
     *
     * @return string 
     */
    public function getSliderBgColor()
    {
        return $this->sliderBgColor;
    }

    /**
     * Set textColor
     *
     * @param string $textColor
     * @return WebThemes
     */
    public function setTextColor($textColor)
    {
        $this->textColor = $textColor;
        return $this;
    }

    /**
     * Get textColor
     *
     * @return string 
     */
    public function getTextColor()
    {
        return $this->textColor;
    }

    /**
     * Set textHeaderColor
     *
     * @param string $textHeaderColor
     * @return WebThemes
     */
    public function setTextHeaderColor($textHeaderColor)
    {
        $this->textHeaderColor = $textHeaderColor;
        return $this;
    }

    /**
     * Get textHeaderColor
     *
     * @return string 
     */
    public function getTextHeaderColor()
    {
        return $this->textHeaderColor;
    }

    /**
     * Set topMenuColor
     *
     * @param string $topMenuColor
     * @return WebThemes
     */
    public function setTopMenuColor($topMenuColor)
    {
        $this->topMenuColor = $topMenuColor;
        return $this;
    }

    /**
     * Get topMenuColor
     *
     * @return string 
     */
    public function getTopMenuColor()
    {
        return $this->topMenuColor;
    }

    /**
     * Set topMenuHoverColor
     *
     * @param string $topMenuHoverColor
     * @return WebThemes
     */
    public function setTopMenuHoverColor($topMenuHoverColor)
    {
        $this->topMenuHoverColor = $topMenuHoverColor;
        return $this;
    }

    /**
     * Get topMenuHoverColor
     *
     * @return string 
     */
    public function getTopMenuHoverColor()
    {
        return $this->topMenuHoverColor;
    }

    /**
     * Set footerBgColor
     *
     * @param string $footerBgColor
     * @return WebThemes
     */
    public function setFooterBgColor($footerBgColor)
    {
        $this->footerBgColor = $footerBgColor;
        return $this;
    }

    /**
     * Get footerBgColor
     *
     * @return string 
     */
    public function getFooterBgColor()
    {
        return $this->footerBgColor;
    }

    /**
     * Set footerTextColor
     *
     * @param string $footerTextColor
     * @return WebThemes
     */
    public function setFooterTextColor($footerTextColor)
    {
        $this->footerTextColor = $footerTextColor;
        return $this;
    }

    /**
     * Get footerTextColor
     *
     * @return string 
     */
    public function getFooterTextColor()
    {
        return $this->footerTextColor;
    }

    /**
     * Set footerMenuColor
     *
     * @param string $footerMenuColor
     * @return WebThemes
     */
    public function setFooterMenuColor($footerMenuColor)
    {
        $this->footerMenuColor = $footerMenuColor;
        return $this;
    }

    /**
     * Get footerMenuColor
     *
     * @return string 
     */
    public function getFooterMenuColor()
    {
        return $this->footerMenuColor;
    }

    /**
     * Set footerMenuHoverColor
     *
     * @param string $footerMenuHoverColor
     * @return WebThemes
     */
    public function setFooterMenuHoverColor($footerMenuHoverColor)
    {
        $this->footerMenuHoverColor = $footerMenuHoverColor;
        return $this;
    }

    /**
     * Get footerMenuHoverColor
     *
     * @return string 
     */
    public function getFooterMenuHoverColor()
    {
        return $this->footerMenuHoverColor;
    }

    /**
     * Set projectNameColor
     *
     * @param string $projectNameColor
     * @return WebThemes
     */
    public function setProjectNameColor($projectNameColor)
    {
        $this->projectNameColor = $projectNameColor;
        return $this;
    }

    /**
     * Get projectNameColor
     *
     * @return string 
     */
    public function getProjectNameColor()
    {
        return $this->projectNameColor;
    }

    /**
     * Set projectAddressColor
     *
     * @param string $projectAddressColor
     * @return WebThemes
     */
    public function setProjectAddressColor($projectAddressColor)
    {
        $this->projectAddressColor = $projectAddressColor;
        return $this;
    }

    /**
     * Get projectAddressColor
     *
     * @return string 
     */
    public function getProjectAddressColor()
    {
        return $this->projectAddressColor;
    }

    /**
     * Set projectPriceColor
     *
     * @param string $projectPriceColor
     * @return WebThemes
     */
    public function setProjectPriceColor($projectPriceColor)
    {
        $this->projectPriceColor = $projectPriceColor;
        return $this;
    }

    /**
     * Get projectPriceColor
     *
     * @return string 
     */
    public function getProjectPriceColor()
    {
        return $this->projectPriceColor;
    }

    /**
     * Set projectFeatureColor
     *
     * @param string $projectFeatureColor
     * @return WebThemes
     */
    public function setProjectFeatureColor($projectFeatureColor)
    {
        $this->projectFeatureColor = $projectFeatureColor;
        return $this;
    }

    /**
     * Get projectFeatureColor
     *
     * @return string 
     */
    public function getProjectFeatureColor()
    {
        return $this->projectFeatureColor;
    }
}