<?php

namespace Proxies\__CG__\Entities;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class Ip2location extends \Entities\Ip2location implements \Doctrine\ORM\Proxy\Proxy
{
    private $_entityPersister;
    private $_identifier;
    public $__isInitialized__ = false;
    public function __construct($entityPersister, $identifier)
    {
        $this->_entityPersister = $entityPersister;
        $this->_identifier = $identifier;
    }
    /** @private */
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;

            if (method_exists($this, "__wakeup")) {
                // call this after __isInitialized__to avoid infinite recursion
                // but before loading to emulate what ClassMetadata::newInstance()
                // provides.
                $this->__wakeup();
            }

            if ($this->_entityPersister->load($this->_identifier, $this) === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            unset($this->_entityPersister, $this->_identifier);
        }
    }

    /** @private */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int) $this->_identifier["id"];
        }
        $this->__load();
        return parent::getId();
    }

    public function setIpFrom($ipFrom)
    {
        $this->__load();
        return parent::setIpFrom($ipFrom);
    }

    public function getIpFrom()
    {
        $this->__load();
        return parent::getIpFrom();
    }

    public function setIpTo($ipTo)
    {
        $this->__load();
        return parent::setIpTo($ipTo);
    }

    public function getIpTo()
    {
        $this->__load();
        return parent::getIpTo();
    }

    public function setCountryCode($countryCode)
    {
        $this->__load();
        return parent::setCountryCode($countryCode);
    }

    public function getCountryCode()
    {
        $this->__load();
        return parent::getCountryCode();
    }

    public function setCountryName($countryName)
    {
        $this->__load();
        return parent::setCountryName($countryName);
    }

    public function getCountryName()
    {
        $this->__load();
        return parent::getCountryName();
    }

    public function setRegionName($regionName)
    {
        $this->__load();
        return parent::setRegionName($regionName);
    }

    public function getRegionName()
    {
        $this->__load();
        return parent::getRegionName();
    }

    public function setCityName($cityName)
    {
        $this->__load();
        return parent::setCityName($cityName);
    }

    public function getCityName()
    {
        $this->__load();
        return parent::getCityName();
    }

    public function setLatitude($latitude)
    {
        $this->__load();
        return parent::setLatitude($latitude);
    }

    public function getLatitude()
    {
        $this->__load();
        return parent::getLatitude();
    }

    public function setLongitude($longitude)
    {
        $this->__load();
        return parent::setLongitude($longitude);
    }

    public function getLongitude()
    {
        $this->__load();
        return parent::getLongitude();
    }

    public function setZipCode($zipCode)
    {
        $this->__load();
        return parent::setZipCode($zipCode);
    }

    public function getZipCode()
    {
        $this->__load();
        return parent::getZipCode();
    }

    public function setTimeZone($timeZone)
    {
        $this->__load();
        return parent::setTimeZone($timeZone);
    }

    public function getTimeZone()
    {
        $this->__load();
        return parent::getTimeZone();
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'id', 'ipFrom', 'ipTo', 'countryCode', 'countryName', 'regionName', 'cityName', 'latitude', 'longitude', 'zipCode', 'timeZone');
    }

    public function __clone()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;
            $class = $this->_entityPersister->getClassMetadata();
            $original = $this->_entityPersister->load($this->_identifier);
            if ($original === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            foreach ($class->reflFields AS $field => $reflProperty) {
                $reflProperty->setValue($this, $reflProperty->getValue($original));
            }
            unset($this->_entityPersister, $this->_identifier);
        }
        
    }
}