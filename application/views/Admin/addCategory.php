<script type="application/javascript">
			$(document).ready(function () {
				$('form.ajax-form').on('submit', function() {
					var obj = $(this), // (*) references the current object/form each time
						url = obj.attr('action'),
						method = obj.attr('method'),
						data = {};
			
					obj.find('[name]').each(function(index, value) {
						// console.log(value);
						var obj = $(this),
							name = obj.attr('name'),
							value = obj.val();
						data[name] = value;
					});
			
					$.ajax({
						// see the (*)
						url: url,
						type: method,
						data: data,
						dataType:"json",
						success: function(response) {
							//alert(response.table);
							$('#<?= $tableid ?>').hide(1000);
							$('#<?= $tableid ?>').show(1000);
							$('#<?= $nameid ?>').append(response.option);
							$('#<?= $tableid ?>').append(response.table);
							console.log(response.table);
							// $("#feedback").html(data);
						}
					});
				return false; //disable refresh
			});
});
</script>
<div class="row-fluid">
		<h2>Create a news item</h2>
<!--<div class="alert alert-success hidden">Success!</div>-->
		<?php echo validation_errors(); ?>
		
		<?php echo form_open('admin/addCategory/create', array('class' => 'ajax-form')); ?>
		<label for="title">Category/SubCategory name*</label>
		<input type="input" name="title" /><br />
		
		<label for="parent">Parent Name</label>
		    <select id="<?= $nameid ?>" name="<?= $nameid ?>" >
		        <option value="0">Select One</option>
		        <?php foreach ($cats as $cat): ?>
		            <option value="<?= $cat->getProductTypesId() ?>"><?= $cat->getTypeName() ?></option>
		   
		<?php endforeach ?>
		 </select>
		<input type="submit" name="submit" value="Create Category" />
		</form>
</div>
<div class="row-fluid">
<table id="tblCategories" class="span12">
	<?php foreach ($cats as $cat): ?>
	<tr><td class="span1"><?= $cat->getProductTypesId() ?></td>
		<td class="span2"><?= $cat->getTypeName() ?></td>
		<td class="span2"><?= (!is_null($cat->getParent()))?$cat->getParent()->getTypeName():'none'; ?></td>
	</tr>
	<?php endforeach ?>
</table>
</div>

