
<script type="text/javascript" src="<?= base_url();?>assets/js/addFieldjs.js"></script>
<h2>Create a Field for Category</h2>
<!--<div class="alert alert-success hidden">Success!</div>-->
<?php echo validation_errors(); ?>
<div class="row span12">
	<div class="span4">
	<label for="parent">Parent Name</label>
		    <select id="<?= $nameid ?>" name="<?= $nameid ?>"  >
		        <option value="0" selected="selected">Select One</option>
		        <?php foreach ($cats as $cat): ?>
		            <option value="<?= $cat->getProductTypesId() ?>"><?= $cat->getTypeName()?>(<?= $cat -> getParent() -> getTypeName()?>) </option>
		   		<?php endforeach ?>
		 </select>
	</div>
	
	<?= form_open('admin/addField/createField', array('class' => 'ajax-form')); ?>
	<div id="nextForm" class="span4" >
		<input type="hidden" name="typeid"  id="hdntypeid" />
		<input type="hidden" name="lstcount"  id="hdnlstcount" />
		<label for="title">Field Name*</label>
			<input type="input" name="title" />
		<label for="sku">Field SKU*</label>
			<select name="sku">
				<option value="none">None</option>
				<option value="Katha">Katha</option>
				<option value="Sq.ft">Sq.ft</option>
				<option value="tk">Taka</option>
				<option value="unit">Unit</option>
				<option value="date">Date</option>
			</select>
		<label for="ui">Field UI*</label>
			<select name="ui" id="uitype">
				<option value="text">Text</option>
				<option value="chkbox">Check Box</option>
				<option value="list">Radio</option>
			</select>
		<label for="required">required*</label>
			<select name="required">
					<option value="no">No</option>
					<option value="yes">Yes</option>
			</select>
		<div id="uilist">
			<label for="lst">Options for List</label>
				<input type='text' id='listtext' value='' name='lsttxt1' />
				<input type="button" onclick="addInput()" name="add" value="+" />
				<input type="button" onclick="removeOption()" name="remove" value="-" /><br>
				<div id="text"></div>
			</div>
		<input type="submit" class="btn" name="submit" value="Create Field" />
		
	</div>
	</form>
</div>
<div class="row-fluid span12">
	<div class="span4">
	<table id="tblCategories" class="table">
		<?php foreach ($cats as $cat): ?>
		<tr><td class="span1"><?= $cat->getProductTypesId() ?></td>
			<td class="span2"><?= $cat -> getTypeName() ?></td>
			<td class="span2"><?= (!is_null($cat -> getParent())) ? $cat -> getParent() -> getTypeName() : 'none'; ?></td>
		</tr>
		<?php endforeach ?>
	</table>
	</div>
	<div class="span6">
		<div class="row" id="addtables">
			
	</div>
	
	</div>
</div>
