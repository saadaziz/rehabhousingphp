<style>
<?php if($editmode=='0'):?>
.myhidden{display:none;}
<?php else:?>
.alert{display:none;}
<?php endif ?>
</style>
<script type="text/javascript" charset="utf-8">
	var fields=<?=isset( $fields)?$fields:2?>;
	var init2='A';
	var charfields=String.fromCharCode(init2.charCodeAt(0) + fields-1);;
	</script>
<script type="text/javascript" src="<?= base_url();?>assets/js/addProduct.js"></script>
<script type="text/javascript" src="<?= base_url();?>assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?= base_url();?>assets/js/tinymce/tinymce.min.js"></script>
<div class="container" >
	<h2>Create Product</h2>
	<div class="alert alert-success myhidden"></div>
	<div class="alert alert-danger myhidden"><?php echo validation_errors(); ?></div>
	<div class="row-fluid">
		<div class="row span5">
			<div class="col-xs-9">
			<div id="radiodiv">
				
			<?php foreach ($heads as $head): ?>
				<input type='radio' name='choices' style="margin:-4px 0 0 0;" <?= (isset($headid)?($headid==$head->getProductTypesId())?'checked=checked':'':'' )?> value="<?= $head->getProductTypesId() ?>">&nbsp;&nbsp;<?= $head->getTypeName()?>
			<?php endforeach ?>
			</div>
			<div class="inithid myhidden">
			<table >
				<tr>
					<td>
						<label for="category">Category</label>
					</td>
					<td>
					    <select id="category" name="category" class="form-control input-xsmall"  >
					    	<?php if(isset($catid)) {
										echo '<option value="0" selected="selected">Select One</option>'; 
										foreach ($cats as $key ) {
											echo '<option value="'.$key->getProductTypesId().'" '.(($catid==$key->getProductTypesId())?'selected="selected"':'').'>'.$key->getTypeName().'</option>';
										}
									 }?>
					 	</select>
					 </td>
				</tr>
			 </table>
			 <div id="nextForm myhidden" >
			 <?= form_open_multipart(base_url().'AddPost/Step2', array('id' => 'product-form','novalidate'=>'novalidate')); ?>
			 <fieldset>
			 
			 	<input type="hidden" name="pid"  id="hdnpid" value='<?=isset($pid)?$pid:'0'?>' />
			 	<input type="hidden" name="editmode"  id="hdneditmode" value='<?=isset($editmode)?$editmode:'0'?>' />
				<input type="hidden" name="catid"  id="hdncatid" value='<?=isset($catid)?$catid:''?>' />
				<input type="hidden" name="lstcount"  id="hdnlstcount" value='<?=isset($unitcount)?$unitcount:'2'?>' />
				<div class="control-group">
				<table>
					<tr>
						<td>
							<label for="pname">Title <span class="required">*</span></label>
						</td>
						<td>
							<div class="controls">
								<input id="pname" type="text" name="pname" class="form-control" maxlength="250" value="<?=set_value('pname',(isset( $pname)?$pname:'')); ?>" placeholder="Title here" />
							</div>
						</td>
					</tr>
				</table>
				</div>
			

				<div id="extrafields" class="myhidden control-group">
					<?= isset($extrafields)?$extrafields:'' ?>
				</div>
				
				<div id="forunitdet" class="row-fluid myhidden">
					<div class="">
						<table id="tblunitdetails" class="table table-stripped" >
							<tr>
								<td class="tdwidth2">Unit Type</td>
								<td class="tdwidth2">Bedroom</td>
								<td class="tdwidth2">Bathroom</td>
								<td class="tdwidth2">Size(sqft)</td>
								<td class="tdwidth2">Price(per sqft)</td>
								<td class="tdwidth2">Floor Plan</td>
								<td>
										<div class="span1">
											<input type="button" id="addubit" onclick="addInput()" name="add" value="+" />
											<input type="button" id="removeubit" onclick="removeOption()" name="remove" value="-" />
										</div>
									</td>
							</tr>
							<tr id="trid1">
								<?php $unitcount=(isset($unitcount)?$unitcount:1)?>
								<?php $x='A'?>
									<td class="tdwidth2">Type <?=$x?> <input class="tdwidth" type="hidden" name="type1"  id="hdntype<?=$x?>" value='Type <?=$x?>' /></td>
									<td class="tdwidth2"><input class="tdwidth" id="punitbed1" type="text" class='input-mini' name="punitbed1" maxlength="20" placeholder="Bedroom" value="<?=isset($unitdetails)? $unitdetails[0]->getBedroom():''?>" /></td>
									<td class="tdwidth2"><input class="tdwidth" id="punitbath1" type="text" class='input-mini' name="punitbath1" maxlength="20" placeholder="Bathroom" value="<?=isset($unitdetails)?$unitdetails[0]->getBathroom():''?>"/></td>
									<td class="tdwidth2"><input class="tdwidth" id="punitSize1" type="text" class='input-mini' name="punitSize1" maxlength="20"  placeholder="Size" value="<?=isset($unitdetails)?$unitdetails[0]->getSize():''?>" /></td>
									<td class="tdwidth2"><input class="tdwidth" id="punitPrice1" type="text" name="punitPrice1" class='input-mini' maxlength="20" placeholder="Price" value="<?=isset($unitdetails)?$unitdetails[0]->getPrice():''?>" /></td>
									<td class="tdwidth2" colspan="2">
										<div class="fileuploader"> <input type="text" class="filename " placeholder="<?=isset($unitdetails)?(!is_null($unitdetails[0]->getImage())?$unitdetails[0]->getImage()->getImageName():'No file Selected'):''?>"/> <input type="button" name="file" class="filebutton" value="Browse" /><input type="file" name="pUnitimg1" id="pUnitimg1" /></div>
									</td>	<!--<input type="file" name="pUnitimg1" id="pUnitimg1"  /></td>-->
									
								</tr>
								<?php if($unitcount>1):?>
									<?php for($i=2;$i<=$unitcount;$i++):?>
										<tr id="trid<?=$i?>">
											<td class="tdwidth2">Type <?= ++$x?><input type="hidden" name="type<?=$i?>"  id="hdntype<?=$x?>" value='Type <?=$x?>' /></td>
											<td class="tdwidth2"><input class="tdwidth" id="punitbed<?=$i?>" type="text" class='input-mini' name="punitbed<?=$i?>" maxlength="20" placeholder="Bedroom" value="<?=isset($unitdetails)? $unitdetails[$i-1]->getBedroom():''?>"/></td>
											<td class="tdwidth2"><input class="tdwidth" id="punitbath<?=$i?>" type="text" class='input-mini' name="punitbath<?=$i?>" maxlength="20" placeholder="Bathroom" value="<?=isset($unitdetails)? $unitdetails[$i-1]->getBathroom():''?>"/></td>
											<td class="tdwidth2"><input class="tdwidth" id="punitSize<?=$i?>" type="text" class='input-mini' name="punitSize<?=$i?>" maxlength="20"  placeholder="Size" value="<?=isset($unitdetails)? $unitdetails[$i-1]->getSize():''?>" /></td>
											<td class="tdwidth2"><input class="tdwidth" id="punitPrice<?=$i?>" type="text" name="punitPrice<?=$i?>" class='input-mini' maxlength="20" placeholder="Price" value="<?=isset($unitdetails)? $unitdetails[$i-1]->getPrice():''?>"/></td>
											<td class="tdwidth2">
											<div class="fileuploader"> <input class="tdwidth" type="text" class="filename" placeholder="<?=isset($unitdetails)?(!is_null($unitdetails[$i-1]->getImage())?$unitdetails[$i-1]->getImage()->getImageName():'No file Selected'):''?>"/> <input type="button" name="file" class="filebutton" value="Browse" /><input type="file" name="pUnitimg<?=$i?>" id="pUnitimg<?=$i?>" /></div>
											</td>
										</tr>
									<?php endfor?>
								<?php endif?>
							
						</table>
					</div>
					
				</div>
				<div class="control-group" >
					<label class="control-label" for="pnegotiable">Price negotiable?<span class="required">*</span></label>
					<div class="controls">
						<select id="pnegotiable" name="pnegotiable">
							<option value="no" <?=isset($nego)?($nego=='no')?'selected="Selected"':'':''?> >No</option>
							<option value="yes" <?=isset($nego)?($nego=='yes')?'selected="Selected"':'':''?> >Yes</option>
			 			</select>
			 		</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="pdesc">Description<span class="required">*</span></label>
					<div class="controls">
						<?= form_textarea( array('class'=>'mceEditor','name' => 'pdesc', 'rows' => '20', 'cols' => '60', 'value' => set_value('pdesc',(isset( $pdesc)?$pdesc:''))  ) )?>
					</div>
				</div>

				
				<br>
				<div class="form-actions">
					<input type="submit" class="btn btn-danger" name="submit" value="<?=($editmode==1)?'Edit Post':'Add Post' ?>" />
					</div>
				</fieldset>
		</form>
		</div>
	</div>
	</div>
	<div class="col-xs-3">
		Right Contant
	</div>
	
	</div>
			 
	
	</div>

	
</div>
<link rel="stylesheet" href="<?= base_url();?>assets/css/jquery-ui-1.10.3.custom.min.css">
<script src="<?= base_url();?>assets/js/jquery.validate.min.js"></script>
<script language="javascript" type="text/javascript">
        tinyMCE.init({
           theme : "modern",
            mode : "textareas",
            editor_selector :"mceEditor",
            plugins : "insertdatetime,preview,visualchars,nonbreaking",
            theme_advanced_buttons2_add: 'separator,forecolor,backcolor',
            theme_advanced_buttons3_add: 'insertdate,inserttime,preview,visualchars,nonbreaking',
            theme_advanced_disable: "styleselect,formatselect,removeformat",
            plugin_insertdate_dateFormat : "%Y-%m-%d",
            plugin_insertdate_timeFormat : "%H:%M:%S",
            theme_advanced_toolbar_align : "left",
            theme_advanced_resize_horizontal : false,
            theme_advanced_resizing : true,
            apply_source_formatting : true,
            spellchecker_languages : "+English=en",
            extended_valid_elements :"border=0|alt|title|width|height|align|name],"
            +"p,"
            +"i,"
            +"b,"
            +"br,"
            +"em",
            invalid_elements: "script,span,tr,td,tbody,font,body,table,img,a[href|target|name|title]"
           /* mode : "textareas",
            editor_selector :"mceEditor",
    		plugins: [
		        "advlist autolink lists link image charmap print preview anchor",
		        "searchreplace visualblocks code fullscreen",
		        "insertdatetime media table contextmenu paste"
		    ],
		    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
			*/
        
        });
        $(document).ready(function () {

	});
</script>  
<style>

.fileuploader {
    position: relative;
    display: inline-block;
    overflow: hidden;
    cursor: default;
}

.filename {
    float: left;
    display: inline-block;
    outline: 0 none;
    width: 100px;
    overflow: hidden;
    border: 1px solid #CCCCCC;
    color: #777;
    text-shadow: 1px 1px 0px #fff;
    border-radius: 5px 0px 0px 5px;
    box-shadow: 0px 0px 1px #fff inset;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding: 6px 10px;
}

.filebutton {
    float: left;
    display: inline-block;
    outline: 0 none;
    cursor: pointer;
    border: 1px solid #CCCCCC;
    border-radius: 5px 5px 5px 5px;
    box-shadow: 0 0 1px #fff inset;
    color: #555555;
    margin-left: 3px;
    padding: 6px 12px;
    background: #DDDDDD;
    background:-moz-linear-gradient(top, #EEEEEE 0%, #DDDDDD 100%);
    background:-webkit-gradient(linear, left top, left bottom, color-stop(0%, #EEEEEE), color-stop(100%, #DDDDDD));filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#EEEEEE', endColorstr='#DDDDDD', GradientType=0);
}

.fileuploader input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    border: 0;
    cursor: pointer;
    filter:alpha(opacity=0);
    -moz-opacity: 0;
    -khtml-opacity: 0;
    opacity: 0;
    margin: 0;
    padding: 0;
    width:100%;
}
    </style>
    