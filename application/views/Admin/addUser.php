<script type="text/javascript" src="<?= base_url();?>assets/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<?= base_url();?>assets/js/addUser.js"></script>
<script src="<?= base_url();?>assets/js/jquery.validate.min.js"></script>
<div id="defdiv" style="height: 40px;margin-bottom: 20px;"></div>
<div class="alert alert-danger"></div>
<div class="alert alert-success"></div>
<div class="container" style="margin-top:10px;">
	<div class="row-fluid">
		
		<?= form_open('Admin/addUser/createUser/', array('id' => 'user-form')); ?>
				<div class="span5 col-xs-9">
				<div class="contactLeft">
            	<h2>Post Your Property Requirement</h2>
                <h4>Get matching properties delivered on your email</h4>
                <div class="post1">1</div>
                <div class="post2">Tell us your requirements</div>
                <span style="float:right; margin-right:8px;">Fields marked with an asterik are mandatory</span>
        </div>	
				 <table class="col-xs-8" style="margin-left: 100px;">
						<tr>
							<td >
								<b>Company</b>
							</td>
							<td>
								<input type="checkbox" name="iscompany" id="iscompany" style="margin:-4px 0 0 0;" value="off"/>
							</td>
						</tr>
						<tr>
							<td width="200px">
								<label for="name" id="lblname">Name/Company name <span class="required">*</span></label>
							</td>
							<td>
								<?=form_error('name'); ?>
							<input class="form-control" id="name" type="text" name="name" maxlength="100" value="<?=set_value('name'); ?>"  placeholder="Name here"/>
							</td>
						</tr>
					</table>
					
					<table class="divcompany col-xs-8"  style="margin-left: 100px;">
						<tr>
							<td width="200px">
								<label for="contact_person">Contact person<span class="required">*</span></label>
							</td>
							<td>
								<input class="form-control" id="contact_person" type="text" name="contact_person" value="<?=set_value('contact_person'); ?>" placeholder="Your Name here" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="designation">Designation<span class="required">*</span></label>
							</td>
							<td>
								<input class="form-control" id="designation" type="text" name="designation" value="<?=set_value('designation'); ?>" placeholder="Designation no here" />
							</td>
						</tr>
					</table>
					
					
					 <table class="col-xs-8"  style="margin-left: 100px;">
						<tr>
							<td width="200px">
								<label for="password">Password <span class="required">*</span></label>
							</td>
							<td>
								<?=form_error('password'); ?>
								<input class="form-control" id="password" type="password" name="password" maxlength="50" value="<?=set_value('password'); ?>" placeholder="Type your password here" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="retype_password">Retype password<span class="required">*</span></label>
							</td>
							<td>
								<?=form_error('retype_password'); ?>
								<input class="form-control" id="retype_password" type="password" name="retype_password" maxlength="50" value="<?=set_value('retype_password'); ?>" placeholder="Retype your password" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="email">Email <span class="required">*</span></label>
							</td>
							<td>
								<?=form_error('email'); ?>
								<input class="form-control" id="email" type="text" name="email" maxlength="50" value="<?=set_value('email'); ?>" placeholder="Type your email here" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="cell_no">Cell No <span class="required">*</span></label>
							</td>
							<td>
								<?=form_error('cell_no'); ?>
								<input class="form-control" id="cell_no" type="text" name="cell_no" value="<?=set_value('cell_no'); ?>" placeholder="Type your Cell no here" />
							</td>
						</tr>
					</table>
					
				
					
					<table class="divcompany col-xs-8"  style="margin-left: 100px;">
						<tr>
							<td width="200px">
								<label for="address">Address<span class="required">*</span></label>
							</td>
							<td>
								<?= form_textarea( array( 'name' => 'address', 'rows' => '5', 'cols' => '50', 'value' => set_value('address') ) )?>
							</td>
						</tr>
						<tr>
							<td><label><b>Member</b></label></td>
							<td>
								<input type="checkbox" id="chkrehab" name="chkrehab" style="margin:-4px 0 0 0;">&nbsp;&nbsp;Rehab
								<input type="checkbox" id="chkblda"  name="chkblda"  style="margin:-4px 0 0 0;">&nbsp;&nbsp;BLDA
							</td>
						</tr>
						<!--<p>
							<label for="ufile">Upload Logo <span class="required">*</span></label>
							<input type="file" name="ufile"  />
						</p>-->
						
						<tr>
							<td>
								<label for="phone_no">Phone No <span class="required">*</span></label>
							</td>
							<td>
								<input class="form-control" id="phone_no" type="text" name="phone_no" value="<?=set_value('phone_no'); ?>" placeholder="Your landline no here" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="website">Website Address</label>
							</td>
							<td>
								<input class="form-control" id="website" type="text" name="website" value="www.<?=set_value('website'); ?>" placeholder="Website address here" />
							</td>
						</tr>
					</table>
					<table class="col-xs-8"  style="margin-left: 100px;">
						<tr>
							<td style="text-align: right;">
								<input type="submit" class="btn btn-danger" name="submit" value="Create User" />
							</td>
						</tr>
					</table>	
				</div>
		</form>
		<div class="span5 col-xs-3">
		Right Contant
		</div>
	</div>

	
</div>
