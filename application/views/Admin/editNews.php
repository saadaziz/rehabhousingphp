<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="application/javascript">
			$(document).ready(function () {
				$('form.ajax-form').on('submit', function() {
					var obj = $(this), // (*) references the current object/form each time
						url = obj.attr('action'),
						method = obj.attr('method'),
						data = {};
			
					obj.find('[name]').each(function(index, value) {
						// console.log(value);
						var obj = $(this),
							name = obj.attr('name'),
							value = obj.val();
						data[name] = value;
					});
			
					$.ajax({
						// see the (*)
						url: url,
						type: method,
						data: data,
						//dataType:"json",
						success: function(response) {
							//alert(response.table);
							$('#tblCategories').append(response);
							console.log(response.table);
							// $("#feedback").html(data);
						}
					});
		return false; //disable refresh
	});
});
</script>
  <script language="javascript" type="text/javascript">
        tinyMCE.init({
           theme : "modern",
            mode : "textareas",
            editor_selector :"mceEditor",
            plugins : "insertdatetime,preview,visualchars,nonbreaking",
            theme_advanced_buttons2_add: 'separator,forecolor,backcolor',
            theme_advanced_buttons3_add: 'insertdate,inserttime,preview,visualchars,nonbreaking',
            theme_advanced_disable: "styleselect,formatselect,removeformat",
            plugin_insertdate_dateFormat : "%Y-%m-%d",
            plugin_insertdate_timeFormat : "%H:%M:%S",
            theme_advanced_toolbar_align : "left",
            theme_advanced_resize_horizontal : false,
            theme_advanced_resizing : true,
            apply_source_formatting : true,
            spellchecker_languages : "+English=en",
            extended_valid_elements :"border=0|alt|title|width|height|align|name],"
            +"p,"
            +"i,"
            +"b,"
            +"br,"
            +"em",
            invalid_elements: "script,span,tr,td,tbody,font,body,table,img,a[href|target|name|title]"
        
        });
        
    </script>  
<div class="row-fluid">
		<h2>Create a news item</h2>
<!--<div class="alert alert-success hidden">Success!</div>-->
		<?php echo validation_errors(); ?>
		
		<?php echo form_open('admin/addNews/update', array('class' => 'ajax-form1')); ?>
		<label for="title">News Title*</label>
		<input type="input" name="news_title" value="<?= $news->getNewsTitle() ?>"/><br />
		<input type="hidden" name="id" value="<?= $news->getNewsId() ?>"/><br />
		
		
		<!-- Place this in the body of the page content -->
		<label for="parent">Description</label>
			<?= form_textarea( array('class'=>'mceEditor','name' => 'description', 'rows' => '20', 'cols' => '10', 'value' => $news->getDescription() ) )?><br/>
		<input type="submit" name="submit" value="Update" />
		</form>
</div>