<script type="text/javascript">
var webroot = '/rehabhousing/';
window.localStorage.setItem('webroot',webroot);

function initialize(x) {
	var marker;
	var map;
	  var mapOptions = {
	    zoom: 15,
	    center: x
	  };
	
	  map = new google.maps.Map(document.getElementById('map-canvas'),
	          mapOptions);
	
	  marker = new google.maps.Marker({
	    map:map,
	    draggable:true,
	    animation: google.maps.Animation.DROP,
	    position: x
	  });
  //google.maps.event.addListener(marker, 'click', toggleBounce);
  google.maps.event.addListener(marker, 'dragend', function(evt){
    document.getElementById('hdnlat').value = String(evt.latLng.lat().toFixed(3));
    document.getElementById('hdnlong').value =String(evt.latLng.lng().toFixed(3)) ;

	});
}
$(document).ready(function () {
	var timing=1000;
	$("#gmaps").hide();
	
var divid=0;
	$('#ddlDivision').on('change', function () {
		divid = $(this).val(); 
		if(divid!="0")
		{
			$("#parazone").show();
			$.post(webroot+ "admin/addproduct/getZones/"+divid, function( data ) {
				$( "#pzone" ).html( data );
			});
		}
		else
			$("#pzone").hide();				
		$("#hdndivision").val(divid);
	});
	$('#pzone').on('change', function () {
		$("#gmaps").hide();
		var zoneid = $(this).val(); 
		if(zoneid!="0")
		{
			$.post(webroot+ "admin/addproduct/setlatlang/"+zoneid, function( data ) {
				var parliament = new google.maps.LatLng(data.lat, data.long);
 				document.getElementById('hdnlat').value = (data.lat);
    			document.getElementById('hdnlong').value =(data.long);
				initialize(parliament);
				//console.log(parliament);
			},'json');
			$("#gmaps").show(timing);
		}
		else
			$("#pzone").hide();
		$("#hdndivision").val(divid);
	});
		$('#location-form').validate({
	    rules: {
	      address: {
	      	minlength: 10,
	        required: true
	      },
	      division: {
	      	required:true	      	
	      }
	    },
		  showErrors: function(errorMap, errorList) {
		    $.each(this.successList, function(index, value) {
		      return $(value).popover("hide");
		    });
		    return $.each(errorList, function(index, value) {
		      var _popover;
		      console.log(value.message);
		      _popover = $(value.element).popover({
		        trigger: "manual",
		        placement: "right",
		        content: value.message,
		        template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
		      });
		      _popover.data("popover").options.content = value.message;
		      return $(value.element).popover("show");
		    });
		  }
		 	
	  });
	});
</script>
<style>#map-canvas{width: 600px; height: 500px;}</style>
<?php if(isset($pid)):?>
	
	<h2>Insert Location for <?= $pname?></h2>
 <?= form_open_multipart(base_url().'AddPost/Step3', array('id' => 'location-form','novalidate'=>'novalidate')); ?>
 <input type="hidden" name="pid"  id="hdnpid" value='<?=isset($pid)?$pid:'0'?>' />
				<input type="hidden" name="divid"  id="hdndivision" value='<?=isset($selectedDiv)?$selectedDiv:''?>'/>
				<input type="hidden" name="lat"  id="hdnlat" value='<?= isset($lat)?$lat:'' ?>'/>
				<input type="hidden" name="long"  id="hdnlong" value='<?= isset($long)?$long:'' ?>'/>
			 	<input type="hidden" name="editmode"  id="hdneditmode" value='<?=isset($editmode)?$editmode:'0'?>' />
	<fieldset>
		
		<div class="control-group">
					<label for="division" class="control-label">Division <span class="required">*</span></label>
					<div class="controls">
					<?= form_dropdown('division', $division, (isset($selectedDiv)?$selectedDiv:'0'),'id="ddlDivision"');?>
					</div>
				</div>
		<div class="control-group" id="parazone" class="myhidden">
			<label class="control-label" for="pzone">Area <span class="required">*</span></label>
			<div class="controls">
				<select id="pzone" name="pzone">
					<?php if($editmode!='0'): ?>
						<?php echo '<option value="0" selected="selected">Select One</option>'; 
						foreach ($zones as $key ) {
							echo '<option value="'.$key->getLocId().'" '.(($zoneid==$key->getLocId())?'selected="selected"':'').'>'.$key->getNameEn().'</option>';
						}?>
						<?php endif ?>
			 	</select>
			</div>
		</div>
		<div class="span5">
			<div id="gmaps" class="myhidden">
					<div id='map-canvas'></div>
				</div>
			
		</div>
		<div class="control-group">
			<label class="control-label" for="address">Address<span class="required">*</span></label>
				<div class="controls span4">
					<?= form_textarea( array( 'name' => 'address', 'rows' => '5', 'cols' => '60', 'value' => set_value('address',(isset( $address)?$address:'')) ) )?>
				</div>
		</div>
		<div class="form-actions">
					<input type="submit" class="btn btn-danger" name="submit" value="Insert Location" />
					</div>
	</fieldset>
</form>

	<?php endif ?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyBo5zgSirVXMgcjei16THadNtnj3f0RB6Q"></script>
<script src="<?= base_url();?>assets/js/jquery.validate.min.js"></script>
<?=isset($js)? $js:''?>