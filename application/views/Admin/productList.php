<script>$('body').tooltip({
    selector: '[rel=tooltip]'
}); </script>

	<button type="button" id="approvebutton" class="btn btn-primary btn-sm" >Approve</button>
	<button type="button" id="rejectbutton" class="btn btn-danger btn-sm" >Reject</button>
	<button type="button" id="topbutton" class="btn btn-warning btn-sm" >Top project</button>
	<a class="btn btn-info btn-sm" href="<?=base_url()?>admin/addproduct" target="_blank">Add More</a>
	<div style="display: none;" id="divTop" class="span6">
		<?= form_open_multipart('admin/productlist/insertTopProject', array('id' => 'topProduct-form')); ?>
		<input type="hidden" id="hdnpid" name="pid" />
			 <fieldset>
				<table class="table-responsive span5">
					<tr><td>Title for top Project</td><td><input type="text" name="txttitle"  class="form-control input-xsmall" /></td></tr>
					<tr><td>Location for top Project</td><td><input type="text" name="txtloc" class="form-control input-xsmall" /></td></tr>
					<tr><td>Price for top Project</td><td><input type="text" name="txtprice" class="form-control input-xsmall" /></td></tr>
					<tr><td>Bedroom Bathroom for top Project</td><td><input type="text" name="txtbedbath" class="form-control input-xsmall" /></td></tr>
					<tr><td>Image for top Project</td><td><input type="file" name="imgtop" /></td></tr>
					<tr><td><button type="submit" class="btn btn-default btn-success">Submit</button></td></tr>
				</table>
			</fieldset>
		</form>
	</div>
	<table id="tblProducts" class="table">
		<tr><th><input type="checkbox" id="checkall" /></th><th>ID</th><th>Title</th><th>Category</th><th>Top?</th><th>Remaining</th><th>Approved?</th><th>User</th><th>Price</th><th>Inserted</th><th></th></tr>
		<?php foreach ($products as $product): ?>
		<tr id="<?= $product->getProductId() ?>"><td class="span1"><input type="checkbox" /></td>
			<td class="span1"><?= $product->getProductId() ?></td>
			<td class="span2"><?= $product -> getTitle() ?></td>
			<td class="span2"><div class="tl" data-placement="bottom" data-toggle="tooltip" title="<?= $product ->getType()->getParent()->getTypeName()?>"><?= $product -> getType()->getTypeName() ?></div></td>
			<td class="span2"><?=($product -> getTopProject() == 'yes')?'<button type="button" class="btn btn-success btn-sm"> '.$product -> getTopProject().'</button>':
                    '<button type="button" class="btn btn-danger btn-sm">'. $product -> getTopProject().'</button>'?></td>
			<td class="span2">
				<?php
			     $interval = $product->getActiveTo()->diff(new DateTime());
				echo $interval->m.' months '. $interval->d.' days';?></td>
			<td class="span2">
				<?php if ($product -> getApproval() == 'approved'): ?>
					<button type="button" class="btn btn-success btn-sm">Accepted</button>
				<?php elseif ($product->getApproval() == 'pending'): ?>
						<button type="button" class="btn btn-warning btn-sm">Pending</button>
				<?php else: ?>
					<button type="button" class="btn btn-danger btn-sm">Rejected</button>
				<?php endif; ?></td>
			<td class="span2"><?= $product -> getUser()->getName() ?></td>
			<td class="span2"><?= $product -> getPrice() ?></td>
			<td class="span2"><?= $product -> getInsertTime()->format('Y-m-d') ?></td>
			<td><a class="btn btn-info btn-mini" target="_blank" href="<?=base_url()?>admin/addproduct/edit/<?= $product->getProductId() ?>">Edit</a></td>
		</tr>
		<?php endforeach ?>
	</table>
	<?= $this->pagination->create_links();?>
</div>
<script>
var webroot = '/rehabhousing/';
$("#approvebutton").click(function(){
	var ids='';
	$('#tblProducts tr').filter(':has(:checkbox:checked)').each(function() {
        	$tr = $(this);
        if(this.id!='')
        	ids+=this.id+'-';
	});
	$.post( webroot+"admin/productlist/setapproved/"+ids+"/approved/"+<?=$offset?>, function( data ) {
		//console.log(data.htmls);
		//alert(data.htmls);
		$( "#tblProducts" ).html( data.htmls );		
		
	},'json');
	
});
$("#rejectbutton").click(function(){
	var ids='';
	$('#tblProducts tr').filter(':has(:checkbox:checked)').each(function() {
        	$tr = $(this);
        if(this.id!='')
        	ids+=this.id+'-';
	});
	$.post( webroot+"admin/productlist/setapproved/"+ids+"/rejected/"+<?=$offset?>, function( data ) {
		//console.log(data.htmls);
		//alert(data.htmls);
		$( "#tblProducts" ).html( data.htmls );		
		
	},'json');
	
});
$("#topbutton").click(function(){
	var ids='';
	$('#tblProducts tr').filter(':has(:checkbox:checked)').each(function() {
        	$tr = $(this);
        if(this.id!='')
        	ids+=this.id+'-';
	});
	$.post( webroot+"admin/productlist/setapproved/"+ids+"/topproject/"+<?=$offset?>, function( data ) {
		console.log(data);
		$( "#hdnpid" ).val(data.keys);
		alert(data.keys);
		$( "#tblProducts" ).html( data.htmls );		
		if(data.viewtop=='1')
			$('#divTop').show(1000);
		else
			$('#divTop').css({ "display": 'none'});
	},'json');
	
});
$("#checkall").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });
/*$('#topProduct1').on('submit', function() {
	if(confirm('Do u want to input that field')){
		var obj = $(this), 
		url = obj.attr('action'),
		method = obj.attr('method'),
		data = {};
		obj.find('[name]').each(function(index, value) {
		var obj = $(this),
		name = obj.attr('name'),
		value = obj.val();
		data[name] = value;
		});
		$.ajax({
			url: url,
			type: method,
			data: data,
			success: function(response2) {
				console.log(response2);
			}		
		});
		return false; //disable refresh
	}
});*/
</script>