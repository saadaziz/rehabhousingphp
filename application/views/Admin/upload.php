<!-- Bootstrap styles -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css">
<!-- Generic page styles -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/styleforFileUp.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.fileupload-ui-noscript.css"></noscript>
	<div>
	<b>Images for :<?=$pname?></b><?=$saad?>
	</div>
<div id="fileupload">
       <!-- Upload function on action form -->
	<form action="<?=base_url()?>admin/addProduct/do_upload" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="productid"  id="hdncatid" value="<?=$pid?>" />
		<input type="hidden" name="imgcount"  id="hdnimgcount" value="2" />
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="userfile" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel upload</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" class="toggle">
                <!-- The loading indicator is shown during file processing -->
                <span class="fileupload-loading"></span>
            </div>
            <!-- The global progress information -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress information -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table"><tbody class="files"><tbody>
        	 <?php if($edit==1):?>
        	 	<?php foreach($imgs as $img):?>
        	 		<?php if($img->getImageType()=='floorplan') continue;?>
	        	 <tr class="template-download fade in">
			        <td>
			            <span class="preview">
			                
			                    <a href="<?=$img->getImageWatermark()?>" title="<?=$img->getImageName()?>" download="<?=$img->getImageName()?>" data-gallery=""><img height="100px" width="100px" src="<?=base_url().'uploads/watermark/'.$img->getImageName()?>"></a>
			                
			            </span>
			        </td>
			        <td>
			            <p class="name">
			                
			                    <a href="<?=$img->getImageWatermark()?>" title="<?=$img->getImageName()?>" download="<?=$img->getImageName()?>" data-gallery=""><?=$img->getImageName()?></a>
			                
			            </p>
			            
			        </td>
			        <td>
			            <span class="size"><?=$img->getImageHeight()?>X<?=$img->getImageWidth()?></span>
			        </td>
			        <td>
			            
			                <button class="btn btn-danger delete" data-type="DELETE" data-url="<?=base_url()?>upload/deleteImage/<?=$img->getImageId()?>">
			                    <i class="glyphicon glyphicon-trash"></i>
			                    <span>Delete</span>
			                </button>
			                <input name="delete" value="1" class="toggle" type="checkbox">
			            
			        </td>
	    		</tr>
   			<?php endforeach; ?>
        <?php endif; ?>
        </tbody></table>
       
    </form>
</div>

            <!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            {% if( (file.error)){ %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <p class="size">{%=o.formatFileSize(file.size)%}</p>
            {% if (!o.files.error) { %}
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            {% } %}
        </td>
        <td>
            {% if (!o.files.error && !i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
	
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery=""><img src="{%=file.thumbnailUrl%}" width="100px" height="100px"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?= base_url(); ?>assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?= base_url(); ?>assets/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?= base_url(); ?>assets/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="<?= base_url(); ?>assets/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?= base_url(); ?>assets/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload-image.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="<?= base_url(); ?>assets/js/main.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="js/cors/jquery.xdr-transport.js"></script>
<![endif]-->