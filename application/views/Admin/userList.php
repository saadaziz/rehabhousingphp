<div class="row-fluid span12">
		<table class="table table-striped" id="tblusers">
			<tr><th>Name</th><th>Email</th><th>Company</th><th>Mobile No</th><th>Change pass</th><th>Active</th><th>Premium</th><th>Login</th></tr>
			<?php foreach ($users as $user): ?>
			<tr id="<?= $user->getUserId() ?>">
				<td><?= $user->getUserId() ?></td>
				<td><?= $user->getName() ?></td>
				<td><?= (!is_null($user->getCompany()))?'yes('.$user->getCompany()->getPhoneNo().')':'no';  ?></td>
				<td><?= $user->getCellNo() ?></td>
				<td ><div class="input-append">
					<input class="span2" id="txt<?= $user->getUserId() ?>" type="password" />
					<button type="button" class="btn  btn-sm btn-warning password">Change</button>
					</div></td>
					
				<td><?= ($user->getActive()=='active')?'<button type="button" class="btn btn-success btn-sm active">Active</button>':
				'<button type="button" class="btn btn-danger btn-sm active">Inactive</button>' ?></td>
				<td><?= ($user->getPremium()=='yes')?'<button type="button" class="btn btn-success btn-sm premium">Premium</button>':
				'<button type="button" class="btn btn-danger btn-sm premium">Regular</button>' ?></td>
				<td><a type="button" class="btn btn-info btn-sm login">Login</a></td>
			</tr>
			<?php endforeach ?>
		</table>
			<?= $this->pagination->create_links();?>
	</div>
	<script>
var webroot = '/rehabhousing/';
$(".active").click(function(){
	var parent =$(this).parent();
	var id=parent.parent().attr('id');
	$.post( webroot+"admin/userList/setActive/"+id+"/active/"+<?=$offset?>, function( data ) {
		parent.html( data.htmls );
	},'json');
});
$(".premium").click(function(){
	var parent =$(this).parent();
	var id=parent.parent().attr('id');
	$.post( webroot+"admin/userList/setActive/"+id+"/premium/"+<?=$offset?>, function( data ) {
		parent.html( data.htmls );
	},'json');
});
$(".password").click(function(){
	var parent =$(this).prev();
	var id=parent.attr('id');
	//alert($("#"+id).val());
	$.post( webroot+"admin/userList/changePass/"+id+"/"+$("#"+id).val()+"/"+<?=$offset?>, function( data ) {
		
	},'json');
});
$(".login").click(function(){
	var parent =$(this).parent();
	var id=parent.parent().attr('id');
	$.post( webroot+"admin/userList/login/"+id+"/premium/"+<?=$offset?>, function( data ) {
		window.location=webroot;
	},'json');
});
</script>