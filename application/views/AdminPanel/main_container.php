<!-- Main content starts -->


    <!-- Sidebar ends -->

  	<!-- Main bar -->
  	<div class="mainbar">
      
	    <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	        <h2 class="pull-left">Dashboard</h2>


			<!-- Breadcrumb -->
			<div class="bread-crumb pull-right">
			  <a href="index.html"><i class="icon-home"></i> Home</a> 
			  <!-- Divider -->
			  <span class="divider">/</span> 
			  <a href="#" class="bread-current">Dashboard</a>
			</div>

			<div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->



	    <!-- Matter -->

	    <div class="matter">
        <div class="container">

          <!-- Today status. jQuery Sparkline plugin used. -->

          <div class="row">
            <div class=" col-xs-12 col-sm-12 col-md-12"> 
              <!-- List starts -->
             <a href="#"><div class="message-box"><img class="focus" src="<?= base_url();?>assets/adminPanel/style/images/visitor-feedback.png" />Visitors<lable class="counter">2</lable></div></a>
             <a href="#"><div class="message-box"><img class="focus" src="<?= base_url();?>assets/adminPanel/style/images/sale.png" />Sale Ads<lable class="counter">12</lable></div></a>
             <a href="#"><div class="message-box"><img class="focus" src="<?= base_url();?>assets/adminPanel/style/images/rental.png" />Rental Ads<lable class="counter">4</lable></div></a>
             <a href="#"><div class="message-box"><img class="focus" src="<?= base_url();?>assets/adminPanel/style/images/jobs.png" />Job Ads<lable class="counter">3</lable></div></a>
             <a href="#"><div class="message-box"><img class="focus" src="<?= base_url();?>assets/adminPanel/style/images/feedback.png" />Feedbacks<lable class="counter">0</lable></div></a>
			 
            </div>
          </div>

          <!-- Today status ends -->
		  
		    <!-- Chats, File upload and Recent Comments -->
          <div class="row">

            <div class="col-md-4">
              
              <!-- Chat Widget -->
              <div class="widget wgreen">
                <!-- Widget title -->
                <div class="widget-head">
                  <div class="pull-left">Sale Ads</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <!-- Widget content -->
                  <div class="padd">
                    
                    <ul class="chats">

                      <!-- Chat by us. Use the class "by-me". -->
                      <li class="by-me">
                        <div class="chat-content">
                          <!-- In meta area, first include "name" and then "time" -->
                          <div class="chat-meta">Ashok <span class="pull-right">3 hours ago</span></div>
                          Vivamus diam elit diam, consectetur dapibus adipiscing elit.
                          <div class="clearfix"></div>
                        </div>
                      </li> 

                      <!-- Chat by other. Use the class "by-other". -->
                      <li class="by-other">
                        <!-- Use the class "pull-right" in avatar -->
                        <div class="chat-content">
                          <!-- In the chat meta, first include "time" then "name" -->
                          <div class="chat-meta">3 hours ago <span class="pull-right">Ravi</span></div>
                          Vivamus diam elit diam, consectetur fconsectetur dapibus adipiscing elit.
                          <div class="clearfix"></div>
                        </div>
                      </li>   

                      <li class="by-me">
                        <div class="chat-content">
                          <div class="chat-meta">Ashok <span class="pull-right">4 hours ago</span></div>
                          Vivamus diam elit diam, consectetur fermentum sed dapibus eget, Vivamus consectetur dapibus adipiscing elit.
                          <div class="clearfix"></div>
                        </div>
                      </li>                                                                                  

                    </ul>

                  </div>
                </div>

              </div>

            </div>


            <!-- File Upload widget -->
            <div class="col-md-4">

              <div class="widget worange">
                <!-- Widget title -->
                <div class="widget-head">
                  <div class="pull-left">Rental Ads</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content referrer">
                  <!-- Widget content -->
                  
                  <table class="table  table-bordered ">
                    <tr>
                      <th><center>#</center></th>
                      <th>Browsers</th>
                      <th>Visits</th>
                    </tr>
                    <tr>
                      <td><img src="<?= base_url();?>assets/adminPanel/img/icons/chrome.png" alt="" />
                      <td>Google Chrome</td>
                      <td>3,005</td>
                    </tr> 
                    <tr>
                      <td><img src="<?= base_url();?>assets/adminPanel/img/icons/firefox.png" alt="" />
                      <td>Mozilla Firefox</td>
                      <td>2,505</td>
                    </tr> 
                    <tr>
                      <td><img src="<?= base_url();?>assets/adminPanel/img/icons/ie.png" alt="" />
                      <td>Internet Explorer</td>
                      <td>1,405</td>
                    </tr> 
                    <tr>
                      <td><img src="<?= base_url();?>assets/adminPanel/img/icons/opera.png" alt="" />
                      <td>Opera</td>
                      <td>4,005</td>
                    </tr> 
                    <tr>
                      <td><img src="<?= base_url();?>assets/adminPanel/img/icons/safari.png" alt="" />
                      <td>Safari</td>
                      <td>505</td>
                    </tr>                                                                    
                  </table>

                </div>
                  <div class="widget-foot">
                  </div>
              </div>

            </div>


            <!-- Project widget -->
            <div class="col-md-4">
              <div class="widget wred">
                <!-- Widget title -->
                <div class="widget-head">
                  <div class="pull-left">Job Ads</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <!-- Widget content -->
                  <!-- Task list starts -->
                  <ul class="project">

                    <li>
                      <p>
                        <!-- Name -->
                        Hospital Management System
                      </p>

                      <p class="p-meta">
                        <!-- Due date & % Completed -->
                        <span>Due : 26/2/2012 - 80% Done</span> 
                      </p>

                     
                        <!-- Progress bar -->
                       <div class="progress progress-striped active">
						  <div class="progress-bar progress-bar-info"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
							<span class="sr-only">80% Complete</span>
						  </div>
					   </div>
                    </li>


                    <li>
                      <p>
                        <!-- Checkbox -->
                        <input value="check1" type="checkbox">
                        <!-- Name -->
                        School Download System
                      </p>

                      <p class="p-meta">
                        <!-- Due date & % Completed -->
                        <span>Due : 26/2/2012 - 80% Done</span> 
                      </p>

                      
                        <!-- Progress bar -->
                        <div class="progress progress-striped active">
						  <div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
							<span class="sr-only">90% Complete</span>
						  </div>
					    </div>
                    </li>

                    <li>
                      <p>
                        <!-- Checkbox -->
                        <input value="check1" type="checkbox"> 
                        <!-- Name -->
                        Question and Answers Script
                      </p>

                      <p class="p-meta">
                        <!-- Due date & % Completed -->
                        <span>Due : 26/2/2012 - 80% Done</span> 
                      </p>

                      
                        <!-- Progress bar -->
                        <div class="progress progress-striped active">
						  <div class="progress-bar progress-bar-danger"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
							<span class="sr-only">70% Complete</span>
						  </div>
					    </div>
                    </li>                                                              

                  </ul>
                  <div class="clearfix"></div>  


                </div>
                <div class="widget-foot">

                </div>
              </div>
            </div>

          </div>

          <!-- Dashboard Graph starts -->

          <div class="row">
            <div class="col-md-8">

              <!-- Widget -->
              <div class="widget wlightblue">
                <!-- Widget head -->
                <div class="widget-head">
                  <div class="pull-left">Monthly Visitor Status</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>             

                <!-- Widget content -->
                <div class="widget-content">
                  <div class="padd">

                    <!-- Bar chart (Blue color). jQuery Flot plugin used. -->
                    <div id="bar-chart"></div>


                  </div>
                </div>
                <!-- Widget ends -->

              </div>
            </div>

            <div class="col-md-4">

              <div class="widget wblue">

                <div class="widget-head">
                  <div class="pull-left">Today Status</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>             

                <div class="widget-content">
                  <div class="padd">

                    <!-- Visitors, pageview, bounce rate, etc., Sparklines plugin used -->
                    <ul class="current-status">
                      <li>
                        <span id="status1"></span> <span class="bold">Visits : 2000</span> <i class="icon-arrow-up green"></i>
                      </li>
                      <li>
                        <span id="status2"></span> <span class="bold">Unique Visitors : 1,345</span> <i class="icon-arrow-down red"></i>
                      </li>
                      <li>
                        <span id="status3"></span> <span class="bold">Pageviews : 2000</span> <i class="icon-arrow-up green"></i>
                      </li>
                      <li>
                        <span id="status4"></span> <span class="bold">Pages / Visit : 2000</span> <i class="icon-arrow-down red"></i>
                      </li>
                      <li>
                        <span id="status5"></span> <span class="bold">Avg. Visit Duration : 2000</span> <i class="icon-arrow-down red"></i>
                      </li>
                      <li>
                        <span id="status6"></span> <span class="bold">Bounce Rate : 2000</span> <i class="icon-arrow-up green"></i>
                      </li>   
                      <li>
                        <span id="status7"></span> <span class="bold">% New Visits : 2000</span> <i class="icon-arrow-up green"></i>
                      </li>                                                                                                            
                    </ul>

                  </div>
                </div>

              </div>

            </div>
          </div>
          <!-- Dashboard graph ends ->

          <div class="row">
           
            <!-- Server Status -->
            <div class="col-md-4">
              <div class="widget wlightblue">
                <!-- Widget title -->
                <div class="widget-head">
                  <div class="pull-left">Server Status</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <!-- Widget content -->
                  
                  <table class="table  table-bordered ">
                    <tr>
                      <td>Domain</td>
                      <td>sitedomain.com</td>
                    </tr> 
                    <tr>
                      <td>IP Address</td>
                      <td>192.425.2.4</td>
                    </tr> 
                    <tr>
                      <td>Disk Space</td>
                      <td>600GB / 60000GB</td>
                    </tr> 
                    <tr>
                      <td>Bandwidth</td>
                      <td>1000GB / 2000GB</td>
                    </tr> 
                    <tr>
                      <td>PHP Version</td>
                      <td>5.1.1</td>
                    </tr> 
                    <tr>
                      <td>MySQL Databases</td>
                      <td>10</td>
                    </tr>                                                                                                     
                  </table>

                </div>
              </div>
            </div>
			
            <div class="col-md-8">
              <div class="widget wgreen">
                <div class="widget-head">
                  <div class="pull-left">Quick Post</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>  
                  <div class="clearfix"></div>
                </div>
                
                <div class="widget-content">
                  <div class="padd">
                    
                      <div class="form quick-post">
                                      <!-- Edit profile form (not working)-->
                                      <form class="form-horizontal">
                                          <!-- Title -->
                                          <div class="form-group">
                                            <label class="control-label col-lg-3" for="title">Title</label>
                                            <div class="col-lg-9"> 
                                              <input type="text" class="form-control" id="title">
                                            </div>
                                          </div>   
                                          <!-- Content -->
                                          <div class="form-group">
                                            <label class="control-label col-lg-3" for="content">Content</label>
                                            <div class="col-lg-9">
                                              <textarea class="form-control" id="content"></textarea>
                                            </div>
                                          </div>                           
                                          <!-- Cateogry -->
                                          <div class="form-group">
                                            <label class="control-label col-lg-3">Category</label>
                                            <div class="col-lg-9">                               
                                                <select class="form-control">
                                                  <option value="">- Choose Cateogry -</option>
                                                  <option value="1">General</option>
                                                  <option value="2">News</option>
                                                  <option value="3">Media</option>
                                                  <option value="4">Funny</option>
                                                </select>  
                                            </div>
                                          </div>          
                                          
                                          <!-- Buttons -->
                                          <div class="form-group">
                                             <!-- Buttons -->
											 <div class="col-lg-offset-2 col-lg-9">
												<button type="submit" class="btn btn-success">Send</button>
												<button type="reset" class="btn btn-default">Reset</button>
											 </div>
                                          </div>
                                      </form>
                    </div>
                  

                  </div>
                </div>

                <div class="widget-foot">
                    <!-- Footer goes here -->
                </div>

              </div> 
            </div>            
          </div>  


        </div>
	  </div>

		<!-- Matter ends -->

    </div>

   <!-- Mainbar ends -->	    	
   <div class="clearfix"></div>

</div>
<!-- Content ends -->


<!-- Notification box starts -->
   <div class="slide-box">

        <!-- Notification box head -->
        <div class="slide-box-head bred">
          <!-- Title -->
          <div class="pull-left">Notification Box</div>          
          <!-- Icon -->
          <div class="slide-icons pull-right">
            <a href="#" class="sminimize"><i class="icon-chevron-down"></i></a> 
            <a href="#" class="sclose"><i class="icon-remove"></i></a>
          </div>
          <div class="clearfix"></div>
        </div>

        <div class="slide-content">

          <!-- It is default bootstrap nav tabs. See official bootstrap doc for doubts -->
            <ul class="nav nav-tabs">
              <!-- Tab links -->
              <li class="active"><a href="#tab1" data-toggle="tab"><i class="icon-bar-chart"></i></a></li>
              <li><a href="#tab2" data-toggle="tab"><i class="icon-phone"></i></a></li>
              <li><a href="#tab3" data-toggle="tab"><i class="icon-comments"></i></a></li>
            </ul>

            <!-- Tab content -->
            
            <div class="tab-content">

              <div class="tab-pane active" id="tab1">

                <!-- Graph #1 -->
                <div class="slide-data">
                  <div class="slide-data-text">Today Earnings</div>
                  <div class="slide-data-result">$5,0000 <i class="icon-arrow-up red"></i> </div>
                  <div class="clearfix"></div>
                  <hr />
                  <span id="todayspark4" class="spark"></span>
                </div>

                <!-- Graph #2 -->
                <div class="slide-data">
                  <div class="slide-data-text">Yesterday Earnings</div>
                  <div class="slide-data-result">$4,6000 <i class="icon-arrow-down green"></i> </div>
                  <div class="clearfix"></div>
                  <hr />
                  <span id="todayspark5" class="spark"></span>
                </div>                

              </div>

              <div class="tab-pane" id="tab2">
                <h5>Have some content here.</h5>
                <p>Aliquam dui libero, pharetra nec venenatis in, scelerisque quis odio. In hac habitasse platea dictumst. Etiam porta placerat turpis, eget fermentum neque egestas at. Vestibulum ullamcorper, augue a sollicitudin vestibulum, orci purus semper felis, lobortis consequat nisi nunc eu enim. </p>
              </div>

              <div class="tab-pane" id="tab3">
                <h5>Have some content here.</h5>
                <p>Aliquam dui libero, pharetra nec venenatis in, scelerisque quis odio. In hac habitasse platea dictumst. Etiam porta placerat turpis, eget fermentum neque egestas at. Vestibulum ullamcorper, augue a sollicitudin vestibulum, orci purus semper felis, lobortis consequat nisi nunc eu enim.</p>
              </div>              

            </div>

        </div>
      
    </div>

<!-- Notification box ends -->   

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 

<!-- JS -->
<script src="<?= base_url();?>assets/adminPanel/js/jquery.js"></script> <!-- jQuery -->
<script src="<?= base_url();?>assets/adminPanel/js/bootstrap.js"></script> <!-- Bootstrap -->
<script src="<?= base_url();?>assets/adminPanel/js/jquery-ui-1.10.2.custom.min.js"></script> <!-- jQuery UI -->
<script src="<?= base_url();?>assets/adminPanel/js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
<script src="<?= base_url();?>assets/adminPanel/js/jquery.rateit.min.js"></script> <!-- RateIt - Star rating -->
<script src="<?= base_url();?>assets/adminPanel/js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto -->

<!-- jQuery Flot -->
<script src="<?= base_url();?>assets/adminPanel/js/excanvas.min.js"></script>
<script src="<?= base_url();?>assets/adminPanel/js/jquery.flot.js"></script>
<script src="<?= base_url();?>assets/adminPanel/js/jquery.flot.resize.js"></script>
<script src="<?= base_url();?>assets/adminPanel/js/jquery.flot.pie.js"></script>
<script src="<?= base_url();?>assets/adminPanel/js/jquery.flot.stack.js"></script>

<script src="<?= base_url();?>assets/adminPanel/js/jquery.gritter.min.js"></script> <!-- jQuery Gritter -->
<script src="<?= base_url();?>assets/adminPanel/js/sparklines.js"></script> <!-- Sparklines -->
<script src="<?= base_url();?>assets/adminPanel/js/jquery.cleditor.min.js"></script> <!-- CLEditor -->
<script src="<?= base_url();?>assets/adminPanel/js/bootstrap-datetimepicker.min.js"></script> <!-- Date picker -->
<script src="<?= base_url();?>assets/adminPanel/js/bootstrap-switch.min.js"></script> <!-- Bootstrap Toggle -->
<script src="<?= base_url();?>assets/adminPanel/js/filter.js"></script> <!-- Filter for support page -->
<script src="<?= base_url();?>assets/adminPanel/js/custom.js"></script> <!-- Custom codes -->
<script src="<?= base_url();?>assets/adminPanel/js/charts.js"></script> <!-- Custom chart codes -->

<!-- Script for this page -->
<script type="text/javascript">
$(function () {

    /* Bar Chart starts */

    var d1 = [];
    for (var i = 0; i <= 30; i += 1)
        d1.push([i, parseInt(Math.random() * 30)]);

    var d2 = [];
    for (var i = 0; i <= 30; i += 1)
        d2.push([i, parseInt(Math.random() * 30)]);


    var stack = 0, bars = true, lines = false, steps = false;
    
    function plotWithOptions() {
        $.plot($("#bar-chart"), [ d1, d2 ], {
            series: {
                stack: stack,
                lines: { show: lines, fill: true, steps: steps },
                bars: { show: bars, barWidth: 0.8 }
            },
            grid: {
                borderWidth: 0, hoverable: true, color: "#777"
            },
            colors: ["#52b9e9", "#0aa4eb"],
            bars: {
                  show: true,
                  lineWidth: 0,
                  fill: true,
                  fillColor: { colors: [ { opacity: 0.9 }, { opacity: 0.8 } ] }
            }
        });
    }

    plotWithOptions();
    
    $(".stackControls input").click(function (e) {
        e.preventDefault();
        stack = $(this).val() == "With stacking" ? true : null;
        plotWithOptions();
    });
    $(".graphControls input").click(function (e) {
        e.preventDefault();
        bars = $(this).val().indexOf("Bars") != -1;
        lines = $(this).val().indexOf("Lines") != -1;
        steps = $(this).val().indexOf("steps") != -1;
        plotWithOptions();
    });

    /* Bar chart ends */

});


/* Curve chart starts */

$(function () {
    var sin = [], cos = [];
    for (var i = 0; i < 14; i += 0.5) {
        sin.push([i, Math.sin(i)]);
        cos.push([i, Math.cos(i)]);
    }

    var plot = $.plot($("#curve-chart"),
           [ { data: sin, label: "sin(x)"}, { data: cos, label: "cos(x)" } ], {
               series: {
                   lines: { show: true, 
                            fill: true,
                            fillColor: {
                              colors: [{
                                opacity: 0.05
                              }, {
                                opacity: 0.01
                              }]
                          }
                  },
                   points: { show: true }
               },
               grid: { hoverable: true, clickable: true, borderWidth:0 },
               yaxis: { min: -1.2, max: 1.2 },
               colors: ["#fa3031", "#43c83c"]
             });


    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            width: 100,
            left: x + 5,
            border: '1px solid #000',
            padding: '2px 8px',
            color: '#ccc',
            'background-color': '#000',
            opacity: 0.9
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#curve-chart").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;
                    
                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);
                    
                    showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
                }
            }
            else {
                $("#tooltip").remove();
                previousPoint = null;            
            }
    }); 

    $("#curve-chart").bind("plotclick", function (event, pos, item) {
        if (item) {
            $("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
            plot.highlight(item.series, item.datapoint);
        }
    });

});

/* Curve chart ends */
</script>

</div>