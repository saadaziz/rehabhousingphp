<div class="mainBody"> 
 
<div class="content">

  	<!-- Sidebar -->
    <div class="sidebar">
        <div class="sidebar-dropdown"><a href="#">Navigation</a></div>

        <div class="sidebar-inner">



          <!-- Search form -->
          <div class="sidebar-widget">
             <div class="my_bazar">
			 My Bazar<img class="bazar-icon" src="<?= base_url();?>assets/adminPanel/style/images/shop.png" />
			 </div>
          </div>

          <!--- Sidebar navigation -->
          <!-- If the main navigation has sub navigation, then add the class "has_submenu" to "li" of main navigation. -->
          <ul class="navi">

            <!-- Use the class nred, ngreen, nblue, nlightblue, nviolet or norange to add background color. You need to use this in <li> tag. -->

            <li class="nred current"><a href="#"><i class="icon-desktop"></i> Dashboard</a></li>

            <!-- Menu with sub menu -->
            <li class="has_submenu nlightblue">
              <a href="#">
                <!-- Menu name with icon -->
                <i class="icon-th"></i> My Sale Ads 
                <!-- Icon for dropdown -->
                <span class="pull-right"><i class="icon-angle-down"></i></span>
              </a>

              <ul>
                <li><a href="widgets1.html">All Ads</a></li>
                <li><a href="widgets2.html">Published</a></li>
                <li><a href="widgets2.html">Waiting for Verification</a></li>
                <li><a href="widgets2.html">Rejected</a></li>
                <li><a href="widgets2.html">Add New</a></li>
              </ul>
            </li>

            <li class="has_submenu nviolet">
              <a href="#">
                <i class="icon-th-large"></i>My Rental Ads
                <span class="pull-right"><i class="icon-angle-down"></i></span>
              </a>

              <ul>
                <li><a href="widgets1.html">All Ads</a></li>
                <li><a href="widgets2.html">Published</a></li>
                <li><a href="widgets2.html">Waiting for Verification</a></li>
                <li><a href="widgets2.html">Rejected</a></li>
                <li><a href="widgets2.html">Add New</a></li>
              </ul>
            </li> 
            <li class="has_submenu nred">
              <a href="#">
                <i class="icon-folder-open"></i> My Job Ads
                <span class="pull-right"><i class="icon-angle-down"></i></span>
              </a>
              
              <ul>
                <li><a href="widgets1.html">All Ads</a></li>
                <li><a href="widgets2.html">Published</a></li>
                <li><a href="widgets2.html">Waiting for Verification</a></li>
                <li><a href="widgets2.html">Rejected</a></li>
                <li><a href="widgets2.html">Add New</a></li>
              </ul>
            </li> 
            <li class="has_submenu ngreen">
              <a href="#">
                <i class="icon-gift"></i> Buyer Feedback
                <span class="pull-right"><i class="icon-angle-down"></i></span>
              </a>
              
              <ul>
                <li><a href="widgets1.html">All Feedbacks</a></li>
                <li><a href="widgets2.html">Unread</a></li>
                <li><a href="widgets2.html">Read</a></li>
              </ul>
            </li> 
            <li class="has_submenu nblue">
              <a href="#">
                <i class="icon-wrench"></i> Settings
                <span class="pull-right"><i class="icon-angle-down"></i></span>
              </a>
              
              <ul>
                <li><a href="widgets1.html">Edit Website (color/Title/meta/URL)</a></li>
                <li><a href="widgets2.html">Change Images (Logo/Banner)</a></li>
                <li><a href="widgets2.html">Profile (View/Edit)</a></li>
              </ul>
            </li> 

            <li class="norange"><a href="charts.html"><i class="icon-off"></i> Logout</a></li>

          </ul>




          <!-- Date -->

          <div class="sidebar-widget">
            <div id="todaydate"></div>
          </div>



        </div>

    </div>