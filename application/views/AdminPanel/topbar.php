<!-- Stylesheets -->
  <link href="<?= base_url();?>assets/adminPanel/style/bootstrap.css" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="<?= base_url();?>assets/adminPanel/style/font-awesome.css"> 
  <!-- jQuery UI -->
  <link rel="stylesheet" href="<?= base_url();?>assets/adminPanel/style/jquery-ui.css"> 
  <!-- Calendar -->
  <link rel="stylesheet" href="<?= base_url();?>assets/adminPanel/style/fullcalendar.css">
  <!-- prettyPhoto -->
  <link rel="stylesheet" href="<?= base_url();?>assets/adminPanel/style/prettyPhoto.css">   
  <!-- Star rating -->
  <link rel="stylesheet" href="<?= base_url();?>assets/adminPanel/style/rateit.css">
  <!-- Date picker -->
  <link rel="stylesheet" href="<?= base_url();?>assets/adminPanel/style/bootstrap-datetimepicker.min.css">
  <!-- jQuery Gritter -->
  <link rel="stylesheet" href="<?= base_url();?>assets/adminPanel/style/jquery.gritter.css">
  <!-- CLEditor -->
  <link rel="stylesheet" href="<?= base_url();?>assets/adminPanel/style/jquery.cleditor.css"> 
  <!-- Bootstrap toggle -->
  <link rel="stylesheet" href="<?= base_url();?>assets/adminPanel/style/bootstrap-switch.css">
  <!-- Main stylesheet -->
  <link href="<?= base_url();?>assets/adminPanel/style/style.css" rel="stylesheet">
  <!-- Widgets stylesheet -->
  <link href="<?= base_url();?>assets/adminPanel/style/widgets.css" rel="stylesheet">   
 
  
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="<?= base_url();?>assets/adminPanel/js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon/favicon.png">
 <div class="src_bg">
            <div class="container topheight">
               <div class="" role="banner">
  
    <div class="containerk">
      <!-- Site name for smallar screens -->
      <nav class="top-nav">
	  	<ul>
			<li class="company-logo"><img title="Company Logo" src="<?= base_url();?>/uploads/logo_samples1.jpg" /></li>
			<li class="active main"><a href=""><img class="icon" src="<?= base_url();?>assets/adminPanel/style/images/tv.png" />Dashboard</a><img class="arrow" src="<?= base_url();?>assets/adminPanel/style/images/arrowdown.png" /></li>
			<li class="dropdown main" id="accountmenu"><img class="icon" src="<?= base_url();?>assets/adminPanel/style/images/set.png" />
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">Settings<img class="arrowdwn" src="<?= base_url();?>assets/adminPanel/style/images/arrowdwn.png" /></a>
					    <ul class="dropdown-menu">
							<li><img class="arrow-up" src="<?= base_url();?>assets/adminPanel/style/images/arrowup.png" /></li>
							<li>
								<ul class="inner-dropdown">
									<li><a href="#"><img class="sub-menu" src="<?= base_url();?>assets/adminPanel/style/images/edit.png" />PHP</a></li>
									<li class="divider"></li>  
									<li><a href="#"><img class="sub-menu" src="<?= base_url();?>assets/adminPanel/style/images/circle.png" />MySQL</a></li>
									<li class="divider"></li> 
									<li><a href="#"><img class="sub-menu" src="<?= base_url();?>assets/adminPanel/style/images/food.png" />JavaScript</a></li>
								</ul>
							</li>
                        </ul>
			</li>
			<li class="main"><a href=""><img class="icon" src="<?= base_url();?>assets/adminPanel/style/images/mgs.png" />Messages<lable class="counter-mgs">20</lable></a></li>
			<li class="main"><a href=""><img class="icon" src="<?= base_url();?>assets/adminPanel/style/images/out.png" />Logout</a></li>
			<li class="company-logo">
				<button class="postAdButton">POST YOUR AD</button>
			</li>
		</ul>
	  </nav>  
	  <script type="text/javascript" src="/twitter-bootstrap/twitter-bootstrap-v2/docs/assets/js/bootstrap-dropdown.js"></script>  
		<script type="text/javascript">  
			$(document).ready(function () {  
				$('.dropdown-toggle').dropdown();  
			});  
	   </script> 
  </div>

</div>
				<!--start top searcAera Right-->
                <!--end top searcAera Right-->
            </div>
          <div class="clr"></div>
       </div>
