
<script src="<?php echo base_url() ?>assets/js/jscolor.js"></script>

<!----------------------Javascript For Run Time Color Show----------------------->

<script>
	function change(divName,inputName,cssColor,hov){
		var myList = document.getElementsByClassName(divName)
		var add=document.getElementById(inputName).value
		var add1=hov+cssColor+":#"+add+" !important";
		for (var i = 0; i < myList.length; i++) {
			myList[i].style.cssText = add1;
    		//xx[i].style.color="red";
			}
	
	}
	function closeFunction()
	{
		document.getElementById('colorbox').style.visibility= "hidden";
		document.getElementById('free').style.height= "auto";
		document.getElementById('open-edit').style.visibility= "visible";
	}
	
	function openFunc()
	{
		//alert('fbmfj');
		document.getElementById('colorbox').style.visibility= "visible";
		document.getElementById('free').style.height= "210px";
		document.getElementById('open-edit').style.visibility= "hidden";
	}
	
</script>
<style type="text/css">
	.free {
		height: 210px;
	}
	.colorbox button{
		float: right;
	}
	#open-edit{
		visibility: hidden;
	}
</style>
<!----------------------Javascript For Run Time Color Show----------------------->
<div class="free" id="free"><button id="open-edit" onclick="openFunc()">Edit Templete Color</button></div>
<div class="colorbox" id="colorbox">
	<button onclick="closeFunction()">Try it</button>
		<h2>Create a web jobs item</h2>
		<?php echo validation_errors(); ?>
		
		<?php echo form_open('templete/changeTemplete/add'); ?>
		<div class="block">
			<label for="title">Body Background Color*</label>
			<input type="text" id="one"  class="color input-box" value="<?= $themes->getBodyBgColor(); ?>" size="6" name="body_bg_color" onchange="change('bg-color','one','background','')"/>
		</div>
		<div class="block">
			<label for="title">Slider Background Color*</label>
			<input type="text" id="two" class="color input-box" size="6" value="<?= $themes->getSliderBgColor(); ?>"  name="slider_bg_color" onchange="change('slider-bg','two','background','')"/>
		</div>
		<div class="block">
			<label for="title">Text Color*</label>
			<input type="text" id="three" class="color input-box"  value="<?= $themes->getTextColor(); ?>" name="text_color" onchange="change('text-color','three','color','')"/>
		</div>
		<div class="block">
			<label for="title">Text Header Color*</label>
			<input type="text" id="four"  class="color input-box"  value="<?= $themes->getTextHeaderColor(); ?>" name="text_header_color" onchange="change('text-header','four','color','')"/>
		</div>
		<div class="block">
			<label for="title">Top Menu Color*</label>
			<input  type="text" id="five"  class="color input-box"  value="<?= $themes->getTopMenuColor(); ?>" name="top_menu_color" onchange="change('top-menu','five','color','')"/>
		</div>
		<div class="block">
			<label for="title">Top Menu hover Color*</label>
			<input type="text" id="six"  class="color input-box"  value="<?= $themes->getTopMenuHoverColor(); ?>" name="top_menu_hover_color" onchange="change('cative-company' ,'six','color' , '')"/>
		</div>
		<div class="block">
			<label for="title">Footer Background Color*</label>
			<input type="text"  id="seven" class="color input-box"  value="<?= $themes->getFooterBgColor(); ?>" name="footer_bg_color" onchange="change('main-footer','seven','background','')"/>
		</div>
		<div class="block">
			<label for="title">Footer Text Color*</label>
			<input type="text" id="eight"  class="color input-box" value="<?= $themes->getFooterTextColor(); ?>" name="footer_text_color" onchange="change('footer-text','eight','color','')"/>
		</div>
		<div class="block">
			<label for="title">Footer Menu Color*</label>
			<input type="text" id="nine"  class="color input-box" value="<?= $themes->getFooterMenuHoverColor(); ?>" name="footer_menu_color" onchange="change('footer-menu','nine','color')"/>
		</div>
		<div class="block">
			<label for="title">Footer Menu Hover Color*</label>
			<input type="text" id="ten"  class="color input-box" value="<?= $themes->getFooterMenuColor(); ?>" name="footer_menu_hover_color" onchange="change('footer-menu','ten','color',':hover')"/>
		</div>
		<div class="block">
			<label for="title">Project Name Color*</label>
			<input type="text" id="eleven"  class="color input-box" value="<?= $themes->getProjectNameColor(); ?>" name="project_name_color" onchange="change('proName','eleven','color','')"/>
		</div>
		<div class="block">
			<label for="title">Project Address Color*</label>
			<input type="text" id="twelve"  class="color input-box" value="<?= $themes->getProjectAddressColor(); ?>" name="project_address_color" onchange="change('proLoc','twelve','color','')"/>
		</div>
		<div class="block">
			<label for="title">project Price Color*</label>
			<input type="text"  id="thirteen" class="color input-box" value="<?= $themes->getProjectPriceColor(); ?>" name="project_price_color" onchange="change('proPrice','thirteen','color','')"/>
		</div>
		<div class="block">
			<label for="title">Project Feature Color*</label>
			<input type="text" id="fourteen"  class="color input-box" value="<?= $themes->getProjectFeatureColor(); ?>" name="project_feature_color" onchange="change('proRoome','fourteen','color','')"/>
		</div>
		<div class="block">
			<input type="submit"  class="btn btn-success" name="submit" value="Create" />
		</div>
		
		</form>
</div>

