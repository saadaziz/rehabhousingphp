    			  
              </li>
            </ul>
          </div>
        </div>
      <!-- End: PRODUCT LIST -->
    </div>
    <!-- End: MAIN CONTENT -->
	<!-- Start: FOOTER -->
    <footer class="main-footer">
     <div class="footer-text">
	  <div class="bottom-menu">
		<ul>
			<li class="menu-footer"><a class="footer-menu" href="#">Home</a></li>
			<li class="menu-footer"><a class="footer-menu" href="#">About Us</a></li>
			<li class="menu-footer"><a class="footer-menu" href="#">Products</a></li>
			<li class="menu-footer menu-last"><a class="footer-menu" href="#">Contact Us</a></li>	
		</ul>
	  </div>
      <hr class="footer-divider">
      <div class="container">
        <p>
          &copy; 2013 Synesis IT, Inc. All Rights Reserved.
        </p>
      </div>
     </div>
    </footer>
    <!-- End: FOOTER -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/boot-business.js"></script>
  </body>
</html>
