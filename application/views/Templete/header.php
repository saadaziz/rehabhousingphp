<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Bootbusiness | Short description about company">
    <meta name="author" content="Your name">
    <title>Title </title>
    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap responsive -->
    <link href="<?php echo base_url()?>assets/css/design.css" rel="stylesheet">
    <!-- My design -->
	
	
	<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
	<!-- Saimon vai er style.css -->
    <link href="<?php echo base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
    <!-- Font awesome - iconic font with IE7 support --> 
    <link href="<?php echo base_url()?>assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/font-awesome-ie7.css" rel="stylesheet">
    <!-- Bootbusiness theme -->
    <link href="<?php echo base_url()?>assets/css/boot-business.css" rel="stylesheet">
	<style type="text/css">
	
			.bg-color{background: #<?= $themes->getBodyBgColor(); ?>;}
			.slider-bg {background: #<?= $themes->getSliderBgColor();?>;}
			.text-color{color: #<?= $themes->getTextColor();?> !important;}
			.text-header{color: #<?= $themes->getTextHeaderColor();?>;}
			.top-menu{color: #<?= $themes->getTopMenuColor();?> !important;}
			.menu a.cative-company, .top-menu:hover {color: #<?= $themes->getTopMenuHoverColor();?> !important;}
			.main-footer{ background:#<?= $themes->getFooterBgColor();?> !important;}		
			.main-footer{ color:#<?= $themes->getFooterTextColor();?> !important;}
			.footer-menu:hover{color: #<?= $themes->getFooterMenuHoverColor();?> !important;}
			.footer-menu{color:#<?= $themes->getFooterMenuColor();?> !important;}
			.proName {color: #<?= $themes->getProjectNameColor();?> !important;}
			.proLoc {color: #<?= $themes->getProjectAddressColor();?> !important;}
			.proPrice {color: #<?= $themes->getProjectPriceColor();?> !important;}
			.proRoome {color: #<?= $themes->getProjectFeatureColor();?> !important;}
</style>
  </head>
  <body class="bg-color">
    <!-- Start: HEADER -->
    <header>
      <!-- Start: Navigation wrapper -->
      <div class="navbar navbar-fixed-top">
       		<div class="headerBar">
			  <div class="container">
			  	<div style="width:1024px; margin:0 auto; ">
					<ul class="top-company">
						<li><img class="logo-com" title="Company Logo" src="<?php echo base_url();?>assets/img/logo.png" height="25" width="70"/></li>
						<li>Welcome to Rehabhousing.com , </li>
						<li><a href="#">Join Free </a> | <a href="#"> Sign In </a> | <a href="#"> My Rehab</a></li>
						<li><select name="" class="search-select"><option value="">RehabHousing.com</option><option value="">On This Site</option></select></li>
						<li><input type="text" class="search" name="search" placeholder="Search"/><img class="search-icon" src='<?php echo base_url()?>assets/img/search.png' /></li>
						<li><button>Post Buying Request</button></li>
					</ul>
					</div>
			  </div>
		 	</div> 
      </div>
      <!-- End: Navigation wrapper -->   
    </header>
    <!-- End: HEADER -->
    <!-- Start: MAIN CONTENT -->	
    <div class="content com-logo">
      <!-- Start: slider -->
      <div class="com-logo">
		<img src="<?php echo base_url()?>assets/img/logo.jpg" title="Company logo" alt="Company logo" />
		<div class="address">
			<img src="<?php echo base_url()?>assets/img/glyphicons_442_earphone.png" />+880171055050763<img src="<?php echo base_url()?>assets/img/glyphicons_010_envelope.png" />support@yahoo.com
		</div>
	  </div>
	</div>
	<div class="clr"></div>
    <div class="content">
      <!-- Start: slider -->
      <div class="slider slider-bg">
        <div class="container-fluid">
          <div id="heroSlider" class="carousel slide">
            <div class="carousel-inner">
              <div class="active item">
                <div class="hero-unit">
                  <div class="row-fluid">
                      <img src="<?php echo base_url()?>assets/img/slider.jpg" class="thumbnail" width="100%">	                   
                  </div>                  
                </div>
              </div>
              <div class="item">
                <div class="hero-unit">
                  <div class="row-fluid">
                      <img src="<?php echo base_url()?>assets/img/slider.jpg" class="thumbnail" width="100%">	                   
                  </div>                   
                </div>
              </div>
              <div class="item">
                <div class="hero-unit">
                  <div class="row-fluid">
                      <img src="<?php echo base_url()?>assets/img/slider.jpg" class="thumbnail" width="100%">	                   
                  </div>                   
                </div>
              </div>
              <div class="item">
                <div class="hero-unit">
                  <div class="row-fluid">
                      <img src="<?php echo base_url()?>assets/img/slider.jpg" class="thumbnail" width="100%">	                   
                  </div>                   
                </div>
              </div>
            </div>
			<div class="menu">
				<ul style="text-transform: capitalize">
					<li><a href="#" class="cative-company">Home</a></li>
					<li><a href="#" class="top-menu">Profile</a></li>
	                <li class="dropdown">
	                  <a href="#" class="dropdown-toggle top-menu" data-toggle="dropdown">Project<b class="caret"></b></a>
	                  <ul class="dropdown-menu">
	                    <li><a href="our_works.html">Our works</a></li>
	                    <li><a href="patnerships.html">Partnerships</a></li>
	                    <li><a href="leadership.html">Leadership</a></li>
	                  </ul>
	                </li>
					<li><a href="#" class="top-menu">Contact</a></li>
				</ul>
			</div>
          </div>
        </div>
      </div>
			<div class="image">
				<img src="<?php echo base_url()?>assets/img/info.png" title="Quick Contact" alt="Quick Contact" />
			</div>
      <!-- End: slider -->
      <!-- Start: PRODUCT LIST -->
        <div class="container">
          <div class="row-fluid">
            <ul class="thumbnails">
              <li class="span3 border contact">
			  <div class="hotprojectArea contact-add">
				<div class="listHeading"><img src="<?php echo base_url()?>assets/img/list_icon.png">&nbsp;&nbsp;Contact to Seller</div>
				<div class="contact-info">
					<p class="text-color">Enter your details below & we will call you back.</p>
					<input type="text" placeholder="Your Name" name="name">
					<input type="text" placeholder="Email Address" name="email">
					<input type="text" placeholder="Contact Number" name="contact_no">
					<textarea name="description" placeholder="Type your details here......." ></textarea>
					<button class="btn btn-primary" type="button">Submit</button>
					<button class="btn btn-success" type="button">Reset</button>
				</div>			
							<!--end hotproRow-->
				<div class="clr"></div>
				</div>
              </li>
              <li class="span9 border">