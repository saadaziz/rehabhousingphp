<?php include('header.php');?>
			  
						<div class="hotprojectArea">
							<div class="listHeading"><img src="img/list_icon.png">&nbsp;&nbsp;About Us</div>
							<!--start hotproRow--> 
							<div class="row-fluid"> 
								<p class="small-text">Imagine your life in a quiet, clean, green world to call your own!
								At Civitech Sampriti, you can enjoy the luxury of essential facilities and 
								services right within your complex. Beautiful and intelligently-designed 
								apartments available here provide you an opportunity to experience an enriched 
								quality of life. Coming up at the centre of Noida, the project 
								blends the sophistication of modern life with the subtle luxury and natural 
								surrounds, making an ideal mix for your dream home.</p>
							</div>
							<!--end hotproRow-->
							<!--end hotproRow-->
							<div class="clr"></div>
						</div>
			  
						<div class="hotprojectArea">
							<div class="listHeading"><img src="img/list_icon.png">&nbsp;&nbsp;Our Projects</div>
							<!--start hotproRow--> 
							<div class="row-fluid"> 
							<div class="projectRow">
								<div class="projectCol span4">
									<div class="imgBox">
										<img src="images/por_img.png" width="120" height="125">
									</div>
									<div class="proName">Rehab Housing.Com</div>
									<div class="proLoc">Bashundhara R/A, Dhaka</div>
									<div class="proPrice">BDT 20,000/Sft</div>
									<div class="proRoome">2/3 Bed, 2/3 Bath</div>
									<div class="details"><a href="">Details</a></div>
								</div>
								<div class="projectCol span4">
									<div class="imgBox">
										<img src="images/por_img.png" width="120" height="125">
									</div>
									<div class="proName">Rehab Housing.Com</div>
									<div class="proLoc">Bashundhara R/A, Dhaka</div>
									<div class="proPrice">BDT 20,000/Sft</div>
									<div class="proRoome">2/3 Bed, 2/3 Bath</div>
									<div class="details"><a href="">Details</a></div>
								</div>
								<div class="projectCol span4">
									<div class="imgBox">
										<img src="images/por_img.png" width="120" height="125">
									</div>
									<div class="proName">Rehab Housing.Com</div>
									<div class="proLoc">Bashundhara R/A, Dhaka</div>
									<div class="proPrice">BDT 20,000/Sft</div>
									<div class="proRoome">2/3 Bed, 2/3 Bath</div>
									<div class="details"><a href="">Details</a></div>
								</div>
							</div>
							</div>
							<!--end hotproRow-->

							<!--start hotproRow--> 
							<div class="row-fluid">  
							<div class="projectRow">
								<div class="projectCol span4">
									<div class="imgBox">
										<img src="images/por_img.png" width="120" height="125">
									</div>
									<div class="proName">Rehab Housing.Com</div>
									<div class="proLoc">Bashundhara R/A, Dhaka</div>
									<div class="proPrice">BDT 20,000/Sft</div>
									<div class="proRoome">2/3 Bed, 2/3 Bath</div>
									<div class="details"><a href="">Details</a></div>
								</div>
								<div class="projectCol span4">
									<div class="imgBox">
										<img src="images/por_img.png" width="120" height="125">
									</div>
									<div class="proName">Rehab Housing.Com</div>
									<div class="proLoc">Bashundhara R/A, Dhaka</div>
									<div class="proPrice">BDT 20,000/Sft</div>
									<div class="proRoome">2/3 Bed, 2/3 Bath</div>
									<div class="details"><a href="">Details</a></div>
								</div>
								<div class="projectCol span4">
									<div class="imgBox">
										<img src="images/por_img.png" width="120" height="125">
									</div>
									<div class="proName">Rehab Housing.Com</div>
									<div class="proLoc">Bashundhara R/A, Dhaka</div>
									<div class="proPrice">BDT 20,000/Sft</div>
									<div class="proRoome">2/3 Bed, 2/3 Bath</div>
									<div class="details"><a href="">Details</a></div>
								</div>
							</div>
							</div>
							<!--end hotproRow-->
							<div class="clr"></div>
						</div>

<?php include('footer.php');?>