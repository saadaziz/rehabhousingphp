<!DOCTYPE html>
<html>
	<head>
		<title><?=$title?></title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8">
	<link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css" type="text/css" media="screen"/>
		<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.min.js"></script>
		<script src="<?=base_url();?>assets/js/ckeditor.js"></script>
		<!-- Place inside the <head> of your HTML
		<script type="text/javascript" src="<?= base_url();?>assets/tinymce/tinymce.min.js"></script> -->
		<script type="text/javascript">
		tinymce.init({
		    selector: "textarea"
		 });
		</script>		
	</head>
	<body>