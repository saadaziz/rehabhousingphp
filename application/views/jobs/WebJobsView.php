<style type="text/css">
	.table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
		background-color: #DBF4FD;
	}
	.jobTitle {
		background: #22B8F1 !important;
		text-align: center;
		color: #FFFFFF;
	}
	.title-head{
		width: 20%;
		border-right: 1px solid #E7E7E7;
	}
	.jobSl{
		background: #84B542 !important;
		width: 10px;
	}

	.last-date{min-width: 110px;}
	.company-head {
		width: 25%;
		border-right: 1px solid #E7E7E7;
	 }
	.education-head {
		border-right: 1px solid #E7E7E7;
		text-align: center;
		width: 61%;
	}
	.job-heading {
		color: #22B8F1;
		font-size: xx-large;
		padding-top: 10px;
		border-bottom: 1px solid;
		margin-bottom: 15px;
		padding-bottom: 5px;
	}
	.joball a {
		color: #000;
		font-size: inherit;
	}
	.job-number {
		font-weight: normal;
		color: #909292;
	}
	.table-body{
		text-align: justify;
	}
	.table-body ul {
		margin: 0px !important;
		padding: 0px !important;
		padding-left: 15px !important;
	}
	.xx-small {
		font-size: xx-small;
		float: right;
		margin-top: 28px;
	}
	.latest-jobs {
		font-size: 16px;
		font-weight: bold;
		color: #666;
	}
	.education ul {
		padding: 0 !important;
		margin: 0px 0px 0px 16px !important;
	}
	.last-date {
		text-align: center;
		font-size: 11px;
	}
	.job-details p {
		background: url(http://172.16.24.159:8080/rehabhousing/img/bulet.png);
	}
</style>
<div class="container">
        <!--start body right-->  
        <div class="row row-offcanvas row-offcanvas-right">
            <div class="col-xs-12 col-sm-9">
	<h2 class="job-heading">Real Estate Jobs</h2>
	<div class="joball">
		<table>
			<tr>
				<td><a href="<?=base_url()?>job/category/AccountingFinance">Accounting/Finance <label class="job-number">(<?=count($AccountingFinance)?>)</label></a></td>
				<td><a href="<?=base_url()?>job/category/HRAdmin">HR/Admin <label class="job-number">(<?=count($HRAdmin)?>)</label></a></td>
				<td><a href="<?=base_url()?>job/category/MarketingSales">Marketing & Sales <label class="job-number">(<?=count($MarketingSales)?>)</label></a></td>
				<td><a href="<?=base_url()?>job/category/DesignCreative">Design & Creative <label class="job-number">(<?=count($DesignCreative)?>)</label></a></td>
			</tr>
			<tr>
				<td><a href="<?=base_url()?>job/category/IT">IT <label class="job-number">(<?=count($IT)?>)</label></a></td>
				<td><a href="<?=base_url()?>job/category/Legal">Legal <label class="job-number">(<?=count($Legal)?>)</label></a></td>
				<td><a href="<?=base_url()?>job/category/CustomerSupportFontDesk">Customer Support/Font Desk <label class="job-number">(<?=count($CustomerSupportFontDesk)?>)</label></a></td>
				<td></td>
			</tr>
		</table>
		
	</div>

	<div class="clear"></div>
	<div>
		<div class="latest-jobs">Latest Jobs</div>
		<table class="table table-striped">
			<tr>
				<th class="jobSl"></th>
				<th class="jobTitle title-head">Job Title</th>
				<th class="jobTitle company-head">Company</th>
				<th class="jobTitle education-head">Education</th>
				<th class="jobTitle last-date">Last Date</th>
			</tr>
			<?php $i=1; foreach ($jobs as $job): ?>
			<?php //$a=$i%2; if($a==1){$class="table-body";}else{$class="table-body";} ?>
			<tr>
				<td class=""><?= $job->getjobsId() ?></td>
				<td class=""><a href="<?=base_url()?>jobs/details/<?=$job->getJobsId()?>"><?= $job->getTitle() ?></a></td>
				<td class=""><?= $job->getCompanyName() ?></td>
				<td class="education">
					<ul>
					<?php 
						$string=$job->getEducationRequirements(); 
						$lists = explode("/end/",$string);	
						$c=count($lists);
						   //show all data use foreach
						$j=1;
						foreach($lists as $list) {
							$list = trim($list);
							if($j!=$c){ echo "<li>$list</li>";}
							$j++;
							}
					?>
					</ul>
				</td>
				<td class="last-date">
					<?= $job->getLastDateOfSubmission()->format("l")?>
					<?="<br>"?>
					<?= $job->getLastDateOfSubmission()->format("F d , Y")?>
					<br>(
					<?php
						$curdate = new DateTime();
						$lastdate = $job->getLastDateOfSubmission();
						$interval = $curdate->diff($lastdate);
						$month=$interval->m; 
						if($month==1){echo $month." month and ";}
						elseif($month>1){echo $month." months and ";}
						echo $interval->d.' days left';
					?>)
				</td>
			</tr>
			<?php endforeach ?>
		</table>
		<?= $this->pagination->create_links()?>
	</div>
</div>
<div class="col-xs-12 col-sm-3">
	<img width="100%" src="<?=base_url()?>/assets/img/a.jpg" />
</div>

</div>
        </div>