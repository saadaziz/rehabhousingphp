<style type="text/css">
	.table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
		background-color: #DBF4FD;
	}
	.jobTitle {
		background: #22B8F1 !important;
		text-align: center;
		color: #FFFFFF;
	}
	.title-head{
		width: 20%;
		border-right: 1px solid #E7E7E7;
	}
	.jobSl{
		background: #84B542 !important;
		width: 10px;
	}
	td{padding:0px 2px;}
	.last-date{min-width: 110px;}
	.company-head {
		width: 25%;
		border-right: 1px solid #E7E7E7;
	 }
	.education-head {
		border-right: 1px solid #E7E7E7;
		text-align: center;
		width: 61%;
	}
	.job-heading {
		color: #22B8F1;
		font-size: xx-large;
		padding-top: 10px;
		border-bottom: 1px solid;
		margin-bottom: 15px;
		padding-bottom: 5px;
	}
	.joball a {
		color: #000;
		font-size: inherit;
	}
	.job-number {
		font-weight: normal;
		color: #909292;
	}
	.table-body{
		text-align: justify;
	}
	.table-body ul {
		margin: 0px !important;
		padding: 0px !important;
		padding-left: 15px !important;
	}
	.xx-small {
		font-size: xx-small;
		float: right;
		margin-top: 28px;
	}
	.latest-jobs {
		font-size: 16px;
		font-weight: bold;
		color: #666;
	}
	.education ul {
		padding: 0 !important;
		margin: 0px 0px 0px 16px !important;
	}
	.last-date {
		text-align: center;
		font-size: 11px;
	}
	
	/* for only job details page-----------*/
	
	.latest-jobs {
		border: 1px solid #CCC;
		border-top-left-radius: 10px;
		border-top-right-radius: 10px;
		padding: 5px 5px 5px 15px;
		background: #EEE;
	}
	div.job-details {
		border: 1px solid #DDD;
		padding: 10px 10px 10px 15px;
	}
	.joball {
		padding-left: 10px;
	}
	.job-title.two {
		width: 120px;
		float: left;
	}
	.job-title {
		font-weight: 600;
	}
	.job-details li {
		list-style-type: none;
		padding-left: 20px;
	}
	.job-details img {
		width: 6px;
		margin: 0px 5px 2px -11px;
	}
	label.job-title.center, .job-details.center {
		text-align: center;
		width: 100%;
	}
	label.job-title.center.redcolor{
		color: #E63737;
	}
	.float-right {
		font-size: xx-small;
		color: #000;
		float: right;
		padding: 4px 20px 0px 0px;
	}
	.text-align-center {
		text-align: center;
		margin-left: -14px;
	}
	ul.job-details {
		margin: 0px;
		padding: 0px 0px 20px;
	}
	
	/* End for only job detrails page css--------------*/
</style>
<div class="container">
        <!--start body right-->  
        <div class="row row-offcanvas row-offcanvas-right">
            <div class="col-xs-12 col-sm-9">
	<h2 class="job-heading">Real Estate Jobs<a href="<?=base_url()?>jobs"><label class="xx-small">< Back to Jobs Home</label></a></h2>
	

	<div class="clear"></div>
	<div class="latest-jobs"><?=$jobdetails->getTitle()?><span class='float-right'>Published Date : <?=$jobdetails->getInsertTime()->format("M d , Y")?></span></div>
		<div class="job-details">
			<label class="job-title two">Company Name :</label><p class="job-details"><?= $jobdetails->getCompanyName() ?></p>
			<label class="job-title two">Job Category :</label>			
			<p class="job-details">
				<?php 
					$companyName=$jobdetails->getJobCatagory();
					switch ($companyName)
					{
					case "AccountingFinance":
					  echo "Accounting/Finance";
					  break;
					case "HRAdmin":
					  echo "HR/Admin";
					  break;
					case "MarketingSales":
					  echo "Marketing & Sales";
					  break;
					case "DesignCreative":
					  echo "Design & Creative";
					  break;
					case "CustomerSupportFontDesk":
					  echo "Customer Support/Font Desk";
					  break;
					default:
					  echo $companyName;
					}
				?>
			</p>
			<label class="job-title two">No. of Vacancy : </label><p class="job-details"><?=$jobdetails->getNoOfVacancies()?></p>
			<label class="job-title">Job Description / Responsibility :</label>
			<ul class="job-details">
				<?php
					$string=$jobdetails->getDescription();
					$lists = explode("/end/",$string);	
						$c=count($lists);
						   //show all data use foreach
						$j=1;
						foreach($lists as $list) {
							$list = trim($list);
							if($j!=$c){ echo "<li><img src='".base_url()."img/bulet.png' />$list</li>";}
							$j++;
							}
				?>
			</ul>
			<label class="job-title">Education Requirements : </label>
			<ul class="job-details">
				
				<?php 
						$string=$jobdetails->getEducationRequirements(); 
						$lists = explode("/end/",$string);	
						$c=count($lists);
						   //show all data use foreach
						$j=1;
						foreach($lists as $list) {
							$list = trim($list);
							if($j!=$c){ echo "<li><img src='".base_url()."img/bulet.png' />$list</li>";}
							$j++;
							}
				 ?>
			</ul>
			<label class="job-title">Experience Requirements :</label>
			<ul class="job-details">
				<?php 
						$string=$jobdetails->getExperience(); 
						$lists = explode("/end/",$string);	
						$c=count($lists);
						   //show all data use foreach
						$j=1;
						foreach($lists as $list) {
							$list = trim($list);
							if($j!=$c){ echo "<li><img src='".base_url()."img/bulet.png' />$list</li>";}
							$j++;
							}
				 ?>
			</ul>
			<label class="job-title">Additional Job Requirements :</label>
			<ul class="job-details">				
				<?php 
						$string=$jobdetails->getAdditionalRequirement(); 
						$lists = explode("/end/",$string);	
						$c=count($lists);
						   //show all data use foreach
						$j=1;
						foreach($lists as $list) {
							$list = trim($list);
							if($j!=$c){ echo "<li><img src='".base_url()."img/bulet.png' />$list</li>";}
							$j++;
							}
				 ?>
			</ul>
			<label class="job-title two">Salary Range : </label><p class="job-details"><?=$jobdetails->getSalaryRange()?></p>
			<label class="job-title two">Job Location :</label><p class="job-details"><?=$jobdetails->getJobLocation()?></p>
			<label class="job-title two">Job Source :</label><p class="job-details"><?=$jobdetails->getJobSource()?></p>
			<label class="job-title center">Apply Instruction</label>
			<ul class="job-details center" style="margin-bottom: 0px">
				<?php 
				$email=$jobdetails->getDesignation();
				if(strlen($email)>5):
				?>
				Send Your CV to <strong><?=$email?></strong><br>
				<?php endif;?>
				<?php 
				$aplyins=$jobdetails->getApplyInstruction();
				if(strlen($aplyins)>0) :
				?>
				<?php if(strlen($email)>5){ echo "or";} ?>
				<?php
					$string=$aplyins;
					$lists = explode("/end/",$string);	
						$c=count($lists);
						   //show all data use foreach
						$j=1;
						foreach($lists as $list) {
							$list = trim($list);
							if($j!=$c){ echo "<li class='text-align-center'>$list</li>";}
							$j++;
							}
				?>
				<?php endif;?>
			</ul>
			<label class="job-title center">Contact Info</label><p class="job-details center"><?=$jobdetails->getContactInfo()?></p>
			<label class="job-title center redcolor">Application dadeline</label><p class="job-details center"><?=$jobdetails->getLastDateOfSubmission()->format("l , F d , Y")?></p>
	</div>
</div>
<div class="col-xs-12 col-sm-3">
	<img width="100%" src="<?=base_url()?>/assets/img/a.jpg" />
</div>

</div>
        </div>
      </div>