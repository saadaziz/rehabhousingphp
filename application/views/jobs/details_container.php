<div class="container">
	<ol class="breadcrumb">
  <li><a href="#">Home</a></li>
  <li><a href="#"><?=$product->getType()->getTypeName()?></a></li>
  <li class="active"><?=$product->getTitle()?></li>
</ol>
</div>
    <!--start body-->
    <div class="container">
        <!--start body right-->  
        <div class="row row-offcanvas row-offcanvas-right">
            <div class="col-xs-12 col-sm-9">
             

             
                
             <!--start hotprojecmaintArea-->      
             <div class="row">
                <div class="detailprojectArea">
                    <h1>Ad title will display here ( Header 1 Tag)</h1> 
					<h2><span class="at">at</span> Banassree, Dhaka (Header 2 tag), <span class="at">Postated by:</span> <span class="matra">MATRA Real Estate Limited</span></h2>
					<span class="at2">Last Update: 20 Nov, 01.23 PM</span>
					<div class="shareDiv">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><a href="#"><img border="0" src="images/report.png" width="17">&nbsp;Report as Fraud</a></td>
								<td><a href="#"><img border="0" src="images/mail.png" width="17">&nbsp;Email to Friend</a></td>
								<td><a id="showShareDiv" href="#"><img border="0" src="images/sare.png" width="15">&nbsp;Share</a></td>
								<td></td>
							</tr>
						</table>
						
						<div class="hideShare" id="shareDiv">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><a href="#"><img border="0" src="images/fb.png" width="20"></a></td>
									<td><a href="#"><img border="0" src="images/yy.png" width="20"></a></td>
									<td><a href="#"><img border="0" src="images/tt.png" width="20"></a></td>
									<td><a href="#"><img border="0" src="images/in.png" width="20"></a></td>
								</tr>
							</table>
						</div>
					</div>
					 <div class="underLine">
					------------------------------------------------------------------------------------------------------------------------------------------
					</div>
                   
                    <!--start detail arer--> 
                   <div class="detailArea">
                   		<!--start detail left--> 
                   		<div class="detailBox">
                               <!--start tab-->
                         
                               <div class="imgTab">
                                    <div class="tabBt active" target="photo">
                                    	<img src="images/photo.png" width="18" height="16"><br /><br />Photos
                                    </div>
                                    <div class="tabBt" target="plan">
                                    	<img src="images/fplan.png" width="18" height="16"><br /><br />Floor Plant
                                    </div>
                                    <div class="tabBt" target="status">
                                    	<img src="images/cstatus.png" width="18" height="16"><br /><br />Current Status
                                    </div>
                                    <div class="tabBt" target="map">
                                    	<img src="images/map.png" width="18" height="16"><br /><br />Location Map
                                    </div>
                               </div>
                               
                               <div id="divphoto" class="targetgalleryDiv" style="padding-top:10px; padding-bottom:10px; background:#eee; margin-top:50px;">
                               			<div id="slideshow-1">
                                            <div id="cycle-1" class="cycle-slideshow" 
                                            data-cycle-fx="tileBlind"
                                            data-cycle-prev="#slideshow-2 .cycle-prev"
                                            data-cycle-next="#slideshow-2 .cycle-next"
                                            pause-on-hover="true"
                                            style="position:relative"
                                            >
                                                <!--a href="images/image-slider-1.jpg" rel="lightbox[plants]">
                                                	<img rel="lightbox[plants]" src="images/image-slider-1.jpg"  alt="" />
                                               </a>
                                                <a href="images/image-slider-2.jpg" rel="lightbox[plants]">
                                                	<img rel="lightbox[plants]" src="images/image-slider-2.jpg"  alt="" />
                                               </a>
                                                <a href="images/image-slider-3.jpg" rel="lightbox[plants]">
                                                	<img rel="lightbox[plants]" src="images/image-slider-3.jpg"  alt="" />
                                               </a>
                                                <a href="images/image-slider-1.jpg" rel="lightbox[plants]">
                                                	<img rel="lightbox[plants]" src="images/image-slider-1.jpg"  alt="" />
                                               </a>
                                                <a href="images/image-slider-2.jpg" rel="lightbox[plants]">
                                                	<img rel="lightbox[plants]" src="images/image-slider-2.jpg"  alt="" />
                                               </a>
                                                <a href="images/image-slider-3.jpg" rel="lightbox[plants]">
                                                	<img rel="lightbox[plants]" src="images/image-slider-3.jpg"  alt="" /-->
                                               </a>
                                               	<img src="images/image-slider-1.jpg" alt="" /> 
                                                <img src="images/image-slider-2.jpg" alt="" />
                                                <img src="images/image-slider-3.jpg" alt="" /> 
                                                <img src="images/image-slider-1.jpg" alt="" />
                                                <img src="images/image-slider-2.jpg" alt="" />
                                                <img src="images/image-slider-3.jpg" alt="" /--->
                                            </div>
                                        </div>
                                        <div id="slideshow-2">
                                            <div id="cycle-2" class="cycle-slideshow"
                                            data-cycle-slides="> img"
                                            data-cycle-timeout="0"
                                            data-cycle-fx="carousel"
                                            data-cycle-carousel-visible="5"
                                            data-cycle-carousel-fluid="true"
                                            data-allow-wrap="false"
                                            style="position:relative;overflow:hidden;">
                                                
                                                <img src="images/image-slider-1.jpg" alt="" width="100" height="100" />
                                                <img src="images/image-slider-2.jpg" alt="" width="100" height="100" />
                                                <img src="images/image-slider-3.jpg" alt="" width="100" height="100" /> 
                                                <img src="images/image-slider-1.jpg" alt="" width="100" height="100" />
                                                <img src="images/image-slider-2.jpg" alt="" width="100" height="100" />
                                                <img src="images/image-slider-3.jpg" alt="" width="100" height="100" />
                                                
                                            </div>
                                            <p>
                                                <a href="#" class="cycle-prev">« prev</a> | <a href="#" class="cycle-next">next »</a>
                                            </p>
                                        </div>
                               </div>
                               
                               <div id="divplan" class="targetgalleryDiv" style="padding-top:10px; padding-bottom:10px; background:#eee; margin-top:50px;">
                               		fdsfsd
                               </div>
                               
                               <div id="divstatus" class="targetgalleryDiv" style="padding-top:10px; padding-bottom:10px; background:#eee; margin-top:50px;">
                                  greg
                               </div>
                               
                               <div id="divmap" class="targetgalleryDiv" style="padding-top:10px; padding-bottom:10px; background:#eee; margin-top:50px;">
                                  gfdgdfg
                               </div>
                               
                               <!--end tab-->
                        </div>
                        <!--end detail left-->
                        
                        <!--start info Area--> 
                       	<div class="detailBox">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td  colspan="3">Project Summary</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Project Type</td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">Apartment</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Project Name</td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">Matra Bunon</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Condition</td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">Ready</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Address</td>
                                <td>:</td>
                                <td class="sum_info">House # 12, Road # 01, Sector - 03 Banassree, Dhaka - 1220</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Size (sft</td>
                                <td>:</td>
                                <td class="sum_info">1020 sft, 1500 sft, 2040 sft</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Bed Room</td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">3 Bed, 4 Bed</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Bath Room</td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">3 Bath, 2 Bath</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Condition</td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">Ready</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Price</td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">BDT 10,000 (Per sft)</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Total Floor</td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">6</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Total Flat</td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">18 nos</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Handover Date</td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">July 02, 2015</td>
                              </tr>
                              <tr>
                                <td class="sum_Title">Features <span style="font-weight:normal; font-size:11px;">(Available)</span></td>
                                <td style="width:3%">:</td>
                                <td class="sum_info">Car parking, Gas, Electricity, Water Security, Servent room, Lift </td>
                              </tr>
                              <tr>
                                <td colspan="3" style="color:#007EFF; text-align:center;" align="center">Project By:</td>
                              </tr>
                              <tr>
                                <td colspan="3" style="color:#706e6e; text-align:center;"><b>MATRA REAL ESTATE LIMITED.</b></td>
                              </tr>
                              <tr>
                                <td colspan="3" style="text-align:center;"><img src="images/member_sticker.jpg" width="217" height="16"></td>
                              </tr>
                            </table>
                        </div>
                        <!--end info Area--> 
                        <div class="clr"></div>
                   </div>
                  
                   <!--end detail arer--> 
                   
                    <div class="underLine">
					------------------------------------------------------------------------------------------------------------------------------------------
					</div>
                    
                    <div>
                        <div class="unit">Unit Details:</div>
                        <button class="cNow">Contact Now</button>
                        <div class="clr"></div>
                    </div>
                    <div class="tbArea">
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="tableTitle">Unit Type </td>
                            <td class="tableTitle">Size</td>
                            <td class="tableTitle">Bed Room</td>
                            <td class="tableTitle">Bath Room</td>
                            <td class="tableTitle">Price</td>
                            <td class="tableTitle">Floor Plan</td>
                          </tr>
                          <tr>
                            <td class="tableContent1">Type A</td>
                            <td class="tableContent1"> 1,205 sft</td>
                            <td class="tableContent1">3, Bed</td>
                            <td class="tableContent1">3 Bath</td>
                            <td class="tableContent1">BDT 10,000</td>
                            <td class="tableContent1"><a href="#">Click to view floor plan</a></td>
                          </tr>
                          <tr>
                            <td class="tableContent2">Type A</td>
                            <td class="tableContent2"> 1,205 sft</td>
                            <td class="tableContent2">3, Bed</td>
                            <td class="tableContent2">3 Bath</td>
                            <td class="tableContent2">BDT 10,000</td>
                            <td class="tableContent2"><a href="#">Click to view floor plan</a></td>
                          </tr>
                        </table>
                    </div>
                    
                    
                    <div class="unit2">Description:</div>
                    
                     <div class="underLine">
					------------------------------------------------------------------------------------------------------------------------------------------
					</div>
                    
                    <div class="shortDetailArea">
                    	<h4>General:</h4>                 
						Earthquake resistant RCC framed structure
                        
                        <h4>Walls Finish:</h4>  
                        Earthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structure
                        <h4>Flooring:</h4> 
                        Earthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structure 
                        <h4>Toilet</h4>  
                         Earthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structur
                        <h4>Kitchen</h4>  
                         Earthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structureEarthquake resistant RCC framed structur
                    </div>
                    
                    <!--start intesrestArea--> 
                    <div class="InterestedArea">
                    	<div class="InterestTop">
                        	<b>Interested?</b> Get in touch with the builder.<br />
                        	<i>Please fill the form below to send the builder a message</i>
                        </div>
                        <div class="interestBox">
                        	<form>
                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td><input type="text" placeholder="name" class="interestBox_input"></td>
                                    <td><input type="email" placeholder="Email Address" required class="interestBox_input"></td>
                                  </tr>
                                  <tr>
                                    <td>
                                    	<select class="interestBox_input" style="height:30px;">
                                            <option value="" >India(+91)</option>
                                            <option value="hurr">Durr</option>
                                        </select>
                                    </td>
                                    <td><input type="text" placeholder="Mobile" class="interestBox_input"></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2"><textarea type="text" style="height:40px" placeholder="I saw your property on CommonFloor, and interested in it. Please contact me at your earliest convenience." class="interestBox_inputbig"></textarea></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">
                                    	<input type="checkbox" />&nbsp;&nbsp;Interested In<br />
                                        <input type="checkbox" />&nbsp;&nbsp;Email me when similar properties are posted<br />
                                        <input type="checkbox" />&nbsp;&nbsp;I want buying Asssistance Service<br />
                                        <input type="checkbox" />&nbsp;&nbsp;I acceept the Terms and Condition
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">
                                    	<button class="int_submit">Submit</button><br />
                                     	<span style="color:#666; font-size:10px;">Your are contact detail are extremely important and secure with us</span>
                                     </td>
                                  </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <!--end intesrestArea--> 
                    <div class="clr"></div>
                </div>
             </div>
             <!--end hotprojecmaintArea-->   
             
           
           
           <div class="detailprojectArea2">
           		<div class="listHeading">Similar Properties</div>
                <!--start bottom slider-->
                 <div style="height:350px; width:728px;">
                    <div id="slider1">
                        <div class="perBottomslidre"><a class="buttons prev" href="#"><img src="images/prev.png" width="18"></a></div>
                        <div class="viewport" style="float:left;">
                            <ul class="overview">
                                <li>
                                  <!--start hotproRow-->  
                                    <div class="projectRowbootom">
                                        <div class="projectColbottom">
                                            <div class="imgBox">
                                                <img src="images/por_img.png" width="120" height="125">
                                            </div>
                                            <div class="proName">Rehab Housing.Com</div>
                                            <div class="proLoc">Bashundhara R/A, Dhaka</div>
                                            <div class="proPrice">BDT 20,000/Sft</div>
                                            <div class="proRoome">2/3 Bed, 2/3 Bath</div>
                                            <button class="details">Details</button>
                                        </div>
                                    </div>
                                    <!--end hotproRow-->
                                </li>
                                <li>
                                    <!--start hotproRow-->  
                                    <div class="projectRowbootom">
                                        <div class="projectColbottom">
                                            <div class="imgBox">
                                                <img src="images/por_img.png" width="120" height="125">
                                            </div>
                                            <div class="proName">Rehab Housing.Com</div>
                                            <div class="proLoc">Bashundhara R/A, Dhaka</div>
                                            <div class="proPrice">BDT 20,000/Sft</div>
                                            <div class="proRoome">2/3 Bed, 2/3 Bath</div>
                                            <button class="details">Details</button>
                                        </div>
                                    </div>
                                    <!--end hotproRow-->
                                </li>									
                                 <li>
                                    <!--start hotproRow-->  
                                    <div class="projectRowbootom">
                                        <div class="projectColbottom">
                                            <div class="imgBox">
                                                <img src="images/por_img.png" width="120" height="125">
                                            </div>
                                            <div class="proName">Rehab Housing.Com</div>
                                            <div class="proLoc">Bashundhara R/A, Dhaka</div>
                                            <div class="proPrice">BDT 20,000/Sft</div>
                                            <div class="proRoome">2/3 Bed, 2/3 Bath</div>
                                           <button class="details">Details</button>
                                        </div>
                                    </div>
                                    <!--end hotproRow-->
                                </li>	
                                <li>
                                    <!--start hotproRow-->  
                                    <div class="projectRowbootom">
                                        <div class="projectColbottom">
                                            <div class="imgBox">
                                                <img src="images/por_img.png" width="120" height="125">
                                            </div>
                                            <div class="proName">Rehab Housing.Com</div>
                                            <div class="proLoc">Bashundhara R/A, Dhaka</div>
                                            <div class="proPrice">BDT 20,000/Sft</div>
                                            <div class="proRoome">2/3 Bed, 2/3 Bath</div>
                                            <div class="details"><a href="">Details</a></div>
                                        </div>
                                    </div>
                                    <!--end hotproRow-->
                                </li>
                                 <li>
                                    <!--start hotproRow-->  
                                    <div class="projectRowbootom">
                                        <div class="projectColbottom">
                                            <div class="imgBox">
                                                <img src="images/por_img.png" width="120" height="125">
                                            </div>
                                            <div class="proName">Rehab Housing.Com</div>
                                            <div class="proLoc">Bashundhara R/A, Dhaka</div>
                                            <div class="proPrice">BDT 20,000/Sft</div>
                                            <div class="proRoome">2/3 Bed, 2/3 Bath</div>
                                            <button class="details">Details</button>
                                        </div>
                                    </div>
                                    <!--end hotproRow-->
                                </li>
                                 <li>
                                    <!--start hotproRow-->  
                                    <div class="projectRowbootom">
                                        <div class="projectColbottom">
                                            <div class="imgBox">
                                                <img src="images/por_img.png" width="120" height="125">
                                            </div>
                                            <div class="proName">Rehab Housing.Com</div>
                                            <div class="proLoc">Bashundhara R/A, Dhaka</div>
                                            <div class="proPrice">BDT 20,000/Sft</div>
                                            <div class="proRoome">2/3 Bed, 2/3 Bath</div>
                                            <button class="details">Details</button>
                                        </div>
                                    </div>
                                    <!--end hotproRow-->
                                </li>
                                 <li>
                                    <!--start hotproRow-->  
                                    <div class="projectRowbootom">
                                        <div class="projectColbottom">
                                            <div class="imgBox">
                                                <img src="images/por_img.png" width="120" height="125">
                                            </div>
                                            <div class="proName">Rehab Housing.Com</div>
                                            <div class="proLoc">Bashundhara R/A, Dhaka</div>
                                            <div class="proPrice">BDT 20,000/Sft</div>
                                            <div class="proRoome">2/3 Bed, 2/3 Bath</div>
                                            <button class="details">Details</button>
                                        </div>
                                    </div>
                                    <!--end hotproRow-->
                                </li>
                            </ul>
                        </div>
                        <div class="perBottomslidre" style="position:absolute; margin-left:705px;">
                            <a class="buttons next" href="#"><img src="images/next.png" width="18"></a>
                        </div>
                    </div>
                 </div>  
                 <!--start bottom slider-->
             </div>
          </div>
           <!--end body left-->
       
            <!--startbody right-->
            <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
              <!--start intesrestArea--> 
                    <div class="InterestedLeftArea">
                    	<div class="InterestLefTop">
                        	<b>Contact to Seller</b>
                        </div>
                        <div class="interestLeftBox">
                        	<form>
                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td colspan="2"><input type="text" placeholder="name" class="interestBoxLeft_input"></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2"><input type="email" placeholder="Email Address" required class="interestBoxLeft_input"></td>
                                  </tr>
                                  <tr>
                                    <td>
                                    	<select class="interestBoxLeft_input2" style="height:20px;">
                                            <option value="" >India(+91)</option>
                                            <option value="hurr">Durr</option>
                                        </select>
                                    </td>
                                    <td><input type="text" placeholder="Mobile" class="interestBoxLeft_input2"></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2"><textarea placeholder="I saw your property on CommonFloor, and interested in it. Please contact me at your earliest convenience." class="interestBoxLeft_inputbig"></textarea></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">
                                    	<input type="checkbox" />&nbsp;&nbsp;Interested In<br />
                                        <input type="checkbox" />&nbsp;&nbsp;Email me when similar properties <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;are posted<br />
                                        <input type="checkbox" />&nbsp;&nbsp;I want buying Asssistance<br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;Service<br />
                                        <input type="checkbox" />&nbsp;&nbsp;I acceept the Terms and<br />
                                        &nbsp;&nbsp;&nbsp;&nbsp; Condition
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">
                                    	<button class="int_submit">Submit</button><br />
                                     	<span style="color:#666; font-size:10px;">Your are contact detail are extremely important and secure with us</span>
                                     </td>
                                  </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <!--end intesrestArea--> 

                <!--start right ad-->
                <div class="butterfly">
                     <h3 class="altText">Set Your Free Property Alerts</h3>
                    <button class="altButton">Subscribe to Free Alerts</button>
                 </div>  
                <!--end right ad-->


                <!--start right_top project-->
               <div class="carTopRightpor_Area">
                       <div class="listHeading"><img src="images/list_icon.png">&nbsp;&nbsp;Top Projects</div>
                         <div class="newsTopPro"> 
                         		<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="topproName" colspan="2">Top Project Name</td>
                                  </tr>	
                                  <tr>
                                    <td class="catLeftTopImg"><img src="images/home.png" width="80" height="70px;"></td>
                                    <td class="topPorText">
                                    	Bashudhara R/A<br />
                                        Dhaka<br />
                                        3 Bed I 3 Bath<br />
                                        <b>BDT 10,000</b> (Per sft)
                                    </td>
                                  </tr>
                                </table>
                         </div>    
                          <div class="newsTopPro"> 
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td class="topproName" colspan="2">Top Project Name</td>
                              </tr>	
                              <tr>
                                <td class="catLeftTopImg"><img src="images/home.png" width="80" height="70px;"></td>
                                <td class="topPorText">
                                    Bashudhara R/A<br />
                                    Dhaka<br />
                                    3 Bed I 3 Bath<br />
                                    <b>BDT 10,000</b> (Per sft)
                                </td>
                              </tr>
                            </table>
                         </div>   
                          
                           <div class="newsTopPro"> 
                         		<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="topproName" colspan="2">Top Project Name</td>
                                  </tr>	
                                  <tr>
                                    <td class="catLeftTopImg"><img src="images/home.png" width="80" height="70px;"></td>
                                    <td class="topPorText">
                                    	Bashudhara R/A<br />
                                        Dhaka<br />
                                        3 Bed I 3 Bath<br />
                                        <b>BDT 10,000</b> (Per sft)
                                    </td>
                                  </tr>
                                </table>
                        	 </div> 
                             
                              
                             <a href="">View More..</a>                   
                 	</div>
                  <!--end right_top_project-->

                

                 <!--start right visit-->
                 <div class="carTopRightpor_Area">
                   <div class="listHeading"><img src="images/list_icon.png">&nbsp;&nbsp;Visitor Statistics</div>
                   	  <div style="width:235px; margin-left:5px;">
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                         	  <tr class="vis_row">
                                <td style="padding:0 0 0 5px;"><b>COUNTRY</b></td>
                                <td style="text-align:right; padding: 0 5px 0 0;"><b>VISIT</b></td>
                              </tr>	
                              <tr class="vis_row">
                                <td style="padding:0 0 0 5px;">India</td>
                                <td style="text-align:right; padding: 0 5px 0 0;">97</td>
                              </tr>
                               <tr class="vis_row">
                                 <td style="padding:0 0 0 5px;">USA</td>
                                <td style="text-align:right; padding: 0 5px 0 0;">97</td>
                              </tr>
                               <tr class="vis_row">
                                 <td style="padding:0 0 0 5px;">India</td>
                                <td style="text-align:right; padding: 0 5px 0 0;">97</td>
                              </tr>
                              <tr class="vis_row">
                                <td style="padding:0 0 0 5px;">India</td>
                                <td style="text-align:right; padding: 0 5px 0 0;">97</td>
                              </tr>
                              <tr class="vis_row">
                                 <td style="padding:0 0 0 5px;">India</td>
                                <td style="text-align:right; padding: 0 5px 0 0;">97</td>
                              </tr>
                          </table>
                       </div>
                 </div>
                 <!--end rightvisit-->  
                 
               <!--start_right_seller_project-->
               <div class="carTopRightpor_Area">
                       <div class="listHeading"><img src="images/list_icon.png">&nbsp;&nbsp;More Properties of This Seller</div>
                         <div class="newsTopPro"> 
                         		<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="topproName" colspan="2">Top Project Name</td>
                                  </tr>	
                                  <tr>
                                    <td class="catLeftTopImg"><img src="images/home.png" width="80" height="70px;"></td>
                                    <td class="topPorText">
                                    	Bashudhara R/A<br />
                                        Dhaka<br />
                                        3 Bed I 3 Bath<br />
                                        <b>BDT 10,000</b> (Per sft)
                                    </td>
                                  </tr>
                                </table>
                         </div>    
                          <div class="newsTopPro"> 
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td class="topproName" colspan="2">Top Project Name</td>
                              </tr>	
                              <tr>
                                <td class="catLeftTopImg"><img src="images/home.png" width="80" height="70px;"></td>
                                <td class="topPorText">
                                    Bashudhara R/A<br />
                                    Dhaka<br />
                                    3 Bed I 3 Bath<br />
                                    <b>BDT 10,000</b> (Per sft)
                                </td>
                              </tr>
                            </table>
                         </div>   
                          
                       <div class="newsTopPro"> 
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td class="topproName" colspan="2">Top Project Name</td>
                              </tr>	
                              <tr>
                                <td class="catLeftTopImg"><img src="images/home.png" width="80" height="70px;"></td>
                                <td class="topPorText">
                                    Bashudhara R/A<br />
                                    Dhaka<br />
                                    3 Bed I 3 Bath<br />
                                    <b>BDT 10,000</b> (Per sft)
                                </td>
                              </tr>
                            </table>
                         </div> 
                         <a href="">View More..</a>                   
                 	</div>
                  <!--end right_seller_project-->
                 
                 
              </div>
             <!--end body right-->
        </div>
      </div>
      <!--end body-->