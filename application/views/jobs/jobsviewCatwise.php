<style type="text/css">
	.table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
		background-color: #DBF4FD;
	}
	.jobTitle {
		background: #22B8F1 !important;
		text-align: center;
		color: #FFFFFF;
	}
	.title-head{
		width: 20%;
		border-right: 1px solid #E7E7E7;
	}
	.jobSl{
		background: #84B542 !important;
		width: 10px;
	}
	td{padding:0px 2px;}
	.last-date{min-width: 110px;}
	.company-head {
		width: 25%;
		border-right: 1px solid #E7E7E7;
	 }
	.education-head {
		border-right: 1px solid #E7E7E7;
		text-align: center;
		width: 61%;
	}
	.job-heading {
		color: #22B8F1;
		font-size: xx-large;
		padding-top: 10px;
		border-bottom: 1px solid;
		margin-bottom: 15px;
		padding-bottom: 5px;
	}
	.joball a {
		color: #000;
		font-size: inherit;
	}
	.job-number {
		font-weight: normal;
		color: #909292;
	}
	.table-body{
		text-align: justify;
	}
	.table-body ul {
		margin: 0px !important;
		padding: 0px !important;
		padding-left: 15px !important;
	}
	.xx-small {
		font-size: xx-small;
		float: right;
		margin-top: 28px;
	}
	.latest-jobs {
		font-size: 16px;
		font-weight: bold;
		color: #666;
	}
	.education ul {
		padding: 0 !important;
		margin: 0px 0px 0px 16px !important;
	}
	.last-date {
		text-align: center;
		font-size: 11px;
	}
	.job-details p {
		background: url(http://172.16.24.159:8080/rehabhousing/img/bulet.png);
	}
</style>
<div class="container">
        <!--start body right-->  
        <div class="row row-offcanvas row-offcanvas-right">
            <div class="col-xs-12 col-sm-9">
	<h2 class="job-heading">Real Estate Jobs<a href="<?=base_url()?>jobs"><label class="xx-small">< Back to Jobs Home</label></a></h2>

	<div class="clear"></div>
	<div>
		<div class="latest-jobs">
				<?php 
					switch ($jobCategory)
					{
					case "AccountingFinance":
					  echo "Accounting/Finance Jobs";
					  break;
					case "HRAdmin":
					  echo "HR/Admin Jobs";
					  break;
					case "MarketingSales":
					  echo "Marketing & Sales Jobs";
					  break;
					case "DesignCreative":
					  echo "Design & Creative Jobs";
					  break;
					case "CustomerSupportFontDesk":
					  echo "Customer Support/Font Desk Jobs";
					  break;
					default:
					  echo $companyName." Jobs";
					}
				?>
		</div>
		<table class="table table-striped">
			<tr>
				<th class="jobSl"></th>
				<th class="jobTitle title-head">Job Title</th>
				<th class="jobTitle company-head">Company</th>
				<th class="jobTitle education-head">Education</th>
				<th class="jobTitle last-date">Last Date</th>
			</tr>
			<?php $i=1; foreach ($catjobs as $catjob): ?>
			<tr>
				<td class=""><?=$i++?></td>
				<td class=""><a href="<?=base_url()?>jobs/details/<?=$catjob->getJobsId()?>"><?= $catjob->getTitle() ?></a></td>
				<td class=""><?= $catjob->getCompanyName() ?></td>
				<td class="education">
					<ul>
					<?php 
						$string=$catjob->getEducationRequirements(); 
						$lists = explode("/end/",$string);	
						$c=count($lists);
						   //show all data use foreach
						$j=1;
						foreach($lists as $list) {
							$list = trim($list);
							if($j!=$c){ echo "<li>$list</li>";}
							$j++;
							}
					?>
					</ul>
				</td>
				<td class="last-date">
					<?= $catjob->getLastDateOfSubmission()->format("l")?>
					<?="<br>"?>
					<?= $catjob->getLastDateOfSubmission()->format("F d , Y")?>
					<br>(
					<?php
						$curdate = new DateTime();
						$lastdate = $catjob->getLastDateOfSubmission();
						$interval = $curdate->diff($lastdate);
						$month=$interval->m; 
						if($month==1){echo $month." month and ";}
						elseif($month>1){echo $month." months and ";}
						echo $interval->d.' days left';
					?>)
				</td>
			</tr>
			<?php endforeach ?>
		</table>
		<?= $this->pagination->create_links()?>
	</div>
</div>
<div class="col-xs-12 col-sm-3">
	<img width="100%" src="<?=base_url()?>/assets/img/a.jpg" />
</div>

</div>
        </div>
      </div>



