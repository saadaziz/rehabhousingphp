
    <div class="topmenuArea">
        <div class="container">
            <div class="homeicon"><a href="index.html"><img alt="" src="<?=base_url()?>assets/images/home.png" width="25px;"></a></div>
            <ul id="navmenu">
                    <li><a href="#">FOR SALE</a>
                        <ul>
                            <li>
                                <div class="submenudiv sorsale">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody><tr>
                                           <td class="submenudiv_link" style="width: 160px;">
                                                <b><u>Ready</u></b><br>
                                                <a href="">Apartment/Flat</a><br>
                                                <a href="">Residential/plot</a><br>
                                                <a href="">Commercial/plot</a><br>
                                                <a href="">Commercial/space</a><br>
                                                <a href="">Agricultural/plot</a><br>
                                                <a href="">Duplex Home</a><br>		
                                                <a href="">House</a><br>
                                                <a href="">Studio Apartment</a>                                               
                                            </td>
                                            <td class="submenudiv_link">
                                                <b><u>Ongoing</u></b><br>
                                                <a href="">Apartment/Flat</a><br>
                                                <a href="">Residential/plot</a><br>
                                                <a href="">Commercial/plot</a><br>
                                                <a href="">Commercial/space</a><br>
                                                <a href="">Agricultural/plot</a><br>
                                                <a href="">Duplex Home</a><br>	
                                                <a href="">House</a><br>
                                                <a href="">Studio Apartment</a>                                               
                                            </td>
                                            <td class="submenudiv_link">
                                                <b><u>Upcoming</u></b><br>
                                                <a href="">Apartment/Flat</a><br>
                                                <a href="">Residential/plot</a><br>
                                                <a href="">Commercial/plot</a><br>
                                                <a href="">Commercial/space</a><br>
                                                <a href="">Agricultural/plot</a><br>
                                                <a href="">Duplex Home</a><br>	
                                                <a href="">House</a><br>
                                                <a href="">Studio Apartment</a>                                               
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b><u>Size (Sft)</u></b></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">   
                                                <a href="">Less Than 500</a><br>
                                                <a href="">1200 Sft -  1500 Sft</a><br>
                                                <a href="">3000 Sft- 3500 Sft</a>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="">500 Sft -  800 Sft</a><br>
                                                <a href="">1500 Sft -  2000 Sft</a><br>
                                                <a href="">800 Sft -  1000 Sft</a>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="">2000 Sft -  2500 Sft</a><br>
                                                <a href="">1000 Sft -  1200 Sft</a><br>
                                                <a href="">2500 Sft- 3000 Sft</a>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b><u>City</u></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="submenudiv_link">
                                                <a href="">Dhaka City</a>
                                                <a class="loc_left" href="">Chittagong City</a>
                                                <a class="loc_left" href="">Sylhet City</a>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><b><u>Divisions</u></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="submenudiv_link">
                                                <a href="">Dhaka Division</a>
                                                <a class="loc_left2" href="">Chittagong Division</a>
                                                <a class="loc_left2" href="">Sylhet Division</a>
                                                <a class="loc_left2" href="">Khulna Division</a><br>
                                                <a href="">Rajshahi Division</a>
                                                <a class="loc_left2" href="">Rangpur Division</a>
                                                <a class="loc_left2" href="">Barisal Division</a>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b><u>Price</u></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="submenudiv_link">
                                                <a href="">Less than 30 lac</a>
                                                <a class="loc_left" href="">30- 45 lac </a>
                                                <a class="loc_left" href="">45-60 Lac</a>
                                                <a class="loc_left" href="">60-  80 Lac</a>
                                                <a class="loc_left" href=""> 80-   100 -Lac</a>
                                            </td>
                                        </tr>
                                       
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>   
                    <li class="menuDevider"></li>    
                    <li><a href="#">FOR RENT</a>
                        <ul>
                            <li>
                                <div class="submenudiv sorsale">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                      
                                        <tbody><tr>
                                            <td colspan="3"><b><u>Type</u></b></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">   
                                                <a href="">Apartment/Flat</a><br>
                                                <a href="">House</a><br>
                                                <a href="">Room</a>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="">Studio Apartment</a><br>
                                                <a href="">Sublet</a><br>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="">Roommate</a><br>
                                                <a href="">Commercial/Space</a>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b><u>Rent For</u></b></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">
                                                <a href="">Family</a>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="">Bachelor(Men/Women)</a>
                                            </td>
                                            <td></td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b><u>Monthly Rent</u></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="submenudiv_link">
                                                <a href="">Less Than BDT 2000</a>
                                                <a class="loc_left2" href="">BDT 2000 - 5000</a>
                                                <a class="loc_left2" href="">BDT  5000 - 8000</a>
                                                <a class="loc_left2" href="">BDT  8000 - 12000</a><br>
                                                <a href="">BDT 12000 - 16000</a>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b><u>Bedroom</u></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="submenudiv_link">
                                                <a href="">1</a>
                                                <a class="loc_left2" href="">2</a>
                                                <a class="loc_left2" href="">3</a>
                                                <a class="loc_left2" href="">4</a>
                                                <a class="loc_left2" href="">5</a>
                                                <a class="loc_left2" href="">6</a>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">LOCATIONS</a>
                         <ul>
                            <li>
                                <div class="submenudiv sorsale">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                      
                                        <tbody><tr>
                                            <td><b><u>City</u></b></td>
                                            <td><b><u>Zone</u></b></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">   
                                                <a class="showSingle" target="1">Dhaka City</a><br>
                                                <a class="showSingle" target="2">Chittagong City</a><br>
                                                <a class="showSingle" target="3">Sylhet City</a><br>
                                                <a class="showSingle" target="4">Dhaka Division</a><br>
                                                <a class="showSingle" target="5">Chittagong Divisionn</a><br>
                                                <a class="showSingle" target="6">Sylhet Division</a><br>
                                                <a class="showSingle" target="7">Barishal Division</a><br>
                                                <a class="showSingle" target="8">Khulna Division</a><br>
                                                <a class="showSingle" target="9">Rajshahi Division</a><br>
                                                <a class="showSingle" target="10">Rangpur Division</a> 
                                            </td>
                                            <td style="width: 350px;" class="submenudiv_link">
                                                <table style="display: table;" id="div1" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>


                                                <table style="display: none;" id="div2" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Chittagong City</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>

                                                <table style="display: none;" id="div3" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Chittagong_3</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>

                                                <table style="display: none;" id="div4" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Chittagong_4</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>

                                                <table style="display: none;" id="div5" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Chittagong_3</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>

                                                <table style="display: none;" id="div6" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Chittagong_6</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>

                                                <table style="display: none;" id="div6" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Chittagong_6</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>

                                                <table style="display: none;" id="div7" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Chittagong_3</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>

                                                <table style="display: none;" id="div8" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Chittagong_8</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>

                                                <table style="display: none;" id="div9" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Chittagong_9</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>

                                                <table style="display: none;" id="div10" class="targetDiv">
                                                    <tbody><tr>
                                                        <td><a href="">Chittagong_10</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                     
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                      <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                     <tr>
                                                        <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                         <td><a href="">Mirpur</a></td>
                                                    </tr>  
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><a href=""><b>More...</b></a></td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">REAL ESATE JOBS</a>
                        <ul>
                            <li>
                                <div class="submenudiv sorsale" style="width: 340px;">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody><tr>
                                           <td class="submenudiv_link" style="width: 200px;">
                                                <a href="">Accounting/Finance</a><br>
                                                <a href="">HR/Admin</a><br>
                                                <a href="">Marketing &amp; sales</a><br>
                                                <a href="">Customer Suport/Front Desk</a>                                             
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="">Design &amp; Creative</a><br>
                                                <a href="">IT</a><br>
                                                <a href="">Legal</a><br>                                             
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">NEWS</a></li>
                    <li class="menuDevider"></li> 
                    <li><a href="#"> OUR SERVICES</a>
                        <ul>
                            <li>
                                <div class="submenudiv sorsale" style="width: 350px;">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody><tr>
                                           <td class="submenudiv_link" style="width: 160px;">
                                                <b><u>Services for Buye</u></b><br>
                                                <a href="">Set Property alert</a><br>
                                                <a href="">Browse Property</a><br>
                                                <a href="">buying assistance service for Local/NRB Buyer</a><br>                                             
                                            </td>
                                            <td class="submenudiv_link vLine">
                                                <b><u>Services for Seller</u></b><br>
                                                <a href="">Post free ad</a><br>
                                                <a href="">Premium Membership</a><br>
                                                <a href="">Top Project Advertiesment</a><br>
                                                <a href="">Banner Advertiesment</a><br>                                               
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="2"><div class="br_line"></div></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><b><u>Website owner</u></b></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">   
                                                <a href="">Be Our Partner</a><br>
                                            </td>
                                           <td></td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">HOME LOAN</a></li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">DEVELOPERS</a></li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">HELP</a>
                        <ul>
                            <li>
                                <div class="submenudiv sorsale" style="width: 120px;">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody><tr>
                                           <td class="submenudiv_link" style="width: 160px;">
                                                <a href="">FAQ</a><br>
                                                <a href="">Contact Us</a><br>
                                                <a href="">Send Quiry</a><br>                                             
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="2"><b><u>Hotline</u></b></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">   
                                                <a href="">01922101145</a><br>
                                            </td>
                                           <td></td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>
       		     </ul>
            </div>
        </div>