<style>
div#sideBar {
    width:50px;
    height:200px;
    float:left;
    position: absolute;
    position:fixed;
    top:40%;
}
.clear { 
    clear:both; 
}
div#sticker {
    background:#AAA;
    width:50px;
    height:200px;
}
.stick {
    top:0px;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
	localStorage.setItem('filters', '<?=$filters?>');
	
var ob = $.parseJSON(localStorage.getItem('filters'));
var ind=0;
$.each( ob, function( i,val ) {
	if(i=='value')
		$( "#constraints" ).html( "<div class='alert alert-info alert-dismissable col-xs-2' style='margin-left:4px; padding: 4px 35px 0px 4px;'>"+
						"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
						"<strong>"+val+"</strong></div>");
});


//sticker from simon vai
    var s = $("#sticker");
    var pos = s.position();
    $(window).scroll(function() {
        var windowpos = $(window).scrollTop();
        s.html("Distance from top:" + pos.top + "<br />Scroll position: " + windowpos);
        if (windowpos >= pos.top) {
            s.addClass("stick");
        } else {
            s.removeClass("stick"); 
        }
    });
});
</script>
<div id="sideBar">
    <!--Some content in your right column/sidebar-->
    <div id="sticker">...start scrolling to watch me stick</div>
</div>
    <script src="<?=base_url()?>assets/js/sidemenu.js"></script>
       <script type="text/javascript">
		ddaccordion.init({
			headerclass: "headerbar", 
			contentclass: "sidesubmenu", 
			revealtype: "mouseover", 
			mouseoverdelay: 200, 
			collapseprev: true, 
			defaultexpanded: [0], 
			onemustopen: true,
			animatedefault: false,
			persiststate: true, 
			toggleclass: ["", "selected"], 
			togglehtml: ["", "", ""], 
			animatespeed: "normal", 
			oninit:function(headers, expandedindices){ 
			},
			onopenclose:function(header, index, state, isuseractivated){ 
			}
		})
	</script>

  <div class="container">
	<ol class="breadcrumb">
  		<li><a href="/">Home</a></li>
    	
  		<?= isset($bd2)?'<li><a href='.base_url().'category/'.urlencode($bd1).' >'.$bd1.'</a></li><li class="active"><a href="#">'.$bd2.'</a></li>':'<li class="active">'.$bd1.'</li>'?>
	</ol>
</div>
      <div class="container"  ng-controller="ProductListCntrl">
            <div class="row row-offcanvas row-offcanvas-right">
                <!--startleft -->
                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
                    <!--start left_latest ad-->
                  <div class="catHeadLine"><img src="<?=base_url()?>assets/images/catIcon.png" height="20" width="20"> &nbsp;Refine Search</div>	
                  <div class="urbangreymenu">
                  	 	<h3 headerindex="0h" class="headerbar">Filter By Category</h3>
                        <ul style="display: none;" contentindex="0c" class="sidesubmenu">
                            <li><a href="#">Sale<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Rent<span class="rightDgt">(461)</span></a></li>
                        </ul>
                        <h3 headerindex="1h" class="headerbar">Filter By Sub-Category</h3>
                        <ul style="display: none;" contentindex="1c" class="sidesubmenu">
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                        </ul>
                      	<h3 headerindex="2h" class="headerbar">Filter By City/District</h3>
                        <ul style="display: none;" contentindex="2c" class="sidesubmenu">
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Sylhet City <span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                        </ul>
                        
                       <h3 headerindex="3h" class="headerbar">Filter By Area</h3>
                        <ul style="display: none;" contentindex="3c" class="sidesubmenu">
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Sylhet City <span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                        </ul>
                        
                        <h3 headerindex="4h" class="headerbar selected">Filter By Size</h3>
                        <ul style="display: block;" contentindex="4c" class="sidesubmenu">
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Sylhet City <span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                        </ul>
                        
                        <h3 headerindex="5h" class="headerbar">Filter By Bathroom</h3>
                        <ul style="display: none;" contentindex="5c" class="sidesubmenu">
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Sylhet City <span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                            <li><a href="#">Dhaka City<span class="rightDgt">(4561)</span></a></li>
                            <li><a href="#">Chittagong City<span class="rightDgt">(461)</span></a></li>
                        </ul>
                    </div>
                    
                     <div class="butterfly">
                         <h3 class="altText">Set Your Free Property Alerts</h3>
                        <button class="altButton">Subscribe to Free Alerts</button>
                     </div>
                    <div class="carTopRightpor_Area">
                       <div class="listHeading"><img src="<?=base_url()?>assets/images/list_icon.png">&nbsp;&nbsp;Top Projects</div>
                         <div class="newsTopPro"> 
                         		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody><tr>
                                    <td class="topproName" colspan="2">Top Project Name</td>
                                  </tr>	
                                  <tr>
                                    <td class="catLeftTopImg"><img src="<?=base_url()?>assets/images/home.png" height="70px;" width="80"></td>
                                    <td class="topPorText">
                                    	Bashudhara R/A<br>
                                        Dhaka<br>
                                        3 Bed I 3 Bath<br>
                                        <b>BDT 10,000</b> (Per sft)
                                    </td>
                                  </tr>
                                </tbody></table>
                         </div>    
                          <div class="newsTopPro"> 
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody><tr>
                                <td class="topproName" colspan="2">Top Project Name</td>
                              </tr>	
                              <tr>
                                <td class="catLeftTopImg"><img src="<?=base_url()?>assets/images/home.png" height="70px;" width="80"></td>
                                <td class="topPorText">
                                    Bashudhara R/A<br>
                                    Dhaka<br>
                                    3 Bed I 3 Bath<br>
                                    <b>BDT 10,000</b> (Per sft)
                                </td>
                              </tr>
                            </tbody></table>
                         </div>   
                          
                           <div class="newsTopPro"> 
                         		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody><tr>
                                    <td class="topproName" colspan="2">Top Project Name</td>
                                  </tr>	
                                  <tr>
                                    <td class="catLeftTopImg"><img src="<?=base_url()?>assets/images/home.png" height="70px;" width="80"></td>
                                    <td class="topPorText">
                                    	Bashudhara R/A<br>
                                        Dhaka<br>
                                        3 Bed I 3 Bath<br>
                                        <b>BDT 10,000</b> (Per sft)
                                    </td>
                                  </tr>
                                </tbody></table>
                        	 </div> 
                             
                              <div class="newsTopPro"> 
                         		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody><tr>
                                    <td class="topproName" colspan="2">Top Project Name</td>
                                  </tr>	
                                  <tr>
                                    <td class="catLeftTopImg"><img src="<?=base_url()?>assets/images"/home.png" height="70px;" width="80"></td>
                                    <td class="topPorText">
                                    	Bashudhara R/A<br>
                                        Dhaka<br>
                                        3 Bed I 3 Bath<br>
                                        <b>BDT 10,000</b> (Per sft)
                                    </td>
                                  </tr>
                                </tbody></table>
                        	 </div>
                             <a href="">View More..</a>                   
                 	</div>
                </div>
                <!--endtLeft -->
              
                <!--startRight --> 
                <div class="col-xs-12 col-sm-9">     
                     <div class="row">
                     	<!--startcatgorry body -->
                      	<div class="catBody">
                        	<div class="catBg">
                            	<div class="catbutton_1"></div>
                            	<div class="showtabSingle catbutton active" target="A">All Ads<span class="adAll">2013</span></div>
                                <div class="showtabSingle catbutton" target="B">Premium Ads<span class="adAll">2013</span></div>
                                <div class="showtabSingle catbutton" target="C">Top Projects<span class="adAll">2013</span></div>
                            </div>
                            <div style="width: 700px; margin-left: 20px; margin-top: 10px;">
                            	<table width="98%">
                            		<tr>
                            			<td><h2 style="color: #333">Your Search</h2></td>
                            			<td style="text-align: right;"><a href="" class="btn btn-default">Set Email alert</a></td>
                            		</tr>
                            		<tr>
                            			<td colspan="2">
                            				<div id="constraints">
	                            				
												</div>
											</div>
                            			</td>
                            			
                            		</tr>
                            	</table>
                            </div>
							<div style="margin-top: 10px;">
	                            <!--startfrist tab div -->
	                            <div style="display: block;" id="divA" class="targettabDiv">
	                            	<!--start row -->
	                            	<?php foreach ($products as $key):?>
	                            		<div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	<?=$key->getTitle()?>
	                                            	<span class="today">
	                                            		<b class="bdt">BDT <?=getmin($key->getPrice())?></b> (Per )<br />
	                                            		<img src="<?=base_url()?>assets/images/premium.png"  width="100"><br />
	                                            		<?=$key->getInsertTime()->format('d,F Y')?>
	                                            	</span>
	                                            </li>
	                                            <li><?=$key->getType()->getTypeName()?> for sale at <?=$key->getZone()->getNameEn()?>-<?=$key->getDivision()->getDivisionEn()?></li>
	                                            <li><?=$key->getSize()?></li>
	                                            <li><?=($key->getBedroom()!='')?$key->getBedroom().' Bed': ''?>   </li>
	                                            <li class="porBy">
	                                            	Project By: <?=$key->getUser()->getName()?>  I  <?= ($key->getUser()->getPremium()=='yes')?'Premium Member':'Member'?>
	                                            	
														
												
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                            		<?php endforeach ?>
	                            	
								<?= $this->pagination->create_links();?>
	                              <!--end row -->
	                            
	                            </div>
	                            <!--endfrist tab div -->
	                            
	                            <!--start2nd tab div -->
	                            <div style="display: none;" id="divB" class="targettabDiv">
	                            	<!--start row -->
	                            	<div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Premium Ads for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              
	                              <!--start row -->
	                              <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Premium Ads for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              
	                              <!--start row -->
	                              <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Premium Ads for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              
	                              <!--start row -->
	                              <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Premium Ads for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              
	                              <!--start row -->
	                              <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Premium Ads for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              <div class="pegDiv">
	                                  <ul class="pagination">
	                                      <li><a href="#">«</a></li>
	                                      <li><a href="#">1</a></li>
	                                      <li><a href="#">2</a></li>
	                                      <li><a href="#">3</a></li>
	                                      <li><a href="#">4</a></li>
	                                      <li><a href="#">5</a></li>
	                                      <li><a href="#">6</a></li>
	                                      <li><a href="#">7</a></li>
	                                      <li><a href="#">107</a></li>
	                                      <li><a href="#">Next</a></li>
	                                      <li><a href="#">Last</a></li>
	                                      <li><a href="#">»</a></li>
	                                   </ul>
	                              </div>
	                            </div>
	                          	<!--end 2nd Tab div -->
	                            
	                            <!--start 3rd Tab div -->
	                            <div style="display: none;" id="divC" class="targettabDiv">
	                            	 <!--start row -->
	                            	 <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Top Projects for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                               <!--end row -->
	                               
	                               <!--start row -->
	                              <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Top Projects for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              <div class="pegDiv">
	                                  <ul class="pagination">
	                                      <li><a href="#">«</a></li>
	                                      <li><a href="#">1</a></li>
	                                      <li><a href="#">2</a></li>
	                                      <li><a href="#">3</a></li>
	                                      <li><a href="#">4</a></li>
	                                      <li><a href="#">5</a></li>
	                                      <li><a href="#">6</a></li>
	                                      <li><a href="#">7</a></li>
	                                      <li><a href="#">107</a></li>
	                                      <li><a href="#">Next</a></li>
	                                      <li><a href="#">Last</a></li>
	                                      <li><a href="#">»</a></li>
	                                   </ul>
	                              </div>
	                            </div> 
                            <!--end 3rd Tab div --> 
                            </div>
                        </div>
                        <!--end catgorry body -->
                      </div>
              	 </div>
                <!--endRight -->  
		    </div>
	    </div>
	    

  



