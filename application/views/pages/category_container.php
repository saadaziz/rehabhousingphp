<style>
div#sideBar {
    width:50px;
    height:200px;
    float:left;
    position: absolute;
    position:fixed;
    top:40%;
}
.clear { 
    clear:both; 
}
div#sticker {
    background:#AAA;
    width:50px;
    height:200px;
}
.stick {
    top:0px;
}
</style>
<script type="text/javascript">
    function setfilters()
    {
        var ob = $.parseJSON(localStorage.getItem('filters'));
        var ind=0;
        $.each( ob, function( i,val ) {
            //if(i=='value')
            //alert(i+" "+val.value);
            $( "#constraints" ).append( "<td><div class='alert alert-info alert-dismissable ' style='margin-left:4px; '>"+
                "<button type='button' id="+i+" class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                "<strong>"+val.value+"</strong></div></td><td></td><td></td><td></td><td></td>");

        });
    }
$(document).ready(function() {
	localStorage.setItem('filters', '<?=$filters?>');
    localStorage.setItem('webroot', '<?=base_url()?>');


    setfilters();
    var s = $("#sticker");
    var pos = s.position();

    var offset=15,position=800;
    $(window).scroll(function() {
        var windowpos = $(window).scrollTop();
        s.html("Distance from top:" + pos.top + "<br />Scroll position: " + windowpos);
        if (windowpos >= pos.top) {
            s.addClass("stick");
        } else {
            s.removeClass("stick"); 
        }
        if(windowpos >position)
        {
            position= position*2;
            $.post( localStorage.getItem('webroot')+"pages/category/getmore/"+offset, function( data ) {
               offset+=15;
                console.log(data);
                $("#divA").append(data);<!--div data loaded-->
            });
        }
    });
    $('.close').on('click',function(){
        var currentfilter = $.parseJSON(localStorage.getItem('filters')),arr = new Array();
        var inputfilter = ($(this).next().html());
        $.each( currentfilter, function( i,val ) {
            if(val.value!=inputfilter)
                arr.push(val);
        });
        localStorage.setItem('filters',JSON.stringify(arr));
        //currentfilter.push({"key");
        console.log(arr);
    });
    $('.districtAlter').on('click',function(){
       var inputfilter = ($(this).attr('href')),arr=new Array();
        var foundinCurrent='0';
        alert(inputfilter);
        var currentfilter = $.parseJSON(localStorage.getItem('filters'));
        $.each( currentfilter, function( i,val ) {

            if(i==inputfilter.split(':')[0].replace('#',''))
            {
                val= inputfilter.split(':')[1];
                foundinCurrent='1';
            }
           // else
            arr.push(val);
        });
        var indx=inputfilter.split(':')[0].replace('#',''),
            val=inputfilter.split(':')[1];
        console.log(arr);

        if(foundinCurrent=='0')
            arr.push({"key":$(this).attr('key'),"value":val});
        localStorage.setItem('filters',JSON.stringify(arr));
            //currentfilter.push({"key");
       console.log(arr);
        $( "#constraints" ).html( "");
        setfilters();
        $.ajax({
            url: "<?=base_url()?>pages/category/loadfiltereddata/",
            type: "POST",
            data: {arr:JSON.stringify(arr)},
            success: function(response2) {
                console.log(response2);
                $("#divA").html(response2);<!--div data loaded-->
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText);
                alert(thrownError);
            }
        });
        if($(this).attr('key')=="district"){
            $.post(  localStorage.getItem('webroot')+"pages/category/loadcities/"+$(this).attr('tagid'), function( data ) {
                console.log(data);
                $('#ulcity').html(data);
            });
        }
    });
});
</script>
<style>
    .newalert{

    }
</style>
<div id="sideBar">
    <!--Some content in your right column/sidebar-->
    <div id="sticker">...start scrolling to watch me stick</div>
</div>

	
  <div class="container">
	<ol class="breadcrumb">
  		<li><a href="/">Home</a></li>
    	
  		<?= isset($bd2)?'<li><a href='.base_url().'category/'.urlencode($bd1).' >'.$bd1.'</a></li><li class="active"><a href="#">'.$bd2.'</a></li>':'<li class="active">'.$bd1.'</li>'?>
	</ol>
</div>
      <div class="container">
            <div class="row row-offcanvas row-offcanvas-right">
                <!--startleft -->
                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
                    <!--start left_latest ad-->
                  <div class="catHeadLine"><img src="<?=base_url()?>assets/images/catIcon.png" height="20" width="20"> &nbsp;Refine Search</div>	
                  <div class="urbangreymenu">
                  	 	<h3 headerindex="0h" class="headerbar">Property For</h3>
                        <ul  contentindex="0c" class="sidesubmenu">
                            <?php foreach($cats as $key): ?>
                                <li><a class="districtAlter" key="category" href="#category:<?=urlencode($key->getTypeName())?>"><?=$key->getTypeName()?><span class="rightDgt">(<?=$key->getAmount()?>)</span></a></li>
                            <?php endforeach ?>
                        </ul>
                        <h3 headerindex="1h" class="headerbar">Property Type</h3>
                        <ul  contentindex="1c" class="sidesubmenu">
                            <?php foreach($subcats as $key): ?>
                                <li><a class="districtAlter" key="subcateogry" href="#subcategory:<?=urlencode($key->getTypeName())?>"><?=$key->getTypeName()?><span class="rightDgt">(<?=$key->getAmount()?>)</span></a></li>
                            <?php endforeach ?>
                        </ul>
                        <h3 headerindex="1h" class="headerbar">Property Condition</h3>
                        <ul  contentindex="1c" class="sidesubmenu">
                            <?php foreach($subcats as $key): ?>
                                <li><a class="districtAlter" key="subcateogry" href="#subcategory:<?=urlencode($key->getTypeName())?>"><?=$key->getTypeName()?><span class="rightDgt">(<?=$key->getAmount()?>)</span></a></li>
                            <?php endforeach ?>
                        </ul>
                      	<h3 headerindex="2h" class="headerbar">City/District</h3>
                        <ul  contentindex="2c" class="sidesubmenu">
                            <?php foreach($districts as $key): ?>
                                <li><a class="districtAlter" key="district" tagid="<?=$key->getDivisionId()?>" href="#area:<?=($key->getDivisionEn())?>"><?=$key->getDivisionEn()?><span class="rightDgt">(<?=$key->getAmount()?>)</span></a></li>
                            <?php endforeach ?>
                        </ul>
                        
                       <h3 headerindex="3h" class="headerbar">Area</h3>
                        <ul  contentindex="3c" class="sidesubmenu" id="ulcity">
                            <?php foreach($areas as $key): ?>
                                <li><a class="districtAlter" key="zone" href="#zone:<?=urlencode($key->getNameEn())?>"><?=$key->getNameEn()?><span class="rightDgt">(<?=$key->getAmount()?>)</span></a></li>
                            <?php endforeach ?>
                        </ul>
                        
                        <h3 headerindex="4h" class="headerbar selected">Size</h3>
                        <ul  contentindex="0c" class="sidesubmenu">
                  	 		<li>
                  	 			<p>
								  
								  <label  id="amount" style="border:0; color:#f6931f;"></label>
								</p>
								<div id="slider-range"></div>
                            </li>
                        </ul>
                        
                        <h3 headerindex="5h" class="headerbar">Bed Room</h3>
                        <ul  contentindex="0c" class="sidesubmenu">
                  	 		<li>
                  	 			<p>
								  
								  <label  id="bed" style="border:0; color:#f6931f;"></label>
								</p>
								<div id="slider-range2"></div>
                            </li>
                        </ul>
                        
                        <h3 headerindex="5h" class="headerbar">Bath Room</h3>
                        <ul  contentindex="0c" class="sidesubmenu">
                  	 		<li>
                  	 			<p>
								  
								  <label  id="bath" style="border:0; color:#f6931f;"></label>
								</p>
								<div id="slider-range3"></div>
                            </li>
                        </ul>
                        
                        <h3 headerindex="5h" class="headerbar">Price</h3>
                        <ul  contentindex="0c" class="sidesubmenu">
                  	 		<li>
                  	 			<p>
								  
								  <label  id="price" style="border:0; color:#f6931f;"></label>
								</p>
								<div id="slider-range4"></div>
                            </li>
                        </ul>
                    </div>
                    
                     <div class="butterfly">
                         <h3 class="altText">Set Your Free Property Alerts</h3>
                        <button class="altButton">Subscribe to Free Alerts</button>
                     </div>
                    <div class="carTopRightpor_Area">
                       <div class="listHeading"><img src="<?=base_url()?>assets/images/list_icon.png">&nbsp;&nbsp;Top Projects</div>
                         <div class="newsTopPro"> 
                         		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody><tr>
                                    <td class="topproName" colspan="2">Top Project Name</td>
                                  </tr>	
                                  <tr>
                                    <td class="catLeftTopImg"><img src="<?=base_url()?>assets/images/home.png" height="70px;" width="80"></td>
                                    <td class="topPorText">
                                    	Bashudhara R/A<br>
                                        Dhaka<br>
                                        3 Bed I 3 Bath<br>
                                        <b>BDT 10,000</b> (Per sft)
                                    </td>
                                  </tr>
                                </tbody></table>
                         </div>    
                          <div class="newsTopPro"> 
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody><tr>
                                <td class="topproName" colspan="2">Top Project Name</td>
                              </tr>	
                              <tr>
                                <td class="catLeftTopImg"><img src="<?=base_url()?>assets/images/home.png" height="70px;" width="80"></td>
                                <td class="topPorText">
                                    Bashudhara R/A<br>
                                    Dhaka<br>
                                    3 Bed I 3 Bath<br>
                                    <b>BDT 10,000</b> (Per sft)
                                </td>
                              </tr>
                            </tbody></table>
                         </div>   
                          
                           <div class="newsTopPro"> 
                         		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody><tr>
                                    <td class="topproName" colspan="2">Top Project Name</td>
                                  </tr>	
                                  <tr>
                                    <td class="catLeftTopImg"><img src="<?=base_url()?>assets/images/home.png" height="70px;" width="80"></td>
                                    <td class="topPorText">
                                    	Bashudhara R/A<br>
                                        Dhaka<br>
                                        3 Bed I 3 Bath<br>
                                        <b>BDT 10,000</b> (Per sft)
                                    </td>
                                  </tr>
                                </tbody></table>
                        	 </div> 
                             
                              <div class="newsTopPro"> 
                         		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody><tr>
                                    <td class="topproName" colspan="2">Top Project Name</td>
                                  </tr>	
                                  <tr>
                                    <td class="catLeftTopImg"><img src="<?=base_url()?>assets/images"/home.png" height="70px;" width="80"></td>
                                    <td class="topPorText">
                                    	Bashudhara R/A<br>
                                        Dhaka<br>
                                        3 Bed I 3 Bath<br>
                                        <b>BDT 10,000</b> (Per sft)
                                    </td>
                                  </tr>
                                </tbody></table>
                        	 </div>
                             <a href="">View More..</a>                   
                 	</div>
                </div>
                <!--endtLeft -->
              
                <!--startRight --> 
                <div class="col-xs-12 col-sm-9">     
                     <div class="row">
                     	<!--startcatgorry body -->
                      	<div class="catBody">
                        	<div class="catBg">
                            	<div class="catbutton_1"></div>
                            	<div class="showtabSingle catbutton active" target="A">All Ads<span class="adAll">2013</span></div>
                                <div class="showtabSingle catbutton" target="B">Premium Ads<span class="adAll">2013</span></div>
                                <div class="showtabSingle catbutton" target="C">Top Projects<span class="adAll">2013</span></div>
                            </div>
                            <div style="width: 700px; margin-left: 20px; margin-top: 10px;">
                            	<table width="98%">
                            		<tr>
                            			<td><h2 style="color: #333">Your Search</h2></td>
                            			<td style="text-align: right;"><a href="" class="btn btn-default">Set Email alert</a></td>
                            		</tr>
                            		<tr>
                            			<td colspan="2">
                            				<table style="width: 50%" >
	                            				<tr id="constraints"> </tr>
												</table>
											</div>
                            			</td>
                            			
                            		</tr>
                            	</table>
                            </div>
							<div style="margin-top: 10px;">
	                            <!--startfrist tab div -->
	                            <div style="display: block;" id="divA" class="targettabDiv">
	                            	<!--start row -->
	                            	<?= $diva?>

	                              <!--end row -->
	                            
	                            </div>
	                            <!--endfrist tab div -->
	                            
	                            <!--start2nd tab div -->
	                            <div style="display: none;" id="divB" class="targettabDiv">
	                            	<!--start row -->
	                            	<div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Premium Ads for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              
	                              <!--start row -->
	                              <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Premium Ads for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              
	                              <!--start row -->
	                              <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Premium Ads for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              
	                              <!--start row -->
	                              <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Premium Ads for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              
	                              <!--start row -->
	                              <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Premium Ads for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              <div class="pegDiv">
	                                  <ul class="pagination">
	                                      <li><a href="#">«</a></li>
	                                      <li><a href="#">1</a></li>
	                                      <li><a href="#">2</a></li>
	                                      <li><a href="#">3</a></li>
	                                      <li><a href="#">4</a></li>
	                                      <li><a href="#">5</a></li>
	                                      <li><a href="#">6</a></li>
	                                      <li><a href="#">7</a></li>
	                                      <li><a href="#">107</a></li>
	                                      <li><a href="#">Next</a></li>
	                                      <li><a href="#">Last</a></li>
	                                      <li><a href="#">»</a></li>
	                                   </ul>
	                              </div>
	                            </div>
	                          	<!--end 2nd Tab div -->
	                            
	                            <!--start 3rd Tab div -->
	                            <div style="display: none;" id="divC" class="targettabDiv">
	                            	 <!--start row -->
	                            	 <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Top Projects for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                               <!--end row -->
	                               
	                               <!--start row -->
	                              <div class="box effect4">
	                                   <div class="por_whtBox">
	                           		   		<img src="<?=base_url()?>assets/images/top_pro.png" width="90">
	                                   </div>
	                                   <div class="por_ListText">
	                                   		<ul>
	                                        	<li class="por_ListHeading">
	                                            	Top Projects for bashundhara
	                                            	<span class="today">Today, 12.00 PM</span>
	                                            </li>
	                                            <li>Apartment for sale at bashundhara, Dhaka</li>
	                                            <li>1200 sft - 1500 sft  I  3 Bed  I  3 Bath </li>
	                                            <li><b class="bdt">BDT 5,500</b> (Per sft)</li>
	                                            <li class="porBy">
	                                            	Project By: Concord Real Esate Limited  I  Member 
	                                                <span class="bdtRehab">REHAB</span>  I  <span class="bdtRehab">RAJUK</span> Approved
	                                            </li>
	                                        </ul>
	                                   </div>
	                              </div>
	                              <!--end row -->
	                              <div class="pegDiv">
	                                  <ul class="pagination">
	                                      <li><a href="#">«</a></li>
	                                      <li><a href="#">1</a></li>
	                                      <li><a href="#">2</a></li>
	                                      <li><a href="#">3</a></li>
	                                      <li><a href="#">4</a></li>
	                                      <li><a href="#">5</a></li>
	                                      <li><a href="#">6</a></li>
	                                      <li><a href="#">7</a></li>
	                                      <li><a href="#">107</a></li>
	                                      <li><a href="#">Next</a></li>
	                                      <li><a href="#">Last</a></li>
	                                      <li><a href="#">»</a></li>
	                                   </ul>
	                              </div>
	                            </div> 
                            <!--end 3rd Tab div --> 
                            </div>
                        </div>
                        <!--end catgorry body -->
                      </div>
              	 </div>
                <!--endRight -->  
		    </div>
	    </div>
	    
	
   
	
	
  



