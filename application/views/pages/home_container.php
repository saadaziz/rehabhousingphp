<div class="container">
        <!--start body right-->  
        <div class="row row-offcanvas row-offcanvas-right">
            <div class="col-xs-12 col-sm-9">
              <!--start banner ad-->
              <div class="bannerad">
                banner ad
              </div>
               <!--end banner ad-->

             <!--start body list-->
              <div class="row">
                <div class="listBox">
                    <div class="listHeading"><img src="<?=base_url()?>assets/images/list_icon.png">&nbsp;&nbsp;Property for Sale</div>
                    <ul>
                        <?php foreach ($SaleCats as $cat): ?>
							<li><a href=""><?=$cat->getTypeName()?><span>(<?=$cat->getAmount()?>)</span></a></li>
						<?php endforeach ?>
                        <li style="line-height:25px;">
                        	<a href="index.html">
	                        	<span class="menuFree2">free</span>
	                         	<img alt="" src="<?=base_url()?>assets/images/list_property.png" width="14px" height="10px;" style="margin-top: -2px;">&nbsp;<b style="color: #EDAF21">Sell Your Property</b>
	                     	</a>
                        </li>
                    </ul>
                </div>
                <div class="listBox">
                    <div class="listHeading"><img src="<?=base_url()?>assets/images/list_icon.png">&nbsp;&nbsp;Property for Rent</div>
                    <ul>
                         <?php foreach ($RentCats as $cat): ?>
							<li><a href=""><?=$cat->getTypeName()?><span>(<?=$cat->getAmount()?>)</span></a></li>
						<?php endforeach ?>
                        <li style="line-height:25px;">
                        	<a href="index.html">
	                        	<span class="menuFree2">free</span>
	                         	<img alt="" src="<?=base_url()?>assets/images/list_property.png" width="14px" height="10px;" style="margin-top: -2px;">&nbsp;<b style="color: #EDAF21;">Rent Your Property</b>
	                     	</a>
                        </li>
                    </ul>
                </div>
                <div class="listBox">
                    <div class="listHeading"><img src="<?=base_url()?>assets/images/list_icon.png">&nbsp;&nbsp;Real Eastate Jobs</div>
                    <ul>
						<?php foreach ($realEstateJobs as $cat): ?>
							<li><a href=""><?=$cat->getTitle()?></a></li>
						<?php endforeach ?>
                        <li style="line-height:25px;"><a href=""><b>Post Your Ad Here..</b></a></li>
                    </ul>
                </div>
             </div>
             <!--end body list-->
             
             <!--start body ad-->      
             <div class="row">
                <div class="bodyad">Ad</div>
                <div class="bodyad">Ad</div>
             </div>
             <!--end body ad--> 
                
             <!--start hotprojecmaintArea-->      
             <div class="row">
                <div class="hotprojectArea">
                    <div class="listHeading"><img src="<?=base_url()?>assets/images/list_icon.png">&nbsp;&nbsp;Real Eastate Jobs</div>
                    <!--start hotproRow-->  
                    
                    <?php $pcount=0;
                    for($i=1; $i <= $row_count;$i++):?>
                    	<div class="projectRow">
                    		<?php for($j=0;$j<3;$j++):?>
		                        <div class="projectCol">
		                            <div class="imgBox">
		                                <img src="<?=base_url()?>assets/images/por_img.png" height="125" width="120">
		                            </div>
		                            <div class="proName"><a href="<?=base_url()?>details/<?=$top_projects[$pcount]->getProductId()?>/<?=str_replace(' ','_',$top_projects[$pcount]->getTitle())?>"><?=$top_projects[$pcount]->getTopProjectTitle();?></a></div>
		                            <div class="proLoc"><a><?=$top_projects[$pcount]->getTopProjectLocation();?></a></div>
		                            <div class="proPrice"><?=$top_projects[$pcount]->getTopProjectPrice();?></div>
		                            <div class="proRoome"><?=$top_projects[$pcount]->getTopProjectBedbath();?></div>
		                            <a href="<?=base_url().'details/'.$top_projects[$pcount]->getProductId()?>/<?=str_replace(' ','_',$top_projects[$pcount]->getTitle())?>"><button class="details btn btn-default">Details</button></a>
		                            <?php $pcount++;
		                            if($pcount>=count($top_projects)) 
		                            {
		                            	echo "</div>";
		                            	break;
									}?>
		                           
		                        </div>
							<?php endfor;?>
						</div>
                    <!--end hotproRow-->
					<?php endfor;?>
                    <div class="clr"></div>
                </div>
             </div>
             <!--end hotprojecmaintArea-->     


               <!--start botrtom banner ad-->
              <div class="bannerad">
                 banner ad
               </div>
               <!--end botrtom banner ad--> 
                
               <!--start developerlogoArea-->      
                <div class="row">
                    <div class="hotprojectArea">
                        <div class="listHeading"><img src="<?=base_url()?>assets/images/list_icon.png">&nbsp;&nbsp;Developers</div>
                        <div class="dev_logoArea">
                            <ul>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                            </ul>
                        </div>  
                        <div class="dev_logoArea">
                            <ul>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                            </ul>
                        </div>  
                        <div class="dev_logoArea">
                            <ul>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                                <li>Logo</li>
                            </ul>
                        </div>  
                    </div>
                </div>
                <!--end developerlogoArea--> 
           </div>
            <!--end body right-->
       
            <!--startbody left-->
            <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
                <!--start left_latest ad-->
                <div class="leftad_Area">
                   <div class="listHeading"><img src="<?=base_url()?>assets/images/list_icon.png" alt="img">&nbsp;&nbsp;Latest Ads</div>
                        <div id="myCarousel" class="carousel  vertical slide" >
                            <!-- Carousel items -->
                            <div class="carousel-inner">  
                                <div class="item active">
                                    <div class="row-fluid">
                                    	<?php foreach ($activeads as $key):?>
                                    	<div class="adLeftBox">
                                            <div class="adsale"><?=$key->getType()->getParent()->getTypeName()?></div>
                                            <div class="adTitleArea">
                                                <a href="<?=base_url().'details/'.$key->getProductId().'/'.urlencode($key->getTitle())?>"><b><?=$key->getTitle()?></b></a><br />
                                                Price:<?=$key->getPrice()?><br />
                                                <i>Today</i>, 08,30 am <a href="<?=base_url().'details/'.$key->getProductId().'/'.urlencode($key->getTitle())?>">View More...</a>
                                            </div>
                                       	</div> 
                                    		<?php endforeach ?>
									</div><!--/row-fluid-->
    	                        </div><!--/item-->
     
                                <div class="item">
                                    <div class="row-fluid">
                                     <?php foreach ($ad1 as $key):?>
                                    	<div class="adLeftBox">
                                            <div class="adsale"><?=$key->getType()->getParent()->getTypeName()?></div>
                                            <div class="adTitleArea">
                                                <a href="<?=base_url().'details/'.$key->getProductId().'/'.urlencode($key->getTitle())?>"><b><?=$key->getTitle()?></b></a><br />
                                                Price:<?=$key->getPrice()?><br />
                                                <i>Today</i>, 08,30 am <a href="<?=base_url().'details/'.$key->getProductId().'/'.urlencode($key->getTitle())?>">View More...</a>
                                            </div>
                                       	</div> 
                                    		<?php endforeach ?>  
                                     </div><!--/row-fluid-->
    		                        </div><!--/item-->
    		                         <div class="item">
                                    <div class="row-fluid">
                                     <?php foreach ($ad2 as $key):?>
                                    	<div class="adLeftBox">
                                            <div class="adsale"><?=$key->getType()->getParent()->getTypeName()?></div>
                                            <div class="adTitleArea">
                                                <a href="<?=base_url().'details/'.$key->getProductId().'/'.urlencode($key->getTitle())?>"><b><?=$key->getTitle()?></b></a><br />
                                                Price:<?=$key->getPrice()?><br />
                                                <i>Today</i>, 08,30 am <a href="<?=base_url().'details/'.$key->getProductId().'/'.urlencode($key->getTitle())?>">View More...</a>
                                            </div>
                                       	</div>
                                    		<?php endforeach ?>  
                                     </div><!--/row-fluid-->
    		                        </div><!--/item-->
    	                        </div><!--/carousel-inner-->     
                         </div><!--/myCarousel-->
                    
                </div>
                <!--end left_latest ad-->

                <!--start left ad-->
                <div class="butterfly">Ad</div>    
                <!--end left ad-->
				
				<script type="text/javascript">
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');
					});
				</script>

                <!--start left_propetrty price-->
                <div class="leftad_Area">
                   <div class="listHeading"><img src="<?=base_url()?>assets/images/list_icon.png">&nbsp;&nbsp;Know Property Price</div>
                   		<div class="bs-example bs-example-tabs">
						      <ul id="myTab" class="nav nav-tabs">
						        <li class="active"><a href="#home" data-toggle="tab">Sale</a></li>
						        <li class=""><a href="#profile" data-toggle="tab">Rent</a></li>
						      </ul>
						      <div id="myTabContent" class="tab-content">
						        <div class="tab-pane fade active in" id="home">
						          	<!--start form-->
                    <div class="priceSrc">
                        <form>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                              <tr>
                                <td>
                                	<b>Category</b>
                                    <select class="form-control input-mini" id="salescat">
                                        <?php foreach($SaleCats as $scat):?>
                                        	<option value="<?=$scat->getProductTypesId()?>"><?=$scat->getTypeName()?></option>
                                        <?php endforeach ?>
                                    </select>
                                </td>
                                </tr>
                                <tr>
                                <td>
                                	<b>Location</b>
                                    <select class="form-control input-mini" id="salesloc">
                                         <?php foreach($popdiv as $locs):?>
                                        	<option value="<?=$locs->getDivisionId()?>"><?=$locs->getDivisionEn()?></option>
                                        <?php endforeach ?>
                                    </select>
                                </td>
                              </tr>
                              <tr>
                                <td class="txColor" colspan="3">--------------------------------------------</td>
                              </tr>
                            </tbody></table>
                        </form>
                   </div> 
                   <!--end form-->
                   
                    <!--start priceList-->
                   <div class="pricesrcList">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblsalesprice">
						</table>
                   </div>
                   <!--end priceList-->
						        </div>
						        <div class="tab-pane fade" id="profile">
						         	<!--start form-->
                    <div class="priceSrc">
                        <form>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                              <tr>
	                                 <td>
	                                 	<b>Category</b>
	                                    <select class="form-control input-mini" id="rentcats">
	                                         <?php foreach($RentCats as $rcat):?>
	                                        	<option value="<?=$rcat->getProductTypesId()?>"><?=$rcat->getTypeName()?></option>
	                                        <?php endforeach ?>
	                                    </select>
	                                </td>
                                </tr>
                                <tr>
	                                <td>
	                                	<b>Location</b>
	                                    <select class="form-control input-mini" id="rentlocs">
	                                          <?php foreach($popdiv as $locs):?>
	                                        	<option value="<?=$locs->getDivisionId()?>"><?=$locs->getDivisionEn()?></option>
	                                        <?php endforeach ?>
	                                    </select>
	                                </td>
                              </tr>
                              <tr>
                                <td class="txColor" colspan="3">--------------------------------------------</td>
                              </tr>
                            </tbody></table>
                        </form>
                   </div> 
                   <!--end form-->
                   
                    <!--start priceList-->
                   <div class="pricesrcList">
                   		<table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblrentprice">
						</table>
					</div>
                   <!--end priceList-->
						        </div>
						      </div>
						    </div>
                    	
                </div>
                <!--end left_propetrty price-->

                 <!--start left ad-->
                 <div class="butterfly">Ad</div>    
                 <!--end left ad-->

                 <!--start left realeastate_news-->
                 <div class="leftad_Area">
                   <div class="listHeading"><img src="<?=base_url()?>assets/images/list_icon.png">&nbsp;&nbsp;Real Eastate News</div>
                     <div  id="idnews">
                       
                     </div>
                 </div>
                 <!--end left realeastate_news-->  
              </div>
             <!--end body left-->
        </div>
        <script>
            var webroot = '/rehabhousing/';
            $(document).ready(function () {
            $('#salescat').on('change', function () {
			$.post( webroot+"pages/home/locprices/"+$(this).val()+"/"+$('#salesloc').val(), function( data ) {
						$( "#tblsalesprice" ).html( data.htmls );
				},'json');
		
			});
			$('#rentcats').on('change', function () {
			$.post( webroot+"pages/home/locprices/"+$(this).val()+"/"+$('#rentloc').val(), function( data ) {
						$( "#tblrentprice" ).html( data.htmls );
				},'json');
		
			});
			$('#rentlocs').on('change', function () {
			$.post( webroot+"pages/home/locprices/"+$('#rentcats').val()+"/"+$('#rentloc').val(), function( data ) {
						$( "#tblrentprice" ).html( data.htmls );
				},'json');
		
			});
			$('#salesloc').on('change', function () {
			$.post( webroot+"pages/home/locprices/"+$('#salescat').val()+"/"+$('#salesloc').val(), function( data ) {
						$( "#tblsalesprice" ).html( data.htmls );
				},'json');
		
			});
                var flg2='1';
                $(window).scroll(function () {
                    if ($(document).scrollTop() >199){
                        if(flg2=='1')
                        {
                        	 $.post( webroot+"pages/home/locprices/", function( data ) {
                                $('#tblsalesprice').append(data.htmls);
                            },'json');
                            $.post( webroot+"pages/home/locprices/8/8", function( data ) {
                                $('#tblrentprice').append(data.htmls);
                            },'json');
                            $.post( webroot+"pages/home/getNews/", function( data ) {
                                $('#idnews').html(data.htmls);
                            },'json');
                            flg2='0';
                        }

                    }

                });
          
           });
        </script>

</div>