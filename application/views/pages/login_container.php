<div class="container">
            <div class="loginLeft">
            	<h2>Start your Trade on RehabHousing.Com</h2>
            	<h3><?=$loginmsg?></h3>
                <div class="logTxt">
                   <input type="checkbox"> &nbsp; 45 million members in 190+ countries<br>
                   <input type="checkbox"> &nbsp; Over 2 million supplier storefronts<br>
                   <input type="checkbox"> &nbsp; Safe and simple trade solutions
                </div>
                <a class="btn postAdButton2" href="<?=base_url()?>register">Create Your Account</a>
            </div>
            <div class="loginRight">
            	<form action="<?=base_url()?>pages/login/checklogin" id="login-form" novalidate="novalidate" method="post">
                	<table border="1" cellpadding="0" cellspacing="0" width="100%">
                    	<tbody><tr>
                        	<td>
                            	<h2>Email Address:</h2>
                                <input placeholder="Email Address" id="email"  class="form-control" type="email" name="email"/>
                            </td>
                        </tr>
                        <tr>
                        	<td>
                            	<h2>Password:</h2>
                                <input placeholder="Password" type="password"  class="form-control" id="pass"  name="pass"/>
                            </td>
                        </tr>
                        <tr>
                        	<td>
                            	<button type="submit" class="btn btn-default">Login</button>
                                <br><br>
                                <a href="">Forgot password?</a><br>
                                <a href="">Join free now!</a>
                            </td>
                        </tr>
                        <tr>
                        	<td class="underLine">-----------------------------------------------------</td>
                        </tr>
                        <tr>
                        	<!--<td style="float:left;"><a href="">Sign in with: &nbsp; <img src="images/fb.png" border="0" width="20"></a></td>-->
                        </tr>
                    </tbody></table>
                </form>
            </div>
	    </div>
	    
<script src="<?= base_url();?>assets/js/md5.js"></script>
<script src="<?= base_url();?>assets/js/jquery.validate.min.js"></script>
<script>
var webroot = '/rehabhousing/';
$(document).ready(function () {
	 $('#login-form').validate({
	    rules: {
	      password: {
	        minlength: 5,
	        required: true
	      },
	      email: {
	      	email: true,
	        required: true
	      }
	    },
		  showErrors: function(errorMap, errorList) {
		    $.each(this.successList, function(index, value) {
		      return $(value).popover("hide");
		    });
		    return $.each(errorList, function(index, value) {
		      var _popover;
		      console.log(value.message);
		      _popover = $(value.element).popover({
		        trigger: "manual",
		        placement: "right",
		        content: value.message,
		        template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
		      });
		      _popover.data("popover").options.content = value.message;
		      return $(value.element).popover("show");
		    });
		  }
	  });
	  /*$('#pass').blur(function(){
	  	alert($(this).attr('name'));
	  	});*/
	  $('#login-form').submit(function(e) {
    				//alert('saaad');
	    				e.preventDefault();
						var obj = $(this), // (*) references the current object/form each time
							url = obj.attr('action'),
							method = obj.attr('method'),
							data = {};
						obj.find('[name]').each(function(index, value) {
							
							if($(this).attr('name')!='pass')
							{
								var obj = $(this),
								name = obj.attr('name'),
								value = obj.val();
								data[name] = value;
							}
							else{
								var obj = $(this),
								name = obj.attr('name'),
								value = obj.val();
								data[name] =  hex_md5(value);
								
							}
						});
						$.ajax({
							url: url,
							type: method,
							data: data,
							dataType:"json",
							success: function(response2) {
								console.log(response2);
								if(response2.stat!='1')
								{
									alert(response2.error);
								}
								else
								{
									$('#topsignup').hide();
									$('#toplogin').html('<a href="<?=base_url()?>UserPanel">User Panel</a>');
									alert(response2.successmsg);
									window.location="<?=$redirecturl?>";
								}
							},
							error:function(xhr){
								alert(xhr.responseText);
							}
						});
					//$(this).clearForm();
					return false; //disable refresh
				});
});
</script>
