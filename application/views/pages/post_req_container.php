<script>
var webroot = '/rehabhousing/';
$(document).ready(function () {
	$.post(webroot+ "admin/addproduct/getZones/"+$(this).val(), function( data ) {
			$( "#selectLoc" ).html( data );
		});
	$("input[name=choices]:radio").change(function () {
		var inp = $('input[name=choices]:checked').val();
		//alert(inp);
		$.post( webroot+"admin/addproduct/getCats/"+inp, function( data ) {
			//console.log(data);
			$('#category').html(data);
		});
	});
	$('#nmdiv').on('change', function () {
		$.post(webroot+ "admin/addproduct/getZones/"+$(this).val(), function( data ) {
			$( "#selectLoc" ).html( data );
		});
	});
	$('#post-req').validate({
	    rules: {
	      size1: {
	      	required: true
	      },
	      size2: {
	      	required: true
	      },
	      price1: {
	        required: true
	      },
	      price2: {
	        required: true
	      },
	      email: {
	      	required:true	      	
	      },
	      cell_no: {
	        minlength: 8,
	        required: true
	      },
	      uname: {
	        minlength: 8,
	        required: true
	      }
	    },
		  showErrors: function(errorMap, errorList) {
		    $.each(this.successList, function(index, value) {
		      return $(value).popover("hide");
		    });
		    return $.each(errorList, function(index, value) {
		      var _popover;
		      console.log(value.message);
		      _popover = $(value.element).popover({
		        trigger: "manual",
		        placement: "right",
		        content: value.message,
		        template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
		      });
		      _popover.data("popover").options.content = value.message;
		      return $(value.element).popover("show");
		    });
		  }
		 	
	  });	
});
</script>

<script src="<?= base_url();?>assets/js/jquery.validate.min.js"></script>
<div class="container">
            <div class="contactLeft">
            	<h2>Post Your Property Requirement</h2>
                <h4>Get matching properties delivered on your email</h4>
                <div class="post1">1</div>
                <div class="post2">Tell us your requirements</div>
                <span style="float:right; margin-right:8px;">Fields marked with an asterik are mandatory</span>
                	<div class="contacDiv">
                        <form action="" id="post-req" novalidate="novalidate">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            	<tbody><tr>
                                    <td>Want to</td>
                                    <td><div id="radiodiv"><input type="radio" class="" checked="checked" name="choices" value="3">&nbsp;Buy&nbsp;&nbsp;&nbsp;<input type="radio" value="4" name="choices">&nbsp;Rent</div></td>
                               </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                               <tr>
                                    <td>Porperty Type:</td>
                                    <td>
                                    	<select id="category" name="category"  class="form-control input-xsmall contawidth"  >
									    	<?php if(isset($cats)) {
														echo '<option value="0" selected="selected">Select One</option>'; 
														foreach ($cats as $key ) {
															echo '<option value="'.$key->getProductTypesId().'" >'.$key->getTypeName().'</option>';
														}
													 }?>
									 	</select>
                                    </td>
                                    <td>&nbsp;</td>
                              </tr>	
                               <tr><td colspan="3">&nbsp;</td></tr>
                               <tr>
                                    <td>Condition:</td>
                                    <td>
                                       <select class="contawidth form-control">
                                          <option value="0">-- Select Condition --</option> 
                                          <option value="Ongoing">Ongoin</option>  
                                          <option value="Ready">Ready</option>  
                                          <option value="Upcoming">Upcoming</option> 
                                      </select>
                                    </td>
                              </tr>	
                              <td>&nbsp;</td>
                               <tr><td colspan="3">&nbsp;</td></tr>
                               <tr>
                                    <td>Division/City:</td>
                                    <td>
                                       <select class="contawidth form-control" name="nmdiv" id="nmdiv">
                                          <option value="0">-- Select City --</option> 
                                          <?php foreach($city as $key):?>
                                          		<option value="<?=$key->getDivisionId()?>"><?=$key->getDivisionEn()?></option> 
                                          <?php endforeach?>
                                      </select>
                                    </td>
                                    <td>&nbsp;</td>
                              </tr>	
                                 <tr><td colspan="3">&nbsp;</td></tr>
                               <tr>
                                    <td>Location:</td>
                                    <td>
                                       <select class="contawidth form-control" name="nmloc" id="selectLoc">
                                         
                                      </select>
                                    </td>
                                    <td>&nbsp;</td>
                              </tr>	
                               <tr><td colspan="3">&nbsp;</td></tr>
                            	<tr>
                                    <td>Size:</td>
                                    <td><input class="contawidth form-control input-mini input-mini2" required="" placeholder="Size from" type="text" name="size1">
                                    <spann class="spanTo">To</spann> 
                                    <input class="contawidth form-control input-mini input-mini2" required="" placeholder="Size To" type="text" name="size2"></td>
                               </tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
                            	<tr>
                                    <td>Bed:</td>
                                    <td colspan="2"><select name="bed" class="contawidth form-control input-mini3">
                                    	<option value="1">1</option>
                                    	<option value="2">2</option>
                                    	<option value="3">3</option>
                                    	<option value="4">4</option>
                                    	<option value="5">5</option>
                                    	<option value="6">5+</option>
                                    </select>
                                    
                                    	<span class="spanTo">Bath:</span>
	                                    <select name="bath" class="contawidth form-control input-mini3">
	                                    	<option value="1">1</option>
	                                    	<option value="2">2</option>
	                                    	<option value="3">3</option>
	                                    	<option value="4">4</option>
	                                    	<option value="5">5</option>
	                                    	<option value="6">5+</option>
	                                    </select>
	                                </td>
                               </tr>
                               <tr><td colspan="3">&nbsp;</td></tr>
                               <tr>
                                    <td>Price(in total):</td>
                                    <td>
                                    	<input class="contawidth form-control input-mini2" required="" placeholder="Price from" type="text" name="price1">
                                    	<spann class="spanTo">To</spann> 
                                    	<input class="contawidth form-control input-mini2" required="" placeholder="Price to" type="text" name="price2"></td>
                               		<td>&nbsp;</td>
                               </tr>
                               <tr><td colspan="3"><h3>Contact Inforation</h3></td></tr>
                               <tr>
                                    <td>Name:</td>
                                    <td><input class="contawidth form-control" required="" placeholder="Name" type="text" name="uname"></td>
                               </tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
                            	<tr>
                                    <td>Email ID:</td>
                                    <td><input class="contawidth form-control" required="" placeholder="Email ID" type="text" name="email"></td>
                               		<td>&nbsp;</td>
                               </tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
                                
                            	<tr>
                                    <td>Conuntry Code:</td>
                                    <td><input class="contawidth form-control"  placeholder="Conuntry Code" type="text" name="countrycode"></td>
                               		<td>&nbsp;</td>
                               </tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
                                <tr>
                                    <td>Mobile Number:</td>
                                    <td><input class="contawidth form-control" required="" placeholder="Mobile Number" type="text" name="cell_phone"></td>
                               		<td>&nbsp;</td>
                               </tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
                              <tr>
                              	<td colspan="3" style="text-align:center"> <button>Submit Your Request</button></td>
                              </tr>
                              <tr><td colspan="3">&nbsp;</td></tr>
                              <tr><td colspan="3">&nbsp;</td></tr>
                              <tr><td colspan="3">&nbsp;</td></tr>
                            </tbody></table>   
                        </form>
                    </div>
               		<div class="post1">2</div>
               		<div class="post2">Mobile verification</div>
                    <div class="post1">3</div>
               		<div class="post2">Confirmation</div>
            </div>
            <div class="contacRight">
            	<h3>For any assistance:</h3>
                <b>8801922101145, +88029143576</b>      
                <h3>For Sale enquiries:</h3>
                8801922101145,<br> +88029143576
                <div class="postInnerLeft">
                	<b>How it works</b>
                </div>
            </div>
	    </div>
	    <div class="container">
       	  <div class="pem_memeberLogo">
            	<div class="listHeading">Few of our Premium Members</div>
                    <div id="wn">
                        <div style="position: absolute; left: -119px; top: 0px; visibility: visible;" id="lyr1">
                            <div id="inner1">
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" border="0" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img id="rpt1" src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img id="rpt1" src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img id="rpt1" src="<?=base_url()?>assets/images/prem_logo.png" alt="" height="25"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img id="rpt1" src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                                  <a href="#"><img id="rpt1" src="<?=base_url()?>assets/images/prem_logo.png" alt="" width="164"></a>
                            </div>
                        </div>
                    </div> 
            	</div>
          </div>
          
  <script src="<?=base_url()?>assets/js/dw_con_scroller.js" type="text/javascript"></script>
	<script type="text/javascript">
        if ( DYN_WEB.Scroll_Div.isSupported() ) {	
            DYN_WEB.Event.domReady( function() {
                var wndo = new DYN_WEB.Scroll_Div('wn', 'lyr1');
                wndo.makeSmoothAuto( {axis:'h', bRepeat:true, repeatId:'rpt1', speed:50, bPauseResume:true} );
                
            });
        }
    </script>