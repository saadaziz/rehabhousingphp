<!doctype html>
<html lang="en" ng-app="rehabApp">
<head>
    <meta charset="utf-8">
    <title>Testing ... ..... .......</title>
    <link rel="stylesheet" href="<?=base_url()?>assets/angularothers/app.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/angularothers/bootstrap.min.css"><script src="<?=base_url()?>assets/lib/angular/angular.js"></script>

    <script data-require="angular-ui-bootstrap@0.3.0" data-semver="0.3.0" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.3.0.min.js"></script>

    <script src="<?=base_url()?>assets/angularothers/controllers.js"></script>
</head>
<body ng-controller="PhoneListCtrl">

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <!--Sidebar content-->


           Search: <input  ng-model="query.subcat" />
            Sort by:
            <select ng-model="orderProp">
                <option value="title">Alphabetical</option>
                <option value="insertTime">Newest</option>
            </select>
<script>

</script>
        </div>
        <div class="span10">
            <h4>{{products.length}} Posts here</h4>
            <!--Body content-->
            <div data-pagination="" data-num-pages="numPages()"
                 data-current-page="currentPage" data-max-size="maxSize"
                 data-boundary-links="true"></div>
            <ul class="phones">
                <li ng-repeat="product in filteredProducts | filter:query | orderBy:orderProp | limitTo:numPerPage">
                    {{product.title}}({{product.id}})
                    <p>{{product.cat}}</p>
                    <p>{{product.subcat}}</p>
                    Time <p>{{product.insertTime}}</p>
                    Price <p>{{product.price}}</p>
                    Size <p>{{product.size}}</p>

                </li>
            </ul>

        </div>
    </div>
</div>

</body>
</html>

