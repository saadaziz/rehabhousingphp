
    <div class="topmenuArea">
        <div class="container">
            <div class="homeicon"><a href="index.html"><img alt="" src="<?=base_url()?>assets/images/home.png" width="25px;"></a></div>
            <ul id="navmenu">
                    <li><a href="<?=base_url()?>category/Sale">FOR SALE</a>
                        <ul>
                            <li>
                                <div class="submenudiv sorsale2">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                       <tbody>
                                       <tr colspan="3" class="menuLinspace">
                                        	<td><u>Property Type</u></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">   
                                                <a href="<?=base_url()?>category/Sale/subcategory/<?=urlencode('Apartment-Flat')?>">Apartment / Flat</a><br>
                                                <a href="<?=base_url()?>category/Sale/subcategory/<?=urlencode('Residential Plot')?>">Residential Plot</a><br>
                                                <a href="<?=base_url()?>category/Sale/subcategory/<?=urlencode('Agricultural Land')?>">Agricultural Land</a><br>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="<?=base_url()?>category/Sale/subcategory/<?=urlencode('House')?>">House</a><br>
                                                <a href="<?=base_url()?>category/Sale/subcategory/<?=urlencode('Office Space')?>">Office Space</a><br>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="<?=base_url()?>category/Sale/subcategory/<?=urlencode('Commercial Space')?>">Commercial Space</a><br />
                                                <a href="<?=base_url()?>category/Sale/subcategory/<?=urlencode('Commercial Plot')?>">Commercial Plot</a>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                        <tr class="menuLinspace">
                                            <td colspan="3"><b><u>Size (Sft)</u></b></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">   
                                                <a href="<?=base_url()?>category/Sale/size/<?=urlencode('Less500')?>">Less Than 500</a><br>
                                                <a href="<?=base_url()?>category/Sale/size/<?=urlencode('1501-2000')?>">1501 Sft -  2000 Sft</a><br>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="<?=base_url()?>category/Sale/size/<?=urlencode('501-1000')?>">501 Sft -  1000 Sft</a><br>
                                                <a href="<?=base_url()?>category/Sale/size/<?=urlencode('2001-3000')?>">2001 Sft -  3000 Sft</a><br>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="<?=base_url()?>category/Sale/size/<?=urlencode('1001-1500')?>">1001 Sft -  1500 Sft</a><br>
                                                <a href="<?=base_url()?>category/Sale/size/<?=urlencode('More3000')?>">Above  3000 Sft</a><br>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                         <tr class="menuLinspace">
                                            <td colspan="3"><b><u>City / Divisions</u></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="submenudiv_link">
                                             <div style="float: left">	
                                                <a href="">Dhaka City</a><br />
                                                <a href="">Chittagong City</a><br />
                                                <a href="">Sylhet City</a><br />
                                             </div>
                                             <div style="float: left">
                                             	<a class="loc_left2" href="">Dhaka Division</a><br />
                                                <a class="loc_left2" href="">Chittagong Division</a><br />
                                                <a class="loc_left2" href="">Sylhet Division</a><br />
                                             </div>
                                             <div style="float: left">
                                             	<a class="loc_left2" href="">Khulna Division</a><br />
                                                <a class="loc_left2" href="">Rajshahi Division</a><br />
                                             </div>
                                             <div style="float: left">
                                             	<a class="loc_left2" href="">Barisal Division</a><br />
                                                <a class="loc_left2" href="">Rangpur Division</a><br />
                                              </div>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                        <tr class="menuLinspace">
                                            <td colspan="3"><b><u>Price</u></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="submenudiv_link">
                                            	<div>
	                                                <a href="">Less than 30 lac</a>
	                                                <a class="loc_left" href="">30 - 45 lac </a>
	                                                <a class="loc_left" href="">45 - 60 Lac</a>
	                                                <a href="">60 - 80 Lac</a>
	                                                <a class="loc_left" href=""> 80 - 1 crore</a><br />
	                                                <a href=""> Above 1 crore</a>
                                                </div>
                                        <tr>
                                            <td colspan="3"><div class="br_line"></div></td>
                                        </tr>
                                        
                                        <tr class="menuLinspace">
                                            <td colspan="3"><b><u>Bedroom</u></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="submenudiv_link">
                                            	<div style="float: left">
	                                                <a href="<?=base_url()?>category/Sale/bed/1">1</a>
	                                                <a class="loc_left2" href="<?=base_url()?>category/Sale/bed/2">2</a>
	                                                <a class="loc_left2" href="<?=base_url()?>category/Sale/bed/3">3</a>
	                                                <a class="loc_left2" href="<?=base_url()?>category/Sale/bed/4">4</a>
	                                                <a class="loc_left2" href="<?=base_url()?>category/Sale/bed/5">5</a>
	                                                <a class="loc_left2" href="<?=base_url()?>category/Sale/bed/5+">Above 5</a>
	                                             </div>
                                                <div class="proDiv">
                                                	<table>
                                                		<tr>
                                                			<td>
	                                                			<a href="index.html">
	                                                				<img alt="" src="<?=base_url()?>assets/images/email.png" width="16px;">&nbsp;<b style="color: #EDAF21">Set Property Alert</b>
	                                                			</a>
                                                			</td>
                                                		</tr>
                                                		<tr style="line-height: 5px;">
														    <td>&nbsp;</td>
													   </tr>
                                                		<tr>
                                                			<td>
	                                                			<a href="index.html">
	                                                				<span class="menuFree">free</span>
	                                                				<img alt="" src="<?=base_url()?>assets/images/list_property.png" width="16px" height="10px;">&nbsp;<b style="color: #EDAF21">Sell Your Property</b>
	                                                			</a>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </td>
                                        </tr>
                                       
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>   
                    <li class="menuDevider"></li>    
                    <li><a href="<?=base_url()?>category/Rent">FOR RENT</a>
                        <ul>
                            <li>
                                <div class="submenudiv sorsale2">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                      
                                        <tbody>
                                        <tr colspan="4" class="menuLinspace">
                                        	<td><u>Porperty Type</u></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">   
                                                <a href="<?=base_url()?>category/Rent/subcategory/<?=urlencode('Apartment-Flat')?>">Apartment / Flat</a><br>
                                                <a href="<?=base_url()?>category/Rent/subcategory/<?=urlencode('Sublet')?>">Sublet</a><br>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="<?=base_url()?>category/Rent/subcategory/<?=urlencode('House')?>">House</a><br>
                                                <a href="<?=base_url()?>category/Rent/subcategory/<?=urlencode('Room')?>">Room</a><br>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="<?=base_url()?>category/Rent/subcategory/<?=urlencode('Commercial Space')?>">Commercial Space</a><br />
                                                <a href="<?=base_url()?>category/Rent/subcategory/<?=urlencode('Roommate')?>">Roommate</a>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                         <tr>
                                            <td colspan="4"><div class="br_line"></div></td>
                                        </tr>
                                         <tr class="menuLinspace">
                                            <td colspan="4"><b><u>City / Divisions</u></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="submenudiv_link">
                                             <div style="float: left">	
                                                <a href="">Dhaka City</a><br />
                                                <a href="">Chittagong City</a><br />
                                                <a href="">Sylhet City</a><br />
                                             </div>
                                             <div style="float: left">
                                             	<a class="loc_left2" href="">Dhaka Division</a><br />
                                                <a class="loc_left2" href="">Chittagong Division</a><br />
                                                <a class="loc_left2" href="">Sylhet Division</a><br />
                                             </div>
                                             <div style="float: left">
                                             	<a class="loc_left2" href="">Khulna Division</a><br />
                                                <a class="loc_left2" href="">Rajshahi Division</a><br />
                                             </div>
                                             <div style="float: left">
                                             	<a class="loc_left2" href="">Barisal Division</a><br />
                                                <a class="loc_left2" href="">Rangpur Division</a><br />
                                              </div>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="4"><div class="br_line"></div></td>
                                        </tr>
                                        <tr colspan="4" class="menuLinspace">
                                        	<td><u>Rent For</u></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">
                                                <a href="">Family</a>
                                            </td>
                                            <td class="submenudiv_link" >
                                                <a href="">Bachelor</a>
                                            </td>
                                             <td class="submenudiv_link">
                                                <a href="">Men</a>
                                            </td>
                                            <td class="submenudiv_link">
                                                <a href="">Women</a>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="4"><div class="br_line"></div></td>
                                        </tr>
                                        <tr colspan="4" class="menuLinspace">
                                        	<td><u>Monthly Rent</u></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="submenudiv_link">
                                                <a href="">Less Than BDT 5000</a>
                                                <a class="loc_left2" href=""> 5,001 - 10,000</a>
                                                <a class="loc_left2" href="">  10,001 - 15000</a>
                                                <a class="loc_left2" href=""> 15,001 - 20,000</a><br>
                                                <a href="">20,001 - 30,000</a>
                                                <a class="loc_left2" href="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30,001 - 50,000</a>
                                                <a class="loc_left2" href=""> 50,001 - 1 lac</a>
                                                <a class="loc_left2" href="">Above 1 lac</a>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="4"><div class="br_line"></div></td>
                                        </tr>
                                          <tr colspan="4" class="menuLinspace">
                                        	<td><u>Bedroom</u></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="submenudiv_link">
                                            	<div style="float: left">
	                                                <a href="">1</a>
	                                                <a class="loc_left2" href="">2</a>
	                                                <a class="loc_left2" href="">3</a>
	                                                <a class="loc_left2" href="">4</a>
	                                                <a class="loc_left2" href="">5</a>
	                                                <a class="loc_left2" href="">Above 5</a>
	                                             </div>
	                                             <div class="proDiv">
                                                	<table>
                                                		<tr>
                                                			<td>
	                                                			<a href="index.html">
	                                                				<img alt="" src="<?=base_url()?>assets/images/email.png" width="16px;">&nbsp;<b style="color: #EDAF21">Set Property Alert</b>
	                                                			</a>
                                                			</td>
                                                		</tr>
                                                		<tr style="line-height: 5px;">
														    <td>&nbsp;</td>
													   </tr>
                                                		<tr>
                                                			<td>
	                                                			<a href="index.html">
	                                                				<span class="menuFree">free</span>
	                                                				<img alt="" src="<?=base_url()?>assets/images/list_property.png" width="16px" height="10px;">&nbsp;<b style="color: #EDAF21">Rent Your Property</b>
	                                                			</a>
                                                			</td>
                                                		</tr>
                                                	</table>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="menuDevider"></li> 
                    <li ><a href="#">LOCATIONS</a>
                         <ul>
                            <li>
                                <div class="submenudiv2 sorsale">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td class="submenudiv_link citylocMenu">  
                                            	<b><u>City</u></b><br> 
                                                <a style="margin-top: 10px;" class="showSingle" target="1">Dhaka City</a><br>
                                                <a style="margin-top: 10px;" class="showSingle" target="2">Chittagong City</a><br>
                                                <a style="margin-top: 10px;" class="showSingle" target="3">Sylhet City</a><br>
                                                <a style="margin-top: 10px;" class="showSingle" target="4">Dhaka Division</a><br>
                                                <a style="margin-top: 10px;" class="showSingle" target="5">Chittagong Divisionn</a><br>
                                                <a style="margin-top: 10px;" class="showSingle" target="6">Sylhet Division</a><br>
                                                <a style="margin-top: 10px;" class="showSingle" target="7">Barishal Division</a><br>
                                                <a style="margin-top: 10px;" class="showSingle" target="8">Khulna Division</a><br>
                                                <a style="margin-top: 10px;" class="showSingle" target="9">Rajshahi Division</a><br>
                                                <a style="margin-top: 10px;" class="showSingle" target="10">Rangpur Division</a> <br>
                                            </td>
                                            <td style="display: none;" id="div1"  class="submenudiv_link targetDiv zoneMenu">
                                            	<table>
                                            		<tr class="menuLinspace">
                                            			<td colspan="2"><u>Zone</u></td>
                                            		</tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr> 
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</ td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="2"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3" ><a style="float: right;" href=""><b>More..</b></a></td></tr>
                                            	</table>
                                            </td>
                                              <td style="display: none;" id="div2"  class="submenudiv_link targetDiv zoneMenu">
                                            	<table>
                                            		<tr class="menuLinspace">
                                            			<td colspan="2"><u>Zone</u></td>
                                            		</tr>
                                            		<tr>
                                            			<td><a href="">Chittagong</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr> 
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</ td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="2"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3" ><a style="float: right;" href=""><b>More..</b></a></td></tr>
                                            	</table>
                                            </td>
                                             <td style="display: none;" id="div3"  class="submenudiv_link targetDiv zoneMenu">
                                            	<table>
                                            		<tr class="menuLinspace">
                                            			<td colspan="2"><u>Zone</u></td>
                                            		</tr>
                                            		<tr>
                                            			<td><a href="">Dhaka</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr> 
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</ td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="2"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3" ><a style="float: right;" href=""><b>More..</b></a></td></tr>
                                            	</table>
                                            </td>
                                            <td style="display: none;" id="div4"  class="submenudiv_link targetDiv zoneMenu">
                                            	<table>
                                            		<tr class="menuLinspace">
                                            			<td colspan="2"><u>Zone</u></td>
                                            		</tr>
                                            		<tr>
                                            			<td><a href="">Dhaka</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr> 
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</ td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="2"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3" ><a style="float: right;" href=""><b>More..</b></a></td></tr>
                                            	</table>
                                            </td>
                                            <td style="display: none;" id="div5"  class="submenudiv_link targetDiv zoneMenu">
                                            	<table>
                                            		<tr class="menuLinspace">
                                            			<td colspan="2"><u>Zone</u></td>
                                            		</tr>
                                            		<tr>
                                            			<td><a href="">Dhaka</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr> 
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</ td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="2"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3" ><a style="float: right;" href=""><b>More..</b></a></td></tr>
                                            	</table>
                                            </td>
                                            <td style="display: none;" id="div6"  class="submenudiv_link targetDiv zoneMenu">
                                            	<table>
                                            		<tr class="menuLinspace">
                                            			<td colspan="2"><u>Zone</u></td>
                                            		</tr>
                                            		<tr>
                                            			<td><a href="">Dhaka</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr> 
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</ td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="2"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="7" ><a style="float: right;" href=""><b>More..</b></a></td></tr>
                                            	</table>
                                            </td>
                                            <td style="display: none;" id="div7"  class="submenudiv_link targetDiv zoneMenu">
                                            	<table>
                                            		<tr class="menuLinspace">
                                            			<td colspan="2"><u>Zone</u></td>
                                            		</tr>
                                            		<tr>
                                            			<td><a href="">Dhaka</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr> 
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</ td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="2"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3" ><a style="float: right;" href=""><b>More..</b></a></td></tr>
                                            	</table>
                                            </td>
                                            <td style="display: none;" id="div8"  class="submenudiv_link targetDiv zoneMenu">
                                            	<table>
                                            		<tr class="menuLinspace">
                                            			<td colspan="2"><u>Zone</u></td>
                                            		</tr>
                                            		<tr>
                                            			<td><a href="">Dhaka</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr> 
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</ td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="2"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3" ><a style="float: right;" href=""><b>More..</b></a></td></tr>
                                            	</table>
                                            </td>
                                            <td style="display: none;" id="div9"  class="submenudiv_link targetDiv zoneMenu">
                                            	<table>
                                            		<tr class="menuLinspace">
                                            			<td colspan="2"><u>Zone</u></td>
                                            		</tr>
                                            		<tr>
                                            			<td><a href="">Dhaka</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr> 
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</ td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="2"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3" ><a style="float: right;" href=""><b>More..</b></a></td></tr>
                                            	</table>
                                            </td>
                                            <td style="display: none;" id="div10"  class="submenudiv_link targetDiv zoneMenu">
                                            	<table>
                                            		<tr class="menuLinspace">
                                            			<td colspan="2"><u>Zone</u></td>
                                            		</tr>
                                            		<tr>
                                            			<td><a href="">Dhaka</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr> 
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</ td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="2"></td></tr>
                                            		<tr>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            			<td><a href="">Location</a></td>
                                            		</tr>
                                            		<tr class="menuLinspace"> <td colspan="3" ><a style="float: right;" href=""><b>More..</b></a></td></tr>
                                            	</table>
                                            </td>
	                                       </tr>
	                                          </tbody>
                                             </table>
                                           </td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">REAL ESATE JOBS</a>
                        <ul>
                            <li>
                                <div class="submenudiv sorsale" style="width: 340px;">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        	<tr class="menuLinspace">
                                        		<td><u>Job Categories</u></td>
                                        	</tr>
                                        	<tr>
	                                           <td class="submenudiv_link" style="width: 200px;">
	                                                <a href="">Accounting/Finance</a><br>
	                                                <a href="">HR/Admin</a><br>
	                                                <a href="">Marketing &amp; sales</a><br>
	                                                <a href="">Customer Suport/Front Desk</a>                                             
	                                            </td>
	                                            <td class="submenudiv_link">
	                                                <a href="">Design &amp; Creative</a><br>
	                                                <a href="">IT</a><br>
	                                                <a href="">Legal</a><br>                                             
	                                            </td>
	                                        </tr>
                                    	</tbody>
                                    </table>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">NEWS</a></li>
                    <li class="menuDevider"></li> 
                    <li><a href="#"> OUR SERVICES</a>
                        <ul>
                            <li>
                                <div class="submenudiv sorsale" style="width: 350px;">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
										<tr class="menuLinspace">
											<td><u>Services for Buyer</u></td>
											<td><u>Services for Seller</u></td>
                                        </tr>
                                         <tr>
                                           <td class="submenudiv_link" style="width: 165px;">
                                                <a href="">Set Property alert</a><br>
                                                <a href="">Post Buying Requierments</a><br>
                                                <a href="">Browse Property</a><br>
                                                <a href="">Buying Assistance Service</a><br>                                             
                                            </td>
                                            <td class="submenudiv_link vLine">
                                                <a href="">Post free ad</a><br>
                                                <a href="">Premium Membership</a><br>
                                                <a href="">Top Project Advertiesment</a><br>
                                                <a href="">Banner Advertiesment</a><br>                                               
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="2"><div class="br_line"></div></td>
                                        </tr>
                                        <tr class="menuLinspace">
                                            <td colspan="2"><b><u>Website owner</u></b></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">   
                                                <a href="">Be Our Partner</a><br>
                                            </td>
                                           <td></td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">HOME LOAN</a></li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">DEVELOPERS</a></li>
                    <li class="menuDevider"></li> 
                    <li><a href="#">HELP</a>
                        <ul>
                            <li>
                                <div class="submenudiv sorsale" style="width: 120px;">
                                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody><tr>
                                           <td class="submenudiv_link" style="width: 160px;">
                                                <a href="">How to Buy</a><br> 
                                                <a href="">How to Sell</a><br> 
                                                <a href="">Contact Us</a><br>
                                                <a href="">Send Quiry</a><br> 
                                                <a href="">FAQ</a><br>                                            
                                            </td>
                                        </tr>

                                        <tr class="menuLinspace">
                                            <td colspan="2"><b><u>Hotline</u></b></td>
                                        </tr>
                                        <tr>
                                            <td class="submenudiv_link">   
                                                <b style="font-size: 14px; color: #ff920a">01922101145</b><br>
                                            </td>
                                           <td></td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </li>
                        </ul>
                    </li>
       		     </ul>
            </div>
        </div>