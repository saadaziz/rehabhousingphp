'use strict';

/* Controllers */

var phonecatApp = angular.module('rehabapp', []);

phonecatApp.controller('ProductListCntrl', function($scope, $http) {
var arr = $.parseJSON(localStorage.getItem('filters'));

  $http.get().success(function(data) {
    $scope.phones = data;
  });

  $scope.orderProp = 'age';
});
