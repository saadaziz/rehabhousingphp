'use strict';

/* Controllers */

var phonecatApp = angular.module('rehabApp', ['ui.bootstrap']);
/*phonecatApp.filter('isSubcat', function() {
    return function(input,value) {
        var out = [];
        for (var i = 0; i < input.length; i++) {
            if(input.subcat=value){
                out.push(input[i]);
                console.log('here'+value);
            }
        }

        return out;
    }
});*/
phonecatApp.controller('PhoneListCtrl', function($scope, $http) {
        $scope.filteredProducts = []
        ,$scope.currentPage = 1
        ,$scope.numPerPage = 10
        ,$scope.maxSize = 5;

    $scope.products=[];

    $scope.getProducts = function() {
        $http.get('http://shad-pc:8080/rehabhousing/test_angular/get_products').success(function(data) {
            $scope.products = data;
            $scope.numPages = function () {
                return Math.ceil($scope.products.length / $scope.numPerPage);
            };
            $scope.isSubcat = function(product) {
                return product.subcat === "1";
            };
            $scope.$watch('currentPage + numPerPage', function() {
                var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    ,end = begin + $scope.numPerPage;
                $scope.filteredProducts = $scope.products.slice(begin, end);

            });
        });
    };

    $scope.getProducts();

    $scope.orderProp = '-insertTime';
});
