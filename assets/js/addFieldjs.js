var webroot = '/rehabhousing/';
window.localStorage.setItem('webroot',webroot);
var fields = 2;
	function addInput() {
		if (fields != 6) {
		document.getElementById('text').innerHTML += "<input type='text' id='id" + fields + "' value='' name='lsttxt"+fields+"' />";
		fields ++;
		} else {
		alert('Not more then five fields');
		}
	}
	function removeOption()
	{
		if(fields>0)
		{
			fields--;
			var x=document.getElementById("id"+fields);
			x.remove(x.selectedIndex);
		}
	}
    		$(document).ready(function () {
    			var timing=900;
    			$("#nextForm").hide();
    			$("#tblfields").hide();
    			$("#uilist").hide();
	    		var obj = $(this), // (*) references the current object/form each time
				url = obj.attr('action'),
				method = obj.attr('method'),
				data = {};
	    		var drtypeid="0";
	    		 $('#uitype').on('change', function () {
	    		 	if($(this).val()=='list')
	    		 	{
	    		 		$("#uilist").show();		    		 		
	    		 	}
	    		 	else
	    		 		$("#uilist").hide(1000);	
	    		 });
		        $('#parent').on('change', function () {
					drtypeid = $(this).val(); 
					//console.log(drtypeid);
					if(drtypeid!="0")
					{
						$("#nextForm").show(timing);
						$("#tblfields").show(timing);
						$.post( webroot+"admin/addfield/getFields/"+drtypeid, function( data ) {
							$( "#addtables" ).html( data );
						});
						$("#hdntypeid").val(drtypeid);
					}
					else
					{
						$("#nextForm").hide();
						$("#tblfields").hide();					
					}
				});
				$('form.ajax-form').on('submit', function() {
					if(confirm('Do u want to input that field')){
					fields-=1;
					var obj = $(this), // (*) references the current object/form each time
						url = obj.attr('action'),
						method = obj.attr('method'),
						data = {};
					$("#hdnlstcount").val(fields);
					//console.log(fields);
					obj.find('[name]').each(function(index, value) {
						
						var obj = $(this),
							name = obj.attr('name'),
							value = obj.val();
						data[name] = value;
					});
					$.ajax({
						url: url,
						type: method,
						data: data,
						success: function(response2) {
							console.log(response2);
							$('#tblfields').hide();
							$('#tblfields').show(timing);
							$('#tblfields').append(response2);
						}
					});
					return false; //disable refresh
					}
				});
		});