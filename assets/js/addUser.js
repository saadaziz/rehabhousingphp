var webroot = '/rehabhousing/';
$(document).ready(function () {
			var timing =900;
			$('.alert').hide();
			$('#iscompany').removeAttr('checked');
		  	$('.divcompany').hide();
		  	$('#iscompany').change(function() {
		  		if(this.checked)
		  		{
		        	$('.divcompany').show(timing);
		        	$('#lblname').html('Company Name<span class="required">*</span>');
		        	$('#iscompany').val('on');
	        	}
	        	else
	        	{
	        		$('.divcompany').hide();
		        	$('#lblname').html('Name<span class="required">*</span>');
		        	$('#iscompany').val('off');
	        	}
	     	});
	    $('#user-form').validate({
	    rules: {
	      password: {
	        minlength: 8,
	        required: true
	      },
	      name: {
	      	minlength: 5,
	        required: true
	      },
	      retype_password: {
	        minlength: 8,
	        required: true
	      },
	      email: {
	      	required:true	      	
	      },
	      cell_no: {
	        minlength: 11,
	        required: true
	      }
	    },
		  showErrors: function(errorMap, errorList) {
		    $.each(this.successList, function(index, value) {
		      return $(value).popover("hide");
		    });
		    return $.each(errorList, function(index, value) {
		      var _popover;
		      console.log(value.message);
		      _popover = $(value.element).popover({
		        trigger: "manual",
		        placement: "right",
		        content: value.message,
		        template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
		      });
		      _popover.data("popover").options.content = value.message;
		      return $(value.element).popover("show");
		    });
		  }
		 	
	  });
    		$('#user-form').submit(function(e) {
    			if (confirm('Do you want to save?')) {
    				//alert('saaad');
	    				e.preventDefault();
						var obj = $(this), // (*) references the current object/form each time
							url = obj.attr('action'),
							method = obj.attr('method'),
							data = {};
						obj.find('[name]').each(function(index, value) {
							var obj = $(this),
								name = obj.attr('name'),
								value = obj.val();
							data[name] = value;
						});
						$.ajax({
							url: url,
							type: method,
							data: data,
							dataType:"json",
							success: function(response2) {
								$('#defdiv').hide();
								if(response2.stat!='1')
								{
									$('.alert-success').hide();
									$('.alert-danger').show();
									$('.alert-danger').html(response2.error);
									//console.log(response2.error);
									$("html, body").animate({ scrollTop:0 }, timing+200);
									$(this).clearForm();
								}
								else
								{
									$('.alert-danger').hide();
									$('.alert-success').show();
									$('.alert-success').html(response2.data);
									/*$('#tblusers').hide();
									$('#tblusers').show(timing);
									$('#tblusers').append(response2.tabledata);*/
									//alert('Successfully inserted Data.');
									$("html, body").animate({ scrollTop: 100 }, timing+200);
									$(this).clearForm();
								}
							}
						});
					}
					return false; //disable refresh
				});
				/*var bar = $('.bar');
				var percent = $('.percent');
				var status = $('#status');
				//function DoSave() {
				$('#user-form').submit(function() {
					if (confirm('Do you want to save?')) {
						
						var obj = $('#user-form');
						var data = $(this).serialize();
						console.log(data);
						var options = {
							url: obj.attr('action'),
							type : obj.attr('method'),
							data:data,
							dataType:"json",
						    beforeSend: function() {
						        status.empty();
						        var percentVal = '0%';
						        bar.width(percentVal);
						        percent.html(percentVal);
						    },
						    uploadProgress: function(event, position, total, percentComplete) {
						        var percentVal = percentComplete + '%';
						        bar.width(percentVal);
						        percent.html(percentVal);
								//console.log(percentVal, position, total);
						    },
						    success: function(res) {
						    	
						    	//var ret = $.parseJSON(res); 
						        var percentVal = '100%';
						        bar.width(percentVal);
						        percent.html(percentVal);
						        //alert('Successfully inserted.');
						        if(res.stat!='1')
								{
									alert('Error occured.');
									$('.alert-success').hide();
									$('.alert-danger').show(timing);
									$('.alert-danger').html(res.error);
									console.log(res.error);
								}
								else	
								{
									$('.alert-danger').hide();
									$('.alert-success').show(timing);
									$('.alert-success').html(res.data);
									$('#tblusers').hide();
									$('#tblusers').show(timing);
									$('#tblusers').append(res.tabledata);
									alert('Successfully inserted Data.');
								}
								console.log(res);
						    },
							complete: function(xhr) {
								status.html(xhr.responseText);
								
							console.log(xhr.responseText);
							},
							error: function()
							{
								console.log("<font color='red'> ERROR: unable to upload files</font>");
						
							}
						};
						
						
						 $(this).ajaxForm(options); 
						 console.log('data');
						 
					}
					return false;
				//}
				}); */
	});