$(document).ready(function () {
		$('#myCarousel').carousel({
			interval: 6000
		});
		$('.carousel').carousel('cycle');
	
		
	
	
		jQuery('.targetDiv').hide();
		jQuery('#div' + $('.showSingle').attr('target')).show();
		jQuery('#showall').click(function () {
			jQuery('.targetDiv').show();
		});
		jQuery('.showSingle').click(function () {
			jQuery('.targetDiv').hide();
			jQuery('#div' + $(this).attr('target')).show();
		});
	
	
		$('#showbookingForm').click(function () {
			$('#bookingForm').fadeIn('500');
			$('#showbookingForm').fadeOut(200);
		});
	
		jQuery('.targettabDiv').hide();
		jQuery('#div' + $('.showtabSingle').attr('target')).show();
		jQuery('.showtabSingle').click(function () {
			jQuery('.targettabDiv').hide();
			jQuery('#div' + $(this).attr('target')).show();
		});
	
	   
		
		jQuery('.active').show();
		jQuery('#divA' + $('.showtabSingle').attr('target')).show();
		jQuery('#showall').click(function () {
			jQuery('.active').show();
		});
		jQuery('.showtabSingle').click(function () {
			jQuery('.active').show();
			jQuery('#divA' + $(this).attr('target')).show();
			$('.showtabSingle').removeClass('active');
			$(this).addClass('active');
		});
	
	
	
	
	
	


	
		jQuery('.tabBt').click(function () {
			jQuery('.active').show();
			jQuery('#divphoto' + $(this).attr('target')).show();
			$('.tabBt').removeClass('active');
			$(this).addClass('active');
		});
		
		jQuery(function(){
			jQuery('.targetgalleryDiv').hide();
                  jQuery('#div'+$('.tabBt').attr('target')).show();
            jQuery('#showall').click(function(){
                  jQuery('.targetgalleryDiv').show();
            });
            jQuery('.tabBt').click(function(){
                  jQuery('.targetgalleryDiv').hide();
                  jQuery('#div'+$(this).attr('target')).show();
            });
    	});
	   
	   

		$("#showShareDiv").click(function(){
			if($("#shareDiv").is(':visible'))
			{
				$("#shareDiv").fadeOut('slow');
			}
			else
			{
				$("#shareDiv").fadeIn('slow');
			}
		});	
		
		
			$('#cycle-1').on('cycle-before',function(event, optionHash){
				console.log(optionHash.nextSlide);
				$('#cycle-2').cycle('goto',optionHash.nextSlide);
			});
			
			$('#cycle-1').on('cycle-next cycle-prev',function(event, optionHash){
				console.log(optionHash.currSlide);
				$('#cycle-2').cycle('goto',optionHash.currSlide);
			});
			
			$('#cycle-2 .cycle-slide').click(function(){
				var index = $('#cycle-2').data('cycle.API').getSlideIndex(this);
				$('#cycle-1').cycle('goto', index);
			});
		

});




$(function() {
	    $( "#slider-range" ).slider({
	      range: true,
	      min: 0,
	      max: 500,
	      values: [ 75, 300 ],
	      slide: function( event, ui ) {
	        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
	      }
	    });
	    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
	      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
	  });



$(function() {
    $( "#slider-range2" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        $( "#bed" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#bed" ).val( "$" + $( "#slider-range2" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range2" ).slider( "values", 1 ) );
  });


$(function() {
    $( "#slider-range3" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        $( "#bath" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#bath" ).val( "$" + $( "#slider-range3" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range3" ).slider( "values", 1 ) );
  });


$(function() {
    $( "#slider-range4" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        $( "#price" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#price" ).val( "$" + $( "#slider-range4" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range4" ).slider( "values", 1 ) );
  });
